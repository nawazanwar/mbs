<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class DesignationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('designations')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('designations')->insert([
       [
        'id' => '1',
        'created_by' => 'Admin',
        'updated_by' =>'Admin',
        'school_id' => null,
        'name' => 'Principle',
        'note' => 'Hello Subject 2',
        'status' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'deleted_at' => NULL
       ],
       [
        'id' => '2',
        'created_by' => 'admin',
        'updated_by' =>'admin',
        'school_id' => null,
        'name' => 'Teacher',
        'note' => 'Hello Subject 3',
        'status' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
        'deleted_at' => NULL
       ]
       ]);
    }
}
