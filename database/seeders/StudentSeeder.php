<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('students')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('students')->insert([
            [
                'id' => '1',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'student_type_id' => null,
                'admission_no' => null,
                'admission_date' => '01/02/2021',
                'guardian_id' => null,
                'relation_with' => 'mother',
                'registration_no' => '1',
                'group' => 'A',
                'name' => 'Munsoor',
                'phone' => '123456789',
                'email' => 'mans@gmail.com',
                'present_address' => 'kar xyz',
                'permanent_address' => 'kar xyz',
                'gender' => 'Male',
                'blood_group' => 'A',
                'religion' => 'Muslim',
                'caste' => 'Jutt',
                'dob' => '01/02/2021',
                'age' => '21',
                'photo' => null,
                'is_library_member' => 'yes',
                'is_hostel_member' => 'yes',
                'is_transport_member' => 'yes',
                'discount_id' => '2',
                'previous_school' => 'Kohinoor',
                'previous_class' => '8',
                'transfer_certificate' => 'yes',
                'health_condition' => 'good',
                'national_id' => null,
                'second_language' => 'urdu',
                'father_name' => 'shanker',
                'father_phone' => null,
                'father_education' => 'Ba Pass',
                'father_profession' => 'Plumber',
                'father_designation' => 'chicago',
                'father_photo' => 'yes',
                'mother_name' => 'ok',
                'mother_phone' => 'xxxxxxxxxx',
                'mother_education' => 'BA Pass 2',
                'mother_profession' => 'Homemade',
                'mother_designation' => 'ISB',
                'mother_photo' => 'yes',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => '2',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'student_type_id' => null,
                'admission_no' => null,
                'admission_date' => '01/02/2021',
                'guardian_id' => null,
                'relation_with' => 'mother',
                'registration_no' => '1',
                'group' => 'AA',
                'name' => 'Salu',
                'phone' => '6456567675',
                'email' => 'salu@gmail.com',
                'present_address' => 'Kohinoor xyz',
                'permanent_address' => 'Kohinoor xyz',
                'gender' => 'Male',
                'blood_group' => 'A',
                'religion' => 'Muslim',
                'caste' => 'Jutt',
                'dob' => '01/02/2021',
                'age' => '21',
                'photo' => null,
                'is_library_member' => 'yes',
                'is_hostel_member' => 'yes',
                'is_transport_member' => 'yes',
                'discount_id' => '2',
                'previous_school' => 'Kohinoor',
                'previous_class' => '8',
                'transfer_certificate' => 'yes',
                'health_condition' => 'good',
                'national_id' => null,
                'second_language' => 'urdu',
                'father_name' => 'shanker',
                'father_phone' => null,
                'father_education' => 'Ba Pass',
                'father_profession' => 'Plumber',
                'father_designation' => 'chicago',
                'father_photo' => 'yes',
                'mother_name' => 'ok',
                'mother_phone' => 'xxxxxxxxxx',
                'mother_education' => 'BA Pass 2',
                'mother_profession' => 'Homemade',
                'mother_designation' => 'ISB',
                'mother_photo' => 'yes',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ]
        ]);
    }
}
