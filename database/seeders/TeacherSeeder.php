<?php
namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('teachers')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('teachers')->insert([
            [
                'id' => '1',
                'created_by' => '1',
                'updated_by' => '1',
                'school_id' => '1',
                'national_id' => null,
                'salary_grade_id' => null,
                'salary_type' => 'monthly',
                'responsibility' => 'Cashier',
                'name' => 'Emily',
                'email' => 'eml@gmail.com',
                'phone' => '534546567',
                'present_address' => 'Kohinoor xyz',
                'permanent_address' => 'Kohinoor xyz',
                'gender' => 'Male',
                'blood_group' => 'A+',
                'religion' => 'Muslim',
                'dob' => '01/02/2021',
                'joining_date' => '01/02/2021',
                'resign_date' => '01/02/2021',
                'photo' => null,
                'resume' => 'Yes',
                'facebook_url' => 'xyz.com',
                'linkedin_url' => 'xyz.com',
                'twitter_url' => 'xyz.com',
                'google_plus_url' => 'xyz.com',
                'instagram_url' => 'xyz.com',
                'pinterest_url' => 'xyz.com',
                'youtube_url' => 'youtube.com',
                'is_view_on_web' => 'yes',
                'other_info' => 'Additional info',
                'display_order' => 'xyz',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => '2',
                'created_by' => '1',
                'updated_by' => '1',
                'school_id' => '1',
                'national_id' => null,
                'salary_grade_id' => null,
                'salary_type' => 'monthly',
                'responsibility' => 'Teacher',
                'name' => 'John',
                'email' => 'John@gmail.com',
                'phone' => '123456789',
                'present_address' => 'Chicago xyz',
                'permanent_address' => 'Chicago xyz',
                'gender' => 'Male',
                'blood_group' => 'B+',
                'religion' => 'Muslim',
                'dob' => '01/02/2021',
                'joining_date' => '01/02/2021',
                'resign_date' => '01/02/2021',
                'photo' => null,
                'resume' => 'Yes',
                'facebook_url' => 'facebook.com',
                'linkedin_url' => 'twitter.com',
                'twitter_url' => 'xyz.com',
                'google_plus_url' => 'xyz.com',
                'instagram_url' => 'insta.com',
                'pinterest_url' => 'pint.com',
                'youtube_url' => 'youtube.com',
                'is_view_on_web' => 'yes',
                'other_info' => 'Additional info',
                'display_order' => 'xyz',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ]
        ]);
    }
}
