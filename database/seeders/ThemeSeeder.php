<?php

namespace Database\Seeders;
use App\Enum\ThemeEnum;
use App\Models\Theme;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ThemeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('themes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach (ThemeEnum::getTranslationKeys() as $key => $value) {
            Theme::create([
                'name' => $value,
                'color'=>ThemeEnum::getColorByKey($key)
            ]);
        }
    }
}
