<?php

namespace Database\Seeders;

use App\Models\School;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchoolSeeder extends Seeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('schools')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        School::insert([
            [
                'name' => 'Test School1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'is_main_branch' => true
            ], [
                'name' => 'Test School2',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'is_main_branch' => true
            ], [
                'name' => 'Test School3',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'is_main_branch' => true
            ]
        ]);
    }
}
