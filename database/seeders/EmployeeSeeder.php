<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('employees')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('employees')->insert([
            [
                'id' => '1',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'user_id' => null,
                'national_id' => null,
                'designation_id' => '2',
                'salary_grade_id' => '2',
                'salary_type' => 'online',
                'name' => 'Simmon',
                'email' => 'xyz@gmail.com',
                'phone' => 'xxxxxxxxxxxxxxx',
                'present_address' => 'Kohinoor xyz',
                'permanent_address' => 'Kohinoor xyz',
                'gender' => 'Male',
                'blood_group' => 'A',
                'religion' => 'Muslim',
                'dob' => '01/02/2021',
                'joining_date' => '01/10/21',
                'resign_date' => '01/10/21',
                'photo' => null,
                'resume' => 'Yes',
                'facebook_url' => 'facebook.com',
                'linkedin_url' => 'twitter.com',
                'twitter_url' => 'xyz.com',
                'google_plus_url' => 'xyz.com',
                'instagram_url' => 'insta.com',
                'pinterest_url' => 'pint.com',
                'youtube_url' => 'youtube.com',
                'other_info' => 'Additional info',
                'display_order' => 'xyz',
                'is_view_on_web' => 'yes',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => '2',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'user_id' => null,
                'national_id' => null,
                'designation_id' => '2',
                'salary_grade_id' => '2',
                'salary_type' => 'online',
                'name' => 'Ferin',
                'email' => 'Fer@gmail.com',
                'phone' => 'xxxxxxxxxxxx',
                'present_address' => 'Peoples 2 xyz',
                'permanent_address' => 'Peoples 2 xyz',
                'gender' => 'Male',
                'blood_group' => 'B',
                'religion' => 'Muslim',
                'dob' => '01/02/2021',
                'joining_date' => '01/10/21',
                'resign_date' => '01/10/21',
                'photo' => null,
                'resume' => 'Yes',
                'facebook_url' => 'facebook.com',
                'linkedin_url' => 'twitter.com',
                'twitter_url' => 'xyz.com',
                'google_plus_url' => 'xyz.com',
                'instagram_url' => 'insta.com',
                'pinterest_url' => 'pint.com',
                'youtube_url' => 'youtube.com',
                'other_info' => 'Additional info',
                'display_order' => 'xyz',
                'is_view_on_web' => 'yes',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ]
        ]);
    }
}
