<?php

namespace Database\Seeders;

use App\Enum\LanguageEnum;
use App\Enum\TableEnum;
use App\Models\Language;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguageSeeder extends Seeder
{

    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table(TableEnum::LANGUAGES)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        foreach (LanguageEnum::getTranslationKeys() as $key => $value) {
            Language::create([
                'name' => $key,
                'label' => $value
            ]);
        }
    }
}
