<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('subjects')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('subjects')->insert([
            [
                'id' => 1,
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'class_id' => null,
                'teacher_id' => null,
                'name' => 'Urdu',
                'code' => '123',
                'author' => 'Fedrick',
                'note' => 'Hello Subject',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 2,
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'class_id' => null,
                'teacher_id' => null,
                'name' => 'English',
                'code' => '121',
                'author' => 'Sherlin',
                'note' => 'Hello Subject2',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 3,
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'class_id' => null,
                'teacher_id' => null,
                'name' => 'Math',
                'code' => '221',
                'author' => 'Marlin',
                'note' => 'Hello Subject3',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
        ]);
    }
}
