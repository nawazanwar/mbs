<?php

namespace Database\Seeders;

use App\Enum\TableEnum;
use App\Models\Permission;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeders.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table(TableEnum::ROLE_PERMISSION)->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $permissions = Permission::all();
        foreach ($permissions as $permission) {
            DB::table(TableEnum::ROLE_PERMISSION)->insert([
                'permission_id' => $permission->id,
                'role_id' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
