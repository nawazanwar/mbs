<?php

use Database\Seeders\LanguageSeeder;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\RolePermissionSeeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\SchoolSeeder;
use Database\Seeders\ThemeSeeder;
use Database\Seeders\UserRoleSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\ContinentsTableSeeder;
use Database\Seeders\CountriesTableSeeder;
use Database\Seeders\DivisionsTableSeeder;
use Database\Seeders\CitiesTableSeeder;
use Database\Seeders\ClassesSeeder;
use Database\Seeders\ClassSeeder;
use Database\Seeders\DesignationSeeder;
use Database\Seeders\EmployeeSeeder;
use Database\Seeders\GuardianSeeder;
use Database\Seeders\SectionSeeder;
use Database\Seeders\StudentActivitySeeder;
use Database\Seeders\StudentSeeder;
use Database\Seeders\StudentTypeSeeder;
use Database\Seeders\SubjectSeeder;
use Database\Seeders\TeacherSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguageSeeder::class);
        $this->call(ThemeSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(UserRoleSeeder::class);
        $this->call(PermissionSeeder::class);
        $this->call(RolePermissionSeeder::class);
        $this->call(SchoolSeeder::class);
    }
}
