<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('classes')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('classes')->insert([
            [
                'id' => 1,
                'created_by' => Carbon::now(),
                'updated_by' => Carbon::now(),
                'school_id' => null,
                'teacher_id' => null,
                'name' => 'One',
                'numeric_name' => '123',
                'note' => 'Hello Class1',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 2,
                'created_by' => Carbon::now(),
                'updated_by' => Carbon::now(),
                'school_id' => null,
                'teacher_id' => null,
                'name' => 'Two',
                'numeric_name' => '1234',
                'note' => 'Hello Class2',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => 3,
                'created_by' => Carbon::now(),
                'updated_by' => Carbon::now(),
                'school_id' => null,
                'teacher_id' => null,
                'name' => 'Three',
                'numeric_name' => '12345',
                'note' => 'Hello Class3',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],

        ]);
    }
}
