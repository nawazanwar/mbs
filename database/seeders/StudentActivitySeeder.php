<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class StudentActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('student_activities')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('student_activities')->insert([
            [
                'id' => '1',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'student_id' => null,
                'class_id' => null,
                'section_id' => null,
                'academic_year_id' => null,
                'activity' => 'Sports',
                'activity_date' => '01/01/21',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => '2',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'student_id' => null,
                'class_id' => null,
                'section_id' => null,
                'academic_year_id' => null,
                'activity' => 'badminton',
                'activity_date' => '01/10/21',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ]
        ]);
    }
}
