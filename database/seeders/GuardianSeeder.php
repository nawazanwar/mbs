<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class GuardianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('guardians')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        DB::table('guardians')->insert([
            [
                'id' => '1',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'user_id' => null,
                'national_id' => null,
                'username' => 'asfand',
                'password' => 'ATM',
                'name' => 'Shanker',
                'phone' => '23435345',
                'email' => 'chu@gmail.com',
                'profession' => 'accountant',
                'present_address' => 'Kohinoor xyz',
                'permanent_address' => 'Kohinoor xyz',
                'religion' => 'Muslim',
                'photo' => null,
                'other_info' => 'Additional info',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ],
            [
                'id' => '2',
                'created_by' => 'admin',
                'updated_by' => 'admin',
                'school_id' => null,
                'user_id' => null,
                'national_id' => null,
                'username' => 'rehman',
                'password' => 'ATM',
                'name' => 'Larvin',
                'phone' => '123456789',
                'email' => 'lar@gmail.com',
                'profession' => 'cashier',
                'present_address' => 'Gate xyz',
                'permanent_address' => 'Gate2 xyz',
                'religion' => 'Muslim',
                'photo' => null,
                'other_info' => 'Additional info',
                'status' => true,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'deleted_at' => NULL
            ]
        ]);
    }
}
