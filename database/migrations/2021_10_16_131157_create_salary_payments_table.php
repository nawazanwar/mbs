<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('salary_payments')) {
            return true;
        }
        Schema::create('salary_payments', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('salary_grade_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('expenditure_id');
            $table->string('salary_type')->nullable();
            $table->string('salary_month')->nullable();
            $table->integer('basic_salary')->nullable();
            $table->integer('house_rent')->nullable();
            $table->integer('transport')->nullable();
            $table->integer('medical')->nullable();
            $table->integer('bonus')->nullable();
            $table->integer('over_time_hourly_rate')->nullable();
            $table->integer('over_time_total_hour')->nullable();
            $table->integer('over_time_amount')->nullable();
            $table->integer('provident_fund')->nullable();
            $table->integer('penalty')->nullable();
            $table->integer('hourly_rate')->nullable();
            $table->integer('total_hour')->nullable();
            $table->integer('gross_salary')->nullable();
            $table->integer('total_allowance')->nullable();
            $table->integer('total_deduction')->nullable();
            $table->integer('net_salary')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('cheque_no')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('payment_to')->nullable();
            $table->longText('note')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_payments');
    }
}
