<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('operations')) {
            return true;
        }
        Schema::create('operations', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('module_id');
            $table->string('operation_name')->nullable();
            $table->string('operation_slug')->nullable();
            $table->boolean('is_view_visible')->default(false);
            $table->boolean('is_add_visible')->default(false);
            $table->boolean('is_update_visible')->default(false);
            $table->boolean('is_delete_visible')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('operations');
    }
}
