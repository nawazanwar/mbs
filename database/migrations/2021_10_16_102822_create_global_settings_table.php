<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGlobalSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('global_settings')) {
            return true;
        }
        Schema::create('global_settings', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('brand_name')-> nullable();
            $table->string('brand_title')-> nullable();
            $table->string('language')-> nullable();
            $table->integer('enable_rtl')->nullable();
            $table->integer('enable_frontend')->nullable();
            $table->string('theme_name')-> nullable();
            $table->string('date_format')->nullable();
            $table->string('time_zone')-> nullable();
            $table->string('brand_logo')-> nullable();
            $table->string('favicon_icon')-> nullable();
            $table->string('brand_footer')-> nullable();
            $table->string('google_analytics')-> nullable();
            $table->string('currency')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->string('splash_image')-> nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('global_settings');
    }
}
