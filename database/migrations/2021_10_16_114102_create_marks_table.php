<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('marks')) {
            return true;
        }
        Schema::create('marks', function (Blueprint $table) {


            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('exam_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('section_id')->nullable();
            $table->foreignId('subject_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('student_id')->nullable();
            $table->foreignId('grade_id');
            $table->integer('written_mark')->nullable();
            $table->integer('written_obtain')->nullable();
            $table->integer('tutorial_mark')->nullable();
            $table->integer('tutorial_obtain')->nullable();
            $table->integer('practical_mark')->nullable();
            $table->integer('practical_obtain')->nullable();
            $table->integer('viva_mark')->nullable();
            $table->integer('viva_obtain')->nullable();
            $table->integer('exam_total_mark')->nullable();
            $table->integer('obtain_total_mark')->nullable();
            $table->longText('remark')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marks');
    }
}
