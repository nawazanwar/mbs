<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('leave_applications')) {
            return true;
        }

        Schema::create('leave_applications', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('role_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('type_id');
            $table->date('leave_from')->nullable();
            $table->date('leave_to')->nullable();
            $table->integer('leave_day')->nullable();
            $table->text('leave_reason')->nullable();
            $table->text('leave_note')->nullable();
            $table->datetime('leave_date')->nullable();
            $table->integer('leave_status')->nullable();
            $table->string('attachment')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_applications');
    }
}
