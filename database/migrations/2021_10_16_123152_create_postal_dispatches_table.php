<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostalDispatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('postal_dispatches')) {
            return true;
        }
        Schema::create('postal_dispatches', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('to_title')->nullable();
            $table->string('reference')->nullable();
            $table->string('address')->nullable();
            $table->string('from_title')->nullable();
            $table->date('dispatch_date')->nullable();
            $table->string('attachment')->nullable();
            $table->longText('note')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postal_dispatches');
    }
}
