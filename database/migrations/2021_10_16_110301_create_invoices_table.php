<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('invoices')) {
            return true;
        }
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('custom_invoice_id')->nullable();
            $table->integer('is_applicable_discount')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->string('invoice_type')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('student_id')->nullable();
            $table->string('month')->nullable();
            $table->integer('gross_amount')->nullable();
            $table->integer('net_amount')->nullable();
            $table->integer('discount')->nullable();
            $table->string('paid_status')->nullable();
            $table->integer('temp_amount')->nullable();
            $table->date('date')->nullable();
            $table->longText('note')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
