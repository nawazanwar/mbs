<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessageRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('message_relationships')) {
            return true;
        }
        Schema::create('message_relationships', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('message_id');
            $table->foreignId('sender_id');
            $table->foreignId('receiver_id');
            $table->foreignId('owner_id');
            $table->foreignId('role_id')->nullable();
            $table->boolean('is_trash')->default(false);
            $table->boolean('is_draft')->default(false);
            $table->boolean('is_favorite')->default(false);
            $table->boolean('is_read')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('message_relationships');
    }
}
