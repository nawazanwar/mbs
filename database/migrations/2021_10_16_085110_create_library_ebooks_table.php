<?php

use App\Enum\TableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibraryEbooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable(TableEnum::LIBRARY_EBOOKS)) {
            return true;
        }
        Schema::create(TableEnum::LIBRARY_EBOOKS, function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('subject_id')->nullable();
            $table->string('name')->nullable();
            $table->string('author')->nullable();
            $table->string('edition')->nullable();
            $table->string('language')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('file_name')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TableEnum::LIBRARY_EBOOKS);
    }
}
