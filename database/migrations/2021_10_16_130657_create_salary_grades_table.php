<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalaryGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('salary_grades')) {
            return true;
        }
        Schema::create('salary_grades', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('grade_name')->nullable();
            $table->integer('basic_salary')->nullable();
            $table->integer('house_rent')->nullable();
            $table->integer('transport')->nullable();
            $table->integer('medical')->nullable();
            $table->integer('over_time_hourly_rate')->nullable();
            $table->integer('provident_fund')->nullable();
            $table->integer('hourly_rate')->nullable();
            $table->integer('total_allowance')->nullable();
            $table->integer('total_deduction')->nullable();
            $table->integer('gross_salary')->nullable();
            $table->integer('net_salary')->nullable();
            $table->longText('note')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_grades');
    }
}
