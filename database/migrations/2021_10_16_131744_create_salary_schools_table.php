<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalarySchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('salary_schools')) {
            return true;
        }
        Schema::create('salary_schools', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('school_code')->nullable();
            $table->string('registration_date')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('currency')->nullable();
            $table->string('currency_symbol')->nullable();
            $table->text('footer')->nullable();
            $table->string('logo')->nullable();
            $table->string('frontend_logo')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->string('academic_year')->nullable();
            $table->string('school_fax')->nullable();
            $table->string('school_lat')->nullable();
            $table->string('school_lng')->nullable();
            $table->string('map_api_key')->nullable();
            $table->string('zoom_api_key')->nullable();
            $table->string('zoom_secret')->nullable();
            $table->integer('enable_frontend')->nullable();
            $table->integer('enable_online_admission')->nullable();
            $table->integer('final_result_type')->nullable();
            $table->string('language')->nullable();
            $table->string('theme_name')->nullable();
            $table->text('about_text')->nullable();
            $table->string('about_image')->nullable();
            $table->integer('enable_rtl')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->string('google_plus_url')->nullable();
            $table->string('youtube_url')->nullable();
            $table->string('instagram_url')->nullable();
            $table->string('pinterest_url')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salary_schools');
    }
}
