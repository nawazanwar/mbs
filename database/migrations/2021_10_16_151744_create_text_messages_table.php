<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTextMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('text_messages')) {
            return true;
        }
        Schema::create('text_messages', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('role_id')->nullable();
            $table->foreignId('sender_role_id')->nullable();
            $table->integer('receivers')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->string('sms_gateway')->nullable();
            $table->string('sms_type')->nullable();
            $table->text('body')->nullable();
            $table->date('absent_date')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('text_messages');
    }
}
