<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('payment_settings')) {
            return true;
        }
        Schema::create('payment_settings', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('paypal_api_username')->nullable();
            $table->string('paypal_api_password')->nullable();
            $table->string('paypal_api_signature')->nullable();
            $table->string('paypal_email')->nullable();
            $table->integer('paypal_demo')->nullable();
            $table->integer('paypal_extra_charge')->nullable();
            $table->integer('paypal_status')->nullable();
            $table->string('stripe_secret')->nullable();
            $table->integer('stripe_demo')->nullable();
            $table->integer('stripe_extra_charge')->nullable();
            $table->integer('stripe_status')->nullable();
            $table->string('payumoney_key')->nullable();
            $table->string('payumoney_salt')->nullable();
            $table->string('payumoney_demo')->nullable();
            $table->string('payu_extra_charge')->nullable();
            $table->string('payumoney_status')->nullable();
            $table->string('ccavenue_key')->nullable();
            $table->string('ccavenue_salt')->nullable();
            $table->integer('ccavenue_demo')->nullable();
            $table->integer('ccavenue_extra_charge')->nullable();
            $table->integer('ccavenue_status')->nullable();
            $table->string('paytm_merchant_key')->nullable();
            $table->string('paytm_merchant_mid')->nullable();
            $table->text('paytm_merchant_website')->nullable();
            $table->text('paytm_industry_type')->nullable();
            $table->integer('paytm_demo')->nullable();
            $table->integer('paytm_extra_charge')->nullable();
            $table->integer('paytm_status')->nullable();
            $table->string('stack_secret_key')->nullable();
            $table->string('stack_public_key')->nullable();
            $table->integer('stack_demo')->nullable();
            $table->integer('stack_extra_charge')->nullable();
            $table->integer('stack_status')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_settings');
    }
}
