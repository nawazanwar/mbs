<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('transactions')) {
            return true;
        }
        Schema::create('transactions', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('invoice_id');
            $table->integer('amount')->nullable();
            $table->string('payment_method')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('cheque_no')->nullable();
            $table->foreignId('transaction_id');
            $table->date('payment_date')->nullable();
            $table->string('pum_first_name')->nullable();
            $table->string('pum_email')->nullable();
            $table->string('pum_phone')->nullable();
            $table->string('stripe_card_number')->nullable();
            $table->string('stack_email')->nullable();
            $table->string('stack_reference')->nullable();
            $table->string('bank_receipt')->nullable();
            $table->string('card_cvv')->nullable();
            $table->string('expire_month')->nullable();
            $table->string('expire_year')->nullable();
            $table->longText('note')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
