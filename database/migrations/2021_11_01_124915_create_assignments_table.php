<?php

use App\Enum\TableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable(TableEnum::ASSIGNMENTS)) {
            return true;
        }
        Schema::create(TableEnum::ASSIGNMENTS, function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();

            $table->foreignId('school_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('section_id')->nullable();
            $table->foreignId('subject_id')->nullable();


            $table->string('title')->nullable();
            $table->date('deadline')->nullable();
            $table->longText('note')->nullable();
            $table->string('assignment')->nullable();

            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TableEnum::ASSIGNMENTS);
    }
}
