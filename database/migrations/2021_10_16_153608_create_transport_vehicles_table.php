<?php

use App\Enum\TableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable(TableEnum::TRANSPORT_VEHICLES)) {
            return true;
        }
        Schema::create(TableEnum::TRANSPORT_VEHICLES, function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('number')->nullable();
            $table->string('model')->nullable();
            $table->string('driver')->nullable();
            $table->string('license')->nullable();
            $table->string('contact')->nullable();
            $table->longText('note')->nullable();
            $table->boolean('is_allocated')->default(false);
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TableEnum::TRANSPORT_VEHICLES);
    }
}
