<?php

use App\Enum\TableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdmitCardSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable(TableEnum::ADMIT_CARD_SETTINGS)) {
            return true;
        }
        Schema::create(TableEnum::ADMIT_CARD_SETTINGS, function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('border_color')->nullable();
            $table->string('top_bg')->nullable();
            $table->string('bottom_bg')->nullable();
            $table->string('school_logo')->nullable();
            $table->string('school_name')->nullable();
            $table->string('school_name_font_size')->nullable();
            $table->string('school_name_color')->nullable();
            $table->string('school_address')->nullable();
            $table->string('school_address_color')->nullable();
            $table->string('admit_font_size')->nullable();
            $table->string('admit_color')->nullable();
            $table->string('admit_bg')->nullable();
            $table->string('title_font_size')->nullable();
            $table->string('title_color')->nullable();
            $table->string('value_font_size')->nullable();
            $table->string('value_color')->nullable();
            $table->string('bottom_text')->nullable();
            $table->string('bottom_text_color')->nullable();
            $table->string('bottom_text_align')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TableEnum::ADMIT_CARD_SETTINGS);
    }
}
