<?php

use App\Enum\TableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (Schema::hasTable(TableEnum::STUDENTS)) {
            return true;
        }
        Schema::create(TableEnum::STUDENTS, function (Blueprint $table) {


            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('guardian_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('section_id')->nullable();
            $table->string('student_type_id')->nullable();
            $table->string('admission_no')->nullable();
            $table->date('admission_date')->nullable();
            $table->string('relation_with')->nullable();
            $table->integer('registration_no')->nullable();
            $table->integer('roll_no')->nullable();
            $table->string('group')->nullable();
            $table->string('present_address')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('gender')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('religion')->nullable();
            $table->string('caste')->nullable();
            $table->string('dob')->nullable();
            $table->string('age')->nullable();
            $table->string('other_info')->nullable();

            $table->string('discount_id')->nullable();
            $table->string('previous_school')->nullable();
            $table->string('previous_class')->nullable();
            $table->string('transfer_certificate')->nullable();
            $table->string('health_condition')->nullable();
            $table->string('national_id')->nullable();
            $table->string('second_language')->nullable();

            $table->string('father_name')->nullable();
            $table->string('father_phone')->nullable();
            $table->string('father_education')->nullable();
            $table->string('father_profession')->nullable();
            $table->string('father_designation')->nullable();
            $table->string('father_photo')->nullable();

            $table->string('mother_name')->nullable();
            $table->string('mother_phone')->nullable();
            $table->string('mother_education')->nullable();
            $table->string('mother_profession')->nullable();
            $table->string('mother_designation')->nullable();
            $table->string('mother_photo')->nullable();

            $table->boolean('active')->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TableEnum::STUDENTS);
    }
}
