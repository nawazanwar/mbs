<?php

use App\Enum\TableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable(TableEnum::ASSIGNMENT_SUBMISSIONS)) {
            return true;
        }
        Schema::create(TableEnum::ASSIGNMENT_SUBMISSIONS, function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();

            $table->foreignId('school_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('section_id')->nullable();
            $table->foreignId('student_id')->nullable();
            $table->foreignId('assignment_id')->nullable();

            $table->string('submission')->nullable();
            $table->timestamp('submitted_at')->nullable();
            $table->longText('note')->nullable();
            $table->boolean('status')->default(false);

            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TableEnum::ASSIGNMENT_SUBMISSIONS);
    }
}
