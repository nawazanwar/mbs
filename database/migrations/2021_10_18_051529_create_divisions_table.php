<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDivisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('divisions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('country_id')->comment('Country ID');
            $table->string('name', 255)->default('')->comment('Division Common Name');
            $table->string('full_name', 255)->nullable()->comment('Division Full Name');
            $table->string('code', 64)->nullable()->comment('ISO 3166-2 Code');
            $table->boolean('has_city')->default(0)->comment('Has city?');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('divisions');
    }
}
