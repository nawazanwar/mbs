<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('exam_attendances')) {
            return true;
        }
        Schema::create('exam_results', function (Blueprint $table) {
            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('exam_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('section_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('student_id')->nullable();
            $table->integer('total_subject')->nullable();
            $table->integer('total_mark')->nullable();
            $table->integer('total_obtain_mark')->nullable();
            $table->integer('avg_grade_point')->nullable();
            $table->integer('grade_id')->nullable();
            $table->string('result_status')->nullable();
            $table->string('merit_rank_in_class')->nullable();
            $table->string('merit_rank_in_section')->nullable();
            $table->longText('remark')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_results');
    }
}
