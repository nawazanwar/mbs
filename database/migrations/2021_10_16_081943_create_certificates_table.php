<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('certificates')) {
            return true;
        }
        Schema::create('certificates', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('name')->nullable();
            $table->string('top_title')->nullable();
            $table->string('sub_title_middle')->nullable();
            $table->text('main_text')->nullable();
            $table->string('footer_left')->nullable();
            $table->string('footer_middle')->nullable();
            $table->string('footer_right')->nullable();
            $table->string('background')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificates');
    }
}
