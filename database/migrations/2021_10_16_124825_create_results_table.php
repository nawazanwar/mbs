<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('results')) {
            return true;
        }
        Schema::create('results', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->foreignId('exam_id')->nullable();
            $table->foreignId('class_id')->nullable();
            $table->foreignId('section_id')->nullable();
            $table->foreignId('academic_year_id')->nullable();
            $table->foreignId('student_id')->nullable();
            $table->integer('avg_grade_point')->nullable();
            $table->integer('exam_total_mark')->nullable();
            $table->integer('obtain_total_mark')->nullable();
            $table->longText('remark')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
