<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('sms_settings')) {
            return true;
        }
        Schema::create('sms_settings', function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();
            $table->string('clickatell_username')->nullable();
            $table->string('clickatell_password')->nullable();
            $table->string('clickatell_api_key')->nullable();
            $table->string('clickatell_from_number')->nullable();
            $table->integer('clickatell_status')->nullable();
            $table->string('twilio_account_sid)')->nullable();
            $table->string('twilio_auth_token')->nullable();
            $table->string('twilio_from_number')->nullable();
            $table->string('clickatell_mo_no')->nullable();
            $table->integer('twilio_status')->nullable();
            $table->string('bulk_username')->nullable();
            $table->string('bulk_password')->nullable();
            $table->integer('bulk_status')->nullable();
            $table->string('msg91_auth_key')->nullable();
            $table->string('msg91_sender_id')->nullable();
            $table->integer('msg91_status')->nullable();
            $table->string('plivo_auth_id')->nullable();
            $table->string('plivo_auth_token')->nullable();
            $table->string('plivo_from_number')->nullable();
            $table->integer('plivo_status')->nullable();
            $table->string('textlocal_username')->nullable();
            $table->string('textlocal_hash_key')->nullable();
            $table->string('textlocal_sender_id')->nullable();
            $table->integer('textlocal_status')->nullable();
            $table->string('smscountry_username')->nullable();
            $table->string('smscountry_password')->nullable();
            $table->string('smscountry_sender_id')->nullable();
            $table->integer('smscountry_status')->nullable();
            $table->string('betasms_username')->nullable();
            $table->string('betasms_password')->nullable();
            $table->string('betasms_sender_id')->nullable();
            $table->integer('betasms_status')->nullable();
            $table->string('bulk_pk_username')->nullable();
            $table->string('bulk_pk_password')->nullable();
            $table->string('bulk_pk_sender_id')->nullable();
            $table->integer('bulk_pk_status')->nullable();
            $table->string('cluster_auth_key')->nullable();
            $table->string('cluster_sender_id')->nullable();
            $table->string('cluster_router')->nullable();
            $table->integer('cluster_status')->nullable();
            $table->string('alpha_username')->nullable();
            $table->string('alpha_hash')->nullable();
            $table->string('alpha_type')->nullable();
            $table->integer('alpha_status')->nullable();
            $table->string('bdbulk_hash')->nullable();
            $table->string('bdbulk_type')->nullable();
            $table->integer('bdbulk_status')->nullable();
            $table->string('mim_api_key')->nullable();
            $table->string('mim_type')->nullable();
            $table->string('mim_sender_id')->nullable();
            $table->integer('mim_status')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_settings');
    }
}
