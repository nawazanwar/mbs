<?php

use App\Enum\TableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibraryBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     if(Schema::hasTable(TableEnum::LIBRARY_BOOKS)) {
            return true;
        }
        Schema::create(TableEnum::LIBRARY_BOOKS, function (Blueprint $table) {

            $table->id();
            $table->foreignId('created_by')->nullable();
            $table->foreignId('updated_by')->nullable();
            $table->foreignId('school_id')->nullable();

            $table->string('custom_id')->nullable();
            $table->string('title')->nullable();
            $table->string('isbn_no')->nullable();
            $table->string('edition')->nullable();
            $table->string('author')->nullable();
            $table->string('language')->nullable();
            $table->decimal('price')->nullable();
            $table->integer('qty')->nullable();
            $table->string('cover')->nullable();
            $table->string('rack_no')->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TableEnum::LIBRARY_BOOKS);
    }
}
