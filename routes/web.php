<?php

use App\Http\Controllers\Dashboard\AbsentEmailController;
use App\Http\Controllers\Dashboard\AbsentSMSController;
use App\Http\Controllers\Dashboard\AdmitCardSettingController;
use App\Http\Controllers\Dashboard\IdCardSettingController;
use App\Http\Controllers\Dashboard\TeacherIdCardController;
use App\Http\Controllers\Dashboard\ExamScheduleController;
use App\Http\Controllers\Dashboard\ExamSuggestionController;
use App\Http\Controllers\Dashboard\AttendanceController;
use App\Http\Controllers\Dashboard\ComplainController;
use App\Http\Controllers\Dashboard\ComplainTypeController;
use App\Http\Controllers\Dashboard\DesignationController;
use App\Http\Controllers\Dashboard\EmployeeController;
use App\Http\Controllers\Dashboard\EventController;
use App\Http\Controllers\Dashboard\ExamAttendanceController;
use App\Http\Controllers\Dashboard\MarkSendBySMSController;
use App\Http\Controllers\Dashboard\ResultSendByEmailController;
use App\Http\Controllers\Dashboard\ResultSendBySMSController;
use App\Http\Controllers\Dashboard\ExamController;
use App\Http\Controllers\Dashboard\ExamGradeController;
use App\Http\Controllers\Dashboard\ExamMarkController;
use App\Http\Controllers\Dashboard\ExamTermController;
use App\Http\Controllers\Dashboard\GalleryController;
use App\Http\Controllers\Dashboard\GalleryImageController;
use App\Http\Controllers\Dashboard\HolidayController;
use App\Http\Controllers\Dashboard\MarkSendByEmailController;
use App\Http\Controllers\Dashboard\MessageEmailController;
use App\Http\Controllers\Dashboard\MessageSMSController;
use App\Http\Controllers\Dashboard\NewsController;
use App\Http\Controllers\Dashboard\NoticeController;
use App\Http\Controllers\Dashboard\OnlineAdmissionController;
use App\Http\Controllers\Dashboard\PageController;
use App\Http\Controllers\Dashboard\ProfileController;
use App\Http\Controllers\Dashboard\ReportController;
use App\Http\Controllers\Dashboard\SliderController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LanguageController;
use Illuminate\Support\Facades\Route;

Route::get('lang/{lang}', [LanguageController::class, 'switchLang'])
    ->name('lang.switch');

require __DIR__ . '/registration.php';
require __DIR__ . '/auth.php';
Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.'], function () {

    Route::get('/', [\App\Http\Controllers\Dashboard\HomeController::class, 'index'])->name('index');
    Route::get('/profile', [ProfileController::class, 'index'])->name('profile.index');
    Route::get('/profile/edit', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::get('/password/reset', [ProfileController::class, 'res'])->name('password.reset.index');
    Route::get('/reports', [ReportController::class, 'index'])->name('reports.index');

    Route::get('/exams', [ExamController::class, 'index'])->name('exams.index');

    Route::get('/exam/attendances', [ExamAttendanceController::class, 'index'])->name('exam.attendances.index');
    Route::get('/exam/attendances/create', [ExamAttendanceController::class, 'create'])->name('exam.attendances.create');
    Route::get('/exam/attendances/edit/{id}', [ExamAttendanceController::class, 'edit'])->name('exam.attendances.edit');
    Route::post('/exam/attendances/store', [ExamAttendanceController::class, 'store'])->name('exam.attendances.store');
    Route::put('/exam/attendances/update/{id}', [ExamAttendanceController::class, 'update'])->name('exam.attendances.update');
    Route::delete('/exam/attendances/destroy/{id}', [ExamAttendanceController::class, 'destroy'])->name('exam.attendances.destroy');

    Route::get('/exam/marks', [ExamMarkController::class, 'index'])->name('exam.marks.index');
    Route::get('/exam/marks/create', [ExamMarkController::class, 'create'])->name('exam.marks.create');
    Route::get('/exam/marks/edit/{id}', [ExamMarkController::class, 'edit'])->name('exam.marks.edit');
    Route::post('/exam/marks/store', [ExamMarkController::class, 'store'])->name('exam.marks.store');
    Route::put('/exam/marks/update/{id}', [ExamMarkController::class, 'update'])->name('exam.marks.update');
    Route::delete('/exam/marks/destroy/{id}', [ExamMarkController::class, 'destroy'])->name('exam.marks.destroy');

    Route::get('/exam/terms', [ExamTermController::class, 'index'])->name('exam.terms.index');
    Route::get('/exam/terms/create', [ExamTermController::class, 'create'])->name('exam.terms.create');
    Route::get('/exam/terms/edit/{id}', [ExamTermController::class, 'edit'])->name('exam.terms.edit');
    Route::post('/exam/terms/store', [ExamTermController::class, 'store'])->name('exam.terms.store');
    Route::put('/exam/terms/update/{id}', [ExamTermController::class, 'update'])->name('exam.terms.update');
    Route::delete('/exam/terms/destroy/{id}', [ExamTermController::class, 'destroy'])->name('exam.terms.destroy');

    Route::get('/exam/grades', [ExamGradeController::class, 'index'])->name('exam.grades.index');
    Route::get('/exam/grades/create', [ExamGradeController::class, 'create'])->name('exam.grades.create');
    Route::get('/exam/grades/edit/{id}', [ExamGradeController::class, 'edit'])->name('exam.grades.edit');
    Route::post('/exam/grades/store', [ExamGradeController::class, 'store'])->name('exam.grades.store');
    Route::put('/exam/grades/update/{id}', [ExamGradeController::class, 'update'])->name('exam.grades.update');
    Route::delete('/exam/grades/destroy/{id}', [ExamGradeController::class, 'destroy'])->name('exam.grades.destroy');

    Route::get('/exam/schedules', [ExamScheduleController::class, 'index'])->name('exam.schedules.index');
    Route::get('/exam/schedules/create', [ExamScheduleController::class, 'create'])->name('exam.schedules.create');
    Route::get('/exam/schedules/edit/{id}', [ExamScheduleController::class, 'edit'])->name('exam.schedules.edit');
    Route::post('/exam/schedules/store', [ExamScheduleController::class, 'store'])->name('exam.schedules.store');
    Route::put('/exam/schedules/update/{id}', [ExamScheduleController::class, 'update'])->name('exam.schedules.update');
    Route::delete('/exam/schedules/destroy/{id}', [ExamScheduleController::class, 'destroy'])->name('exam.schedules.destroy');

    Route::get('/exam/suggestions', [ExamSuggestionController::class, 'index'])->name('exam.suggestions.index');
    Route::get('/exam/suggestions/create', [ExamSuggestionController::class, 'create'])->name('exam.suggestions.create');
    Route::get('/exam/suggestions/edit/{id}', [ExamSuggestionController::class, 'edit'])->name('exam.suggestions.edit');
    Route::post('/exam/suggestions/store', [ExamSuggestionController::class, 'store'])->name('exam.suggestions.store');
    Route::put('/exam/suggestions/update/{id}', [ExamSuggestionController::class, 'update'])->name('exam.suggestions.update');
    Route::delete('/exam/suggestions/destroy/{id}', [ExamSuggestionController::class, 'destroy'])->name('exam.suggestions.destroy');


    Route::get('/mark-send/by-sms', [MarkSendBySMSController::class, 'index'])->name('mark.send.by.sms.index');
    Route::get('/mark-send/by-sms/create', [MarkSendBySMSController::class, 'create'])->name('mark.send.by.sms.create');

    Route::get('/mark-send/by-email', [MarkSendByEmailController::class, 'index'])->name('mark.send.by.email.index');
    Route::get('/mark-send/by-email/create', [MarkSendByEmailController::class, 'create'])->name('mark.send.by.email.create');

    Route::get('/result-send/by-email', [ResultSendByEmailController::class, 'index'])->name('result.send.by.email.index');
    Route::get('/result-send/by-email/create', [ResultSendByEmailController::class, 'create'])->name('result.send.by.email.create');

    Route::get('/result-send/by-sms', [ResultSendBySMSController::class, 'index'])->name('result.send.by.sms.index');
    Route::get('/result-send/by-sms/create', [ResultSendBySMSController::class, 'create'])->name('result.send.by.sms.create');


    Route::get('/messages', [MarkSendBySMSController::class, 'index'])->name('messages.index');

    Route::get('/message/emails', [MessageEmailController::class, 'index'])->name('message.emails.index');
    Route::get('/message/emails/create', [MessageEmailController::class, 'create'])->name('message.emails.create');
    Route::get('/message/emails/edit/{id}', [MessageEmailController::class, 'edit'])->name('message.emails.edit');
    Route::post('/message/emails/store', [MessageEmailController::class, 'store'])->name('message.emails.store');
    Route::put('/message/emails/update/{id}', [MessageEmailController::class, 'update'])->name('message.emails.update');
    Route::delete('/message/emails/destroy/{id}', [MessageEmailController::class, 'destroy'])->name('message.emails.destroy');

    Route::get('/message/sms', [MessageSMSController::class, 'index'])->name('message.sms.index');
    Route::get('/message/sms/create', [MessageSMSController::class, 'create'])->name('message.sms.create');
    Route::get('/message/sms/edit/{id}', [MessageSMSController::class, 'edit'])->name('message.sms.edit');
    Route::post('/message/sms/store', [MessageSMSController::class, 'store'])->name('message.sms.store');
    Route::put('/message/sms/update/{id}', [MessageSMSController::class, 'update'])->name('message.sms.update');
    Route::delete('/message/sms/destroy/{id}', [MessageSMSController::class, 'destroy'])->name('message.sms.destroy');

    Route::get('/complain/types', [ComplainTypeController::class, 'index'])->name('complain.types.index');

    Route::get('/events', [EventController::class, 'index'])->name('events.index');
    Route::get('/events/create', [EventController::class, 'create'])->name('events.create');
    Route::get('/events/edit/{id}', [EventController::class, 'edit'])->name('events.edit');
    Route::post('/events/store', [EventController::class, 'store'])->name('events.store');
    Route::put('/events/update/{id}', [EventController::class, 'update'])->name('events.update');
    Route::delete('/events/destroy/{id}', [EventController::class, 'destroy'])->name('events.destroy');

    require __DIR__ . '/authorization.php';
    require __DIR__ . "/guardian.php";
    require __DIR__ . "/academic.php";
    require __DIR__ . "/teacher.php";
    require __DIR__ . "/assignment.php";
    require __DIR__ . "/certificate.php";
    require __DIR__ . "/definition.php";
    require __DIR__ . "/hrm.php";

    Route::get('/online/admissions', [OnlineAdmissionController::class, 'index'])->name('online.admissions.index');
    Route::get('/online/admissions/create', [OnlineAdmissionController::class, 'create'])->name('online.admissions.create');
    Route::get('/online/admissions/edit/{id}', [OnlineAdmissionController::class, 'edit'])->name('online.admissions.edit');
    Route::post('/online/admissions/store', [OnlineAdmissionController::class, 'store'])->name('online.admissions.store');
    Route::put('/online/admissions/update/{id}', [OnlineAdmissionController::class, 'update'])->name('online.admissions.update');
    Route::delete('/online/admissions/destroy/{id}', [OnlineAdmissionController::class, 'destroy'])->name('online.admissions.destroy');


    require __DIR__ . "/front-office.php";

    Route::get('/designations', [DesignationController::class, 'index'])->name('designations.index');
    Route::get('/designations/create', [DesignationController::class, 'create'])->name('designations.create');
    Route::get('/designations/edit/{id}', [DesignationController::class, 'edit'])->name('designations.edit');
    Route::post('/designations/store', [DesignationController::class, 'store'])->name('designations.store');
    Route::put('/designations/update/{id}', [DesignationController::class, 'update'])->name('designations.update');
    Route::delete('/designations/destroy/{id}', [DesignationController::class, 'destroy'])->name('designations.destroy');

    Route::get('/employees', [EmployeeController::class, 'index'])->name('employees.index');
    Route::get('/employees/create', [EmployeeController::class, 'create'])->name('employees.create');
    Route::get('/employees/edit/{id}', [EmployeeController::class, 'edit'])->name('employees.edit');
    Route::post('/employees/store', [EmployeeController::class, 'store'])->name('employees.store');
    Route::put('/employees/update/{id}', [EmployeeController::class, 'update'])->name('employees.update');
    Route::delete('/employees/destroy/{id}', [EmployeeController::class, 'destroy'])->name('employees.destroy');

    Route::get('/attendances', [AttendanceController::class, 'index'])->name('attendances.index');

    Route::get('/absent/emails', [AbsentEmailController::class, 'index'])->name('absent.emails.index');
    Route::get('/absent/emails/create', [AbsentEmailController::class, 'create'])->name('absent.emails.create');
    Route::get('/absent/emails/edit/{id}', [AbsentEmailController::class, 'edit'])->name('absent.emails.edit');
    Route::post('/absent/emails/store', [AbsentEmailController::class, 'store'])->name('absent.emails.store');
    Route::put('/absent/emails/update/{id}', [AbsentEmailController::class, 'update'])->name('absent.emails.update');
    Route::delete('/absent/emails/destroy/{id}', [AbsentEmailController::class, 'destroy'])->name('absent.emails.destroy');

    Route::get('/absent/sms', [AbsentSMSController::class, 'index'])->name('absent.sms.index');
    Route::get('/absent/sms/create', [AbsentSMSController::class, 'create'])->name('absent.sms.create');
    Route::get('/absent/sms/edit/{id}', [AbsentSMSController::class, 'edit'])->name('absent.sms.edit');
    Route::post('/absent/sms/store', [AbsentSMSController::class, 'store'])->name('absent.sms.store');
    Route::put('/absent/sms/update/{id}', [AbsentSMSController::class, 'update'])->name('absent.sms.update');
    Route::delete('/absent/sms/destroy/{id}', [AbsentSMSController::class, 'destroy'])->name('absent.sms.destroy');

    require __DIR__."/operation.php";
    require __DIR__ . '/configuration.php';
    require __DIR__ . '/finance.php';
    require __DIR__ . '/student.php';

    Route::get('/messages', [MessageEmailController::class, 'index'])->name('messages.index');

    Route::get('/complains', [ComplainController::class, 'index'])->name('complains.index');
    Route::get('/complains/create', [ComplainController::class, 'create'])->name('complains.create');
    Route::get('/complains/edit/{id}', [ComplainController::class, 'edit'])->name('complains.edit');
    Route::post('/complains/store', [ComplainController::class, 'store'])->name('complains.store');
    Route::put('/complains/update/{id}', [ComplainController::class, 'update'])->name('complains.update');
    Route::delete('/complains/destroy/{id}', [ComplainController::class, 'destroy'])->name('complains.destroy');

    Route::get('/complain/types', [ComplainTypeController::class, 'index'])->name('complain.types.index');
    Route::get('/complain/types/create', [ComplainTypeController::class, 'create'])->name('complain.types.create');
    Route::get('/complain/types/edit/{id}', [ComplainTypeController::class, 'edit'])->name('complain.types.edit');
    Route::post('/complain/types/store', [ComplainTypeController::class, 'store'])->name('complain.types.store');
    Route::put('/complain/types/update/{id}', [ComplainTypeController::class, 'update'])->name('complain.types.update');
    Route::delete('/complain/types/destroy/{id}', [ComplainTypeController::class, 'destroy'])->name('complain.types.destroy');

    Route::get('/news', [NewsController::class, 'index'])->name('news.index');
    Route::get('/news/create', [NewsController::class, 'create'])->name('news.create');
    Route::get('/news/edit/{id}', [NewsController::class, 'edit'])->name('news.edit');
    Route::post('/news/store', [NewsController::class, 'store'])->name('news.store');
    Route::put('/news/update/{id}', [NewsController::class, 'update'])->name('news.update');
    Route::delete('/news/destroy/{id}', [NewsController::class, 'destroy'])->name('news.destroy');

    Route::get('/holidays', [HolidayController::class, 'index'])->name('holidays.index');
    Route::get('/holidays/create', [HolidayController::class, 'create'])->name('holidays.create');
    Route::get('/holidays/edit/{id}', [HolidayController::class, 'edit'])->name('holidays.edit');
    Route::post('/holidays/store', [HolidayController::class, 'store'])->name('holidays.store');
    Route::put('/holidays/update/{id}', [HolidayController::class, 'update'])->name('holidays.update');
    Route::delete('/holidays/destroy/{id}', [HolidayController::class, 'destroy'])->name('holidays.destroy');

    Route::get('/notices', [NoticeController::class, 'index'])->name('notices.index');
    Route::get('/notices/create', [NoticeController::class, 'create'])->name('notices.create');
    Route::get('/notices/edit/{id}', [NoticeController::class, 'edit'])->name('notices.edit');
    Route::post('/notices/store', [NoticeController::class, 'store'])->name('notices.store');
    Route::put('/notices/update/{id}', [NoticeController::class, 'update'])->name('notices.update');
    Route::delete('/notices/destroy/{id}', [NoticeController::class, 'destroy'])->name('notices.destroy');

    Route::get('/gallery', [GalleryController::class, 'index'])->name('gallery.index');
    Route::get('/gallery/create', [GalleryController::class, 'create'])->name('gallery.create');
    Route::get('/gallery/edit/{id}', [GalleryController::class, 'edit'])->name('gallery.edit');
    Route::post('/gallery/store', [GalleryController::class, 'store'])->name('gallery.store');
    Route::put('/gallery/update/{id}', [GalleryController::class, 'update'])->name('gallery.update');
    Route::delete('/gallery/destroy/{id}', [GalleryController::class, 'destroy'])->name('gallery.destroy');

    Route::get('/gallery/images', [GalleryImageController::class, 'index'])->name('gallery.images.index');
    Route::get('/gallery/images/create', [GalleryImageController::class, 'create'])->name('gallery.images.create');
    Route::get('/gallery/images/edit/{id}', [GalleryImageController::class, 'edit'])->name('gallery.images.edit');
    Route::post('/gallery/images/store', [GalleryImageController::class, 'store'])->name('gallery.images.store');
    Route::put('/gallery/images/update/{id}', [GalleryImageController::class, 'update'])->name('gallery.images.update');
    Route::delete('/gallery/heads/images/{id}', [GalleryImageController::class, 'destroy'])->name('gallery.images.destroy');

    Route::get('/sliders', [SliderController::class, 'index'])->name('sliders.index');
    Route::get('/sliders/create', [SliderController::class, 'create'])->name('sliders.create');
    Route::get('/sliders/edit/{id}', [SliderController::class, 'edit'])->name('sliders.edit');
    Route::post('/sliders/store', [SliderController::class, 'store'])->name('sliders.store');
    Route::put('/sliders/update/{id}', [SliderController::class, 'update'])->name('sliders.update');
    Route::delete('/sliders/destroy/{id}', [SliderController::class, 'destroy'])->name('sliders.destroy');

    Route::get('/pages', [PageController::class, 'index'])->name('pages.index');
    Route::get('/pages/create', [PageController::class, 'create'])->name('pages.create');
    Route::get('/pages/edit/{id}', [PageController::class, 'edit'])->name('pages.edit');
    Route::post('/pages/store', [PageController::class, 'store'])->name('pages.store');
    Route::put('/pages/update/{id}', [PageController::class, 'update'])->name('pages.update');
    Route::delete('/pages/destroy/{id}', [PageController::class, 'destroy'])->name('pages.destroy');

    //Generate Card

    Route::get('/id-card-settings', [IdCardSettingController::class, 'index'])->name('id-card-settings.index');
    Route::get('/id-card-settings/create', [IdCardSettingController::class, 'create'])->name('id-card-settings.create');
    Route::get('/id-card-settings/edit/{id}', [IdCardSettingController::class, 'edit'])->name('id-card-settings.edit');
    Route::post('/id-card-settings/store', [IdCardSettingController::class, 'store'])->name('id-card-settings.store');
    Route::put('/id-card-settings/update/{id}', [IdCardSettingController::class, 'update'])->name('id-card-settings.update');
    Route::delete('/id-card-settings/destroy/{id}', [IdCardSettingController::class, 'destroy'])->name('id-card-settings.destroy');

    Route::get('/admit-card-settings', [AdmitCardSettingController::class, 'index'])->name('admit-card-settings.index');
    Route::get('/admit-card-settings/create', [AdmitCardSettingController::class, 'create'])->name('admit-card-settings.create');
    Route::get('/admit-card-settings/edit/{id}', [AdmitCardSettingController::class, 'edit'])->name('admit-card-settings.edit');
    Route::post('/admit-card-settings/store', [AdmitCardSettingController::class, 'store'])->name('admit-card-settings.store');
    Route::put('/admit-card-settings/update/{id}', [AdmitCardSettingController::class, 'update'])->name('admit-card-settings.update');
    Route::delete('/admit-card-settings/destroy/{id}', [AdmitCardSettingController::class, 'destroy'])->name('admit-card-settings.destroy');

    Route::get('/teacher-id-cards', [TeacherIdCardController::class, 'index'])->name('teacher-id-cards.index');
    Route::get('/teacher-id-cards/create', [TeacherIdCardController::class, 'create'])->name('teacher-id-cards.create');
    Route::get('/teacher-id-cards/edit/{id}', [TeacherIdCardController::class, 'edit'])->name('teacher-id-cards.edit');
    Route::post('/teacher-id-cards/store', [TeacherIdCardController::class, 'store'])->name('teacher-id-cards.store');
    Route::put('/teacher-id-cards/update/{id}', [TeacherIdCardController::class, 'update'])->name('teacher-id-cards.update');
    Route::delete('/teacher-id-cards/destroy/{id}', [TeacherIdCardController::class, 'destroy'])->name('teacher-id-cards.destroy');

    Route::get('/student-id-cards', [TeacherIdCardController::class, 'index'])->name('student-id-cards.index');
    Route::get('/student-id-cards/create', [TeacherIdCardController::class, 'create'])->name('student-id-cards.create');
    Route::get('/student-id-cards/edit/{id}', [TeacherIdCardController::class, 'edit'])->name('student-id-cards.edit');
    Route::post('/student-id-cards/store', [TeacherIdCardController::class, 'store'])->name('student-id-cards.store');
    Route::put('/student-id-cards/update/{id}', [TeacherIdCardController::class, 'update'])->name('student-id-cards.update');
    Route::delete('/student-id-cards/destroy/{id}', [TeacherIdCardController::class, 'destroy'])->name('student-id-cards.destroy');

    Route::get('/employee-id-cards', [TeacherIdCardController::class, 'index'])->name('employee-id-cards.index');
    Route::get('/employee-id-cards/create', [TeacherIdCardController::class, 'create'])->name('employee-id-cards.create');
    Route::get('/employee-id-cards/edit/{id}', [TeacherIdCardController::class, 'edit'])->name('employee-id-cards.edit');
    Route::post('/employee-id-cards/store', [TeacherIdCardController::class, 'store'])->name('employee-id-cards.store');
    Route::put('/employee-id-cards/update/{id}', [TeacherIdCardController::class, 'update'])->name('employee-id-cards.update');
    Route::delete('/employee-id-cards/destroy/{id}', [TeacherIdCardController::class, 'destroy'])->name('employee-id-cards.destroy');

    Route::get('/student-admit-cards', [TeacherIdCardController::class, 'index'])->name('student-admit-cards.index');
    Route::get('/student-admit-cards/create', [TeacherIdCardController::class, 'create'])->name('student-admit-cards.create');
    Route::get('/student-admit-cards/edit/{id}', [TeacherIdCardController::class, 'edit'])->name('student-admit-cards.edit');
    Route::post('/student-admit-cards/store', [TeacherIdCardController::class, 'store'])->name('student-admit-cards.store');
    Route::put('/student-admit-cards/update/{id}', [TeacherIdCardController::class, 'update'])->name('student-admit-cards.update');
    Route::delete('/student-admit-cards/destroy/{id}', [TeacherIdCardController::class, 'destroy'])->name('student-admit-cards.destroy');

});


Route::get('/{school_id?}', [HomeController::class, 'index'])->name('home');