<?php

use App\Http\Controllers\Dashboard\Assignment\AssignmentController;
use App\Http\Controllers\Dashboard\Assignment\AssignmentSubmissionController;
use Illuminate\Support\Facades\Route;

Route::get('/assignments', [AssignmentController::class, 'index'])->name('assignments.index');
Route::get('/assignments/create', [AssignmentController::class, 'create'])->name('assignments.create');
Route::get('/assignments/edit/{id}', [AssignmentController::class, 'edit'])->name('assignments.edit');
Route::post('/assignments/store', [AssignmentController::class, 'store'])->name('assignments.store');
Route::put('/assignments/update/{id}', [AssignmentController::class, 'update'])->name('assignments.update');
Route::delete('/assignments/destroy/{id}', [AssignmentController::class, 'destroy'])->name('assignments.destroy');

Route::get('/assignment/submissions', [AssignmentSubmissionController::class, 'index'])->name('assignment.submissions.index');
Route::get('/assignment/submissions/create', [AssignmentSubmissionController::class, 'create'])->name('assignment.submissions.create');
Route::get('/assignment/submissions/edit/{id}', [AssignmentSubmissionController::class, 'edit'])->name('assignment.submissions.edit');
Route::post('/assignment/submissions/store', [AssignmentSubmissionController::class, 'store'])->name('assignment.submissions.store');
Route::put('/assignment/submissions/update/{id}', [AssignmentSubmissionController::class, 'update'])->name('assignment.submissions.update');
Route::delete('/assignment/submissions/destroy/{id}', [AssignmentSubmissionController::class, 'destroy'])->name('assignment.submissions.destroy');