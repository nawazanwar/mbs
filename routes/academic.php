<?php

use App\Http\Controllers\Dashboard\Academic\AcademicYearController;
use App\Http\Controllers\Dashboard\Academic\ClassesController;
use App\Http\Controllers\Dashboard\Academic\ClassPromotionController;
use App\Http\Controllers\Dashboard\Academic\ClassRoutineController;
use App\Http\Controllers\Dashboard\Academic\LiveClassController;
use App\Http\Controllers\Dashboard\Academic\SectionController;
use App\Http\Controllers\Dashboard\Academic\StudyMaterialController;
use App\Http\Controllers\Dashboard\Academic\SubjectController;
use App\Http\Controllers\Dashboard\Academic\SyllabusController;
use Illuminate\Support\Facades\Route;

Route::get('/academic/years', [AcademicYearController::class, 'index'])->name('academic.years.index');
Route::get('/academic/years/create', [AcademicYearController::class, 'create'])->name('academic.years.create');
Route::get('/academic/years/edit/{id}', [AcademicYearController::class, 'edit'])->name('academic.years.edit');
Route::post('/academic/years/store', [AcademicYearController::class, 'store'])->name('academic.years.store');
Route::put('/academic/years/update/{id}', [AcademicYearController::class, 'update'])->name('academic.years.update');
Route::delete('/academic/years/destroy/{id}', [AcademicYearController::class, 'destroy'])->name('academic.years.destroy');

Route::get('/classes/live', [LiveClassController::class, 'index'])->name('classes.live.index');
Route::get('/classes/live/create', [LiveClassController::class, 'create'])->name('classes.live.create');
Route::get('/classes/live/edit/{id}', [LiveClassController::class, 'edit'])->name('classes.live.edit');
Route::post('/classes/live/store', [LiveClassController::class, 'store'])->name('classes.live.store');
Route::put('/classes/live/update/{id}', [LiveClassController::class, 'update'])->name('classes.live.update');
Route::delete('/classes/live/destroy/{id}', [LiveClassController::class, 'destroy'])->name('classes.live.destroy');

Route::get('/classes', [ClassesController::class, 'index'])->name('classes.index');
Route::get('/classes/create', [ClassesController::class, 'create'])->name('classes.create');
Route::get('/classes/edit/{id}', [ClassesController::class, 'edit'])->name('classes.edit');
Route::post('/classes/store', [ClassesController::class, 'store'])->name('classes.store');
Route::put('/classes/update/{id}', [ClassesController::class, 'update'])->name('classes.update');
Route::delete('/templates/destroy/{id}', [ClassesController::class, 'destroy'])->name('classes.destroy');

Route::get('/class/routines', [ClassRoutineController::class, 'index'])->name('class.routines.index');
Route::get('/class/routines/create', [ClassRoutineController::class, 'create'])->name('class.routines.create');
Route::get('/class/routines/edit/{id}', [ClassRoutineController::class, 'edit'])->name('class.routines.edit');
Route::post('/class/routines/store', [ClassRoutineController::class, 'store'])->name('class.routines.store');
Route::put('/class/routines/update/{id}', [ClassRoutineController::class, 'update'])->name('class.routines.update');
Route::delete('/class/routines/destroy/{id}', [ClassRoutineController::class, 'destroy'])->name('class.routines.destroy');

Route::get('/class/promotions', [ClassPromotionController::class, 'index'])->name('class.promotions.index');

Route::get('/sections', [SectionController::class, 'index'])->name('sections.index');
Route::get('/sections/create', [SectionController::class, 'create'])->name('sections.create');
Route::get('/sections/edit/{id}', [SectionController::class, 'edit'])->name('sections.edit');
Route::post('/sections/store', [SectionController::class, 'store'])->name('sections.store');
Route::put('/sections/update/{id}', [SectionController::class, 'update'])->name('sections.update');
Route::delete('/sections/destroy/{id}', [SectionController::class, 'destroy'])->name('sections.destroy');

Route::get('/syllabus', [SyllabusController::class, 'index'])->name('syllabus.index');
Route::get('/syllabus/create', [SyllabusController::class, 'create'])->name('syllabus.create');
Route::get('/syllabus/edit/{id}', [SyllabusController::class, 'edit'])->name('syllabus.edit');
Route::post('/syllabus/store', [SyllabusController::class, 'store'])->name('syllabus.store');
Route::put('/syllabus/update/{id}', [SyllabusController::class, 'update'])->name('syllabus.update');
Route::delete('/syllabus/destroy/{id}', [SyllabusController::class, 'destroy'])->name('syllabus.destroy');

Route::get('/subjects', [SubjectController::class, 'index'])->name('subjects.index');
Route::get('/subjects/create', [SubjectController::class, 'create'])->name('subjects.create');
Route::get('/subjects/edit/{id}', [SubjectController::class, 'edit'])->name('subjects.edit');
Route::post('/subjects/store', [SubjectController::class, 'store'])->name('subjects.store');
Route::put('/subjects/update/{id}', [SubjectController::class, 'update'])->name('subjects.update');
Route::delete('/subjects/destroy/{id}', [SubjectController::class, 'destroy'])->name('subjects.destroy');

Route::get('/study/materials', [StudyMaterialController::class, 'index'])->name('study.materials.index');
Route::get('/study/materials/create', [StudyMaterialController::class, 'create'])->name('study.materials.create');
Route::get('/study/materials/edit/{id}', [StudyMaterialController::class, 'edit'])->name('study.materials.edit');
Route::post('/study/materials/store', [StudyMaterialController::class, 'store'])->name('study.materials.store');
Route::put('/study/materials/update/{id}', [StudyMaterialController::class, 'update'])->name('study.materials.update');
Route::delete('/study/materials/destroy/{id}', [StudyMaterialController::class, 'destroy'])->name('study.materials.destroy');

