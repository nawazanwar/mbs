<?php


use App\Http\Controllers\Dashboard\DueFeeEmailController;
use App\Http\Controllers\Dashboard\DueFeeSMSController;
use App\Http\Controllers\Dashboard\Finance\DiscountController;
use App\Http\Controllers\Dashboard\Finance\ExpenditureController;
use App\Http\Controllers\Dashboard\Finance\ExpenditureHeadController;
use App\Http\Controllers\Dashboard\Finance\FeeCollectionController;
use App\Http\Controllers\Dashboard\Finance\FeeTypeController;
use App\Http\Controllers\Dashboard\Finance\IncomeController;
use App\Http\Controllers\Dashboard\Finance\IncomeHeadController;
use App\Http\Controllers\Dashboard\Finance\PayrollSalaryGradeController;
use App\Http\Controllers\Dashboard\Finance\PayrollSalaryHistoryController;
use App\Http\Controllers\Dashboard\Finance\PayrollSalaryPaymentController;
use App\Http\Controllers\Dashboard\InvoiceController;
use App\Http\Controllers\Dashboard\ReceiptController;
use Illuminate\Support\Facades\Route;

Route::get('/payroll/salary/grades', [PayrollSalaryGradeController::class, 'index'])->name('salary.grades.index');
Route::get('/payroll/salary/grades/create', [PayrollSalaryGradeController::class, 'create'])->name('salary.grades.create');
Route::get('/payroll/salary/grades/edit/{id}', [PayrollSalaryGradeController::class, 'edit'])->name('salary.grades.edit');
Route::post('/payroll/salary/grades/store', [PayrollSalaryGradeController::class, 'store'])->name('salary.grades.store');
Route::put('/payroll/salary/grades/update/{id}', [PayrollSalaryGradeController::class, 'update'])->name('salary.grades.update');
Route::delete('/payroll/salary/grades/{id}', [PayrollSalaryGradeController::class, 'destroy'])->name('salary.grades.destroy');

Route::get('/payroll/salary/payments', [PayrollSalaryPaymentController::class, 'index'])->name('salary.payments.index');
Route::get('/payroll/salary/payments/create', [PayrollSalaryPaymentController::class, 'create'])->name('salary.payments.create');
Route::get('/payroll/salary/payments/edit/{id}', [PayrollSalaryPaymentController::class, 'edit'])->name('salary.payments.edit');
Route::post('/payroll/salary/payments/store', [PayrollSalaryPaymentController::class, 'store'])->name('salary.payments.store');
Route::put('/payroll/salary/payments/update/{id}', [PayrollSalaryPaymentController::class, 'update'])->name('salary.payments.update');
Route::delete('/payroll/salary/payments/{id}', [PayrollSalaryPaymentController::class, 'destroy'])->name('salary.payments.destroy');

Route::get('/payroll/salary/histories', [PayrollSalaryHistoryController::class, 'index'])->name('salary.histories.index');
Route::get('/payroll/salary/histories/create', [PayrollSalaryHistoryController::class, 'create'])->name('salary.histories..create');
Route::get('/payroll/salary/histories/edit/{id}', [PayrollSalaryHistoryController::class, 'edit'])->name('salary.histories..salaries.edit');
Route::post('/payroll/salary/histories/store', [PayrollSalaryHistoryController::class, 'store'])->name('salary.histories.store');
Route::put('/payroll/salary/histories/update/{id}', [PayrollSalaryHistoryController::class, 'update'])->name('salary.histories.update');
Route::delete('/payroll/salary/histories/{id}', [PayrollSalaryHistoryController::class, 'destroy'])->name('salary.histories.destroy');


Route::get('/discounts', [DiscountController::class, 'index'])->name('discounts.index');
Route::get('/discounts/create', [DiscountController::class, 'create'])->name('discounts.create');
Route::get('/discounts/edit/{id}', [DiscountController::class, 'edit'])->name('discounts.edit');
Route::post('/discounts/store', [DiscountController::class, 'store'])->name('discounts.store');
Route::put('/discounts/update/{id}', [DiscountController::class, 'update'])->name('discounts.update');
Route::delete('/discounts/destroy/{id}', [DiscountController::class, 'destroy'])->name('discounts.destroy');

Route::get('/fee/types', [FeeTypeController::class, 'index'])->name('fee.types.index');
Route::get('/fee/types/create', [FeeTypeController::class, 'create'])->name('fee.types.create');
Route::get('/fee/types/edit/{id}', [FeeTypeController::class, 'edit'])->name('fee.types.edit');
Route::post('/fee/types/store', [FeeTypeController::class, 'store'])->name('fee.types.store');
Route::put('/fee/types/update/{id}', [FeeTypeController::class, 'update'])->name('fee.types.update');
Route::delete('/fee/types/destroy/{id}', [FeeTypeController::class, 'destroy'])->name('fee.types.destroy');

Route::get('/fee/collections', [FeeCollectionController::class, 'index'])->name('fee.collections.index');
Route::get('/fee/collections/create', [FeeCollectionController::class, 'create'])->name('fee.collections.create');
Route::get('/fee/collections/edit/{id}', [FeeCollectionController::class, 'edit'])->name('fee.collections.edit');
Route::post('/fee/collections/store', [FeeCollectionController::class, 'store'])->name('fee.collections.store');
Route::put('/fee/collections/update/{id}', [FeeCollectionController::class, 'update'])->name('fee.collections.update');
Route::delete('/fee/collections/destroy/{id}', [FeeCollectionController::class, 'destroy'])->name('fee.collections.destroy');

Route::get('/invoices', [InvoiceController::class, 'index'])->name('invoices.index');
Route::get('/invoices/create', [InvoiceController::class, 'create'])->name('invoices.create');
Route::get('/invoices/edit/{id}', [InvoiceController::class, 'edit'])->name('invoices.edit');
Route::post('/invoices/store', [InvoiceController::class, 'store'])->name('invoices.store');
Route::put('/invoices/update/{id}', [InvoiceController::class, 'update'])->name('invoices.update');
Route::delete('/invoices/destroy/{id}', [InvoiceController::class, 'destroy'])->name('invoices.destroy');

Route::get('/receipts', [ReceiptController::class, 'index'])->name('receipts.index');

Route::get('/dues/fee/emails', [DueFeeEmailController::class, 'index'])->name('dues.fee.emails.index');
Route::get('/dues/fee/emails/create', [DueFeeEmailController::class, 'create'])->name('dues.fee.emails.create');
Route::get('/dues/fee/emails/edit/{id}', [DueFeeEmailController::class, 'edit'])->name('dues.fee.emails.edit');
Route::post('/dues/fee/emails/store', [DueFeeEmailController::class, 'store'])->name('dues.fee.emails.store');
Route::put('/dues/fee/emails/update/{id}', [DueFeeEmailController::class, 'update'])->name('dues.fee.emails.update');
Route::delete('/dues/fee/emails/destroy/{id}', [DueFeeEmailController::class, 'destroy'])->name('dues.fee.emails.destroy');

Route::get('/dues/fee/sms', [DueFeeSMSController::class, 'index'])->name('dues.fee.sms.index');
Route::get('/dues/fee/sms/create', [DueFeeSMSController::class, 'create'])->name('dues.fee.sms.create');
Route::get('/dues/fee/sms/edit/{id}', [DueFeeSMSController::class, 'edit'])->name('dues.fee.sms.edit');
Route::post('/dues/fee/sms/store', [DueFeeSMSController::class, 'store'])->name('dues.fee.sms.store');
Route::put('/dues/fee/sms/update/{id}', [DueFeeSMSController::class, 'update'])->name('dues.fee.sms.update');
Route::delete('/dues/fee/sms/destroy/{id}', [DueFeeSMSController::class, 'destroy'])->name('dues.fee.sms.destroy');

Route::get('/income/heads', [IncomeHeadController::class, 'index'])->name('income.heads.index');
Route::get('/income/heads/create', [IncomeHeadController::class, 'create'])->name('income.heads.create');
Route::get('/income/heads/edit/{id}', [IncomeHeadController::class, 'edit'])->name('income.heads.edit');
Route::post('/income/heads/store', [IncomeHeadController::class, 'store'])->name('income.heads.store');
Route::put('/income/heads/update/{id}', [IncomeHeadController::class, 'update'])->name('income.heads.update');
Route::delete('/income/heads/destroy/{id}', [IncomeHeadController::class, 'destroy'])->name('income.heads.destroy');

Route::get('/expenditure/heads', [ExpenditureHeadController::class, 'index'])->name('expenditure.heads.index');
Route::get('/expenditure/heads/create', [ExpenditureHeadController::class, 'create'])->name('expenditure.heads.create');
Route::get('/expenditure/heads/edit/{id}', [ExpenditureHeadController::class, 'edit'])->name('expenditure.heads.edit');
Route::post('/expenditure/heads/store', [ExpenditureHeadController::class, 'store'])->name('expenditure.heads.store');
Route::put('/expenditure/heads/update/{id}', [ExpenditureHeadController::class, 'update'])->name('expenditure.heads.update');
Route::delete('/expenditure/heads/destroy/{id}', [ExpenditureHeadController::class, 'destroy'])->name('expenditure.heads.destroy');

Route::get('/incomes', [IncomeController::class, 'index'])->name('incomes.index');
Route::get('/incomes/create', [IncomeController::class, 'create'])->name('incomes.create');
Route::get('/incomes/edit/{id}', [IncomeController::class, 'edit'])->name('incomes.edit');
Route::post('/incomes/store', [IncomeController::class, 'store'])->name('incomes.store');
Route::put('/incomes/update/{id}', [IncomeController::class, 'update'])->name('incomes.update');
Route::delete('/incomes/destroy/{id}', [IncomeController::class, 'destroy'])->name('incomes.destroy');

Route::get('/expenditures', [ExpenditureController::class, 'index'])->name('expenditures.index');
Route::get('/expenditures/create', [ExpenditureController::class, 'create'])->name('expenditures.create');
Route::get('/expenditures/edit/{id}', [ExpenditureController::class, 'edit'])->name('expenditures.edit');
Route::post('/expenditures/store', [ExpenditureController::class, 'store'])->name('expenditures.store');
Route::put('/expenditures/update/{id}', [ExpenditureController::class, 'update'])->name('expenditures.update');
Route::delete('/expenditures/destroy/{id}', [ExpenditureController::class, 'destroy'])->name('expenditures.destroy');
