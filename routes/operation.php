<?php

use App\Http\Controllers\Dashboard\Operation\HostelController;
use App\Http\Controllers\Dashboard\Operation\HostelMemberController;
use App\Http\Controllers\Dashboard\Operation\HostelRoomController;
use App\Http\Controllers\Dashboard\Operation\LibraryBookController;
use App\Http\Controllers\Dashboard\Operation\LibraryBookIssueController;
use App\Http\Controllers\Dashboard\Operation\LibraryEbookController;
use App\Http\Controllers\Dashboard\Operation\LibraryMemberController;
use App\Http\Controllers\Dashboard\Operation\TransportMemberController;
use App\Http\Controllers\Dashboard\Operation\TransportRouteController;
use App\Http\Controllers\Dashboard\Operation\TransportVehicleController;
use Illuminate\Support\Facades\Route;

Route::get('/library/members', [LibraryMemberController::class, 'index'])->name('library.members.index');
Route::get('/library/members/create', [LibraryMemberController::class, 'create'])->name('library.members.create');
Route::get('/library/members/edit/{id}', [LibraryMemberController::class, 'edit'])->name('library.members.edit');
Route::post('/library/members/store', [LibraryMemberController::class, 'store'])->name('library.members.store');
Route::put('/library/members/update/{id}', [LibraryMemberController::class, 'update'])->name('library.members.update');
Route::delete('/library/members/destroy/{id}', [LibraryMemberController::class, 'destroy'])->name('library.members.destroy');

Route::get('/library/books', [LibraryBookController::class, 'index'])->name('library.books.index');
Route::get('/library/books/create', [LibraryBookController::class, 'create'])->name('library.books.create');
Route::get('/library/books/edit/{id}', [LibraryBookController::class, 'edit'])->name('library.books.edit');
Route::post('/library/books/store', [LibraryBookController::class, 'store'])->name('library.books.store');
Route::put('/library/books/update/{id}', [LibraryBookController::class, 'update'])->name('library.books.update');
Route::delete('/library/books/destroy/{id}', [LibraryBookController::class, 'destroy'])->name('library.books.destroy');

Route::get('/library/book/issues', [LibraryBookIssueController::class, 'index'])->name('library.book_issues.index');
Route::get('/library/books/issues/create', [LibraryBookIssueController::class, 'create'])->name('library.book_issues.create');
Route::get('/library/books/issues/edit/{id}', [LibraryBookIssueController::class, 'edit'])->name('library.book_issues.edit');
Route::post('/library/books/issues/store', [LibraryBookIssueController::class, 'store'])->name('library.book_issues.store');
Route::put('/library/books/issues/update/{id}', [LibraryBookIssueController::class, 'update'])->name('library.book_issues.update');
Route::delete('/library/books/issues/destroy/{id}', [LibraryBookIssueController::class, 'destroy'])->name('library.book_issues.destroy');

Route::get('/library/ebooks', [LibraryEbookController::class, 'index'])->name('library.ebooks.index');
Route::get('/library/ebooks/create', [LibraryEbookController::class, 'create'])->name('library.ebooks.create');
Route::get('/library/ebooks/edit/{id}', [LibraryEbookController::class, 'edit'])->name('library.ebooks.edit');
Route::post('/library/ebooks/store', [LibraryEbookController::class, 'store'])->name('library.ebooks.store');
Route::put('/library/ebooks/update/{id}', [LibraryEbookController::class, 'update'])->name('library.ebooks.update');
Route::delete('/library/ebooks/destroy/{id}', [LibraryEbookController::class, 'destroy'])->name('library.ebooks.destroy');

Route::get('/hostels', [HostelController::class, 'index'])->name('hostels.index');
Route::get('/hostels/create', [HostelController::class, 'create'])->name('hostels.create');
Route::get('/hostels/edit/{id}', [HostelController::class, 'edit'])->name('hostels.edit');
Route::post('/hostels/store', [HostelController::class, 'store'])->name('hostels.store');
Route::put('/hostels/update/{id}', [HostelController::class, 'update'])->name('hostels.update');
Route::delete('/hostels/destroy/{id}', [HostelController::class, 'destroy'])->name('hostels.destroy');

Route::get('/hostel/rooms', [HostelRoomController::class, 'index'])->name('hostel.rooms.index');
Route::get('/hostel/rooms/create', [HostelRoomController::class, 'create'])->name('hostel.rooms.create');
Route::get('/hostel/rooms/edit/{id}', [HostelRoomController::class, 'edit'])->name('hostel.rooms.edit');
Route::post('/hostel/rooms/store', [HostelRoomController::class, 'store'])->name('hostel.rooms.store');
Route::put('/hostel/rooms/update/{id}', [HostelRoomController::class, 'update'])->name('hostel.rooms.update');
Route::delete('/hostel/rooms/destroy/{id}', [HostelRoomController::class, 'destroy'])->name('hostel.rooms.destroy');

Route::get('/hostel/members', [HostelMemberController::class, 'index'])->name('hostel.members.index');
Route::get('/hostel/members/create', [HostelMemberController::class, 'create'])->name('hostel.members.create');
Route::get('/hostel/members/edit/{id}', [HostelMemberController::class, 'edit'])->name('hostel.members.edit');
Route::post('/hostel/members/store', [HostelMemberController::class, 'store'])->name('hostel.members.store');
Route::put('/hostel/members/update/{id}', [HostelMemberController::class, 'update'])->name('hostel.members.update');
Route::delete('/hostel/members/destroy/{id}', [HostelMemberController::class, 'destroy'])->name('hostel.members.destroy');

Route::get('/transport/vehicles', [TransportVehicleController::class, 'index'])->name('transport.vehicles.index');
Route::get('/transport/vehicles/create', [TransportVehicleController::class, 'create'])->name('transport.vehicles.create');
Route::get('/transport/vehicles/edit/{id}', [TransportVehicleController::class, 'edit'])->name('transport.vehicles.edit');
Route::post('/transport/vehicles/store', [TransportVehicleController::class, 'store'])->name('transport.vehicles.store');
Route::put('/transport/vehicles/update/{id}', [TransportVehicleController::class, 'update'])->name('transport.vehicles.update');
Route::delete('/transport/vehicles/destroy/{id}', [TransportVehicleController::class, 'destroy'])->name('transport.vehicles.destroy');

Route::get('/transport/routes', [TransportRouteController::class, 'index'])->name('transport.routes.index');
Route::get('/transport/routes/create', [TransportRouteController::class, 'create'])->name('transport.routes.create');
Route::get('/transport/routes/edit/{id}', [TransportRouteController::class, 'edit'])->name('transport.routes.edit');
Route::post('/transport/routes/store', [TransportRouteController::class, 'store'])->name('transport.routes.store');
Route::put('/transport/routes/update/{id}', [TransportRouteController::class, 'update'])->name('transport.routes.update');
Route::delete('/transport/routes/destroy/{id}', [TransportRouteController::class, 'destroy'])->name('transport.routes.destroy');

Route::get('/transport/members', [TransportMemberController::class, 'index'])->name('transport.members.index');
Route::get('/transport/members/create', [TransportMemberController::class, 'create'])->name('transport.members.create');
Route::get('/transport/members/edit/{id}', [TransportMemberController::class, 'edit'])->name('transport.members.edit');
Route::post('/transport/members/store', [TransportMemberController::class, 'store'])->name('transport.members.store');
Route::put('/transport/members/update/{id}', [TransportMemberController::class, 'update'])->name('transport.members.update');
Route::delete('/transport/members/destroy/{id}', [TransportMemberController::class, 'destroy'])->name('transport.members.destroy');
