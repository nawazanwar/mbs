<?php


use App\Http\Controllers\Dashboard\Configuration\LanguageController;
use App\Http\Controllers\Dashboard\Configuration\LeaveApplicationController;
use App\Http\Controllers\Dashboard\Configuration\LeaveTypeController;
use App\Http\Controllers\Dashboard\Configuration\SchoolController;
use App\Http\Controllers\Dashboard\Configuration\SettingEmailController;
use App\Http\Controllers\Dashboard\Configuration\SettingGeneralController;
use App\Http\Controllers\Dashboard\Configuration\SettingPaymentController;
use App\Http\Controllers\Dashboard\Configuration\SettingSmsController;
use App\Http\Controllers\Dashboard\Configuration\TemplateEmailController;
use App\Http\Controllers\Dashboard\Configuration\TemplateSMSController;
use App\Http\Controllers\Dashboard\Configuration\ThemeController;
use Illuminate\Support\Facades\Route;





Route::get('/themes', [ThemeController::class, 'index'])->name('themes.index');
Route::get('/themes/create', [ThemeController::class, 'create'])->name('themes.create');
Route::get('/themes/edit/{id}', [ThemeController::class, 'edit'])->name('themes.edit');
Route::post('/themes/store', [ThemeController::class, 'store'])->name('themes.store');
Route::put('/themes/update/{id}', [ThemeController::class, 'update'])->name('themes.update');
Route::delete('/themes/destroy/{id}', [ThemeController::class, 'destroy'])->name('themes.destroy');

Route::get('/languages', [LanguageController::class, 'index'])->name('languages.index');
Route::get('/languages/create', [LanguageController::class, 'create'])->name('languages.create');
Route::get('/languages/edit/{id}', [LanguageController::class, 'edit'])->name('languages.edit');
Route::post('/languages/store', [LanguageController::class, 'store'])->name('languages.store');
Route::put('/languages/update/{id}', [LanguageController::class, 'update'])->name('languages.update');
Route::delete('/languages/destroy/{id}', [LanguageController::class, 'destroy'])->name('languages.destroy');

Route::get('/settings/generals', [SettingGeneralController::class, 'index'])->name('settings.generals.index');
Route::get('/settings/generals/create', [SettingGeneralController::class, 'create'])->name('settings.generals.create');
Route::get('/settings/generals/edit/{id}', [SettingGeneralController::class, 'edit'])->name('settings.generals.edit');
Route::post('/settings/generals/store', [SettingGeneralController::class, 'store'])->name('settings.generals.store');
Route::put('/settings/generals/update/{id}', [SettingGeneralController::class, 'update'])->name('settings.generals.update');
Route::delete('/settings/generals/destroy/{id}', [SettingGeneralController::class, 'destroy'])->name('settings.generals.destroy');

Route::get('/settings/payments', [SettingPaymentController::class, 'index'])->name('settings.payments.index');
Route::get('/settings/payments/create', [SettingPaymentController::class, 'create'])->name('settings.payments.create');
Route::get('/settings/payments/edit/{id}', [SettingPaymentController::class, 'edit'])->name('settings.payments.edit');
Route::post('/settings/payments/store', [SettingPaymentController::class, 'store'])->name('settings.payments.store');
Route::put('/settings/payments/update/{id}', [SettingPaymentController::class, 'update'])->name('settings.payments.update');
Route::delete('/settings/payments/destroy/{id}', [SettingPaymentController::class, 'destroy'])->name('settings.payments.destroy');

Route::get('/settings/sms', [SettingSmsController::class, 'index'])->name('settings.sms.index');
Route::get('/settings/sms/create', [SettingSmsController::class, 'create'])->name('settings.sms.create');
Route::get('/settings/sms/edit/{id}', [SettingSmsController::class, 'edit'])->name('settings.sms.edit');
Route::post('/settings/sms/store', [SettingSmsController::class, 'store'])->name('settings.sms.store');
Route::put('/settings/sms/update/{id}', [SettingSmsController::class, 'update'])->name('settings.sms.update');
Route::delete('/settings/sms/destroy/{id}', [SettingSmsController::class, 'destroy'])->name('settings.sms.destroy');

Route::get('/settings/emails', [SettingEmailController::class, 'index'])->name('settings.emails.index');
Route::get('/settings/emails/create', [SettingEmailController::class, 'create'])->name('settings.emails.create');
Route::get('/settings/emails/edit/{id}', [SettingEmailController::class, 'edit'])->name('settings.emails.edit');
Route::post('/settings/emails/store', [SettingEmailController::class, 'store'])->name('settings.emails.store');
Route::put('/settings/emails/update/{id}', [SettingEmailController::class, 'update'])->name('settings.emails.update');
Route::delete('/settings/emails/destroy/{id}', [SettingEmailController::class, 'destroy'])->name('settings.emails.destroy');

Route::get('/schools', [SchoolController::class, 'index'])->name('schools.index');
Route::get('/schools/create', [SchoolController::class, 'create'])->name('schools.create');
Route::get('/schools/edit/{id}', [SchoolController::class, 'edit'])->name('schools.edit');
Route::post('/schools/store', [SchoolController::class, 'store'])->name('schools.store');
Route::put('/schools/update/{id}', [SchoolController::class, 'update'])->name('schools.update');
Route::delete('/schools/destroy/{id}', [SchoolController::class, 'destroy'])->name('schools.destroy');


Route::get('/template/sms', [TemplateSMSController::class, 'index'])->name('template.sms.index');
Route::get('/template/sms/create', [TemplateSMSController::class, 'create'])->name('template.sms.create');
Route::get('/template/sms/edit/{id}', [TemplateSMSController::class, 'edit'])->name('template.sms.edit');
Route::post('/template/sms/store', [TemplateSMSController::class, 'store'])->name('template.sms.store');
Route::get('/template/sms/update/{id}', [TemplateSMSController::class, 'update'])->name('template.sms.update');
Route::get('/template/sms/destroy/{id}', [TemplateSMSController::class, 'destroy'])->name('template.sms.destroy');

Route::get('/template/emails', [TemplateEmailController::class, 'index'])->name('template.emails.index');
Route::get('/template/emails/create', [TemplateEmailController::class, 'create'])->name('template.emails.create');
Route::get('/template/emails/edit/{id}', [TemplateEmailController::class, 'edit'])->name('template.emails.edit');
Route::post('/template/emails/store', [TemplateEmailController::class, 'store'])->name('template.emails.store');
Route::get('/template/emails/update/{id}', [TemplateEmailController::class, 'update'])->name('template.emails.update');
Route::get('/template/emails/destroy/{id}', [TemplateEmailController::class, 'destroy'])->name('template.emails.destroy');


Route::get('/leave/types', [LeaveTypeController::class, 'index'])->name('leave.types.index');
Route::get('/leave/types', [LeaveTypeController::class, 'index'])->name('leave.types.index');
Route::get('/leave/types/create', [LeaveTypeController::class, 'create'])->name('leave.types.create');
Route::get('/leave/types/edit/{id}', [LeaveTypeController::class, 'edit'])->name('leave.types.edit');
Route::post('/leave/types/store', [LeaveTypeController::class, 'store'])->name('leave.types.store');
Route::get('/leave/types/update/{id}', [LeaveTypeController::class, 'update'])->name('leave.types.update');
Route::get('/leave/types/destroy/{id}', [LeaveTypeController::class, 'destroy'])->name('leave.types.destroy');

Route::get('/leave/applications', [LeaveApplicationController::class, 'index'])->name('leave.applications.index');
Route::get('/leave/applications/create', [LeaveApplicationController::class, 'create'])->name('leave.applications.create');
Route::get('/leave/applications/edit/{id}', [LeaveApplicationController::class, 'edit'])->name('leave.applications.edit');
Route::post('/leave/applications/store', [LeaveApplicationController::class, 'store'])->name('leave.applications.store');
Route::get('/leave/applications/update/{id}', [LeaveApplicationController::class, 'update'])->name('leave.applications.update');
Route::get('/leave/applications/destroy/{id}', [LeaveApplicationController::class, 'destroy'])->name('leave.applications.destroy');