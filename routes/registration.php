<?php

use App\Http\Controllers\RegistrationController;
use Illuminate\Support\Facades\Route;

Route::get('/registration', [RegistrationController::class, 'index'])
    ->name('registration.index');
Route::get('/registration/create', [RegistrationController::class, 'create'])
    ->name('registration.create');
Route::get('/registration/edit/{id}', [RegistrationController::class, 'edit'])
    ->name('registration.edit');
Route::post('/registration/store', [RegistrationController::class, 'store'])
    ->name('registration.store');
Route::put('/registration/update/{id}', [RegistrationController::class, 'update'])
    ->name('registration.update');
Route::delete('/registration/destroy/{id}', [RegistrationController::class, 'destroy'])
    ->name('registration.destroy');