<?php


use App\Http\Controllers\Dashboard\Certificate\CertificateController;
use App\Http\Controllers\Dashboard\Certificate\CertificateTypeController;
use Illuminate\Support\Facades\Route;

Route::get('/certificates', [CertificateController::class, 'index'])->name('certificates.index');
Route::get('/certificates/create', [CertificateController::class, 'create'])->name('certificates.create');
Route::get('/certificates/edit/{id}', [CertificateController::class, 'edit'])->name('certificates.edit');
Route::post('/certificates/store', [CertificateController::class, 'store'])->name('certificates.store');
Route::put('/certificates/update/{id}', [CertificateController::class, 'update'])->name('certificates.update');
Route::delete('/certificates/destroy/{id}', [CertificateController::class, 'destroy'])->name('certificates.destroy');

Route::get('/certificate/types', [CertificateTypeController::class, 'index'])->name('certificate.types.index');
Route::get('/certificate/types/create', [CertificateTypeController::class, 'create'])->name('certificate.types.create');
Route::get('/certificate/types/edit/{id}', [CertificateTypeController::class, 'edit'])->name('certificate.types.edit');
Route::post('/certificate/types/store', [CertificateTypeController::class, 'store'])->name('certificate.types.store');
Route::put('/certificate/types/update/{id}', [CertificateTypeController::class, 'update'])->name('certificate.types.update');
Route::delete('/certificate/types/destroy/{id}', [CertificateTypeController::class, 'destroy'])->name('certificate.types.destroy');