<?php

use App\Http\Controllers\Dashboard\Student\StudentActivityController;
use App\Http\Controllers\Dashboard\Student\StudentController;
use App\Http\Controllers\Dashboard\Student\StudentTypeController;
use Illuminate\Support\Facades\Route;

Route::get('/students', [StudentController::class, 'index'])->name('students.index');
Route::get('/students/create', [StudentController::class, 'create'])->name('students.create');
Route::get('/students/edit/{id}', [StudentController::class, 'edit'])->name('students.edit');
Route::post('/students/store', [StudentController::class, 'store'])->name('students.store');
Route::put('/students/update/{id}', [StudentController::class, 'update'])->name('students.update');
Route::delete('/students/destroy/{id}', [StudentController::class, 'destroy'])->name('students.destroy');

Route::get('/student/activities', [StudentActivityController::class, 'index'])->name('student.activities.index');
Route::get('/student/activities/create', [StudentActivityController::class, 'create'])->name('student.activities.create');
Route::get('/student/activities/edit/{id}', [StudentActivityController::class, 'edit'])->name('student.activities.edit');
Route::post('/student/activities/store', [StudentActivityController::class, 'store'])->name('student.activities.store');
Route::put('/student/activities/update/{id}', [StudentActivityController::class, 'update'])->name('student.activities.update');
Route::delete('/student/activities/destroy/{id}', [StudentActivityController::class, 'destroy'])->name('student.activities.destroy');

Route::get('/student/types', [StudentTypeController::class, 'index'])->name('student.types.index');
Route::get('/student/types/create', [StudentTypeController::class, 'create'])->name('student.types.create');
Route::get('/student/types/edit/{id}', [StudentTypeController::class, 'edit'])->name('student.types.edit');
Route::post('/student/types/store', [StudentTypeController::class, 'store'])->name('student.types.store');
Route::put('/student/types/update/{id}', [StudentTypeController::class, 'update'])->name('student.types.update');
Route::delete('/student/types/destroy/{id}', [StudentTypeController::class, 'destroy'])->name('student.types.destroy');
