<?php


use Illuminate\Support\Facades\Route;

Route::resource('hrm', 'Dashboard\HRM\HrmController');
Route::resource('hrm-person-types', 'Dashboard\HRM\PersonTypeController');
Route::resource('hrm-tax-statuses', 'Dashboard\HRM\TaxStatusController');
Route::resource('hrm-job-statuses', 'Dashboard\HRM\JobStatusController');
Route::resource('hrm-tax-types', 'Dashboard\HRM\TaxTypeController');
Route::resource('hrm-casts', 'Dashboard\HRM\CastController');
Route::resource('hrm-departments', 'Dashboard\HRM\DepartmentController');
Route::resource('hrm-designations', 'Dashboard\HRM\DesignationController');
Route::resource('hrm-employers', 'Dashboard\HRM\EmployerController');
Route::resource('hrm-emp-types', 'Dashboard\HRM\EmployeeTypeController');
Route::resource('hrm-emp-sub-types', 'Dashboard\HRM\EmployeeSubTypeController');
Route::resource('hrm-business-types', 'Dashboard\HRM\BusinessTypeController');
Route::resource('hrm-business-sub-types', 'Dashboard\HRM\BusinessSubTypeController');
