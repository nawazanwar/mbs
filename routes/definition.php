<?php

use Illuminate\Support\Facades\Route;

Route::resource('titles', 'Dashboard\Definition\TitleController');
Route::resource('genders', 'Dashboard\Definition\GenderController');
Route::resource('religions', 'Dashboard\Definition\ReligionController');
Route::resource('blood-groups', 'Dashboard\Definition\BloodGroupController');
Route::resource('professions', 'Dashboard\Definition\ProfessionController');
Route::resource('specializations', 'Dashboard\Definition\SpecializationController');
Route::resource('qualifications', 'Dashboard\Definition\QualificationController');
Route::resource('territories', 'Dashboard\Definition\TerritoriesController');
Route::resource('relations', 'Dashboard\Definition\RelationController');
Route::resource('nationalities', 'Dashboard\Definition\NationalityController');
Route::resource('postfixes', 'Dashboard\Definition\PostFixController');
Route::resource('prefixes', 'Dashboard\Definition\PreFixController');

Route::resource('countries', 'Dashboard\Definition\CountryController');
Route::resource('states', 'Dashboard\Definition\StateController');
Route::resource('districts', 'Dashboard\Definition\DistrictController');
Route::resource('cities', 'Dashboard\Definition\CityController');
Route::resource('tehsils', 'Dashboard\Definition\TehsilController');
Route::resource('colonies', 'Dashboard\Definition\ColonyController');

Route::resource('ministries', 'Dashboard\Definition\MinistryController');
Route::resource('organization', 'Dashboard\Definition\OrganizationController');
Route::resource('ranks', 'Dashboard\Definition\RankController');
Route::resource('grades', 'Dashboard\Definition\GradeController');
Route::resource('wings', 'Dashboard\Definition\WingController');
