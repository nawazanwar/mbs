<?php

use App\Http\Controllers\Dashboard\FrontOffice\CallLogController;
use App\Http\Controllers\Dashboard\FrontOffice\PostalDispatchController;
use App\Http\Controllers\Dashboard\FrontOffice\PostalReceiveController;
use App\Http\Controllers\Dashboard\FrontOffice\VisitorController;
use App\Http\Controllers\Dashboard\FrontOffice\VisitorPurposeController;
use Illuminate\Support\Facades\Route;

Route::get('/visitors', [VisitorController::class, 'index'])->name('visitors.index');
Route::get('/visitors/create', [VisitorController::class, 'create'])->name('visitors.create');
Route::get('/visitors/edit/{id}', [VisitorController::class, 'edit'])->name('visitors.edit');
Route::post('/visitors/store', [VisitorController::class, 'store'])->name('visitors.store');
Route::put('/visitors/update/{id}', [VisitorController::class, 'update'])->name('visitors.update');
Route::delete('/visitors/destroy/{id}', [VisitorController::class, 'destroy'])->name('visitors.destroy');

Route::get('/visitor/purposes', [VisitorPurposeController::class, 'index'])->name('visitor.purposes.index');
Route::get('/visitor/purposes/create', [VisitorPurposeController::class, 'create'])->name('visitor.purposes.create');
Route::get('/visitor/purposes/edit/{id}', [VisitorPurposeController::class, 'edit'])->name('visitor.purposes.edit');
Route::post('/visitor/purposes/store', [VisitorPurposeController::class, 'store'])->name('visitor.purposes.store');
Route::put('/visitor/purposes/update/{id}', [VisitorPurposeController::class, 'update'])->name('visitor.purposes.update');
Route::delete('/visitor/purposes/destroy/{id}', [VisitorPurposeController::class, 'destroy'])->name('visitor.purposes.destroy');

Route::get('/call/logs', [CallLogController::class, 'index'])->name('call.logs.index');
Route::get('/call/logs/create', [CallLogController::class, 'create'])->name('call.logs.create');
Route::get('/call/logs/edit/{id}', [CallLogController::class, 'edit'])->name('call.logs.edit');
Route::post('/call/logs/store', [CallLogController::class, 'store'])->name('call.logs.store');
Route::put('/call/logs/update/{id}', [CallLogController::class, 'update'])->name('call.logs.update');
Route::delete('/call/logs/destroy/{id}', [CallLogController::class, 'destroy'])->name('call.logs.destroy');


Route::get('/postal/dispatches', [PostalDispatchController::class, 'index'])->name('postal.dispatches.index');
Route::get('/postal/dispatches/create', [PostalDispatchController::class, 'create'])->name('postal.dispatches.create');
Route::get('/postal/dispatches/edit/{id}', [PostalDispatchController::class, 'edit'])->name('postal.dispatches.edit');
Route::post('/postal/dispatches/store', [PostalDispatchController::class, 'store'])->name('postal.dispatches.store');
Route::put('/postal/dispatches/update/{id}', [PostalDispatchController::class, 'update'])->name('postal.dispatches.update');
Route::delete('/postal/dispatches/destroy/{id}', [PostalDispatchController::class, 'destroy'])->name('postal.dispatches.destroy');

Route::get('/postal/receives', [PostalReceiveController::class, 'index'])->name('postal.receives.index');
Route::get('/postal/receives/create', [PostalReceiveController::class, 'create'])->name('postal.receives.create');
Route::get('/postal/receives/edit/{id}', [PostalReceiveController::class, 'edit'])->name('postal.receives.edit');
Route::post('/postal/receives/store', [PostalReceiveController::class, 'store'])->name('postal.receives.store');
Route::put('/postal/receives/update/{id}', [PostalReceiveController::class, 'update'])->name('postal.receives.update');
Route::delete('/postal/receives/destroy/{id}', [PostalReceiveController::class, 'destroy'])->name('postal.receives.destroy');
