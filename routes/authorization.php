<?php

use App\Http\Controllers\Dashboard\Authorization\PermissionController;
use App\Http\Controllers\Dashboard\Authorization\RoleController;
use App\Http\Controllers\Dashboard\Authorization\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/roles/', [RoleController::class, 'index'])->name('roles.index');
Route::get('/roles/create', [RoleController::class, 'create'])->name('roles.create');
Route::get('/roles/edit/{id}', [RoleController::class, 'edit'])->name('roles.edit');
Route::post('/roles/store', [RoleController::class, 'store'])->name('roles.store');
Route::put('/roles/update/{id}', [RoleController::class, 'update'])->name('roles.update');
Route::delete('/roles/destroy/{id}', [RoleController::class, 'destroy'])->name('roles.destroy');

Route::get('/permissions/', [PermissionController::class, 'index'])->name('permissions.index');
Route::get('/permissions/create', [PermissionController::class, 'create'])->name('permissions.create');
Route::get('/permissions /edit/{id}', [PermissionController::class, 'edit'])->name('permissions.edit');
Route::post('/permissions/store', [PermissionController::class, 'store'])->name('permissions.store');
Route::put('/permissions/update/{id}', [PermissionController::class, 'update'])->name('permissions.update');
Route::delete('/permissions/destroy/{id}', [PermissionController::class, 'destroy'])->name('permissions.destroy');

Route::get('/users/', [UserController::class, 'index'])->name('users.index');
Route::get('/users/create', [UserController::class, 'create'])->name('users.create');
Route::get('/users /edit/{id}', [UserController::class, 'edit'])->name('users.edit');
Route::post('/users/store', [UserController::class, 'store'])->name('users.store');
Route::put('/users/update/{id}', [UserController::class, 'update'])->name('users.update');
Route::delete('/users/destroy/{id}', [UserController::class, 'destroy'])->name('users.destroy');