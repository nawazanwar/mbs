<?php

use App\Http\Controllers\Dashboard\Teacher\LectureController;
use App\Http\Controllers\Dashboard\Teacher\TeacherController;
use Illuminate\Support\Facades\Route;

Route::get('/teachers', [TeacherController::class, 'index'])->name('teachers.index');
Route::get('/teachers/create', [TeacherController::class, 'create'])->name('teachers.create');
Route::get('/teachers/edit/{id}', [TeacherController::class, 'edit'])->name('teachers.edit');
Route::post('/teachers/store', [TeacherController::class, 'store'])->name('teachers.store');
Route::put('/teachers/update/{id}', [TeacherController::class, 'update'])->name('teachers.update');
Route::delete('/teachers/destroy/{id}', [TeacherController::class, 'destroy'])->name('teachers.destroy');

Route::get('/lectures', [LectureController::class, 'index'])->name('lectures.index');
Route::get('/lectures/create', [LectureController::class, 'create'])->name('lectures.create');
Route::get('/lectures/edit/{id}', [LectureController::class, 'edit'])->name('lectures.edit');
Route::post('/lectures/store', [LectureController::class, 'store'])->name('lectures.store');
Route::put('/lectures/update/{id}', [LectureController::class, 'update'])->name('lectures.update');
Route::delete('/lectures/destroy/{id}', [LectureController::class, 'destroy'])->name('lectures.destroy');
