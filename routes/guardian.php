<?php

use App\Http\Controllers\Dashboard\GuardianController;
use Illuminate\Support\Facades\Route;

Route::get('/guardians', [GuardianController::class, 'index'])->name('guardians.index');
Route::get('/guardians/create', [GuardianController::class, 'create'])->name('guardians.create');
Route::get('/guardians/edit/{id}', [GuardianController::class, 'edit'])->name('guardians.edit');
Route::post('/guardians/store', [GuardianController::class, 'store'])->name('guardians.store');
Route::put('/guardians/update/{id}', [GuardianController::class, 'update'])->name('guardians.update');
Route::delete('/guardians/destroy/{id}', [GuardianController::class, 'destroy'])->name('guardians.destroy');

