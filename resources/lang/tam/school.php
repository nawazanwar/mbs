<?php
return [

    'a-'=>'A-',
    'a+'=>'A+',
    'ab-'=>'AB-',
    'ab+'=>'AB+',
    'about'=>'பற்றி',
    'about_school'=>'பள்ளி பற்றி',
    'absent_all'=>'அனைத்தும் இல்லை',
    'absent_email'=>'இல்லாத மின்னஞ்சல்',
    'absent_sms'=>'இல்லாத எஸ்எம்எஸ்',
    'ac'=>'ஏசி',
    'academic_information'=>'கல்வித் தகவல்',
    'academic_year'=>'கல்வி ஆண்டில்',
    'academics'=>'கல்வியாளர்கள்',
    'accounting'=>'கணக்கியல்',
    'accounting_balance'=>'கணக்கியல் இருப்பு',
    'action'=>'செயல்',
    'activity'=>'செயல்பாடு',
    'add'=>'கூட்டு',
    'add_class'=>'வகுப்பைச் சேர்க்கவும்',
    'address'=>'முகவரி',
    'address_information'=>'முகவரி தகவல்',
    'admin_logo'=>'நிர்வாக லோகோ',
    'administration'=>'நிர்வாகம்',
    'administrations'=>'நிர்வாகங்கள்',
    'administrator'=>'நிர்வாகி',
    'admission_date'=>'சேர்க்கை தேதி',
    'admission_no'=>'சேர்க்கை எண்',
    'admit_card_setting'=>'அட்மிட் கார்டு அமைப்பு',
    'admit_student'=>'மாணவர் சேர்க்கை',
    'admit_title_background'=>'தலைப்பு பின்னணியை ஒப்புக்கொள்ளுங்கள்',
    'admit_title_color'=>'தலைப்பு வண்ணத்தை ஒப்புக்கொள்',
    'admit_title_font_size'=>'தலைப்பு எழுத்துரு அளவை ஒப்புக்கொள்ளவும்',
    'alpha_net'=>'ஆல்பா நெட்',
    'amount'=>'தொகை',
    'announcement'=>'அறிவிப்பு',
    'api_key'=>'அபி கீ',
    'applicant'=>'விண்ணப்பதாரர்',
    'applicant_type'=>'விண்ணப்பதாரர் வகை',
    'approved_application'=>'"அங்கீகரிக்கப்பட்ட விண்ணப்பம்"',
    'assignment'=>'பணி',
    'assignment_submission'=>'பணி சமர்ப்பிப்பு',
    'attachment'=>'இணைப்பு',
    'attend_all'=>'அனைத்திலும் கலந்துகொள்',
    'attendance'=>'வருகை',
    'author'=>'நூலாசிரியர்',
    'avatar'=>'அவதாரம்',
    'average_of_all_exam'=>'அனைத்து தேர்வுகளின் சராசரி',
    'b-'=>'பி-',
    'b+'=>'பி+',
    'back'=>'மீண்டும்',
    'bank_name'=>'வங்கி பெயர்',
    'bank_receipt'=>'வங்கி பற்றுச்சீட்டு',
    'basic_information'=>'அடிப்படை தகவல்',
    'basic_salary'=>'அடிப்படை சம்பளம்',
    'bd_bulk'=>'BD மொத்தமாக',
    'beta_sms'=>'எஸ்எம்எஸ் அடிக்கவும்',
    'birth_date'=>'பிறந்த தேதி',
    'black'=>'கருப்பு',
    'blood_group'=>'இரத்த வகை',
    'body'=>'உடல்',
    'book_cover'=>'புத்தக உறை',
    'book_id'=>'புத்தக ஐடி',
    'border_color'=>'பார்டர் நிறம்',
    'bottom_background'=>'கீழ் பின்னணி',
    'bottom_signature'=>'கீழே கையெழுத்து',
    'boys'=>'சிறுவர்கள்',
    'bulk'=>'மொத்தமாக',
    'bulk_admission'=>'மொத்த சேர்க்கை',
    'bulk_pk'=>'மொத்த பிகே',
    'call_date'=>'அழைப்பு தேதி',
    'call_duration'=>'அழைப்பு காலம்',
    'call_log'=>'அழைப்பு பதிவு',
    'call_type'=>'அழைப்பு வகை',
    'cancel'=>'ரத்து செய்',
    'caption'=>'தலைப்பு',
    'card_logo'=>'அட்டை லோகோ',
    'card_school_name'=>'அட்டை பள்ளி பெயர்',
    'cash'=>'பணம்',
    'caste'=>'சாதி',
    'center'=>'மையம்',
    'certificate'=>'சான்றிதழ்',
    'certificate_type'=>'சான்றிதழ் வகை',
    'ch'=>'சைன்ஸ்',
    'char_set'=>'சார் செட்',
    'check_in'=>'செக் இன்',
    'check_out'=>'சரிபார்',
    'cheque'=>'காசோலை',
    'cheque_no'=>'சரிபார்ப்பு எண்',
    'cheque_number'=>'எண் சரிபார்க்கவும்',
    'class'=>'வர்க்கம்',
    'class_date'=>'வகுப்பு தேதி',
    'class_lecture'=>'வகுப்பு விரிவுரை',
    'class_routine'=>'வகுப்பு வழக்கம்',
    'class_teacher'=>'வகுப்பாசிரியர்',
    'click_tell'=>'சொல்லு என்பதைக் கிளிக் செய்யவும்',
    'code'=>'குறியீடு',
    'collaborations'=>'ஒத்துழைப்புகள்',
    'combine'=>'இணைக்கவும்',
    'complain'=>'புகார் செய்',
    'complain_by'=>'மூலம் புகார்',
    'complain_date'=>'புகார் தேதி',
    'complain_type'=>'புகார் வகை',
    'configurations'=>'கட்டமைப்புகள்',
    'cost_per_seat'=>'ஒரு இருக்கைக்கான செலவு',
    'cover_image'=>'அட்டைப் படம்',
    'create'=>'உருவாக்கு',
    'create_guardian'=>'கார்டியனை உருவாக்கவும்',
    'csv'=>'Csv',
    'currency'=>'நாணய',
    'currency_symbol'=>'நாணய சின்னம்',
    'daily_statement'=>'தினசரி அறிக்கை',
    'daily_transaction'=>'தினசரி பரிவர்த்தனை',
    'dark_orange'=>'அடர் ஆரஞ்சு',
    'dashboard'=>'டாஷ்போர்டு',
    'date'=>'தேதி',
    'deadline'=>'காலக்கெடுவை',
    'declined_application'=>'நிராகரிக்கப்பட்ட விண்ணப்பம்',
    'deep_pink'=>'அடர் இளஞ்சிவப்பு',
    'designation'=>'பதவி',
    'discount'=>'தள்ளுபடி',
    'discount_type'=>'தள்ளுபடி வகை',
    'dispatch_date'=>'அனுப்பிய நாள்',
    'display_order'=>'காட்சி வரிசை',
    'dob'=>'DOB',
    'dodger_blue'=>'டாட்ஜர் நீலம்',
    'driver'=>'இயக்கி',
    'due_amount'=>'நிலுவை தொகை',
    'due_date'=>'இறுதி தேதி',
    'due_fee'=>'செலுத்த வேண்டிய கட்டணம்',
    'due_fee_email'=>'செலுத்த வேண்டிய கட்டணம் மின்னஞ்சல்',
    'due_fee_sms'=>'செலுத்த வேண்டிய கட்டணம் எஸ்எம்எஸ்',
    'due_invoice'=>'செலுத்த வேண்டிய விலைப்பட்டியல்',
    'due_receipt'=>'உரிய ரசீது',
    'edition'=>'பதிப்பு',
    'email'=>'மின்னஞ்சல்',
    'email_protocol'=>'மின்னஞ்சல் நெறிமுறை',
    'email_setting'=>'மின்னஞ்சல் அமைப்பு',
    'email_settings'=>'மின்னஞ்சல் அமைப்புகள்',
    'email_template'=>'மின்னஞ்சல் டெம்ப்ளேட்',
    'email_type'=>'மின்னஞ்சல் வகை',
    'employee_attendance'=>'பணியாளர் வருகை',
    'employee_yearly_attendance'=>'ஊழியர்களின் ஆண்டு வருகை',
    'en'=>'ஆங்கிலம்',
    'enable_frontend'=>'Frontend ஐ இயக்கு',
    'enable_rtl'=>'Rtl ஐ இயக்கு',
    'end_time'=>'முடிவு நேரம்',
    'event'=>'நிகழ்வு',
    'event_for'=>'நிகழ்வுக்கான',
    'event_place'=>'நிகழ்வு இடம்',
    'exam'=>'"தேர்வு"',
    'exam_attendance'=>'தேர்வு வருகை',
    'exam_date'=>'தேர்வு தேதி',
    'exam_final_result'=>'தேர்வு இறுதி முடிவு',
    'exam_grade'=>'தேர்வு தரம்',
    'exam_mark'=>'தேர்வு மதிப்பெண்',
    'exam_result'=>'தேர்வு முடிவு',
    'exam_schedule'=>'தேர்வு அட்டவணை',
    'exam_suggestion'=>'தேர்வு பரிந்துரை',
    'exam_term'=>'தேர்வு காலம்',
    'exam_term_result'=>'தேர்வு கால முடிவு',
    'exam_title'=>'தேர்வு தலைப்பு',
    'exam_title_color'=>'தேர்வு தலைப்பு நிறம்',
    'exam_title_font_size'=>'தேர்வு தலைப்பு எழுத்துரு அளவு',
    'excel'=>'எக்செல்',
    'exit guardian'=>'கார்டியனில் இருந்து வெளியேறு',
    'expenditure'=>'செலவு',
    'expenditure_head'=>'செலவினத் தலைப்பு',
    'expenditure_method'=>'செலவு முறை',
    'expenditure_via'=>'செலவு மூலம்',
    'expense'=>'செலவு',
    'facebook'=>'முகநூல்',
    'facebook_url'=>'Facebook Url',
    'father'=>'தந்தை',
    'father_designation'=>'தந்தை பதவி',
    'father_education'=>'தந்தை கல்வி',
    'father_information'=>'தந்தை தகவல்',
    'father_name'=>'தந்தையின் பெயர்',
    'father_phone'=>'அப்பா போன்',
    'father_profession'=>'தந்தை தொழில்',
    'fax'=>'தொலைநகல்',
    'fee_amount'=>'கட்டணத் தொகை',
    'fee_collection'=>'கட்டண வசூல்',
    'fee_type'=>'கட்டண வகை',
    'female'=>'பெண்',
    'finance'=>'நிதி',
    'flat'=>'பிளாட்',
    'follow_up'=>'பின்தொடரவும்',
    'footer'=>'அடிக்குறிப்பு',
    'from_date'=>'தேதியிலிருந்து',
    'from_email'=>'மின்னஞ்சலில் இருந்து',
    'from_name'=>'பெயரிலிருந்து',
    'from_title'=>'தலைப்பிலிருந்து',
    'front_office'=>'முன் அலுவலகம்',
    'front_page'=>'முன்பக்கம்',
    'gallery'=>'கேலரி',
    'gallery_image'=>'கேலி படம்',
    'gender'=>'பாலினம்',
    'genera_setting'=>'பொது அமைப்பு',
    'general_fee'=>'பொது கட்டணம்',
    'generate_card'=>'அட்டையை உருவாக்கவும்',
    'generate_certificate'=>'சான்றிதழை உருவாக்கவும்',
    'girls'=>'பெண்கள்',
    'google_plus'=>'கூகுள் பிளஸ்',
    'google_plus_url'=>'Google Plus Url',
    'grade_name'=>'கிரேடு பெயர்',
    'grade_point'=>'கிரேடு பாயிண்ட்',
    'gross_amount'=>'மொத்த தொகை',
    'gross_salary'=>'மொத்த சம்பளம்',
    'group'=>'குழு',
    'guardian'=>'பாதுகாவலர்',
    'guardian_information'=>'கார்டியன் தகவல்',
    'head'=>'தலை',
    'head_type'=>'கட்டண வகை',
    'health_condition'=>'உடல் நிலை',
    'holidays'=>'விடுமுறை',
    'home'=>'வீடு',
    'hostel'=>'தங்கும் விடுதி',
    'hostel_type'=>'விடுதி வகை',
    'hourly_rate'=>'நேர விகிதம்',
    'house_rent'=>'வீட்டு வாடகை',
    'hrm'=>'HRM',
    'human_resource'=>'மனித வளம்',
    'id_card_setting'=>'அடையாள அட்டை அமைப்பு',
    'id_no_background'=>'ஐடி பின்னணி இல்லை',
    'id_no_color'=>'ஐடி நிறம் இல்லை',
    'id_no_font_size'=>'ஐடி எழுத்துரு அளவு இல்லை',
    'image'=>'படம்',
    'income'=>'வருமானம்',
    'income_head'=>'வருமானத் தலைவர்',
    'income_via'=>'மூலம் வருமானம்',
    'incoming'=>'வருகை',
    'instagram'=>'Instagram',
    'instagram_url'=>'Instagram Url',
    'invoice'=>'விலைப்பட்டியல்',
    'invoice_number'=>'விலைப்பட்டியல் எண்',
    'is_applicable_discount'=>'பொருந்தக்கூடிய தள்ளுபடி',
    'is_guardian'=>'கார்டியன் ஆகும்',
    'is_main_branch'=>'பிரதான கிளை ஆகும்',
    'is_running'=>'ஓடிக்கொண்டிருக்கிறது',
    'is_view_on_web'=>'இணையத்தில் பார்க்கிறது',
    'isbn_no'=>'ISBN எண்',
    'issue_date'=>'வெளியீட்டு தேதி',
    'jazzberry_jam'=>'ஜாஸ்பெர்ரி ஜாம்',
    'joining_date'=>'சேரும் தேதி',
    'label'=>'லேபிள்',
    'language'=>'மொழி',
    'late_all'=>'லேட் ஆல்',
    'latitude'=>'அட்சரேகை',
    'leave_application'=>'விடுமுறை விண்ணப்பம்',
    'leave_type'=>'வகையை விடுங்கள்',
    'lecture'=>'சொற்பொழிவு',
    'left'=>'விட்டு',
    'library'=>'நூலகம்',
    'library_book'=>'நூலகப் புத்தகம்',
    'library_book_issue'=>'நூலகப் புத்தக வெளியீடு',
    'library_ebook'=>'நூலக மின்புத்தகம்',
    'library_id'=>'நூலக ஐடி',
    'library_member'=>'நூலக உறுப்பினர்',
    'light_sea_green'=>'வெளிர் கடல் பச்சை',
    'lime_green'=>'வரி பச்சை',
    'linkedin'=>'லிங்க்டின்',
    'linkedin_url'=>'Linkedin Url',
    'list'=>'பட்டியல்',
    'list_all_schools'=>'அனைத்து பள்ளிகளின் பட்டியல்',
    'live_class'=>'நேரடி வகுப்பு',
    'location'=>'இடம்',
    'logo'=>'சின்னம்',
    'logout'=>'வெளியேறு',
    'longitude'=>'தீர்க்கரேகை',
    'mal'=>'மலாய்',
    'male'=>'ஆண்',
    'manage_academic_years'=>'கல்வி ஆண்டுகளை நிர்வகிக்கவும்',
    'manage_admit_card_setting'=>'அட்மிட் கார்டு அமைப்பை நிர்வகிக்கவும்',
    'manage_assignment'=>'பணியை நிர்வகிக்கவும்',
    'manage_book'=>'புத்தகத்தை நிர்வகிக்கவும்',
    'manage_bulk_admissions'=>'மொத்த சேர்க்கைகளை நிர்வகிக்கவும்',
    'manage_call_log'=>'அழைப்பு பதிவை நிர்வகிக்கவும்',
    'manage_call_logs'=>'அழைப்பு பதிவுகளை நிர்வகிக்கவும்',
    'manage_certificate'=>'சான்றிதழை நிர்வகிக்கவும்',
    'manage_certificate_type'=>'சான்றிதழ் வகையை நிர்வகிக்கவும்',
    'manage_class_routine'=>'வகுப்பு வழக்கத்தை நிர்வகிக்கவும்',
    'manage_classes'=>'வகுப்புகளை நிர்வகிக்கவும்',
    'manage_complain'=>'புகாரை நிர்வகி',
    'manage_complain_type'=>'புகார் வகையை நிர்வகிக்கவும்',
    'manage_dashboard'=>'டாஷ்போர்டை நிர்வகி',
    'manage_designation'=>'பதவியை நிர்வகிக்கவும்',
    'manage_discount'=>'தள்ளுபடியை நிர்வகிக்கவும்',
    'manage_discounts'=>'தள்ளுபடிகள்',
    'manage_due_fee_email'=>'செலுத்த வேண்டிய கட்டண மின்னஞ்சலை நிர்வகிக்கவும்',
    'manage_due_fee_sms'=>'செலுத்த வேண்டிய கட்டண SMS ஐ நிர்வகிக்கவும்',
    'manage_due_invoice'=>'உரிய விலைப்பட்டியலை நிர்வகிக்கவும்',
    'manage_ebook'=>'மின் புத்தகத்தை நிர்வகிக்கவும்',
    'manage_email'=>'மின்னஞ்சலை நிர்வகிக்கவும்',
    'manage_email_setting'=>'மின்னஞ்சல் அமைப்பை நிர்வகிக்கவும்',
    'manage_email_template'=>'மின்னஞ்சல் டெம்ப்ளேட்டை நிர்வகிக்கவும்',
    'manage_employee'=>'பணியாளரை நிர்வகிக்கவும்',
    'manage_event'=>'நிகழ்வை நிர்வகி',
    'manage_events'=>'நிகழ்வுகளை நிர்வகிக்கவும்',
    'manage_exam_attendances'=>'தேர்வு வருகைகளை நிர்வகிக்கவும்',
    'manage_exam_grades'=>'தேர்வு தரங்களை நிர்வகிக்கவும்',
    'manage_exam_marks'=>'தேர்வு மதிப்பெண்களை நிர்வகிக்கவும்',
    'manage_exam_schedules'=>'தேர்வு அட்டவணைகளை நிர்வகிக்கவும்',
    'manage_exam_suggestions'=>'தேர்வு பரிந்துரைகளை நிர்வகிக்கவும்',
    'manage_exam_terms'=>'தேர்வு விதிமுறைகளை நிர்வகிக்கவும்',
    'manage_expenditure'=>'செலவினங்களை நிர்வகிக்கவும்',
    'manage_expenditure_head'=>'செலவினத் தலைப்பை நிர்வகிக்கவும்',
    'manage_expenditure_heads'=>'செலவுத் தலைப்புகளை நிர்வகிக்கவும்',
    'manage_fee_collection'=>'கட்டண சேகரிப்பை நிர்வகிக்கவும்',
    'manage_fee_collections'=>'கட்டண சேகரிப்புகளை நிர்வகிக்கவும்',
    'manage_fee_type'=>'கட்டண வகையை நிர்வகிக்கவும்',
    'manage_fee_types'=>'கட்டண வகைகளை நிர்வகிக்கவும்',
    'manage_front_end'=>'முன் முனையை நிர்வகி',
    'manage_frontend_page'=>'முன் பக்கத்தை நிர்வகி',
    'manage_frontend_pages'=>'முன் பக்கங்களை நிர்வகிக்கவும்',
    'manage_gallery'=>'கேலரியை நிர்வகி',
    'manage_gallery_image'=>'கேலரி படத்தை நிர்வகி',
    'manage_general_setting'=>'பொது அமைப்பை நிர்வகிக்கவும்',
    'manage_guardians'=>'பாதுகாவலர்களை நிர்வகிக்கவும்',
    'manage_holidays'=>'விடுமுறை நாட்களை நிர்வகிக்கவும்',
    'manage_hostel'=>'விடுதியை நிர்வகிக்கவும்',
    'manage_id_card_setting'=>'அடையாள அட்டை அமைப்பை நிர்வகிக்கவும்',
    'manage_income'=>'வருமானத்தை நிர்வகிக்கவும்',
    'manage_income_head'=>'வருமானத் தலைவரை நிர்வகிக்கவும்',
    'manage_income_heads'=>'வருமானத் தலைவர்களை நிர்வகிக்கவும்',
    'manage_incomes'=>'"வருமானங்களை நிர்வகி"',
    'manage_invoice'=>'விலைப்பட்டியல் நிர்வகிக்கவும்',
    'manage_invoices'=>'"இன்வாய்ஸ்களை நிர்வகி"',
    'manage_languages'=>'மொழிகளை நிர்வகி',
    'manage_leave'=>'விடுப்பை நிர்வகிக்கவும்',
    'manage_leave_application'=>'விடுப்பு விண்ணப்பத்தை நிர்வகிக்கவும்',
    'manage_leave_type'=>'விடுப்பு வகையை நிர்வகிக்கவும்',
    'manage_leaves'=>'"இலைகளை நிர்வகி"',
    'manage_lectures'=>'விரிவுரைகளை நிர்வகிக்கவும்',
    'manage_library_book'=>'நூலக புத்தகத்தை நிர்வகிக்கவும்',
    'manage_library_issue'=>'நூலகச் சிக்கலை நிர்வகி',
    'manage_library_member'=>'நூலக உறுப்பினரை நிர்வகி',
    'manage_live_classes'=>'நேரலை வகுப்புகளை நிர்வகிக்கவும்',
    'manage_mark'=>'குறியை நிர்வகிக்கவும்',
    'manage_member'=>'உறுப்பினரை நிர்வகி',
    'manage_message'=>'செய்தியை நிர்வகிக்கவும்',
    'manage_messages'=>'செய்திகளை நிர்வகிக்கவும்',
    'manage_news'=>'மாங்கே செய்திகள்',
    'manage_notice'=>'மாங்கே அறிவிப்பு',
    'manage_online_admission'=>'ஆன்லைன் சேர்க்கையை நிர்வகிக்கவும்',
    'manage_paid_receipt'=>'பணம் செலுத்திய ரசீதை நிர்வகிக்கவும்',
    'manage_payment_setting'=>'கட்டண அமைப்பை நிர்வகிக்கவும்',
    'manage_permissions'=>'மாங்கே அனுமதிகள்',
    'manage_postal_dispatches'=>'அஞ்சல் அனுப்புதல்களை நிர்வகிக்கவும்',
    'manage_postal_receives'=>'அஞ்சல் வரவுகளை நிர்வகிக்கவும்',
    'manage_receipts'=>'"ரசீதுகளை நிர்வகி"',
    'manage_roles'=>'பாத்திரங்களை நிர்வகிக்கவும்',
    'manage_room'=>'அறையை நிர்வகிக்கவும்',
    'manage_salary_grades'=>'சம்பள தரங்களை நிர்வகிக்கவும்',
    'manage_salary_history'=>'சம்பள வரலாற்றை நிர்வகிக்கவும்',
    'manage_salary_payments'=>'சம்பள கொடுப்பனவுகளை நிர்வகிக்கவும்',
    'manage_school'=>'பள்ளியை நிர்வகிக்கவும்',
    'manage_sections'=>'பிரிவுகளை நிர்வகிக்கவும்',
    'manage_slider'=>'ஸ்லைடரை நிர்வகி',
    'manage_sms'=>'எஸ்எம்எஸ் நிர்வகிக்கவும்',
    'manage_sms_setting'=>'SMS அமைப்பை நிர்வகிக்கவும்',
    'manage_sms_template'=>'எஸ்எம்எஸ் டெம்ப்ளேட்டை நிர்வகிக்கவும்',
    'manage_student'=>'மாணவரை நிர்வகி',
    'manage_student_activities'=>'மாணவர் செயல்பாடுகளை நிர்வகிக்கவும்',
    'manage_student_types'=>'மாணவர் வகைகளை நிர்வகிக்கவும்',
    'manage_study_materials'=>'ஆய்வுப் பொருட்களை நிர்வகிக்கவும்',
    'manage_subjects'=>'பாடங்களை நிர்வகிக்கவும்',
    'manage_submission'=>'சமர்ப்பிப்பை நிர்வகிக்கவும்',
    'manage_syllabus'=>'பாடத்திட்டத்தை நிர்வகிக்கவும்',
    'manage_teachers'=>'ஆசிரியர்களை நிர்வகிக்கவும்',
    'manage_themes'=>'தீம்களை நிர்வகிக்கவும்',
    'manage_transport_member'=>'போக்குவரத்து உறுப்பினரை நிர்வகி',
    'manage_transport_route'=>'போக்குவரத்து வழியை நிர்வகிக்கவும்',
    'manage_user'=>'பயனரை நிர்வகி',
    'manage_vehicle'=>'வாகனத்தை நிர்வகிக்கவும்',
    'manage_visitor_purpose'=>'பார்வையாளர் நோக்கத்தை நிர்வகிக்கவும்',
    'manage_visitors'=>'பார்வையாளர்களை நிர்வகிக்கவும்',
    'mark_from'=>'மார்க் படிவம்',
    'mark_send_by_email'=>'மின்னஞ்சல் மூலம் அனுப்புவதைக் குறிக்கவும்',
    'mark_send_by_sms'=>'SMS மூலம் அனுப்புவதைக் குறிக்கவும்',
    'mark_sheet'=>'மதிப்பீட்டு தாள்',
    'mark_to'=>'மார்க் டு',
    'maroon'=>'மெரூன்',
    'media_gallery'=>'மீடியா கேலரி',
    'media_information'=>'ஊடக தகவல்',
    'medical_allowance'=>'மருத்துவ கொடுப்பனவு',
    'medium_purple'=>'நடுத்தர ஊதா',
    'merit_list'=>'தகுதி பட்டியல்',
    'message'=>'செய்தி',
    'min_sms'=>'குறைந்தபட்ச எஸ்எம்எஸ்',
    'month'=>'மாதம்',
    'mother'=>'அம்மா',
    'mother_designation'=>'அம்மா பதவி',
    'mother_education'=>'தாய் கல்வி',
    'mother_name'=>'தாய் பெயர்',
    'mother_phone'=>'அம்மா போன்',
    'mother_profession'=>'தாய் தொழில்',
    'msg_91'=>'MSG 91',
    'my_profile'=>'என் சுயவிவரம்',
    'name'=>'பெயர்',
    'national_id'=>'தேசிய ஐடி',
    'navy_blue'=>'கடற்படை நீலம்',
    'net_amount'=>'நிகர தொகை',
    'net_salary'=>'நிகர சம்பளம்',
    'news'=>'செய்தி',
    'no'=>'இல்லை',
    'no_record_found'=>'பதிவு எதுவும் கிடைக்கவில்லை',
    'non_ac'=>'ஏசி இல்லாதது',
    'note'=>'குறிப்பு',
    'notice'=>'கவனிக்கவும்',
    'notice_for'=>'இதற்கான அறிவிப்பு',
    'numeric_name'=>'எண் பெயர்',
    'o-'=>'ஓ-',
    'o+'=>'O+',
    'online_admission'=>'ஆன்லைன் சேர்க்கை',
    'only_based_on_final_exam'=>'இறுதித் தேர்வின் அடிப்படையில் மட்டுமே',
    'operations'=>'செயல்பாடுகள்',
    'other'=>'மற்றவை',
    'other_info'=>'மற்ற தகவல்',
    'other_information'=>'பிற தகவல்',
    'otherinfo'=>'மற்ற தகவல்',
    'outgoing'=>'வெளிச்செல்லும்',
    'over_time_hourly_rate'=>'ஓவர் டைம் ஹவர்லி ரேட்',
    'paid'=>'செலுத்தப்பட்டது',
    'paid_receipt'=>'பணம் செலுத்திய ரசீது',
    'paid_status'=>'கட்டண நிலை',
    'parent_school'=>'பெற்றோர் பள்ளி',
    'password'=>'கடவுச்சொல்',
    'pay_stack'=>'பே ஸ்டேக்',
    'pay_tm'=>'PayTM',
    'pay_u_money'=>'PayUMoney',
    'payment_method'=>'பணம் செலுத்தும் முறை',
    'payment_setting'=>'கட்டண அமைப்பு',
    'payment_settings'=>'கட்டண அமைப்புகள்',
    'payments'=>'கொடுப்பனவுகள்',
    'paypal'=>'பேபால்',
    'payroll'=>'ஊதியம்',
    'pdf'=>'Pdf',
    'percentage'=>'சதவிதம்',
    'permanent_address'=>'நிரந்தர முகவரி',
    'ph_select_assignment'=>'பணியைத் தேர்ந்தெடுக்கவும்',
    'ph_select_class'=>'வகுப்பைத் தேர்ந்தெடுக்கவும்',
    'ph_select_discount'=>'தள்ளுபடி என்பதைத் தேர்ந்தெடுக்கவும்',
    'ph_select_discount_type'=>'கட்டண வகையைத் தேர்ந்தெடுக்கவும்',
    'ph_select_expenditure_head'=>'செலவினத் தலைப்பைத் தேர்ந்தெடுக்கவும்',
    'ph_select_expenditure_method'=>'செலவு முறையைத் தேர்ந்தெடுக்கவும்',
    'ph_select_hostel'=>'விடுதியைத் தேர்ந்தெடுக்கவும்',
    'ph_select_hostel_type'=>'விடுதி வகையைத் தேர்ந்தெடுக்கவும்',
    'ph_select_income_head'=>'வருமானத் தலைவர்',
    'ph_select_income_method'=>'வருமான முறை',
    'ph_select_paid_status'=>'Ph கட்டண நிலையைத் தேர்ந்தெடுக்கவும்',
    'ph_select_payment_method'=>'Ph கட்டணம் செலுத்தும் முறையைத் தேர்ந்தெடுக்கவும்',
    'ph_select_role'=>'Ph பாத்திரத்தைத் தேர்ந்தெடுக்கவும்',
    'ph_select_section'=>'பிரிவைத் தேர்ந்தெடுக்கவும்',
    'ph_select_student'=>'மாணவரைத் தேர்ந்தெடுக்கவும்',
    'ph_select_template'=>'Ph டெம்ப்ளேட்டைத் தேர்ந்தெடுக்கவும்',
    'ph_select_user'=>'பயனரைத் தேர்ந்தெடுக்கவும்',
    'ph_select_visitor_purpose'=>'பார்வையாளர் நோக்கத்தைத் தேர்ந்தெடுக்கவும்',
    'ph_subject'=>'பாடத்தைத் தேர்ந்தெடுக்கவும்',
    'phone'=>'தொலைபேசி',
    'photo'=>'புகைப்படம்',
    'pinterest'=>'Pinterest',
    'pinterest_url'=>'Pinterest Url',
    'plivo'=>'பிலிவோ',
    'postal_dispatch'=>'அஞ்சல் அனுப்புதல்',
    'postal_receive'=>'அஞ்சல் பெறுதல்',
    'present_address'=>'தற்போதைய முகவரியில்',
    'present_all'=>'அனைத்தையும் வழங்கு',
    'previous_school'=>'முந்தைய பள்ளி',
    'price'=>'விலை',
    'profession'=>'தொழில்',
    'profile'=>'சுயவிவரம்',
    'promotion'=>'பதவி உயர்வு',
    'provident_fund'=>'வருங்கால வைப்பு நிதி',
    'quantity'=>'அளவு',
    'radical_red'=>'தீவிர சிவப்பு',
    'rebecca_purple'=>'ரெபேக்கா ஊதா',
    'receipt'=>'ரசீது',
    'receive_date'=>'பெறும் தேதி',
    'receive_type'=>'பெறுதல் வகை',
    'receiver_type'=>'ரிசீவர் வகை',
    'reciept'=>'ரசீது',
    'red'=>'சிவப்பு',
    'reference'=>'குறிப்பு',
    'reg_date'=>'பதிவு தேதி',
    'register_new_school'=>'புதிய பள்ளியை பதிவு செய்யவும்',
    'registration_no'=>'பதிவு எண்',
    'relation_with_guardian'=>'கார்டியனுடனான உறவு',
    'religion'=>'மதம்',
    'report'=>'அறிக்கை',
    'reset_password'=>'கடவுச்சொல்லை மீட்டமைக்க',
    'responsibility'=>'பொறுப்பு',
    'result_card'=>'முடிவு அட்டை',
    'result_send_by_email'=>'முடிவு மின்னஞ்சல் மூலம் அனுப்பப்படும்',
    'result_send_by_sms'=>'எஸ்எம்எஸ் மூலம் முடிவு அனுப்பப்படும்',
    'resume'=>'தற்குறிப்பு',
    'return_date'=>'திரும்பும் தேதி',
    'right'=>'சரி',
    'role'=>'பங்கு',
    'role_permission'=>'பங்கு அனுமதி',
    'roll_no'=>'ரோல் எண்',
    'room_no'=>'அறை எண்',
    'room_type'=>'அறையின் வகை',
    'route_end'=>'பாதை முடிவு',
    'route_name'=>'பாதை பெயர்',
    'route_start'=>'பாதை தொடக்கம்',
    'salary_grade'=>'சம்பள தரம்',
    'salary_history'=>'சம்பள வரலாறு',
    'salary_payment'=>'சம்பளம் செலுத்துதல்',
    'salary_type'=>'சம்பள வகை',
    'save'=>'சேமிக்கவும்',
    'school'=>'பள்ளி',
    'school_about'=>'பள்ளி பற்றி',
    'school_address'=>'பள்ளி முகவரி',
    'school_address_color'=>'பள்ளி முகவரி நிறம்',
    'school_date'=>'பள்ளி தேதி',
    'school_latitude'=>'அட்சரேகை',
    'school_name'=>'பள்ளி பெயர்',
    'school_name_color'=>'பள்ளி பெயர் நிறம்',
    'school_name_font_size'=>'பள்ளி பெயர் FontSize',
    'school_settings'=>'பள்ளி அமைப்புகள்',
    'search_here'=>'இங்கே தேடவும்',
    'seat_total'=>'மொத்த இருக்கை',
    'second_language'=>'இரண்டாம் மொழி',
    'section'=>'பிரிவு',
    'select_guardian_type'=>'கார்டியன் வகையைத் தேர்ந்தெடுக்கவும்',
    'send_date'=>'அனுப்பும் தேதி',
    'sender'=>'அனுப்புபவர்',
    'session_end'=>'அமர்வு முடிவு',
    'session_start'=>'அமர்வு ஆரம்பம்',
    'session_year'=>'அமர்வு ஆண்டு',
    'setting_information'=>'தகவல் அமைத்தல்',
    'settings'=>'அமைத்தல்',
    'signature_align'=>'கையொப்பம் சீரமைத்தல்',
    'signature_background'=>'கையொப்ப பின்னணி',
    'signature_color'=>'கையொப்ப நிறம்',
    'slate_gray'=>'மாநில சாம்பல்',
    'sms'=>'எஸ்எம்எஸ்',
    'sms_cluster'=>'எஸ்எம்எஸ் கிளஸ்டர்',
    'sms_country'=>'எஸ்எம்எஸ் நாடு',
    'sms_setting'=>'எஸ்எம்எஸ் அமைப்பு',
    'sms_settings'=>'SMS அமைப்புகள்',
    'sms_template'=>'எஸ்எம்எஸ் டெம்ப்ளேட்',
    'social_information'=>'சமூக தகவல்',
    'start_date'=>'தொடக்க தேதி',
    'start_time'=>'வகுப்பு நேரம்',
    'status'=>'நிலை',
    'stop_fare'=>'நிறுத்த கட்டணம்',
    'stop_km'=>'கிமீ நிறுத்து',
    'stop_name'=>'நிறுத்து பெயர்',
    'student'=>'மாணவர்',
    'student_activity'=>'மாணவர் செயல்பாடு',
    'student_admit_card'=>'மாணவர் சேர்க்கை அட்டை',
    'student_attendance'=>'மாணவர் வருகை',
    'student_id_card'=>'மாணவர் அடையாள அட்டை',
    'student_invoice'=>'மாணவர் விலைப்பட்டியல்',
    'student_list'=>'மாணவர் பட்டியல்',
    'student_type'=>'மாணவர் வகை',
    'student_yearly_attendance'=>'மாணவர்களின் ஆண்டு வருகை',
    'study_material'=>'ஆய்வுப் பொருள்',
    'subject'=>'பொருள்',
    'subject_code'=>'பொருள் குறியீடு',
    'subject_color'=>'பொருள் நிறம்',
    'subject_font_size'=>'பொருள் எழுத்துரு அளவு',
    'submission'=>'சமர்ப்பணம்',
    'submitted_at'=>'சமர்ப்பிக்கப்பட்டது',
    'submitted_by'=>'மூலம் சமர்ப்பிக்கப்பட்டது',
    'syllabus'=>'பாடத்திட்டங்கள்',
    'tam'=>'தமிழ்',
    'teacher'=>'ஆசிரியர்',
    'teacher_attendance'=>'ஆசிரியர் வருகை',
    'teacher_id_card'=>'ஆசிரியர் அடையாள அட்டை',
    'teacher_yearly_attendance'=>'ஆசிரியர் ஆண்டு வருகை',
    'template'=>'டெம்ப்ளேட்',
    'templates'=>'வார்ப்புருக்கள்',
    'temprary_address'=>'தற்காலிக முகவரி',
    'text'=>'செய்தி',
    'text_local'=>'உள்ளூர் உரை',
    'theme'=>'தீம்',
    'time'=>'நேரம்',
    'title'=>'தலைப்பு',
    'title_color'=>'தலைப்பு நிறம்',

];