@include('components.fields.class-list')

@include('components.fields.section-list')

@include('components.fields.section-list')

<div class="row mb-2">
    {{ Form::label('day', __('school.day'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ Form::select('day', ['M' => 'Monday', 'Tu' => 'Tuesday', 'wed' => 'Wednesday', 'Thur' => 'Thursday', 'Fr' => 'Friday', 'sat' => 'Saturday', 'sun' => 'Sunday'], null, ['class' => 'form-control col-md-12 col-xs-12', 'placeholder' => 'Select']) }}
    </div>
</div>

@include('components.fields.teacher-list')

<div class="row mb-2">
    {{ Form::label('start_time', __('school.start_time'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::time('start_time', null, ['start_time', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Start Time']) !!}
    </div>
</div>

<div class="row mb-2">
    {{ Form::label('end_time', __('school.end_time'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

    <div class="col-md-6 col-sm-6 col-xs-12">

        {!! Form::time('end_time', null, ['end_time', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'End Time']) !!}

    </div>
</div>

<div class="row mb-2">

    {{ Form::label('room_no', __('school.room_no'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

    <div class="col-md-6 col-sm-6 col-xs-12">

        {!! Form::number('room_no', null, ['room_no', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Room No']) !!}

    </div>
</div>
