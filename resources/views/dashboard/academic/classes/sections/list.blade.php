@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>

        <td>
            {{ $d->name}}
        </td>
        <td>
            {{ isset($d->class)?$d->class->name:null }}
        </td>
        <td>
            {{ isset($d->teacher)?$d->teacher->user->name:null }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.sections.edit',[$d->id]),
                'delete'=>route('dashboard.sections.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse