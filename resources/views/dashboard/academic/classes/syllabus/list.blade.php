@forelse ($data as $d)
    <tr>
        <td class="text-center">
            {{ $d->id }}
        </td>
        <td>
            {{ $d->title }}
        </td>
        <td>
            {{ isset($d->class_id)?$d->class->name:'' }}
        </td>
        <td>
            {{ isset($d->subject_id)?$d->subject->name:'' }}
        </td>
        <td>
            {{ $d->academic_year_id }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.syllabus.edit',[$d->id]),
                'delete'=>route('dashboard.syllabus.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse