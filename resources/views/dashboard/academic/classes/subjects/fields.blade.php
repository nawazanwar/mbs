@include('components.fields.name')
<div class="row mb-2">
    {{   Form::label('code',__('school.subject_code'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']);}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!  Form::text('code',null,['code','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Subject Code' ]) !!}
    </div>
</div>
<div class="row mb-2">
    {{   Form::label('author',__('school.author'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']);}}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!!  Form::text('author',null,['author','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'Author' ]) !!}

    </div>
</div>
<div class="row mb-2">
    {!! Form::label('type',__('school.type'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('type',\App\Services\SubjectService::getSubjectTypeForDropdown(),null,['id'=>'type','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--']) !!}
    </div>
</div>
@include('components.fields.class-list')
@include('components.fields.teacher-list')
@include('components.fields.note')