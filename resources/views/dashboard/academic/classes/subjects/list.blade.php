@forelse ($data as $d)
    <tr>
        <td class="text-center">
            {{ $d->id }}
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            {{ $d->code }}
        </td>
        <td>
            {{ isset($d->class)?$d->class->name:null }}
        </td>
        <td>
            {{ isset($d->teacher)?$d->teacher->user->name:null }}
        </td>
        <td>
            {{ $d->type }}
        </td>
        <td>
            {{ $d->author }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.subjects.edit',[$d->id]),
                'delete'=>route('dashboard.subjects.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse