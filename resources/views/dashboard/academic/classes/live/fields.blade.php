@include('components.fields.class-list')

@include('components.fields.section-list')

@include('components.fields.subject-list')

@include('components.fields.teacher-list')


<div class="row mb-2">
    {{ Form::label('live_class', __('school.live_class'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ Form::select('size', ['L' => 'Zoom Meet', 'S' => 'Jitsi Meet'], null, ['class' => 'form-control col-md-12 col-xs-12', 'placeholder' => 'Select Meeting']) }}
    </div>
</div>

<div class="fn_add_zoom_setting display">
    <div class="row mb-2">
        {{ Form::label('meeting_id', __('school.meeting_id'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('meeting_id', null, ['meeting_id', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Meeting Id']) !!}
        </div>
    </div>
    <div class="row mb-2">
        {{ Form::label('meeting_password', __('school.meeting_password'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('meeting_password', null, ['meeting_password', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Meeting Password']) !!}
        </div>
    </div>
</div>

<div class="row mb-2">
    {{ Form::label('class_date',__('school.class_date'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('class_date', null, ['class_date', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Class Date']) !!}

    </div>
</div>

<div class="row mb-2">
    {{ Form::label('start_time', __('school.start_time'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::time('start_time', null, ['start_time', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Start Time']) !!}
    </div>
</div>

<div class="row mb-2">
    {{ Form::label('end_time', __('school.end_time'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::time('end_time', null, ['end_time', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'End Time']) !!}
    </div>
</div>
<div class="row mb-2">
    {{ Form::label('class_type', __('school.class_type'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('class_type', null, ['class_type', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Class Type']) !!}
    </div>
</div>

@include('components.fields.note')

<div class="row mb-2">
    {{ Form::label('send_notifications', __('school.send_notification'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ Form::checkbox('send_notifications', 'value') }}
    </div>
</div>
