<div class="row mb-2">
    {!! Form::label('name',__('school.name'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('name',null,['id'=>'name','class'=>'form-control col-md-7 col-xs-12','required']) !!}
        @error('name')
        <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('numeric_name',__('school.numeric_name'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('numeric_name',null,['id'=>'numeric_name','class'=>'form-control col-md-7 col-xs-12','required']) !!}
        @error('numeric_name')
        <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>
@include('components.fields.teacher-list')
@include('components.fields.note')
