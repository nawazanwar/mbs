<div class="row mb-2">
    {!! Form::label('start_session',__('school.session_start'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('start_session',null,['id'=>'start_session','class'=>'form-control col-md-7 col-xs-12','required']) !!}
        @error('start_session')
        <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('end_session',__('school.session_end'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('end_session',null,['id'=>'end_session','class'=>'form-control col-md-7 col-xs-12','required']) !!}
        @error('end_session')
        <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>



    @include('components.fields.note')
