@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->start_session }}
        </td>
        <td>
            {{ $d->end_session }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.academic.years.edit',[$d->id]),
                'delete'=>route('dashboard.academic.years.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse