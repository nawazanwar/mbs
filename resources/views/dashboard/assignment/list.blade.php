@forelse ($data as $d)
    <tr>
        <td class="text-center">
            {{ $d->id }}
        </td>
        <td>
            {{ $d->title }}
        </td>
        <td>
            {{ isset($d->class)?$d->class->name:null }}
        </td>
        <td>
            {{ isset($d->section)?$d->section->name:null }}
        </td>
        <td>

        </td>
        <td>
            {{ $d->deadline }}
        </td>
        <td>
            <img src="{{ asset($d->assignment) }}" class="avatar">
        </td>
        <td>

        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.assignments.edit',[$d->id]),
                'delete'=>route('dashboard.assignments.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse