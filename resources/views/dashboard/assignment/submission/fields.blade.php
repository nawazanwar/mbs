@include('components.fields.class-list')
@include('components.fields.section-list')
@include('components.fields.student-list')
@include('components.fields.assignment-list')

<div class="item row mb-2">
    {!! Form::label('submission',__('school.submission'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::file('submission',['id'=>'submission','class'=>'form-control col-md-7 col-xs-12']) !!}
        <div class="text-info">Valid file format submission. Ex: doc, docx, jpg, jpeg, pdf, ppt, pptx.</div>
    </div>
</div>
@include('components.fields.note')
