@forelse ($data as $d)
    <tr>
        <td class="text-center">
            {{ $d->id }}
        </td>
        <td>
            {{ isset($d->assignment)?$d->assignment->title:null }}
        </td>
        <td>
            {{ isset($d->class)?$d->class->name:null }}
        </td>
        <td>
            {{ isset($d->section)?$d->section->name:null }}
        </td>
        <td>
            {{ isset($d->subject)?$d->subject->name:null }}
        </td>
        <td>

        </td>
        <td>
            <img src="{{ asset($d->submission) }}" class="avatar">
        </td>
        <td>
            {{ $d->submitted_at }}
        </td>

        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.assignment.submissions.edit',[$d->id]),
                'delete'=>route('dashboard.assignment.submissions.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse