@include('components.fields.title')
@include('components.fields.class-list')
@include('components.fields.section-list')
@include('components.fields.subject-list')
<div class="row mb-2">
    {!! Form::label('deadline',__('school.deadline'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('deadline',null,['id'=>'deadline','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="item row mb-2">
    {!! Form::label('assignment',__('school.assignment'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::file('assignment',['id'=>'assignment','class'=>'form-control col-md-7 col-xs-12']) !!}
        <div class="text-info">Valid file format submission. Ex: doc, docx, jpg, jpeg, pdf, ppt, pptx.</div>
    </div>
</div>
@include('components.fields.note')
