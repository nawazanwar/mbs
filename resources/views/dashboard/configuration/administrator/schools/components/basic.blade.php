<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.basic_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! Form::label('code',__('school.code')) !!}
                {!! Form::text('code',null,['id'=>'code','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('name',__('school.name')) !!}
                {!! Form::text('name',null,['id'=>'name','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('address',__('school.address')) !!}
                {!! Form::text('address',null,['id'=>'address','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('phone',__('school.phone')) !!}
                {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control']) !!}
            </div>
        </div>

        <div class="row">
            <div class="col">
                {!! Form::label('reg_date',__('school.reg_date')) !!}
                {!! Form::text('reg_date',null,['id'=>'reg_date','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('email',__('school.email')) !!}
                {!! Form::email('email',null,['id'=>'email','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('fax',__('school.fax')) !!}
                {!! Form::text('fax',null,['id'=>'fax','class'=>'form-control']) !!}
            </div>
        </div>
    </div>
</div>