<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.branch_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! Form::label('is_main_branch',__('school.is_main_branch')) !!}
                {!! Form::select('is_main_branch',['1'=>'yes','0'=>'no'],null,['id'=>'is_main_branch','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--']) !!}
            </div>
            <div class="col">
                {!! Form::label('parent_school_id',__('school.parent_school')) !!}
                {!! Form::select('parent_school_id',\App\Services\SchoolService::getSchoolDropdown(),null,['id'=>'parent_school_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--','disabled']) !!}
            </div>
        </div>
    </div>
</div>