<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.setting_information')}}</h6>
    </div>
    <div class="card-body">

        <div class="row">
            <div class="col">
                {!! Form::label('latitude',__('school.latitude')) !!}
                {!! Form::text('latitude',null,['id'=>'latitude','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('longitude',__('school.longitude')) !!}
                {!! Form::email('longitude',null,['id'=>'longitude','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('about',__('school.about')) !!}
                {!! Form::text('about',null,['id'=>'about','class'=>'form-control']) !!}
            </div>

        </div>

        <div class="row">
            <div class="col">
                {!! Form::label('language_id',__('school.language')) !!}
                {!! Form::select('language_id',\App\Services\LanguageService:: getLanguageForDropdown(),null,['id'=>'language_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--']) !!}
            </div>
            <div class="col">
                {!! Form::label('theme_id',__('school.theme')) !!}
                {!! Form::select('theme_id',\App\Services\ThemeService::getThemeForDropdown(),null,['id'=>'theme_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--']) !!}
            </div>
        </div>
    </div>
</div>