<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.social_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! Form::label('facebook',__('school.facebook')) !!}
                {!! Form::text('facebook',null,['id'=>'facebook','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('twitter',__('school.twitter')) !!}
                {!! Form::text('twitter',null,['id'=>'twitter','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('linkedin',__('school.linkedin')) !!}
                {!! Form::text('linkedin',null,['id'=>'linkedin','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('linkedin',__('school.linkedin')) !!}
                {!! Form::text('linkedin',null,['id'=>'linkedin','class'=>'form-control']) !!}
            </div>
        </div>

        <div class="row">
            <div class="col">
                {!! Form::label('youtube',__('school.youtube')) !!}
                {!! Form::text('youtube',null,['id'=>'youtube','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('instagram',__('school.instagram')) !!}
                {!! Form::email('instagram',null,['id'=>'instagram','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('pinterest',__('school.pinterest')) !!}
                {!! Form::text('pinterest',null,['id'=>'pinterest','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('status',__('school.status')) !!}
                {!! Form::text('status',null,['id'=>'status','class'=>'form-control']) !!}
            </div>
        </div>
    </div>
</div>