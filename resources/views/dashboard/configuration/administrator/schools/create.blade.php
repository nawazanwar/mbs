@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
            <div class="row">
                @include('dashboard.partials.administrator-left-bar')
            </div>
        </div>
        <div class="col-12 col-md-9 col-lg-9 col-xl-9 col-xxl-9">
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button bg-white " type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {{__('school.manage_school')}}
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show bg-white" aria-labelledby="headingOne"
                         data-bs-parent="#accordionExample" style="">
                        <div class="accordion-body pt-2">
                            <div class="card shadow-none pt-0">
                                <form action="http://localhost/myalif/teacher/add.html" name="add" id="add" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" accept-charset="utf-8" novalidate="novalidate">
                                @include('components.fields.form-create-header',['url'=>'dashboard.schools.index'])
                                <div class="card-body">
                                    @include('dashboard.configuration.administrator.schools.fields')
                                    @include('components.fields.store')
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection