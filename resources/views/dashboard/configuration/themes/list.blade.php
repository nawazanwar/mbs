@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            <span class="badge text-white shadow-sm w-100" style="background-color:#{{$d->color}};height:20px;">
            </span>
        </td>
        <td class="text-center">
            @if($d->status)
                <span class="badge bg-success text-white shadow-sm">Active</span>
            @else
                <span class="badge bg-danger text-white shadow-sm">In Active</span>
            @endif
        </td>
        <td class="text-center">
            <div class="btn-group">
                <button type="button" class="btn btn-sm btn-outline-secondary">Action</button>
                <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">	<span class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a class="dropdown-item " href="{{ route('dashboard.themes.edit',[$d->id]) }}">Edit</a>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse