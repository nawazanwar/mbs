<div class="row mb-2">
    {{ Form::label('role_id', __('school.applicant_type'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ Form::select('role_id', \App\Services\RoleService::getRolesForDropdown(), null, ['class' => 'form-control col-md-12 col-xs-12', 'placeholder' => 'Select Applicant']) }}
    </div>
</div>

@include('components.fields.class-list')

<div class="row mb-2">
    {{ Form::label('type_id', __('school.leave_type'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ Form::select('type_id', ['L' => 'Large', 'S' => 'Small'], null, ['class' => 'form-control col-md-12 col-xs-12', 'placeholder' => 'Select Type']) }}
    </div>
</div>

<div class="row mb-2">
    {{ Form::label('leave_day', __('school.leave_day'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
    {!! Form::select('leave_day', \App\Services\LeaveService::getLeaveDayForDropdown(), null,['class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Select Day']) !!}
    </div>
</div>

<div class="row mb-2">
    {{ Form::label('leave_from', __('school.leave_from'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('leave_from', null, ['leave_from', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Leave Form']) !!}
    </div>
</div>

<div class="row mb-2">
    {{ Form::label('leave_to', __('school.leave_to'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('leave_to', null, ['leave_to', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Leave To']) !!}
    </div>
</div>

<div class="row mb-2">
    {{ Form::label('leave_reason', __('school.leave_reason'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::textarea('leave_reason', null, ['leave_reason', 'class' => 'form-control col-md-7 col-xs-12', 'rows' => 3, 'placeholder' => 'Leave Reason']) !!}
    </div>
</div>

@include('components.fields.attachment')
