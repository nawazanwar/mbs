<div class="row mb-2">
    {{ Form::label('role_id', __('school.applicant_type'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {{ Form::select('role_id', \App\Services\RoleService::getRolesForDropdown(), null, ['class' => 'form-control col-md-12 col-xs-12', 'placeholder' => 'Select']) }}
    </div>
</div>
<div class="row mb-2">
    {{ Form::label('type', __('school.leave_type'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('type', null, ['type', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Leave Type']) !!}
    </div>
</div>
