@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ isset($d->school)?$d->school->school_name:"" }}
        </td>
        <td>
            {{ $d->role->name}}
        </td>
        <td>
            {{ $d->type }}
        </td>
        <td>
            {{ $d->total_leave }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.dashboard.leave.types.edit',[$d->id]),
                'delete'=>route('dashboard.leave.types.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse