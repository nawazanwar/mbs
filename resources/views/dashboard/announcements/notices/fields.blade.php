
    @include('components.fields.title')

    <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date">Date <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input class="form-control col-md-7 col-xs-12" name="date" id="add_date" value="" placeholder="Date" required="required" type="text" autocomplete="off">
            <div class="help-block"></div>
        </div>
    </div>

    <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="role_id">Notice for <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="role_id" id="role_id" required="required">
                <option value="">--Select--</option>
                <option value="0">All</option>
                <option value="1">Super Admin</option>
                <option value="2">Admin</option>
                <option value="3">Guardian</option>
                <option value="4">Student</option>
                <option value="5">Teacher</option>
                <option value="6">Accountant</option>
                <option value="7">Librarian</option>
                <option value="8">Receptioniast</option>
                <option value="9">Staff</option>
                <option value="13">Servent</option>
                <option value="15">Hostel Manager</option>
                <option value="16">Test</option>
                <option value="17">XXXXXX</option>

            </select>
            <div class="help-block"></div>
        </div>
    </div>

    <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notice">Notice <span class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea class="form-control col-md-7 col-xs-12" name="notice" id="add_notice" required="required" placeholder="Notice" data-origin="textarea"></textarea>
        </div>
    </div>

    <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="is_view_on_web">Is View on Web?</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="is_view_on_web" id="is_view_on_web">
                <option value="">--Select--</option>
                <option value="1">Yes</option>
                <option value="0">No</option>
            </select>
            <div class="help-block"></div>
        </div>
    </div>