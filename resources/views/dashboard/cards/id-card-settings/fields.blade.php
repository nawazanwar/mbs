<div class="row mb-2">
    {!! Form::label('border_color',__('school.border_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('border_color','#e01ab5',['id'=>'border_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('top_bg',__('school.top_bg'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('top_bg','#e01ab5',['id'=>'top_bg','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('school_name',__('school.card_school_name'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('school_name','#e01ab5',['id'=>'top_bg','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('school_name_font_size',__('school.school_name_font_size'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('school_name_font_size','#e01ab5',['id'=>'school_name_font_size','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('school_name_color',__('school.school_name_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('school_name_color','#e01ab5',['id'=>'school_name_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('school_address',__('school.school_address'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::text('school_address',null,['id'=>'school_address','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('school_address_color',__('school.school_address_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('school_address_color','#e01ab5',['id'=>'school_address_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('id_font_size',__('school.id_no_font_size'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::number('id_font_size',null,['id'=>'id_font_size','class'=>'form-control col-md-7 col-xs-12',"max"=>20]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('id_color',__('school.id_no_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('id_color','#e01ab5',['id'=>'id_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('id_bg',__('school.id_no_background'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('id_bg','#e01ab5',['id'=>'id_bg','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('title_font_size',__('school.title_font_size'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::number('title_font_size',null,['id'=>'title_font_size','class'=>'form-control col-md-7 col-xs-12',"max"=>12]) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('title_color',__('school.title_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('title_color','#e01ab5',['id'=>'title_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('value_font_size',__('school.value_font_size'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::number('value_font_size',null,['id'=>'value_font_size','class'=>'form-control col-md-7 col-xs-12',"max"=>12]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('value_color',__('school.value_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('value_color','#e01ab5',['id'=>'value_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('exam_font_size',__('school.exam_title_font_size'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::number('exam_font_size',null,['id'=>'exam_font_size','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('exam_color',__('school.exam_title_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('exam_color','#e01ab5',['id'=>'exam_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('subject_font_size',__('school.subject_font_size'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::number('subject_font_size',null,['id'=>'subject_font_size','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('subject_color',__('school.subject_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('subject_color','#e01ab5',['id'=>'subject_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('bottom_text',__('school.bottom_signature'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::text('bottom_text',null,['id'=>'bottom_text','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('bottom_bg',__('school.signature_background'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('bottom_bg','#e01ab5',['id'=>'bottom_bg','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('bottom_text_color',__('school.signature_color'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::color('bottom_text_color','#e01ab5',['id'=>'bottom_text_color','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('bottom_text_align',__('school.signature_align'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('bottom_text_align',\App\Services\CardService::getSignatureAlignments(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>'--Select--']) !!}
    </div>
</div>
<div class="item row mb-2">
    {!! Form::label('school_logo',__('school.card_logo'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::file('school_logo',['id'=>'school_logo','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
