    @include('components.fields.class-list')

    @include('components.fields.section-list')

    @include('components.fields.student-list')

    <div class="row mb-2 ">
        {!! html::decode(Form::label('activity_date', __('school.date'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::date('activity_date', null, ['id' => 'activity_date', 'class' => 'form-control col-md-7 col-xs-12', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    <div class="row mb-2">
        {!! html::decode(Form::label('activity', __('school.activity'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::textarea('activity', null, ['id' => 'activity', 'class' => 'form-control col-md-7 col-xs-12', 'autocomplete' => 'off', 'rows' => 2]) !!}
        </div>
    </div>
