@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ isset($d->school)?$d->school->name:"" }}
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">

        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse