<div class="card-body">
    <div class="row">
        <div class="col">
                {!! Form::label('academic_year_id', __('school.academic_year'), ['class' => 'control-label']) !!}
                {!! Form::select('academic_year_id', [], null, ['id' => 'academic_year_id', 'class' => 'form-control col-md-7 col-xs-12', 'autocomplete' => 'off']) !!}
        </div>
        <div class="col">
                {!! Form::label('class_id', __('school.class'), ['class' => 'control-label']) !!}
                {!! Form::select('class_id', \App\Services\ClassService::getClassesForDropdown(), null, ['id' => 'class_id', 'class' => 'form-control col-md-7 col-xs-12 form-select', 'placeholder' => __('school.ph_select_class')]) !!}
        </div>
        <div class="col">
                {!! Form::label('section_id', __('school.section'), ['class' => 'control-label']) !!}
                {!! Form::select('section_id', [], null, ['id' => 'section_id', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => __('school.ph_select_section')]) !!}
        </div>
        <div class="col">
                {!! Form::label('class_id', __('school.csv'), ['class' => 'control-label']) !!}
                <input class="form-control col-md-7 col-xs-12" name="bulk_student" id="bulk_student" type="file">
        </div>
        <div class="col row">
                <label for="">&nbsp;</label>
                <a href="#" class="btn btn-success btn-md">Generate
                    CSV</a>
        </div>
    </div>
</div>
