@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ isset($d->school)?$d->school->name:"" }}
        </td>
        <td>
            <img src="{{ asset(isset($d->user)?$d->user->photo:null) }}" class="img-fluid avatar">
        </td>
        <td>
            {{ isset($d->user)?$d->user->name:null }}
        </td>
        <td>
            {{ $d->group }}
        </td>
        <td>
            {{ isset($d->class)?$d->class->name:"" }}
        </td>
        <td>
            {{ isset($d->section)?$d->section->name:"" }}
        </td>
        <td>
            {{ $d->roll_no }}
        </td>
        <td>
            {{ isset($d->user)?$d->user->email:null }}
        </td>
        <td class="text-center">
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse