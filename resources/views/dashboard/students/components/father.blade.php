<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.father_information') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! Form::label('father_name', __('school.father_name'), ['class' => 'control-label']) !!}
                {!! Form::text('father_name', null, ['id' => 'father_name', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('father_phone', __('school.father_phone'), ['class' => 'control-label']) !!}
                {!! Form::text('father_phone', null, ['id' => 'father_phone', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('father_education', __('school.father_education'), ['class' => 'control-label']) !!}
                {!! Form::text('father_education', null, ['id' => 'father_education', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('father_profession', __('school.father_profession'), ['class' => 'control-label']) !!}
                {!! Form::text('father_profession', null, ['id' => 'father_profession', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                {!! Form::label('father_designation', __('school.father_designation'), ['class' => 'control-label']) !!}
                {!! Form::text('father_designation', null, ['id' => 'father_designation', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('father_photo', __('school.photo'), ['class' => 'control-label']) !!}
                {!! Form::file('father_photo', ['id' => 'father_photo', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
            </div>
        </div>
    </div>
</div>