<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.guardian_information') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! html::decode(Form::label('guardian[name]', __('school.name') . '<i class="text-danger"> *</i>', ['class' => 'control-label '])) !!}
                {!! Form::text('guardian[name]', null, ['id' => 'guardian[name]', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('guardian[phone]', __('school.phone') . '<i class="text-danger"> *</i>', ['class' => 'control-label '])) !!}
                {!! Form::text('guardian[phone]', null, ['id' => 'guardian[phone]', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('guardian[email]', __('school.email'). '<i class="text-danger">*</i>', ['class' => 'control-label '])) !!}
                {!! Form::text('guardian[email]', null, ['id' => 'guardian[email]', 'class' => 'form-control', 'placeholder' => 'guardian_email', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('guardian[password]', __('school.password') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::text('guardian[password]', null, ['id' => 'guardian[password]', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                {!! html::decode(Form::label('guardian[profession]', __('school.profession') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::text('guardian[profession]', null, ['id' => 'guardian[profession]', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('guardian[religion]', __('school.religion') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::text('guardian[religion]', null, ['id' => 'guardian[religion]', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('guardian[national_id]', __('school.national_id') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::text('guardian[national_id]', null, ['id' => 'guardian[national_id]', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                {!! html::decode(Form::label('guardian[present_address]', __('school.present_address') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::textarea('guardian[present_address]', null, ['id' => 'guardian[present_address]', 'class' => 'form-control', 'placeholder' => 'Permanent Address', 'autocomplete' => 'off', 'rows' => 2]) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('guardian[permanent_address]', __('school.permanent_address'), ['class' => 'control-label'])) !!}
                {!! Form::textarea('guardian[permanent_address]', null, ['id' => 'guardian[permanent_address]', 'class' => 'form-control', 'placeholder' => 'Temporary Address', 'autocomplete' => 'off', 'rows' => 2]) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::label('guardian_photo', __('school.photo'), ['class' => 'control-label']) !!}
                {!! Form::file('guardian_photo', ['id' => 'guardian_photo', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
            </div>
        </div>
    </div>
</div>