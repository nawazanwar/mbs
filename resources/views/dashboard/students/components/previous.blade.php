<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.previous_school') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! html::decode(Form::label('previous_school', __('school.school_name'), ['class' => 'control-label'])) !!}
                {!! Form::text('previous_school', null, ['id' => 'previous_school', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('previous_class', __('school.class'), ['class' => 'control-label'])) !!}
                {!! Form::text('previous_class', null, ['id' => 'previous_class', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('transfer_certificate', __('school.transfer_certificate'), ['class' => 'control-label'])) !!}
                {!! Form::file('transfer_certificate', ['id' => 'transfer_certificate', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
            </div>
        </div>
    </div>
</div>