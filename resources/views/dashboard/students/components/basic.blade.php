<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.basic_information') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! Form::label('name', __('school.name'), ['class' => 'control-label']) !!}
                {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('phone', __('school.phone') . '<i class="text-danger">*</i>', ['class' => 'control-label '])) !!}
                {!! Form::number('phone', null, ['id' => 'phone', 'class' => 'form-control', 'placeholder' => 'Phone',  'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('email', __('school.email'), ['class' => 'control-label '])) !!}
                {!! Form::text('email', null, ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Email', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('password', __('school.password') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::text('password', null, ['id' => 'password', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                {!! Form::label('gender', __('school.gender'), ['class' => 'control-label']) !!}
                {!! Form::select('gender', \App\Services\UserService::getGenderForDropdown(), null, ['id' => 'gender', 'class' => 'form-control','placeholder'=>'Select Gender']) !!}
            </div>
            <div class="col">
                {!! Form::label('blood_group', __('school.blood_group'), ['class' => 'control-label']) !!}
                {!! Form::select('blood_group', \App\Services\UserService::getBloodGroupDropdown(), null, ['id' => 'blood_group', 'class' => 'form-control','placeholder'=>'Select Blood Group']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('religion', __('school.religion') . '<i class="text-danger">*</i>', ['class' => 'control-label '])) !!}
                {!! Form::text('religion', null, ['id' => 'religion', 'class' => 'form-control', 'placeholder' => 'Religion',  'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('caste', __('school.caste') . '<i class="text-danger">*</i>', ['class' => 'control-label '])) !!}
                {!! Form::text('caste', null, ['id' => 'caste', 'class' => 'form-control', 'placeholder' => 'Religion',  'autocomplete' => 'off']) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                {!! html::decode(Form::label('national_id', __('school.national_id') . '<i class="text-danger">*</i>', ['class' => 'control-label '])) !!}
                {!! Form::text('national_id', null, ['id' => 'national_id', 'class' => 'form-control', 'placeholder' => 'Name',  'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('admission_no', __('school.admission_no'), ['class' => 'control-label']) !!}
                {!! Form::text('admission_no', null, ['id' => 'admission_no', 'class' => 'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('admission_date', __('school.admission_date'), ['class' => 'control-label']) !!}
                {!! Form::date('admission_date', null, ['id' => 'admission_date', 'class' => 'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('dob', __('school.birth_date'), ['class' => 'control-label']) !!}
                {!! Form::date('dob', null, ['id' => 'dob', 'class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>