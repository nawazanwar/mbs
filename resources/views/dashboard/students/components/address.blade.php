
<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.address_information') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! html::decode(Form::label('present_address', __('school.present_address') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::textarea('present_address', null, ['id' => 'present_address', 'class' => 'form-control', 'placeholder' => 'Permanent Address',  'autocomplete' => 'off', 'rows' => 2]) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('permanent_address', __('school.permanent_address'), ['class' => 'control-label'])) !!}
                {!! Form::textarea('permanent_address', null, ['id' => 'permanent_address', 'class' => 'form-control', 'placeholder' => 'Temporary Address', 'autocomplete' => 'off', 'rows' => 2]) !!}
            </div>
        </div>
    </div>
</div>