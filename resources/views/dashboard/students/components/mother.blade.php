<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.mother_information') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! Form::label('mother_name', __('school.mother_name'), ['class' => 'control-label']) !!}
                {!! Form::text('mother_name', null, ['id' => 'mother_name', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('mother_phone', __('school.mother_phone'), ['class' => 'control-label']) !!}
                {!! Form::text('mother_phone', null, ['id' => 'mother_phone', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('mother_education', __('school.mother_education'), ['class' => 'control-label']) !!}
                {!! Form::text('mother_education', null, ['id' => 'mother_education', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('mother_profession', __('school.mother_profession'), ['class' => 'control-label']) !!}
                {!! Form::text('mother_profession', null, ['id' => 'mother_profession', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                {!! Form::label('mother_designation', __('school.mother_designation'), ['class' => 'control-label']) !!}
                {!! Form::text('mother_designation', null, ['id' => 'mother_designation', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('mother_photo', __('school.photo'), ['class' => 'control-label']) !!}
                {!! Form::file('mother_photo', ['id' => 'mother_photo', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
            </div>
        </div>
    </div>
</div>
