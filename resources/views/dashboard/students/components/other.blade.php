<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.other_information') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">

            <div class="col">
                {!! html::decode(Form::label('health_condition', __('school.health_condition') . '<i class="text-danger">*</i>', ['class' => 'control-label'])) !!}
                {!! Form::text('health_condition', null, ['id' => 'health_condition', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>

            <div class="col">
                {!! html::decode(Form::label('other_info', __('school.other_info'), ['class' => 'control-label'])) !!}
                {!! Form::text('other_info', null, ['id' => 'other_info', 'class' => 'form-control', 'placeholder' => 'Other Info', 'autocomplete' => 'off', 'rows' => 2]) !!}
            </div>
            <div class="col">
                {!! html::decode(Form::label('photo', __('school.photo'), ['class' => 'control-label'])) !!}
                {!! Form::file('photo', ['id' => 'photo', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
            </div>
        </div>
    </div>
</div><?php
