<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{ __('school.academic_information') }}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! Form::label('student_type_id', __('school.student_type'), ['class' => 'control-label']) !!}
                {!! Form::select('student_type_id',\App\Services\StudentService::getStudentTypeForDropdown(), null, ['id' => 'student_type_id', 'class' => 'form-control form-select', 'placeholder' => 'Select Type', 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('class_id', __('school.class'), ['class' => 'control-label']) !!}
                {!! Form::select('class_id', \App\Services\ClassService::getClassesForDropdown(), null, ['id' => 'class_id', 'class' => 'form-control form-select', 'placeholder' => __('school.ph_select_class')]) !!}
            </div>
            <div class="col">
                {!! Form::label('section_id', __('school.section'), ['class' => 'control-label']) !!}
                {!! Form::select('section_id', \App\Services\SectionService::getSectionForDropdown(), null, ['id' => 'section_id', 'class' => 'form-control', 'placeholder' => __('school.ph_select_section')]) !!}
            </div>
            <div class="col">
                {!! Form::label('group', __('school.group'), ['class' => 'control-label']) !!}
                {!! Form::select('group', \App\Services\StudentService::getStudentGroupsForDrodown(), null, ['id' => 'section_id', 'class' => 'form-control', 'placeholder' => __('school.ph_select_group')]) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col">
                {!! html::decode(Form::label('roll_no', __('school.roll_no') . '<i class="text-danger">*</i>', ['class' => 'control-label '])) !!}
                {!! Form::number('roll_no', null, ['id' => 'roll_no', 'class' => 'form-control',  'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('registration_no', __('school.registration_no'), ['class' => 'control-label']) !!}
                {!! Form::text('registration_no', null, ['id' => 'registration_no', 'class' => 'form-control',  'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! Form::label('discount_id', __('school.discount'), ['class' => 'control-label']) !!}
                {!! Form::select('discount_id',\App\Services\DiscountService::getDiscountForDropdown(), null, ['id' => 'discount_id', 'class' => 'form-control','placeholder'=>__('school.ph_select_discount')]) !!}
            </div>
            <div class="col">
                {!! Form::label('second_language', __('school.second_language'), ['class' => 'control-label']) !!}
                {!! Form::text('second_language', null, ['id' => 'second_language', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
            </div>
        </div>
    </div>
</div>