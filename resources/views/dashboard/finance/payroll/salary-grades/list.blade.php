@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->grade_name }}
        </td>
        <td>
            {{ $d->basic_salary }}
        </td>
        <td>
            {{ $d->house_rent }}
        </td>
        <td>
            {{ $d->transport }}
        </td>
        <td>
            {{ $d->medical }}
        </td>
        <td>
            {{ $d->over_time_houly_rate }}
        </td>
        <td>
            {{ $d->provident_fund }}
        </td>
        <td>
            {{ $d->total_allowance }}
        </td>
        <td>
            {{ $d->total_deduction }}
        </td>
        <td>
            {{ $d->gross_salary }}
        </td>
        <td>
            {{ $d->net_sallary }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">

        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse