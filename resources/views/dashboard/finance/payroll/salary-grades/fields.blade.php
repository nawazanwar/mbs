<div class="row">
    <div class="col">
        {!! Form::label('grade_name',__('school.grade_name')) !!}
        {!! Form::text('grade_name',null,['id'=>'grade_name','class'=>'form-control']) !!}
    </div>
    <div class="col">
        {!! Form::label('basic_salary',__('school.basic_salary')) !!}
        {!! Form::number('basic_salary',null,['id'=>'basic_salary','class'=>'form-control']) !!}
    </div>
    <div class="col">
        {!! Form::label('house_rent',__('school.house_rent')) !!}
        {!! Form::number('house_rent',null,['id'=>'house_rent','class'=>'form-control']) !!}
    </div>
    <div class="col">
        {!! Form::label('transport',__('school.transport')) !!}
        {!! Form::number('transport',null,['id'=>'transport','class'=>'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="col">
        {!! Form::label('medical',__('school.medical_allowance')) !!}
        {!! Form::number('medical',null,['id'=>'medical','class'=>'form-control']) !!}
    </div>
    <div class="col">
        {!! Form::label('over_time_hourly_rate',__('school.over_time_hourly_rate')) !!}
        {!! Form::number('over_time_hourly_rate',null,['id'=>'over_time_hourly_rate','class'=>'form-control']) !!}
    </div>
    <div class="col">
        {!! Form::label('provident_fund',__('school.provident_fund')) !!}
        {!! Form::number('provident_fund',null,['id'=>'provident_fund','class'=>'form-control']) !!}
    </div>
    <div class="col">
        {!! Form::label('hourly_rate',__('school.hourly_rate')) !!}
        {!! Form::number('hourly_rate',null,['id'=>'hourly_rate','class'=>'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="col">
        {!! Form::label('total_allowance',__('school.total_allowance')) !!}
        {!! Form::number('total_allowance',null,['id'=>'total_allowance','class'=>'form-control','readonly']) !!}
    </div>
    <div class="col">
        {!! Form::label('total_deduction',__('school.total_deduction')) !!}
        {!! Form::number('total_deduction',null,['id'=>'total_deduction','class'=>'form-control','readonly']) !!}
    </div>
    <div class="col">
        {!! Form::label('gross_salary',__('school.gross_salary')) !!}
        {!! Form::number('gross_salary',null,['id'=>'gross_salary','class'=>'form-control','readonly']) !!}
    </div>
    <div class="col">
        {!! Form::label('net_salary',__('school.net_salary')) !!}
        {!! Form::number('net_salary',null,['id'=>'net_salary','class'=>'form-control','readonly']) !!}
    </div>
</div>
<div class="row">
    <div class="col">
        {!! Form::label('note',__('school.note')) !!}
        {!! Form::textarea('note',null,['id'=>'note','class'=>'form-control','rows' => 2]) !!}
    </div>
</div>