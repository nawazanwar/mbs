@include('components.fields.title')
<div class="row mb-2">
    {!! Form::label('amount',__('school.amount'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('amount',null,['id'=>'amount','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('discount_type',__('school.discount_type'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('discount_type',\App\Services\DiscountService::getDiscountForDropdown(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_discount_type')]) !!}
    </div>
</div>
@include('components.fields.note')
