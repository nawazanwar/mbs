@include('components.fields.class-list')
@include('components.fields.student-list')
<div class="row mb-2">
    {!! Form::label('template_id',__('school.template'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('template_id',\App\Services\TemplateService::getTemplateForEmailDropdown(),null,['id'=>'template_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_template')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('subject',__('school.subject'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('subject',null,['id'=>'subject','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('body',__('school.body'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::textarea('body',null,['id'=>'body','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>