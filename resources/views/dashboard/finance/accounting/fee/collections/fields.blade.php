@include('components.fields.class-list')

@include('components.fields.student-list')

<div class="row mb-2">
    {!! Form::label('amount',__('school.fee_type'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('amount',null,['id'=>'amount','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('amount',__('school.fee_amount'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::number('amount',null,['id'=>'amount','class'=>'form-control col-md-7 col-xs-12','readonly']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('month',__('school.month'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('month',null,['id'=>'month','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('is_applicable_discount',__('school.is_applicable_discount'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('is_applicable_discount',\App\Services\PaymentService::getApplicableDiscountDropdown(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_discount_type')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('paid_status',__('school.paid_status'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('paid_status',\App\Services\PaymentService::getPaidStatusDropdown(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_paid_status')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('payment_method',__('school.payment_method'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('payment_method',\App\Services\PaymentService::getPaymentMethodForDropdown(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_payment_method')]) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('bank_name',__('school.bank_name'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('bank_name',null,['id'=>'bank_name','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('cheque_no',__('school.cheque_number'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('cheque_no',null,['id'=>'cheque_no','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('bank_receipt',__('school.bank_receipt'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('bank_receipt',null,['id'=>'bank_receipt','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

@include('components.fields.note')