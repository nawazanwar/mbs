<div class="row mb-2">
    {!! Form::label('type',__('school.fee_type'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('type',\App\Services\FeeService::getGeneralFeeTypesDropdown(),null,['id'=>'type','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_discount_type')]) !!}
    </div>
</div>
@include('components.fields.title')
@include('components.fields.note')