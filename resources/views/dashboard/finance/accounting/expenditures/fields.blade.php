<div class="row mb-2">
    {!! Form::label('expenditure_head_id',__('school.expenditure_head'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('expenditure_head_id',\App\Services\ExpenditureService::getHeadDropdown(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_expenditure_head')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('expenditure_via',__('school.expenditure_method'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('expenditure_via',\App\Services\PaymentService::getPaymentMethodForDropdown(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_expenditure_method')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('amount',__('school.amount'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::number('amount',null,['id'=>'amount','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
@include('components.fields.note')