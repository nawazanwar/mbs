<div class="row mb-2">
    {!! Form::label('income_head_id',__('school.income_head'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('income_head_id',\App\Services\IncomeService::getHeadDropdown(),null,['id'=>'bottom_text_align','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_income_head')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('income_via',__('school.payment_method'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::select('income_via',\App\Services\PaymentService::getPaymentMethodForDropdown(),null,['id'=>'income_via','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_income_method')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('amount',__('school.amount'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::number('amount',null,['id'=>'amount','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
@include('components.fields.note')
