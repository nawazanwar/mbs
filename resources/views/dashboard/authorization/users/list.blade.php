@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>

        <td>
            {{ isset($d->school)?$d->school->name:null }}
        </td>
        <td>
            <img src="{{ asset($d->photo) }}" class="img-fluid avatar">
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            {{ $d->phone }}
        </td>
        <td>
            {{ $d->email }}
        </td>
        <td>
            {{ $d->temp_password }}
        </td>
        <td class-="text-center">
            @if($d->active)
                <span class="badge bg-success text-white shadow-sm">{{__('school.active')}}</span>
            @else
                <span class="badge bg-danger text-white shadow-sm">{{__('school.in_active')}}</span>
            @endif
        </td>

        <td class="text-center">
            <div class="btn-group">
                <button type="button" class="btn btn-sm btn-outline-secondary">{{__('school.action')}}</button>
                <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                        data-bs-toggle="dropdown" aria-expanded="false"><span
                            class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('dashboard.roles.index',['uId'=>$d->id]) }}"
                           class="dropdown-item text-black-50" href="#">
                            {{__('school.roles')}} ({{ count($d->roles) }})
                        </a>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse