@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            {{ $d->label }}
        </td>
        <td class="text-center">
            <div class="btn-group">
                <button type="button" class="btn btn-sm btn-outline-secondary">{{__('school.action')}}</button>
                <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle dropdown-toggle-split"
                        data-bs-toggle="dropdown" aria-expanded="false"><span
                            class="visually-hidden">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ route('dashboard.users.index',['rId'=>$d->id]) }}"
                           class="dropdown-item text-black-50" href="#">
                            {{__('school.users')}} ({{ count($d->users) }})
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.permissions.index',['rId'=>$d->id]) }}"
                           class="dropdown-item text-black-50" href="#">
                            {{__('school.permissions')}} ({{ count($d->permissions) }})
                        </a>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse