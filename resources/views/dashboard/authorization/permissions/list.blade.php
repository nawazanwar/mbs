@forelse ($models as $key=>$value)
    <tr>
        <td class='text-center'>
            {{ $key+1 }}
        </td>
        <td>
            {{ str_replace('\\','',str_replace('App\Models','',$value)) }}
        </td>
        @php
            $permissions= \App\Models\Permission::getPermissions($value);
        @endphp
        @foreach($permissions as $permission)
            <td >
                {{ $permission }}
            </td>
        @endforeach
        <td class="text-center d-inline-flex">
            <a href="" class="btn btn-sm btn-primary p-0 mx-2"><i class="bx bx-edit m-1"></i></a>
            <button type="submit" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger p-0"><i
                        class="bx bx-trash-alt m-1"></i></button>
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse