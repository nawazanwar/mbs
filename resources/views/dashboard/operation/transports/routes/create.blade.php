@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
            <div class="row">
                @include('dashboard.partials.transport-left-bar')
            </div>
        </div>
        <div class="col-12 col-md-9 col-lg-9 col-xl-9 col-xxl-9">
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button bg-white " type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {{ __('school.manage_transport_route') }}
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show bg-white" aria-labelledby="headingOne"
                        data-bs-parent="#accordionExample" style="">
                        <div class="accordion-body pt-2">
                            <div class="card shadow-none pt-0">
                                <div class="card-body">
                                @include('components.fields.form-create-header',['url'=>'dashboard.transport.routes.index'])
                                {!! Form::open(['route' => ['dashboard.transport.routes.store'], 'method' => 'POST','files' => true]) !!}
                                @csrf
                                @include('components.fields.created-by')
                                @include('dashboard.operation.transports.routes.fields')
                                @include('components.fields.store')
                                {!! Form::close() !!}
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
@endsection
