<div class="row mb-2">
    {!! Form::label('title',__('school.route_name'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('title',null,['id'=>'title','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('route_start',__('school.route_start'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('route_start',null,['id'=>'route_start','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('route_end',__('school.route_end'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('route_end',null,['id'=>'route_end','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>


@include('components.fields.note')