<div class="row mb-2">
    {!! Form::label('number',__('school.vehicle_number'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('number',null,['id'=>'number','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('model',__('school.vehicle_model'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('model',null,['id'=>'model','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('driver',__('school.driver'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('driver',null,['id'=>'driver','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('license',__('school.vehicle_license'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('license',null,['id'=>'license','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('contact',__('school.vehicle_contact'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('contact',null,['id'=>'contact','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>


    @include('components.fields.note')