@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->school->name }}
        </td>
        <td>
            {{ $d->number }}
        </td>
        <td>
            {{ $d->model }}
        </td>
        <td>
            {{ $d->driver }}
        </td>
        <td>
            {{ $d->license }}
        </td>
        <td>
            {{ $d->contact }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">

        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse