@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->school->name }}
        </td>
        <td>
            {{ $d->class_id }}
        </td>
        <td>
            {{ $d->subject_id }}
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            {{ $d->author }}
        </td>
        <td>
            {{ $d->edition }}
        </td>
        <td>
            {{ $d->language }}
        </td><td>
            {{ $d->cover_image }}
        </td>
        <td>
            {{ $d->file_name }}
        </td>

        <td class="text-center">

        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse