@include('components.fields.class-list')

@include('components.fields.subject-list')

@include('components.fields.name')

<div class="row mb-2">
    {!! Form::label('edition',__('school.edition'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('edition',null,['id'=>'edition','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>


<div class="row mb-2">
    {!! Form::label('author',__('school.author'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('author',null,['id'=>'author','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('language',__('school.language'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('language',null,['id'=>'language','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="item row mb-2">
    {!! Form::label('cover_image',__('school.cover_image'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::file('cover_image',['id'=>'cover_image','class'=>'form-control col-md-7 col-xs-12']) !!}
        <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
    </div>
</div>

<div class="item row mb-2">
    {!! Form::label('file_name',__('school.e_book'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::file('file_name',['id'=>'file_name','class'=>'form-control col-md-7 col-xs-12']) !!}
        <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
    </div>
</div>



