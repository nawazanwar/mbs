@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->school->name }}
        </td>
        <td>
            {{ $d->title }}
        </td>
        <td>
            {{ $d->isbn_no }}
        </td>
        <td>
            {{ $d->edition }}
        </td>
        <td>
            {{ $d->author }}
        </td>
        <td>
            {{ $d->language }}
        </td>
        <td>
            {{ $d->price }}
        </td>
        <td>
            {{ $d->qty }}
        </td>
        <td>
            {{ $d->cover }}
        </td>
        <td>
            {{ $d->rack_no }}
        </td>
        <td class="text-center">

        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse