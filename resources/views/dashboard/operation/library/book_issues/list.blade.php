@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->school->name }}
        </td>
        <td>
            {{ $d-> }}
        </td>
        <td>
            {{ $d->route_start }}
        </td>
        <td>
            {{ $d->route_end }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">

        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse