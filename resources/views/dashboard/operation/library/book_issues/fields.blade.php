@include('components.fields.book-list')
<div class="row mb-2">
    {!! Form::label('isbn_no',__('school.isbn_no'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('isbn_no',null,['id'=>'book_id','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('edition',__('school.edition'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('edition',null,['id'=>'edition','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('author',__('school.author'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('author',null,['id'=>'author','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('language',__('school.language'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('language',null,['id'=>'language','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('price',__('school.price'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('price',null,['id'=>'price','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('qty',__('school.quantity'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('qty',null,['id'=>'qty','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('rack_no',__('school.almira_no'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('rack_no',null,['id'=>'rack_no','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('cover',__('school.book_cover'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('cover',null,['id'=>'cover','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('due_date',__('school.return_date'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('due_date',null,['id'=>'due_date','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
