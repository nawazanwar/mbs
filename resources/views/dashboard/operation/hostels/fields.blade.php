@include('components.fields.name')
<div class="row mb-2">
    {!! Form::label('type',__('school.type'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('type',\App\Services\HostelService::getHostelTypes(),null,['id'=>'type','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_hostel_type')]) !!}
    </div>
</div>
@include('components.fields.address')
@include('components.fields.note')