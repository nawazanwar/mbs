
<div class="row mb-2">
    {!! Form::label('room_no',__('school.room_no'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('room_no',null,['id'=>'room_no','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('room_type',__('school.room_type'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('room_type',\App\Services\HostelService::getRoomTypes(),null,['id'=>'type','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_hostel_type')]) !!}
    </div>
</div>
<div class="row mb-2">
    {!! Form::label('total_seat',__('school.total_seat'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::number('total_seat',null,['id'=>'total_seat','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
@include('components.fields.hostel-list')
<div class="row mb-2">
    {!! Form::label('cost',__('school.cost_per_seat'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::number('cost',null,['id'=>'cost','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>
@include('components.fields.note')