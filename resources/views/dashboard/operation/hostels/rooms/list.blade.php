@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->school->name }}
        </td>
        <td>
            {{ $d->room_no }}
        </td>
        <td>
            {{ $d->room_type }}
        </td>
        <td>
            {{ $d->total_seat }}
        </td>
        <td>
            {{ ($d->hostel)?$d->hostel->name:null }}
        </td>
        <td>
            {{ $d->cost }}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">

        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse