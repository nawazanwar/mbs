@forelse ($data as $d)
    <tr>
        <td class="text-center">
            {{ $d->id }}
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            {{ $d->phone }}
        </td>
        <td>
            {{ $d->call_duration }}
        </td>
        <td>
            {{ $d->call_date}}
        </td>
        <td>
            {{ $d->next_follow_up}}
        </td>
        <td>
            {{ $d->note }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.assignments.edit',[$d->id]),
                'delete'=>route('dashboard.assignments.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse