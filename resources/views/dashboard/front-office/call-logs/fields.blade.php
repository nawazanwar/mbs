@include('components.fields.name')
@include('components.fields.phone')

<div class="row mb-2">
    {!! Form::label('call_duration',__('school.call_duration'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('call_duration',null,['id'=>'call_duration','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('call_date',__('school.call_date'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('call_date',null,['id'=>'call_date','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('next_follow_up',__('school.follow_up'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('next_follow_up',null,['id'=>'next_follow_up','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('call_type',__('school.call_type'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
       {{ __('school.incoming') }} {{ Form::radio('call_type', 'incoming' , true) }}
        {{ __('school.outgoing') }} {{ Form::radio('call_type', 'outgoing' , false) }}
    </div>
</div>

@include('components.fields.note')
