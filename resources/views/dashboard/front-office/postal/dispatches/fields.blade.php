<div class="row mb-2">
    {!! Form::label('to_title',__('school.to_title'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('to_title',null,['id'=>'to_title','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('reference',__('school.reference'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('reference',null,['id'=>'reference','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

@include('components.fields.address')

<div class="row mb-2">
    {!! Form::label('from_title',__('school.from_title'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('from_title',null,['id'=>'from_title','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('dispatch_date',__('school.dispatch_date'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('dispatch_date',null,['id'=>'dispatch_date','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>


@include('components.fields.note')

<div class="item row mb-2">
    {!! Form::label('attachment',__('school.attachment'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::file('attachment',['id'=>'attachment','class'=>'form-control col-md-7 col-xs-12']) !!}
        <div class="text-info">Valid file format submission. Ex: doc, docx, jpg, jpeg, pdf, ppt, pptx.</div>
    </div>
</div>