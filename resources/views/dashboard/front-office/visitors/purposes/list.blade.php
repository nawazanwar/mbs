@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->purpose }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.visitor.purposes.edit',[$d->id]),
                'delete'=>route('dashboard.visitor.purposes.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse