@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ $d->name }}
        </td>
        <td>
            {{ $d->phone }}
        </td>
        <td>
            {{ $d->check_in }}
        </td>
        <td>
            {{ $d->check_out }}
        </td>
        <td>
            {{ $d->note }}
        </td>

        <td></td>

        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.visitors.edit',[$d->id]),
                'delete'=>route('dashboard.visitors.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse