@include('components.fields.name')
@include('components.fields.phone')
@include('components.fields.role-list')
@include('components.fields.user-list')
<div class="row mb-2">
    {!! Form::label('purpose_id',__('school.visitor_purpose'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('purpose_id',\App\Services\VisitorService::getVisitorPurposesForDropdown(),null,['id'=>'purpose_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_visitor_purpose')]) !!}
    </div>
</div>

@include('components.fields.note')



 
