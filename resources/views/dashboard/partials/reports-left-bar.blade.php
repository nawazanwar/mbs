<div class="col-lg-2 box-color">
    @include('dashboard.partials.home')
</div>
<div class="col-lg-2 box-color">
    @include('dashboard.partials.back')
</div>
@foreach(\App\Enum\ReportNavEnum::getTranslationKeys() as $key=>$value)
    <div class="col-lg-2 box-color">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <a href="{{ \App\Enum\ReportNavEnum::getRoute($key) }}"
                       class="widgets-icons rounded-circle mx-auto  mb-3  bg-transparent">
                        {!! \App\Enum\ReportNavEnum::getIcon($key) !!}
                    </a>
                    <a class="mb-0 "
                       href="{{ \App\Enum\ReportNavEnum::getRoute($key) }}">
                        {!! $value !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach