<div class="card radius-10">
    <div class="card-body">
        <div class="text-center">
            <div class="widgets-icons rounded-circle mx-auto  mb-3  bg-transparent">
                <i class="bx bx-home fs-1"></i>
            </div>
            <a class="mb-0 " href="{{ route('dashboard.index') }}">
                {{__('school.home')}}
            </a>
        </div>
    </div>
</div>