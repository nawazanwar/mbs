<div class="col box-color">
    @include('dashboard.partials.home')
</div>
<div class="col box-color">
    @include('dashboard.partials.back')
</div>
@foreach(\App\Enum\DesignationNavEnum::getTranslationKeys() as $key=>$value)
    <div class="col-md-6 col-lg-6 col-xl-6 col-xxl-6 box-color">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <div class="widgets-icons rounded-circle mx-auto  mb-3  bg-transparent">
                        {!! \App\Enum\DesignationNavEnum::getIcon($key) !!}
                    </div>
                    <a class="mb-0 "
                       href="{{ \App\Enum\DesignationNavEnum::getRoute($key) }}">
                        {!! $value !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach