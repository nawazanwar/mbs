<div class="card radius-10">
    <div class="card-body">
        <div class="text-center">
            <div class="widgets-icons rounded-circle mx-auto  mb-3  bg-transparent">
                <i class="bx bx-undo fs-1"></i>
            </div>
            <a class="mb-0 " style="cursor:pointer;" onclick="window.history.back();">
                {{__('school.back')}}
            </a>
        </div>
    </div>
</div>