<div class="col box-color">
    @include('dashboard.partials.home')
</div>
<div class="col box-color">
    @include('dashboard.partials.back')
</div>
@foreach(\App\Enum\AdministrationNavEnum::getTranslationKeys() as $key=>$value)
    <div class="col box-color">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <a href="{{ \App\Enum\AdministrationNavEnum::getRoute($key) }}"
                       class="widgets-icons rounded-circle mx-auto  mb-3  bg-transparent">
                        {!! \App\Enum\AdministrationNavEnum::getIcon($key) !!}
                    </a>
                    <a class="mb-0 "
                       href="{{ \App\Enum\AdministrationNavEnum::getRoute($key) }}">
                        {!! $value !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach