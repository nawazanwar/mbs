<div class="col-12 box-color">
    @include('dashboard.partials.home')
</div>
<div class="col-12 box-color">
    @include('dashboard.partials.back')
</div>
@foreach(\App\Enum\CertificateNavEnum::getTranslationKeys() as $key=>$value)
    <div class="col-12 box-color">
        <div class="card radius-10">
            <div class="card-body">
                <div class="text-center">
                    <div class="widgets-icons rounded-circle mx-auto  mb-3  bg-transparent">
                        {!! \App\Enum\CertificateNavEnum::getIcon($key) !!}
                    </div>
                    <a class="mb-0 "
                       href="{{ \App\Enum\CertificateNavEnum::getRoute($key) }}">
                        {!! $value !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach