@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-12 col-md-3 col-lg-3 col-xl-3 col-xxl-3">
            <div class="row">
                @include('dashboard.partials.students-left-bar')
            </div>
        </div>
        <div class="col-12 col-md-9 col-lg-9 col-xl-9 col-xxl-9">
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button bg-white " type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {{__('school.manage_student')}}
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show bg-white" aria-labelledby="headingOne"
                         data-bs-parent="#accordionExample" style="">
                        <div class="accordion-body pt-2">
                            <div class="card shadow-none pt-0">
                                @include('components.fields.form-create-header',['url'=>'dashboard.students.index'])
                                <div class="x_content"> 
                                    <form action="https://demo.myalif.com/student/add.html" name="add" id="add" class="form-horizontal form-label-left" enctype="multipart/form-data" method="post" accept-charset="utf-8" novalidate="novalidate">
                        
                                    <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title"><strong>Basic Information:</strong></h5>
                                         </div>
                                     </div>
                                     
                                     <div class="row">                  
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                 <label for="name">Name <span class="required">*</span></label>
                                                 <input class="form-control col-md-7 col-xs-12" name="name" id="name" value="" placeholder="Name" required="required" type="text" autocomplete="off">
                                                 <div class="help-block"></div> 
                                             </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                 <label for="admission_no">Admission  No <span class="required">*</span></label>
                                                 <input class="form-control col-md-7 col-xs-12" name="admission_no" id="admission_no" value="" placeholder="Admission  No" required="required" type="text" autocomplete="off">
                                                 <div class="help-block"></div> 
                                             </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                 <label for="admission_date">Admission Date <span class="required">*</span></label>
                                                 <input class="form-control col-md-7 col-xs-12" name="admission_date" id="add_admission_date" value="" placeholder="Admission Date" required="required" type="text" autocomplete="off">
                                                 <div class="help-block"></div> 
                                             </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="dob">Birth Date <span class="required">*</span></label>
                                                 <input class="form-control col-md-7 col-xs-12" name="dob" id="add_dob" value="" placeholder="Birth Date" required="required" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                         </div>
                                         
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                  <label for="gender">Gender <span class="required">*</span></label>
                                                   <select class="form-control col-md-7 col-xs-12" name="gender" id="gender" required="required">
                                                     <option value="">--Select--</option>
                                                                                                                                                         <option value="male">Male</option>
                                                                                                         <option value="female">Female</option>
                                                                                                 </select>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                         
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                  <label for="blood_group">Blood Group</label>
                                                   <select class="form-control col-md-7 col-xs-12" name="blood_group" id="blood_group">
                                                     <option value="">--Select--</option>
                                                                                                                                                         <option value="a_positive">A+</option>
                                                                                                         <option value="a_negative">A-</option>
                                                                                                         <option value="b_positive">B+</option>
                                                                                                         <option value="b_negative">B-</option>
                                                                                                         <option value="o_positive">O+</option>
                                                                                                         <option value="o_negative">O-</option>
                                                                                                         <option value="ab_positive">AB+</option>
                                                                                                         <option value="ab_negative">AB-</option>
                                                                                                     </select>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                   <label for="religion">Religion</label>
                                                   <input class="form-control col-md-7 col-xs-12" name="religion" id="add_religion" value="" placeholder="Religion" type="text" autocomplete="off">
                                                    <div class="help-block"></div>
                                              </div>
                                          </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                   <label for="caste">Caste</label>
                                                   <input class="form-control col-md-7 col-xs-12" name="caste" id="caste" value="" placeholder="Caste" type="text" autocomplete="off">
                                                    <div class="help-block"></div>
                                              </div>
                                          </div>
                                     </div>
                                         
                                     <div class="row"> 
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                  <label for="phone">Phone <span class="required">*</span></label>
                                                  <input class="form-control col-md-7 col-xs-12" name="phone" id="add_phone" value="" placeholder="Phone" required="required" type="text" autocomplete="off">
                                                  <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                <label for="email">Email </label>
                                                <input class="form-control col-md-7 col-xs-12" name="email" id="email" value="" placeholder="Email" type="email" autocomplete="off">
                                                <div class="help-block"></div>
                                             </div>
                                         </div>
                                         
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                  <label for="national_id">National ID </label>
                                                  <input class="form-control col-md-7 col-xs-12" name="national_id" id="national_id" value="" placeholder="National ID" type="text" autocomplete="off">
                                                  <div class="help-block"></div>
                                              </div>
                                          </div>
                                     </div>
                                     
                                       
                                     <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title"><strong>Academic Information:</strong></h5>
                                         </div>
                                     </div>
                                     
                                     <div class="row">  
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                  <label for="type_id">Student Type</label>
                                                     <select class="form-control col-md-7 col-xs-12" name="type_id" id="add_type_id">
                                                     <option value="">--Select--</option>
                                                                                                     </select>
                                                  <div class="help-block"></div>
                                              </div>
                                          </div>
                                         
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                 <label for="class_id">Class <span class="required">*</span></label>
                                                 <select class="form-control col-md-7 col-xs-12 quick-field" name="class_id" id="add_class_id" required="required" onchange="get_section_by_class(this.value, '');">
                                                    <option value="">--Select--</option>
                                                                                               </select>
                                                <div class="help-block"></div>
                                             </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                <label for="section_id">Section <span class="required">*</span></label>
                                                <select class="form-control col-md-7 col-xs-12 quick-field" name="section_id" id="add_section_id" required="required">
                                                    <option value="">--Select--</option>
                                                </select>
                                                <div class="help-block"></div>
                                             </div>
                                         </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="group">Group </label>
                                                 <select class="form-control col-md-7 col-xs-12" name="group" id="group">
                                                     <option value="">--Select--</option>
                                                                                                                                                         <option value="science">Science</option>
                                                                                                         <option value="arts">Arts</option>
                                                                                                         <option value="commerce">Commerce</option>
                                                                                                 </select>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                     </div>
                                     <div class="row"> 
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                  <label for="roll_no">Roll No <span class="required">*</span></label>
                                                  <input class="form-control col-md-7 col-xs-12" name="roll_no" id="roll_no" value="" placeholder="Roll No" required="required" type="text" autocomplete="off">
                                                  <div class="help-block"></div>
                                              </div>
                                          </div>                               
                                    
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="registration_no">Registration No</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="registration_no" id="registration_no" value="" placeholder="Registration No" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="discount_id">Discount</label>
                                                 <select class="form-control col-md-7 col-xs-12 quick-field" name="discount_id" id="add_discount_id">
                                                     <option value="">--Select--</option>
                                                                                                 </select>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="second_language">Second Language</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="second_language" id="second_language" value="" placeholder="Second Language" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                     </div>
                                     
                                    
                                     <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title"><strong>Father Information:</strong></h5>
                                         </div>
                                     </div> 
                                     <div class="row">  
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="father_name">Father Name</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="father_name" id="father_name" value="" placeholder="Father Name" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="father_phone">Father Phone </label>
                                                 <input class="form-control col-md-7 col-xs-12" name="father_phone" id="father_phone" value="" placeholder="Father Phone" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="father_education">Father Education </label>
                                                 <input class="form-control col-md-7 col-xs-12" name="father_education" id="father_education" value="" placeholder="Father Education" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="father_profession">Father Profession</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="father_profession" id="father_profession" value="" placeholder="Father Profession" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="father_designation">Father Designation</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="father_designation" id="father_designation" value="" placeholder="Father Designation" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label>Father Photo</label>
                                                 <div class="btn btn-default btn-file">                                               <input class="form-control col-md-7 col-xs-12" name="father_photo" id="father_photo" type="file">
                                                 </div>
                                                 <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                     </div>
                                     
                                     <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title"><strong>Mother Information:</strong></h5>
                                         </div>
                                     </div> 
                                     <div class="row">  
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="mother_name">Mother Name</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="mother_name" id="mother_name" value="" placeholder="Mother Name" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="mother_phone">Mother Phone</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="mother_phone" id="mother_phone" value="" placeholder="Mother Phone" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="mother_education">Mother Education</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="mother_education" id="mother_education" value="" placeholder="Mother Education" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="mother_profession">Mother Profession</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="mother_profession" id="mother_profession" value="" placeholder="Mother Profession" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="mother_designation">Mother Designation</label>
                                                 <input class="form-control col-md-7 col-xs-12" name="mother_designation" id="mother_designation" value="" placeholder="Mother Designation" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label>Mother Photo</label>
                                                 <div class="btn btn-default btn-file">                                               <input class="form-control col-md-7 col-xs-12" name="mother_photo" id="mother_photo" type="file">
                                                 </div>
                                                 <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                     </div>
                                     
                                     <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title"><strong>Guardian Information:</strong></h5>
                                         </div>
                                     </div>
                                     
                                     <div class="row"> 
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                 <label for="is_guardian">Is Guardian? <span class="required">*</span></label>
                                                 <select class="form-control col-md-7 col-xs-12 quick-field" name="is_guardian" id="is_guardian" required="required" onchange="check_guardian_type(this.value);">
                                                     <option value="">--Select--</option>
                                                     <option value="father">Father</option>
                                                     <option value="mother">Mother</option>
                                                     <option value="other">Other</option>
                                                     <option value="exist_guardian">Guardian Exist</option>
                                                 </select>
                                                 <div class="help-block"></div>
                                             </div>
                                         </div>
                                         
                                         <div class="fn_existing_guardian display">
                                             <div class="col-md-3 col-sm-3 col-xs-12"> 
                                                 <div class="row mb-2">
                                                     <label for="guardian_id">Guardian <span class="required">*</span></label>
                                                     <select class="form-control col-md-7 col-xs-12 quick-field" name="guardian_id" id="add_guardian_id" onchange="get_guardian_by_id(this.value);">
                                                         <option value="">--Select--</option>
                                                                                                         </select>
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>                                  
                                         </div>
                                                                             
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                 <label for="relation_with">Relation With Guardian </label>
                                                 <input class="form-control col-md-7 col-xs-12" name="relation_with" id="add_relation_with" value="" placeholder="Relation With Guardian" type="text">
                                                 <div class="help-block"></div>
                                             </div>
                                         </div> 
                                     </div> 
                                        
                                     
                                     <div class="display fn_except_exist"> 
                                         <div class="row"> 
     
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_name">Name <span class="required">*</span></label>
                                                     <input class="form-control col-md-7 col-xs-12" name="gud_name" id="add_gud_name" value="" placeholder="Name" required="required" type="text">
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_phone">Phone <span class="required">*</span></label>
                                                     <input class="form-control col-md-7 col-xs-12" name="gud_phone" id="add_gud_phone" value="" placeholder="Phone" required="required" type="text">
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_email">Email </label>
                                                     <input class="form-control col-md-7 col-xs-12" name="gud_email" id="add_gud_email" value="" placeholder="Email" required="email" type="email">
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_profession">Profession</label>
                                                     <input class="form-control col-md-7 col-xs-12" name="gud_profession" id="add_gud_profession" value="" placeholder="Profession" type="text">
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>                                   
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_religion">Religion </label>
                                                     <input class="form-control col-md-7 col-xs-12" name="gud_religion" id="add_gud_religion" value="" placeholder="Religion" type="text">
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_national_id">National ID</label>
                                                     <input class="form-control col-md-7 col-xs-12" name="gud_national_id" id="add_gud_national_id" value="" placeholder="National ID" type="text">
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>
                                             <div class="col-md-3 col-sm-3 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_username">Username</label>
                                                     <input class="form-control col-md-7 col-xs-12" name="gud_username" id="add_gud_username" value="" placeholder="Username" type="text" required="required">
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>                                        
     
                                         </div>
                                         
                                         <div class="row">    
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_present_address"> Address</label>
                                                     <textarea class="form-control col-md-7 col-xs-12 textarea-4column" name="gud_present_address" id="add_gud_present_address" placeholder=" Address"></textarea>
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>
     
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="gud_permanent_address"> Address</label>
                                                     <textarea class="form-control col-md-7 col-xs-12 textarea-4column" name="gud_permanent_address" id="add_gud_permanent_address" placeholder=" Address"></textarea>
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>  
                                         </div>
                                         
                                         <div class="row">
                                             <div class="col-md-6 col-sm-6 col-xs-12">
                                                 <div class="row mb-2">
                                                     <label for="other_info">Other Info </label>
                                                     <textarea class="form-control col-md-7 col-xs-12 textarea-4column" name="gud_other_info" id="add_gud_other_info" placeholder="Other Info"></textarea>
                                                     <div class="help-block"></div>
                                                 </div>
                                             </div>                                        
                                         </div>
                                         
                                     </div>
                                     
                                     <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title">
                                                 <strong>
                                                 Address Information: 
                                                 </strong>
                                                 Same as Guarduan Address <input class="" name="same_as_guardian" id="same_as_guardian" value="1" type="checkbox">
                                             </h5>
                                         </div>
                                     </div>
                                     <div class="row">   
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                              <div class="row mb-2">
                                                  <label for="present_address">Present Address </label>
                                                   <textarea class="form-control col-md-7 col-xs-12 textarea-4column" name="present_address" id="add_present_address" placeholder="Present Address"></textarea>
                                                   <div class="help-block"></div>
                                              </div>
                                          </div>                                    
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="permanent_address">Permanent Address</label>
                                                 <textarea class="form-control col-md-7 col-xs-12 textarea-4column" name="permanent_address" id="add_permanent_address" placeholder="Permanent Address"></textarea>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                     </div>
                                     <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title"><strong>Previous School:</strong></h5>
                                         </div>
                                     </div>
                                     <div class="row"> 
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="previous_school">School Name </label>
                                                 <input class="form-control col-md-7 col-xs-12" name="previous_school" id="previous_school" value="" placeholder="School Name" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="previous_class">Class </label>
                                                 <input class="form-control col-md-7 col-xs-12" name="previous_class" id="previous_class" value="" placeholder="Previous Class" type="text" autocomplete="off">
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                         
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                                 <label>Transfer Certificate </label>
                                                 <div class="btn btn-default btn-file">                                               <input class="form-control col-md-7 col-xs-12" name="transfer_certificate" id="transfer_certificate" type="file">
                                                 </div>
                                                 <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                         
                                     </div>
                                        
                                     
                                    <div class="row">                  
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <h5 class="column-title"><strong> Other Information:</strong></h5>
                                         </div>
                                     </div>    
                                     <div class="row">
                                      
                                      <div class="col-md-3 col-sm-3 col-xs-12">
                                          <div class="row mb-2">
                                             <label for="username">Username <span class="required">*</span></label>
                                             <input class="form-control col-md-7 col-xs-12" name="username" id="username" value="" placeholder="Username" required="required" type="text" autocomplete="off">
                                             <div class="help-block"></div>
                                          </div>
                                      </div>
                                      <div class="col-md-3 col-sm-3 col-xs-12">
                                          <div class="row mb-2">
                                             <label for="password">Password <span class="required">*</span></label>
                                             <input class="form-control col-md-7 col-xs-12" name="password" id="password" value="" placeholder="Password" required="required" type="text" autocomplete="off">
                                             <div class="help-block"></div>
                                          </div>
                                      </div>
                                      <div class="col-md-3 col-sm-3 col-xs-12">
                                          <div class="row mb-2">
                                             <label for="health_condition">Health Condition </label>
                                             <input class="form-control col-md-7 col-xs-12" name="health_condition" id="health_condition" value="" placeholder="Health Condition" type="text" autocomplete="off">
                                             <div class="help-block"></div>
                                          </div>
                                      </div>
                                 </div>
                                  <div class="row">                                     
                                          <div class="col-md-6 col-sm-6 col-xs-12">
                                              <div class="row mb-2">
                                                 <label for="other_info">Other Info</label> 
                                                 <textarea class="form-control col-md-6 col-xs-12 textarea-4column" name="other_info" id="other_info" placeholder="Other Info"></textarea>
                                                 <div class="help-block"></div>
                                              </div>
                                          </div>
                                         <div class="col-md-3 col-sm-3 col-xs-12">
                                             <div class="row mb-2">
                                                 <label>Photo</label>
                                                 <div class="btn btn-default btn-file">                                              <input class="form-control col-md-7 col-xs-12" name="photo" id="photo" type="file">
                                                 </div>
                                                 <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
                                                 <div class="help-block"></div>
                                             </div>
                                         </div>
                                          <div class="col-md-3 col-sm-3 col-xs-12">
                                              <div class="row mb-2">
                                              </div>
                                          </div>                                    
                                     </div>
                                                                     
                                     <div class="ln_solid"></div>
                                     <div class="row mb-2 ">
                                         <div class="col-md-6 col-md-offset-3">
                                             <input type="hidden" id="role_id" name="role_id" value="4">
                                             <a href="#" class="btn btn-primary">Cancel</a>
                                             <button id="send" type="submit" class="btn btn-success">Submit</button>
                                         </div>
                                     </div>
                                     </form>                                
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                         <div class="instructions"><strong>Instruction: </strong> Please add Guardian, Class &amp; Section before add Student.</div>
                                     </div>
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection