@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-md-8 col-lg-8 col-xl-8">
            <div class="row row-cols-1 row-cols-md-3 row-cols-xl-5">
                @if(request()->has('type') AND request()->query('type')!='')
                    @switch(request()->query('type'))
                        @case(\App\Enum\MainNavEnum::CONFIGURATIONS)
                            @include('dashboard.partials.configurations-left-bar')
                        @break

                        @case(\App\Enum\MainNavEnum::COLLABORATION)
                            @include('dashboard.partials.collaboration-left-bar')
                        @break

                        @case(\App\Enum\MainNavEnum::HRM)
                            @include('dashboard.partials.hrm-left-bar')
                        @break
                        @case(\App\Enum\MainNavEnum::OPERATIONS)
                            @include('dashboard.partials.operations-left-bar')
                        @break
                        @case(\App\Enum\MainNavEnum::FINANCE)
                            @include('dashboard.partials.finance-left-bar')
                        @break
                        @case(\App\Enum\MainNavEnum::DEFINITIONS)
                        @include('dashboard.partials.definition-left-bar')
                        @break
                    @endswitch
                @else
                    @foreach(\App\Enum\MainNavEnum::getTranslationKeys() as $key=>$value)
                        <div class="col box-color">
                            <div class="card radius-10">
                                <div class="card-body">
                                    <div class="text-center">
                                        <div class="widgets-icons rounded-circle mx-auto  mb-3  bg-transparent">
                                            {!! \App\Enum\MainNavEnum::getIcon($key) !!}
                                        </div>
                                        <a class="mb-0 "
                                           href="{!! \App\Enum\MainNavEnum::getRoute($key) !!}">
                                            {!! $value !!}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="col-md-4 col-lg-4 col-xl-4">
            @include('components.calendar')
        </div>
    </div>
@endsection