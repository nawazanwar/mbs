<div class="row mb-2">
    {!! html::decode(Form::label('exam_term', __('school.exam_term') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('exam_term', [], null, ['id' => 'exam_term', 'class' => 'form-control form-select col-md-7 col-xs-12', 'placeholder' => 'Select Term', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>
@include('components.fields.class-list')
@include('components.fields.subject-list')


<div class="row mb-2">
    {!! html::decode(Form::label('receiver_type', __('school.receiver_type') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('receiver_type',[], null, ['id' => 'receiver_type', 'class' => 'form-control form-select col-md-7 col-xs-12', 'placeholder' => 'Select Type', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! html::decode(Form::label('receiver', __('school.receiver') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('receiver',[], null, ['id' => 'receiver', 'class' => 'form-control form-select col-md-7 col-xs-12', 'placeholder' => 'Select Reciever', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('body',__('school.sms'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::textarea('body',null,['id'=>'body','class'=>'form-control col-md-7 col-xs-12', 'placeholder' => 'SMS Body' , 'rows' => 3]) !!}
    </div>
</div>

<div class="row mb-2">
    {!! html::decode(Form::label('sms_gateway', __('school.gateway') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('sms_gateway',[], null, ['id' => 'sms_gateway', 'class' => 'form-control form-select col-md-7 col-xs-12', 'placeholder' => 'Select Reciever', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>
