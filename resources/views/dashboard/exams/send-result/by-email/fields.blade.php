<div class="row mb-2">
    {!! html::decode(Form::label('exam_id', __('school.exam_term') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('exam_id', \App\Services\ExamService::getExamTitleForDropdown(), null, ['id' => 'exam_id', 'class' => 'form-control form-select col-md-7 col-xs-12', 'placeholder' => 'Select Term', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

@include('components.fields.class-list')

@include('components.fields.subject-list')

<div class="row mb-2">
    {!! html::decode(Form::label('receiver', __('school.receiver') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('receiver', [] , null, ['id' => 'receiver', 'class' => 'form-select form-control col-md-7 col-xs-12', 'placeholder' => 'Select Type', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! Form::label('body',__('school.body'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::textarea('body',null,['id'=>'body','class'=>'form-control col-md-7 col-xs-12', 'placeholder' => 'Email Body' , 'rows' => 3]) !!}
    </div>
</div>
