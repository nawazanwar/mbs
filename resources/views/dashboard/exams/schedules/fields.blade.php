

    <div class="row mb-2">
        {!! html::decode(Form::label('exam_id', __('school.exam_term') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('exam_id', \App\Services\ExamService::getExamTitleForDropdown(), null, ['id' => 'exam_id', 'class' => 'form-control form-select col-md-7 col-xs-12', 'placeholder' => 'Select Term', 'required', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    @include('components.fields.class-list')


    @include('components.fields.subject-list')


    <div class="row mb-2">
        {!! html::decode(Form::label('exam_date', __('school.exam_date') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::date('exam_date', null, ['id' => 'exam_date', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => '', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    <div class="row mb-2">
        {!! html::decode(Form::label('start_time', __('school.start_time') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::time('start_time', null, ['id' => 'start_time', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => '', 'required', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    <div class="row mb-2">
        {!! html::decode(Form::label('end_time', __('school.end_time') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::time('end_time', null, ['id' => 'end_time', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => '', 'required', 'autocomplete' => 'off']) !!}
        </div>
    </div>
    <div class="row mb-2">
        {!! html::decode(Form::label('room_no', __('school.room_no') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::number('room_no', null, ['id' => 'room_no', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Room No', 'required', 'autocomplete' => 'off']) !!}
        </div>
    </div>

    @include('components.fields.note')
