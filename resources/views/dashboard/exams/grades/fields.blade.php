<div class="row mb-2">
    {!! html::decode(Form::label('grade_name', __('school.grade_name') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('grade_name', null, ['id' => 'grade_name', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Name', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! html::decode(Form::label('grade_point', __('school.grade_point') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('grade_point', null, ['id' => 'grade_point', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Enter Point', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! html::decode(Form::label('mark_from', __('school.mark_from') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('mark_from', null, ['id' => 'mark_from', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'From', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

<div class="row mb-2">
    {!! html::decode(Form::label('mark_to', __('school.mark_to') . '<i class="text-danger">*</i>', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('mark_to', null, ['id' => 'mark_to', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'To', 'required', 'autocomplete' => 'off']) !!}
    </div>
</div>

@include('components.fields.note')
