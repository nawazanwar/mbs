<form action="http://localhost/myalif/hostel/room/add.html" name="add" id="add" class="form-horizontal form-label-left"
      method="post" accept-charset="utf-8" novalidate="novalidate">


     <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_no">Room No <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input class="form-control col-md-7 col-xs-12" name="room_no" id="room_no" value="" placeholder="Room No"
                   required="required" type="text" autocomplete="off">
            <div class="help-block"></div>
        </div>
    </div>

     <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="room_type">Room Type <span
                    class="required">*</span></label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="room_type" id="room_type" required="required">
                <option value="">--Select--</option>
                <option value="ac">AC</option>
                <option value="non_ac">Non AC</option>
            </select>
            <div class="help-block"></div>
        </div>
    </div>
     <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="total_seat">Seat Total <span
                    class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input class="form-control col-md-7 col-xs-12" name="total_seat" id="total_seat" value=""
                   placeholder="Seat Total" required="required" type="number" autocomplete="off">
            <div class="help-block"></div>
        </div>
    </div>
     <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hostel_id">Hostel <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <select class="form-control col-md-7 col-xs-12" name="hostel_id" id="add_hostel_id" required="required">
                <option value="">--Select--</option>

            </select>
            <div class="help-block"></div>
        </div>
    </div>

     <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cost">Cost per Seat</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <input class="form-control col-md-7 col-xs-12" name="cost" id="cost" value="" placeholder="Cost per Seat"
                   type="number" autocomplete="off">
            <div class="help-block"></div>
        </div>
    </div>

     <div class="row mb-2">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="note">Note</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea class="form-control col-md-7 col-xs-12" name="note" id="note" placeholder="Note"></textarea>
            <div class="help-block"></div>
        </div>
    </div>

    <div class="ln_solid"></div>
    <div class="row mb-2">
        <div class="col-md-6 col-md-offset-3">
            <a href="http://localhost/myalif/hostel/room/index.html" class="btn btn-primary">Cancel</a>
            <button id="send" type="submit" class="btn btn-success">Submit</button>
        </div>
    </div>
</form>