@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ isset($d->school)?$d->school->name:"" }}
        </td>
        <td>
            <img src="{{ asset(isset($d->user)?$d->user->photo:null) }}" class="img-fluid avatar">
        </td>
        <td>
            {{ isset($d->user)?$d->user->name:null }}
        </td>
        <td>
            {{ $d->national_id }}
        </td>
        <td>
            {{ $d->responsibility }}
        </td>
        <td>
            {{ isset($d->user)?$d->user->phone:null }}
        </td>
        <td>
            {{ $d->gender }}
        </td>
        <td>
            {{ $d->blood_group }}
        </td>
        <td>
            {{ $d->religion }}
        </td>
        <td>
            {{ $d->dob }}
        </td>
        <td>
            {{ $d->present_address }}
        </td>
        <td>
            {{ $d->permanent_address }}
        </td>
        <td>
            {{ $d->salary_type }}
        </td>
        <td>
            {{ $d->username }}
        </td>
        <td>
            {{ $d->password }}
        </td>
        <td>
            {{ $d->salary_grade_id }}
        </td>
        <td>
            {{ isset($d->user)?$d->user->email:null }}
        </td>
        <td>
            {{ $d->joining_date }}
        </td>
        <td>
            {{ $d->resume }}
        </td>
        <td>
            {{ $d->is_view_on_web }}
        </td>
        <td>
            {{ $d->facebook_url }}
        </td>
        <td>
            {{ $d->linkedin_url }}
        </td>
        <td>
            {{ $d->twitter_url }}
        </td>
        <td>
            {{ $d->google_plus_url }}
        </td>
        <td>
            {{ $d->instagram_url }}
        </td>
        <td>
            {{ $d->youtube_url }}
        </td>
        <td>
            {{ $d->pinterest_url }}
        </td>
        <td>
            {{ $d->other_info }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.teachers.edit',[$d->id]),
                'delete'=>route('dashboard.teachers.destroy',[$d->id])
            ])
        </td>
    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse