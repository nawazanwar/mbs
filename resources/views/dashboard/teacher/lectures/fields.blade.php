@include('components.fields.title')
@include('components.fields.class-list')
@include('components.fields.section-list')
@include('components.fields.student-list')
<div class="row mb-2">
    {!! Form::label('lecture_type',__('school.lecture_type'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('lecture_type',\App\Services\LectureService::getLectureTypeForDropdown(),null,['id'=>'lecture_type','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_lecture_type')]) !!}
    </div>
</div>

@include('components.fields.note')