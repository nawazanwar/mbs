<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.academic_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! Form::label('email',__('school.email')) !!}
                {!! Form::email('email',null,['id'=>'email','class'=>'form-control']) !!}
                @error('email')
                <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('password',__('school.password').'<i class="text-danger">*</i>')) !!}
                {!! Form::password('password',['id'=>'password','class'=>'form-control','required' , 'autocomplete' => 'off']) !!}
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('salary_grade_id',__('school.salary_grade').'<i class="text-danger">*</i>')) !!}
                {!! Form::select('salary_grade_id',\App\Services\SalaryService::getSalaryGradeDropdown(),null,['id'=>'salary_grade_id','class'=>'form-control']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! HTML::decode(Form::label('salary_type',__('school.salary_type').'<i class="text-danger">*</i>')) !!}
                {!! Form::select('salary_type',\App\Services\SalaryService::getSalaryTypeDropdown(),null,['id'=>'salary_type','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('joining_date',__('school.joining_date'))) !!}
                {!! Form::date('joining_date',null,['id'=>'joining_date','class'=>'form-control','required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::label('resume',__('school.resume')) !!}
                {!! Form::file('resume',['id'=>'resume','class'=>'form-control']) !!}
                <div class="text-info">Valid file format submission. Ex: doc, docx, jpg, jpeg, pdf, ppt, pptx.</div>
            </div>
        </div>
    </div>
</div>