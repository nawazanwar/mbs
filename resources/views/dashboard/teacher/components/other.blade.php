<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.other_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! Form::label('is_view_on_web',__('school.is_view_on_web')) !!}
                {!! Form::select('is_view_on_web',array(),null,['id'=>'is_view_on_web','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('facebook_url',__('school.facebook_url')) !!}
                {!! Form::text('facebook_url',null,['id'=>'facebook_url','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('linkedin_url',__('school.linkedin_url')) !!}
                {!! Form::text('linkedin_url',null,['id'=>'linkedin_url','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('twitter_url',__('school.twitter_url')) !!}
                {!! Form::text('twitter_url',null,['id'=>'twitter_url','class'=>'form-control']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::label('google_plus_url',__('school.google_plus_url')) !!}
                {!! Form::text('google_plus_url',null,['id'=>'google_plus_url','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('instagram_url',__('school.instagram_url')) !!}
                {!! Form::text('instagram_url',null,['id'=>'instagram_url','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('youtube_url',__('school.youtube_url')) !!}
                {!! Form::text('youtube_url',null,['id'=>'youtube_url','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('pinterest_url',__('school.pinterest_url')) !!}
                {!! Form::text('pinterest_url',null,['id'=>'pinterest_url','class'=>'form-control']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::label('other_info',__('school.other_info')) !!}
                {!! Form::textarea('other_info',null,['id'=>'other_info','class'=>'form-control','rows'=>1]) !!}
            </div>
            <div class="col">
                {!! Form::label('photo',__('school.photo')) !!}
                {!! Form::file('photo',['id'=>'photo','class'=>'form-control']) !!}
                <div class="text-info">Valid file format submission. Ex: doc, docx, jpg, jpeg, pdf, ppt,
                    pptx.
                </div>
            </div>
        </div>
    </div>
</div>