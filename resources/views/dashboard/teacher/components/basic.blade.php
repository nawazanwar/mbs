<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.basic_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row mb-2">
            <div class="col">
                {!! HTML::decode(Form::label('name',__('school.name').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('name',null,['id'=>'name','class'=>'form-control','required']) !!}
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('national_id',__('school.national_id').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('national_id',null,['id'=>'national_id','class'=>'form-control','required']) !!}
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('responsibility',__('school.responsibility').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('responsibility',null,['id'=>'responsibility','class'=>'form-control','required']) !!}
            </div>
            <div class="col">
                {!! Form::label('phone',__('school.phone')) !!}
                {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control']) !!}
            </div>
        </div>
        <div class="row mb-2">
            <div class="col ">
                {!! Form::label('gender',__('school.gender'),['class'=>'form-label']) !!}
                {!! Form::select('gender',\App\Services\UserService::getGenderForDropdown(),null,['id'=>'gender','class'=>'form-control','placeholder'=>'Select Gender']) !!}
            </div>

            <div class="col">
                {!! Form::label('blood_group',__('school.blood_group'),['class'=>'form-label']) !!}
                {!! Form::select('blood_group',\App\Services\UserService::getBloodGroupDropdown(),null,['id'=>'blood_group','class'=>'form-control','placeholder'=>'Select Blood Group']) !!}
            </div>

            <div class="col">
                {!! Form::label('religion',__('school.religion')) !!}
                {!! Form::text('religion',null,['id'=>'religion','class'=>'form-control','rows'=>2]) !!}
            </div>

            <div class="col">
                {!! HTML::decode(Form::label('dob',__('school.dob').'<i class="text-danger">*</i>')) !!}
                {!! Form::date('dob',null,['id'=>'dob','class'=>'form-control','rows'=>2,'required']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::label('present_address',__('school.present_address')) !!}
                {!! Form::textarea('present_address',null,['id'=>'present_address','class'=>'form-control','rows'=>2]) !!}
            </div>
            <div class="col">
                {!! Form::label('permanent_address',__('school.permanent_address')) !!}
                {!! Form::textarea('permanent_address',null,['id'=>'permanent_address','class'=>'form-control','rows'=>2]) !!}
            </div>
        </div>
    </div>
</div>