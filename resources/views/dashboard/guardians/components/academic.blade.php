<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.academic_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! Form::label('national_id',__('school.national_id')) !!}
                {!! Form::text('national_id',null,['id'=>'national_id','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('email',__('school.email').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('email',null,['id'=>'email','class'=>'form-control','required']) !!}
                @error('email')
                <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('password',__('school.password').'<i class="text-danger">*</i>')) !!}
                {!! Form::password('password',['id'=>'password','class'=>'form-control','required']) !!}
            </div>
        </div>
    </div>
</div>