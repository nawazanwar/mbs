<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.basic_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! HTML::decode(Form::label('name',__('school.name').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('name',null,['id'=>'name','class'=>'form-control','required']) !!}
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('phone',__('school.phone').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control','required']) !!}
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('profession',__('school.profession').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('profession',null,['id'=>'profession','class'=>'form-control','required']) !!}
            </div>
            <div class="col">
                {!! Form::label('religion',__('school.religion')) !!}
                {!! Form::text('religion',null,['id'=>'religion','class'=>'form-control']) !!}
            </div>
        </div>
        <div class="row">
            <div class="col">
                {!! Form::label('present_address',__('school.present_address')) !!}
                {!! Form::textarea('present_address',null,['id'=>'present_address','class'=>'form-control','rows'=>2]) !!}
            </div>
            <div class="col">
                {!! Form::label('permanent_address',__('school.permanent_address')) !!}
                {!! Form::textarea('permanent_address',null,['id'=>'permanent_address','class'=>'form-control','rows'=>2]) !!}
            </div>
        </div>
    </div>
</div>