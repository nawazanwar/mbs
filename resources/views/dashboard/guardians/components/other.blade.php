<div class="card shadow-none border">
<div class="card-header bg-white">
    <h6>{{__('school.other_information')}}</h6>
</div>
<div class="card-body">
    <div class="row">
        <div class="col">
            {!! Form::label('other_info',__('school.other_info')) !!}
            {!! Form::text('other_info',null,['id'=>'other_info','class'=>'form-control']) !!}
        </div>
        <div class="col">
            {!! Form::label('photo',__('school.photo')) !!}
            {!! Form::file('photo',['id'=>'photo','class'=>'form-control']) !!}
            <div class="text-info">Valid file format submission. Ex: doc, docx, jpg, jpeg, pdf, ppt, pptx.</div>
        </div>
    </div>
</div>
</div>