@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-12 col-md-2 col-lg-2 col-xl-2 col-xxl-2">
            @include('dashboard.partials.home')
        </div>
        <div class="col-12 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button bg-white " type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {{__('school.create_guardian')}}
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show bg-white" aria-labelledby="headingOne"
                         data-bs-parent="#accordionExample" style="">
                        <div class="accordion-body pt-2">
                            <div class="card shadow-none pt-0">
                                @include('components.fields.form-create-header',['url'=>'dashboard.guardians.index'])
                                <form action="http://myalif.local/guardian/add.html" name="add" id="add" class="form-horizontal form-label-left"
    enctype="multipart/form-data" method="post" accept-charset="utf-8" novalidate="novalidate">
                                <div class="card-body">
                                    @include('dashboard.guardians.field')
                                    @include('components.fields.store')
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection