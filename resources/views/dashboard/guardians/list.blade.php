@forelse ($data as $d)
    <tr>
        <td class='text-center'>
            {{ $d->id }}
        </td>
        <td>
            {{ isset($d->school)?$d->school->name:"" }}
        </td>
        <td>
            <img src="{{ asset(isset($d->user)?$d->user->photo:null) }}" class="img-fluid avatar">
        </td>
        <td>
            {{ isset($d->user)?$d->user->name:null }}
        </td>
        <td>
            {{ isset($d->user)?$d->user->phone:null }}
        </td>
        <td>
            {{ $d->profession }}
        </td>
        <td>
            {{ isset($d->user)?$d->user->email:null }}
        </td>
        <td class="text-center">
            @include('components.common.action',[
                'edit'=>route('dashboard.guardians.edit',[$d->id]),
                'delete'=>route('dashboard.guardians.destroy',[$d->id])
            ])
        </td>

    </tr>
@empty
    <tr>
        <td colspan="20" class="text-center">
            {{__('school.no_record_found')}}
        </td>
    </tr>
@endforelse