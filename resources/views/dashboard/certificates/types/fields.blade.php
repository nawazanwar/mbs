    <div class="row mb-2">
        {{ Form::label('certificate_name', __('school.certificate'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('certificate_name', null, ['certificate_name', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Certificate Name']) !!}

        </div>
    </div>
    <div class="row mb-2">
        {{ Form::label('school_name', 'School Name*', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('school_name', null, ['school_name', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'School Name']) !!}

        </div>
    </div>
    <div class="row mb-2">
        {{ Form::label('certificate_text', 'Certificate Text*', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::textarea('certificate_text', null, ['certificate_text', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Certificate Text', 'rows' => 3]) !!}

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="fn_add_tag">[name] [email] [phone] [class_name] [section] [roll_no] [dob] [gender]
                    [religion] [blood_group] [registration_no] [group] [created_at] [guardian] [present_address]
                    [permanent_address]
                </div>
            </div>
        </div>

    </div>

    <div class="row mb-2">
        {{ Form::label('footer_text', 'Footer left Text', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('footer_left_text',null, ['', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Footer Left Text']) !!}


        </div>
    </div>

    <div class="row mb-2">
        {{ Form::label('footer_text', 'Footer Middle Text', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

        <div class="col-md-9 col-sm-9 col-xs-12">

            {!! Form::text('footer_middle_text',null, ['', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Footer Middle Text']) !!}

        </div>
    </div>

    <div class="row mb-2">
        {{ Form::label('footer_text', 'Footer Right Text', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}

        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::text('footer_right_text',null, ['', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Footer Right Text']) !!}

        </div>
    </div>

    <div class="row mb-2">
        {{ Form::label('backgroud', __('school.background'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
        <div class="col-md-9 col-sm-9 col-xs-12">
            {!! Form::file('footer_right_text', ['', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => 'Footer Right Text']) !!}
            <div class="text-info">jpeg,jpg [1300x700]</div>
        </div>
    </div>
