@extends('layouts.dashboard')
@section('content')
    <div class="row">
        <div class="col-12 col-md-2 col-lg-2 col-xl-2 col-xxl-2">
            <div class="row">
                @include('dashboard.partials.certificate-left-bar')
            </div>
        </div>
        <div class="col-12 col-md-10 col-lg-10 col-xl-10 col-xxl-10">
            <div class="accordion" id="accordionExample">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button bg-white " type="button" data-bs-toggle="collapse"
                                data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            {{__('school.manage_certificate')}}
                        </button>
                    </h2>
                    <div id="collapseOne" class="accordion-collapse collapse show bg-white" aria-labelledby="headingOne"
                         data-bs-parent="#accordionExample" style="">
                        <div class="accordion-body pt-2">
                            <div class="card shadow-none pt-0">
                                @include('components.fields.form-list-header')
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover">
                                            @include('components.fields.table-headings',['headings'=>\App\Enum\TableHeadingsForGuardian::getTranslationKeys()])
                                            <tbody>
                                            <tr>
                                                <td colspan="20" class="text-center">
                                                    {{__('school.no_record_found')}}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection