<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('plugins/fullcalendar/css/main.min.css') }}">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/icons.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <title>@yield('pageTitle')</title>
</head>
<body>
<div class="wrapper">
    @include('includes.dashboard-header')
    <div class="page-wrapper">
        <div class="page-content">
            @yield('content')
        </div>
    </div>
    <div class="overlay toggle-icon"></div>
    @include('includes.back-to-top')
</div>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/fullcalendar/js/main.min.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>