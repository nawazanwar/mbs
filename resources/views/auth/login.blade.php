@extends('layouts.auth')
@section('content')
    <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
        <div class="col mx-auto">
            <div class="card-0 shadow-none radius-10" style="background: #272560;">
                <div class="card-body radius-10">
                    <div class="p-4 rounded">
                        <div class="text-center">
                            <h3 class="text-white">{{__('Sign in')}}</h3>
                        </div>
                        <div class="form-body">
                            <form class="row g-3" method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="col-12">
                                    <label for="email" class="form-label text-white">{{ __('Email Address') }}</label>
                                    <input type="email"
                                           class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email"
                                           value="{{ old('email') }}"
                                           id="email"
                                           required="required"
                                           placeholder="{{__('Email Address')}}">
                                    @if ($errors->has('email'))
                                        <div class="error" role="alert">
                                            {{ $errors->first('email') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="col-12">
                                    <label for="password" class="form-label text-white">{{__('Enter Password')}}</label>
                                    <div class="input-group"
                                         id="show_hide_password">
                                        <input type="password"
                                               class="form-control border-end-0 {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                               id="password"
                                               name="password"
                                               required="required"
                                               placeholder="{{__('Enter Password')}}">
                                        <a href="javascript:;" class="input-group-text bg-transparent">
                                            <i class="bx bx-hide "></i>
                                        </a>
                                    </div>
                                    @if ($errors->has('password'))
                                        <div class="error" role="alert">
                                            {{ $errors->first('password') }}
                                        </div>
                                    @endif
                                </div>
                                <div class="col-12">
                                    <div class="d-grid">
                                        <button type="submit" class="btn bg-gradient text-white">
                                            <i class="bx bxs-lock-open "></i>{{__('Sign in')}}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
