<footer class="page-footer">
    <p class="mb-0">{{__('Copyright')}} © {{ date('Y') }}. {{__('All Right Reserved')}}.</p>
</footer>