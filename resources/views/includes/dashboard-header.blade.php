<header class="header-box">
    <div class="topbar d-flex align-items-center">
        <nav class="navbar navbar-expand">
            <div class="topbar-logo-header">
                <a href="{{ route('home') }}">
                    <img src="{{ asset('images/logo.png') }}" class="logo-icon" alt="logo icon">
                </a>
            </div>
            <div class="mobile-toggle-menu">
                <a href="{{ route('home') }}">
                    <i class="bx bx-home"></i>
                </a>
            </div>
            <div class="top-menu ms-auto">
                <ul class="navbar-nav align-items-center">
                    <li class="nav-item mobile-search-icon">
                        <a class="nav-link" href="#"> <i class="bx bx-search "></i>
                        </a>
                    </li>
                    <li class="nav-item dropdown dropdown-large">
                        <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" role="button"
                           data-bs-toggle="dropdown" aria-expanded="false">
                            <small class="fs-6 text-white">
                                {{ \App\Enum\LanguageEnum::getTranslationKeyBy(app()->getLocale())  }} <i
                                        class="bx bx-chevron-down"></i>
                            </small>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <div class="row row-cols-3 g-3 p-3">
                                @foreach(\App\Enum\LanguageEnum::getTranslationKeys()  as $key=>$language)
                                    <div class="col text-center">
                                        <a class="btn btn-sm  @if($key==app()->getLocale()) btn-success @else btn-primary"
                                           @endif
                                           href="{{ route('lang.switch', $key) }}">
                                            {{ $language }}
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            @auth
                <div class="user-box dropdown">
                    <a class="d-flex align-items-center nav-link dropdown-toggle dropdown-toggle-nocaret" href="#"
                       role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="{{ asset('images/avatars/avatar-1.png') }}" class="user-img" alt="user avatar">
                        <div class="user-info ps-3">
                            <p class="user-name mb-0 text-white">{{ \Illuminate\Support\Facades\Auth::user()->getName() }}</p>
                            <p class="designattion mb-0 text-white">{{ \Illuminate\Support\Facades\Auth::user()->getRoleLabel() }}</p>
                        </div>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end">
                        <li>
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="dropdown-item" href="javascript:;">
                                    <i class="bx bx-log-out-circle "></i><span>{{__('school.logout')}}</span>
                                </button>
                            </form>
                        </li>
                    </ul>
                </div>
            @endauth
            @guest
                <a class="btn btn-primary" href="{{ route('login') }}">
                    {{__('school.login_to_school')}} <i class="bx bx-log-in "></i>
                </a>
            @endguest
        </nav>
    </div>
</header>