@if ($paginator->hasPages())
    <ul class="pagination justify-content-center py-2 mb-0" id="pagination-wrapper">
        @if ($paginator->onFirstPage())
            <li class="page-item first-and-last disabled">
                <a href="javascript:void(0);" class="bg-light page-link border-0 ">
                    <i class="bx bx-arrow-to-left"></i>
                </a>
            </li>
        @else
            <li class="page-item first-and-last">
                <a href="{{ $paginator->previousPageUrl() }}" class="bg-light page-link border-0 ">
                    <i class="bx bx-arrow-to-left"></i>
                </a>
            </li>
        @endif
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="page-item disabled mx-2">
                    <a class="page-link bg-transparent  border-0">{{ $element }}</a>
                </li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item mx-2">
                            <a class="page-link  border-0 active">{{ $page }}</a>
                        </li>
                    @else
                        <li class="page-item mx-2">
                            <a class="page-link bg-transparent  border-0 text-lightslategray font-weight-bold"
                               href="{{ $url }}">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <li class="page-item first-and-last">
                <a href="{{ $paginator->nextPageUrl() }}" class="bg-light page-link border-0 ">
                    <i class="bx bx-arrow-to-right"></i>
                </a>
            </li>
        @else
            <li class="page-item first-and-last disabled">
                <a href="javascript:void(0);" class="bg-light page-link border-0 ">
                    <i class="bx bx-arrow-to-right"></i>
                </a>
            </li>
        @endif
    </ul>
@endif