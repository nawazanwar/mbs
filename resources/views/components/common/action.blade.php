<div class="btn-group">
    <button type="button" class="btn btn-outline-secondary btn-sm">{{__('school.action')}}</button>
    <button type="button" class="btn btn-outline-secondary btn-sm dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">	<span class="visually-hidden">Toggle Dropdown</span>
    </button>
    <ul class="dropdown-menu">
        <li>
            <a class="dropdown-item text-black-50" href="{{$edit}}">{{__('school.edit')}}</a>
        </li>
        <li>
            <a class="dropdown-item text-black-50" href="{{$delete}}">{{__('school.delete')}}</a>
        </li>
    </ul>
</div>