<div class="page-content">
    <div class="card shadow-none">
        <div class="card-header">
            <div class="row align-items-center">
                <div class="col-lg-4 col-xl-3">
                    <a href="{{ route('registration.create') }}" class="btn btn-primary mb-3 mb-lg-0">
                        <i class="bx bxs-plus-square"></i> {{__('school.register_new_school')}}
                    </a>
                </div>
                <div class="col-lg-8 col-xl-9">
                    <form class="float-lg-end mb-0">
                        <div class="row row-cols-lg-auto g-2">
                            <div class="col-12">
                                <div class="position-relative">
                                    <input type="text" class="form-control ps-5" placeholder="{{__('school.search_here')}}">
                                    <span class="position-absolute top-50 product-show translate-middle-y p-lg-3">
                                        <i class="bx bx-search"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-4 ">
                @foreach(\App\Services\SchoolService::getAllSchools() as $key=>$school)
                    <div class="col box-color">
                        <div class="card radius-15">
                            <div class="card-body text-center">
                                <div class="p-4 radius-15">
                                    <img src="{{ asset($school->logo) }}" width="110" height="110"
                                         onerror="this.src='{{ asset('images/default_school_logo.png') }}'"
                                         class="rounded-circle shadow p-1 bg-white" alt="">
                                    <h5 class="mb-0 mt-5 text-white">{{ $school->name }}</h5>
                                    <p class="mb-3 text-white">{{ $school->code }}</p>
                                    <div class="list-inline contacts-social mt-3 mb-3">
                                        <a href="{{ $school->facebook }}" class="list-inline-item border-0">
                                            <i class="bx bxl-facebook "></i>
                                        </a>
                                        <a href="{{ $school->twitter }}" class="list-inline-item border-0">
                                            <i class="bx bxl-twitter "></i>
                                        </a>
                                        <a href="{{ $school->google_plus }}" class="list-inline-item border-0">
                                            <i class="bx bxl-google "></i>
                                        </a>
                                        <a href="{{ $school->linkedin }}" class="list-inline-item border-0">
                                            <i class="bx bxl-linkedin "></i>
                                        </a>
                                    </div>
                                    <div class="d-grid">
                                        <a href="{{ route('dashboard.index',['sId'=>$school->id]) }}"
                                           class="btn btn-white radius-15 ">
                                            {{__('school.manage_school')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>