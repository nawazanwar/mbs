<div class="card-footer overflow-auto">
   {{ $data->links('components.pagination.custom') }}
</div>