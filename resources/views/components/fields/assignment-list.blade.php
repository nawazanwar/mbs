<div class="row mb-2">
    {!! Form::label('assignment_id',__('school.assignment'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('assignment_id',\App\Services\AssignmentService::getAssignmentForDropdown(),null,['id'=>'assignment_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_assignment')]) !!}
    </div>
</div>