<div class="row mb-2">
    {!! Form::label('hostel_id',__('school.hostel'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('hostel_id',\App\Services\HostelService::getHostelsForDropdown(),null,['id'=>'hostel_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_hostel')]) !!}
    </div>
</div>

