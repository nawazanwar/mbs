<div class="row mb-2">
    {{ Form::label('teacher_id', __('school.teacher'), ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) }}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('teacher_id', \App\Services\TeacherService::getTeacherDropdown(), null, ['id' => 'teacher_id', 'class' => 'select2 form-control custom-select float-right', 'placeholder' => 'Select Teacher']) !!}
    </div>
</div>
