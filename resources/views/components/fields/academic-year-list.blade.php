<div class="row mb-2">
    {!! Form::label('academic_year_id',__('school.academic_year'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('academic_year_id',\App\Services\AcademicYearService::getAcademicYearForDropdown(),null,['id'=>'academic_year_id','class'=>'form-control col-md-7 col-xs-12 form-select','placeholder'=>__('school.ph_select_academic_year')]) !!}
    </div>
</div>

