
<div class="row mb-2">
    {!! Form::label('note',__('school.note'),['class'=>'form-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12 ">
        {!! Form::textarea('note',null,['id'=>'note','class'=>'form-control col-md-7 col-xs-12', 'placeholder' => 'Note' , 'rows' => 2]) !!}
    </div>
</div>