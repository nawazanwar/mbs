<div class="row mb-2">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Attachment</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::file('attachment', ['id' => 'attachment', 'class' => 'form-control col-md-7 col-xs-12', 'placeholder' => '', 'autocomplete' => 'off']) !!}
        <div class="text-info">Document file format: .pdf, .doc/docx, .ppt/pptx or .txt</div>
        <div class="help-block"></div>
    </div>
</div>