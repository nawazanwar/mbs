<div class="card-header border-0" style="background-color: transparent !important;">
    <div class="row">
        <div class="col"></div>
        <div class="col justify-content-end d-flex">
            {!! HTML::decode(link_to_route($url,'<i class="bx bx-arrow-to-left fs-6"></i>'.__('school.back'),null,['class'=>'btn btn-sm btn-primary pr-0'])) !!}
        </div>
    </div>
</div>