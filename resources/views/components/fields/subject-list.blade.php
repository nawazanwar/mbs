<div class="row mb-2">
    {!! Form::label('subject_id',__('school.subject'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('subject_id',\App\Services\SubjectService::getSubjectForDropdown(),null,['id'=>'subject_id','class'=>'form-control form-select col-md-7 col-xs-12','placeholder'=>__('school.ph_subject')]) !!}
    </div>
</div>

