<div class="row mb-2">
    {!! Form::label('address',__('school.address'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('address',null,['id'=>'address','class'=>'form-control col-md-7 col-xs-12']) !!}
    </div>
</div>