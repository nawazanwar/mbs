<div class="row mb-2">
    {!! HTML::decode(Form::label('title',__('school.title').'<i class="text-danger">*</i>',['class'=>'control-label col-md-3 col-sm-3 col-xs-12'])) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::text('title',null,['id'=>'title','class'=>'form-control col-md-7 col-xs-12','required']) !!}
        @error('title')
        <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
        @enderror
    </div>
</div>

