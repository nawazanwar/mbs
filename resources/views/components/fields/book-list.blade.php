<div class="row mb-2">
    {!! Form::label('book_id',__('school.book'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('book_id',\App\Services\BookService::getBooksForDropdown(),null,['id'=>'book_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_book')]) !!}
    </div>
</div>

