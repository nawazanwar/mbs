<div class="row mb-2">
    {!! Form::label('class_id',__('school.class'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('class_id',\App\Services\ClassService::getClassesForDropdown(),null,['id'=>'class_id','class'=>'form-control col-md-7 col-xs-12 form-select','placeholder'=>__('school.ph_select_class')]) !!}
    </div>
</div>

