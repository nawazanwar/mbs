
<div class="row mb-2">
    {!! Form::label('user_id',__('school.user'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('user_id',\App\Services\UserService::getUsersForDropdown(),null,['id'=>'user_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_user')]) !!}
    </div>
</div>

