<div class="card-header border-0" style="background-color: transparent !important;">
    <div class="row">
        <div class="col">
            <a class="btn btn-primary btn-sm">
                {{__('school.excel')}}
            </a>
            <a class="btn btn-primary btn-sm">
                {{__('school.csv')}}
            </a>
            <a class="btn btn-primary btn-sm">
                {{__('school.pdf')}}
            </a>
        </div>
        <div class="col justify-content-end d-flex">
            <form method="GET" action="{{ request()->fullUrl() }}" class="mb-0">
                <div class="input-group input-group-sm w-auto me-2">
                    <input type="text" name="s" @if(request()->has('s')) value="{{ request()->get('s') }}"
                           @endif class="form-control"
                           placeholder="{{__('school.search_here')}}">
                    <button class="btn btn-primary" type="submit">
                        <i class="bx bx-search fs-6 pt-1 mx-auto"></i>
                    </button>
                </div>
            </form>
            @if(isset($is_create) AND $is_create==true)
                {!! HTML::decode(link_to_route($url,__('school.create').' <i class="bx bx-plus-circle fs-6 "></i>',null,['class'=>'btn btn-sm btn-primary pr-0'])) !!}
            @endif
        </div>
    </div>
</div>