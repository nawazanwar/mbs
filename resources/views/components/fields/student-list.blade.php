<div class="row mb-2">

    {!! Form::label('student_id',__('school.student'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('student_id',\App\Services\StudentService::getStudentsForDropdown(),null,['id'=>'student_id','class'=>'form-control form-select col-md-7 col-xs-12','placeholder'=>__('school.ph_select_student')]) !!}
    </div>
</div>

