<div class="row my-3">
    <div class="col-12 text-center">
        <button type="button" class="btn btn-danger" onclick="window.history.back();">{{__('school.cancel')}}</button>
        <button  type="submit" class="btn btn-success">{{__('school.save')}}</button>
    </div>
</div>