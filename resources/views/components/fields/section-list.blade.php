<div class="row mb-2">
    {!! Form::label('section_id',__('school.section'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('section_id',\App\Services\SectionService::getSectionForDropdown(),null,['id'=>'section_id','class'=>'form-control form-select col-md-7 col-xs-12','placeholder'=>__('school.ph_select_section')]) !!}
    </div>
</div>

