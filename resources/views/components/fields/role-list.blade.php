<div class="row mb-2">
    {!! Form::label('role_id',__('school.role'),['class'=>'control-label col-md-3 col-sm-3 col-xs-12']) !!}
    <div class="col-md-6 col-sm-6 col-xs-12">
        {!! Form::select('role_id',\App\Services\RoleService::getRolesForDropdown(),null,['id'=>'role_id','class'=>'form-control col-md-7 col-xs-12','placeholder'=>__('school.ph_select_role')]) !!}
    </div>
</div>

