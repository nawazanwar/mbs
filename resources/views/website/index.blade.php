@extends('layouts.app')
@section('content')
    @auth
        @if(\Illuminate\Support\Facades\Auth::user()->hasRole(\App\Enum\RoleEnum::ROLE_SUPER_ADMIN))
            @include('components.dashboard-super-admin')
        @endif
    @endauth
@endsection