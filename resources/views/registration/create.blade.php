@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card shadow-none pt-0">
                <div class="card-header bg-white">
                    <h5>{{ __('school.register_new_school') }}</h5>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => ['registration.store'], 'method' => 'POST','files' => true]) !!}
                    @csrf
                    @include('components.fields.created-by')
                    @include('registration.fields')
                    @include('components.fields.store')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection