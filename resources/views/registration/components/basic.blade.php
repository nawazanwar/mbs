<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.basic_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="row">
            <div class="col">
                {!! HTML::decode(Form::label('code',__('school.code').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('code',null,['id'=>'code','class'=>'form-control','required']) !!}
                @error('code')
                <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('name',__('school.name').'<i class="text-danger">*</i>')) !!}
                {!! Form::text('name',null,['id'=>'name','class'=>'form-control','required']) !!}
                @error('name')
                <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="col">
                {!! HTML::decode(Form::label('email',__('school.email').'<i class="text-danger">*</i>')) !!}
                {!! Form::email('email',null,['id'=>'email','class'=>'form-control']) !!}
                @error('email')
                <small class="mt-1 mb-1 text-danger">{{ $message }}</small>
                @enderror
            </div>
            <div class="col">
                {!! Form::label('phone',__('school.phone')) !!}
                {!! Form::text('phone',null,['id'=>'phone','class'=>'form-control']) !!}
            </div>
        </div>

        <div class="row">
            <div class="col">
                {!! Form::label('reg_date',__('school.reg_date')) !!}
                {!! Form::date('reg_date',null,['id'=>'reg_date','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('address',__('school.address')) !!}
                {!! Form::text('address',null,['id'=>'address','class'=>'form-control']) !!}
            </div>
            <div class="col">
                {!! Form::label('fax',__('school.fax')) !!}
                {!! Form::text('fax',null,['id'=>'fax','class'=>'form-control']) !!}
            </div>
        </div>
    </div>
</div>