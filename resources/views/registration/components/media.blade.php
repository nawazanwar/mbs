<div class="card shadow-none border">
    <div class="card-header bg-white">
        <h6>{{__('school.media_information')}}</h6>
    </div>
    <div class="card-body">
        <div class="item row mb-2">
            {!! Form::label('logo',__('school.logo')) !!}
            <div class="col-md-6 col-sm-6 col-xs-12 ">
                {!! Form::file('logo',['id'=>'logo','class'=>'form-control']) !!}
                <div class="text-info">Image file format: .jpg, .jpeg, .png or .gif</div>
            </div>
        </div>
    </div>
</div>