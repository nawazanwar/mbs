<?php

namespace App\Services;

use App\Models\IncomeHead;
use App\Models\PayrollSalaryGrade;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class SalaryService
{
    public static function getSalaryGradeDropdown()
    {
        return PayrollSalaryGrade::pluck('grade_name', 'id');

    }

    public static function getSalaryTypeDropdown(): array
    {
        return [
            'hourly' => __('school.hourly'),
            'daily' => __('school.daily'),
            'weekly' => __('school.weekly'),
            'monthly' => __('school.monthly')
        ];
    }


    public function applyGlobalSearchForSalaryGrade(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
}
