<?php

namespace App\Services;


use App\Models\Discount;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class DiscountService
{
    public static function getDiscountForDropdown(): array
    {
        return array(
            'flat' => __('school.flat'),
            'percentage' => __('school.percentage'),

        );
    }

    public function applyGlobalSearchForDiscount(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
}
