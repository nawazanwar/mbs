<?php

namespace  App\Services;

use App\Models\ExamTerm;

;

class ExamService
{
    public static function getExamTitleForDropdown(): array
    {
        return ExamTerm::all()->pluck('title','id')->toArray();

    }
}