<?php

namespace App\Services;

use App\Models\ClassModel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ClassService
{
    public static function getClassesForDropdown()
    {
        return ClassModel::where('school_id',SchoolService::getSchoolId())
            ->pluck('classes.name', 'classes.id');
    }

    public function applyGlobalSearch(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('numeric_name', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }
}
