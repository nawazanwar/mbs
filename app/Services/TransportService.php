<?php

namespace  App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

;
class TransportService
{
    public static function applyGlobalSearchForRoutes(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });

    }

    public static function applyGlobalSearchForVehicles(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
}
