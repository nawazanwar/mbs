<?php

namespace App\Services;
use App\Models\IncomeHead;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class IncomeService
{
    public static function getHeadDropdown()
    {
        return IncomeHead::pluck('title','id');
    }


    public function applyGlobalSearchForIncomeHead(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }

    public function applyGlobalSearchForIncome(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
}