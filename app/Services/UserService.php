<?php

namespace App\Services;

use App\Models\Designation;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class UserService
{

    public static function getUsersForDropdown()
    {
        return User::pluck('name', 'id');
    }
    public static function getGenderForDropdown(): array
    {
        return array(
            'male' => __('school.male'),
            'female' => __('school.female'),
        );
    }

    public static function getGuardianTypeForDropdown(): array
    {
        return array(
            'father' => __('school.father'),
            'mother' => __('school.mother'),
            'other' => __('school.other'),
            'exist_guardian' => __('school.exist_guardian'),
        );
    }

    public static function getBloodGroupDropdown(): array
    {
        return array(
            'a+' => __('school.a+'),
            'a-' => __('school.a-'),
            'b+' => __('school.b+'),
            'b-' => __('school.b-'),
            'o+' => __('school.o+'),
            'o-' => __('school.o-'),
            'ab+' => __('school.ab+'),
            'ab-' => __('school.ab-'),
        );
    }

    public static function getDesginationForDropdown(): array
    {
        return Designation::all()->pluck('name','id')->toArray();
    }

    public function applyGlobalSearchForDiscount(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('email', 'like', '%' . $keyword . '%');
        });
    }

}
