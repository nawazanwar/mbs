<?php

namespace  App\Services;

use App\Models\Role;
use Illuminate\Http\Request;


class RoleService
{
    public static function getRolesForDropdown()
    {
        return Role::pluck('label','id');
    }
    public function applyGlobalSearch(Request $request, $data)
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('label', 'like', '%' . $keyword . '%');
        });
    }
}