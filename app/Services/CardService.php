<?php

namespace App\Services;

class CardService
{
    public static function getSignatureAlignments(): array
    {
        return array(
            'left' => __('school.left'),
            'right' => __('school.right'),
            'center' => __('school.center')
        );
    }
}