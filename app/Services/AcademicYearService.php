<?php

namespace App\Services;

use App\Models\AcademicYear;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcademicYearService
{
    public static function getAcademicYearForDropdown()
    {
        return AcademicYear::where('school_id', SchoolService::getSchoolId())
            ->select(DB::raw("CONCAT(academic_years.start_session,' to ',academic_years.end_session) AS name"), 'academic_years.id as id')
            ->pluck('name', 'id');
    }

    public function applyGlobalSearch(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('start_session', 'like', '%' . $keyword . '%')
                ->orWhere('end_session', 'like', '%' . $keyword . '%');
        });
    }
}