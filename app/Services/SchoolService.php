<?php

namespace App\Services;

use App\Models\School;
use Illuminate\Support\Facades\Auth;

class SchoolService
{
    public static function getAllSchools()
    {
        return School::all();
    }

    public static function getSchoolId()
    {
        return session()->get('sId');
    }

    public static function getSchoolName()
    {
        return School::find(self::getSchoolId())->value('name');
    }

    public static function getEnableFrontendForDropdown(): array
    {
        return array(
            '1' => __('school.yes'),
            '0' => __('school.no'),
        );
    }

    public static function getFinalResultTypeForDropdown(): array
    {
        return array(
            '1' => __('school.average_of_all_exam'),
            '0' => __('school.only_based_on_final_exam'),
        );
    }

    public static function getEnableOnlineAdmissionForDropdown(): array
    {
        return array(
            '1' => __('school.no'),
            '0' => __('school.yes'),
        );
    }

    public static function getEnableRtlForDropdown(): array
    {
        return array(
            '0' => __('school.no'),
            '1' => __('school.yes'),
        );
    }

    public static function getSchoolDropdown()
    {
        return School::pluck('name', 'id');
    }
}
