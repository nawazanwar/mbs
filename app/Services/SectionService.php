<?php

namespace  App\Services;

use App\Models\Section;

;
class SectionService
{
    public static function getSectionForDropdown()
    {
        return Section::where('school_id', SchoolService::getSchoolId())
            ->pluck('name', 'id');
    }
}
