<?php

namespace App\Services;

use App\Models\Assignment;
use Illuminate\Http\Request;


class AssignmentService
{
    public static function getAssignmentForDropdown()
    {
        return Assignment::where('school_id', SchoolService::getSchoolId())
            ->pluck('title', 'id');
    }

    public function applyGlobalSearch(Request $request, $data)
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('numeric_name', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }

    public function applyGlobalSearchAssignmentSubmission(Request $request, $data)
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('numeric_name', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }
}
