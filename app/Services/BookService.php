<?php

namespace App\Services;

use App\Models\LibraryBook;

class BookService
{
    public static function getBooksForDropdown()
    {
        return LibraryBook::pluck('title', 'id');
    }
}
