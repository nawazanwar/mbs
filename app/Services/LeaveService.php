<?php

namespace App\Services;

class LeaveService
{
    public static function getLeaveDayForDropdown(): array
    {
        return array(
            'monday' => __('school.monday'),
            'tuesday' => __('school.tuesday'),
            'wednesday' => __('school.wednesday'),
            'thursday' => __('school.thursday'),
            'friday' => __('school.friday'),
            'saturday' => __('school.saturday'),
            'sunday' => __('school.sunday'),
        );
    }
}