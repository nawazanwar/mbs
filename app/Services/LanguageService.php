<?php

namespace App\Services;

use App\Models\Language;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class LanguageService
{
    public static function getLanguageForDropdown()
    {
        return Language::pluck('label','id');
    }

    public function applyGlobalSearch(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('label', 'like', '%' . $keyword . '%');
        });
    }
}
