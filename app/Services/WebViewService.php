<?php

namespace App\Services;

class WebViewService
{
    public static function getWebViewForDropdown(): array
    {
        return array(
            'yes' => __('school.yes'),
            'no' => __('school.no'),
        );
    }
}