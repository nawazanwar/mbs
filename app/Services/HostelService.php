<?php

namespace App\Services;

use App\Models\Hostel;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class HostelService
{
    public static function applyGlobalSearchForRoom(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }

    public static function applyGlobalSearchForHostel(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }

    public static function getHostelTypes(): array
    {
        return [
            'boys' => __('school.boys'),
            'girls' => __('school.girls'),
            'combine' => __('school.combine'),
        ];
    }

    public static function getRoomTypes(): array
    {
        return [
            'ac' => __('school.ac'),
            'non_ac' => __('school.non_ac')
        ];
    }

    public static function getHostelsForDropdown()
    {
        return Hostel::pluck('name', 'id');
    }
}
