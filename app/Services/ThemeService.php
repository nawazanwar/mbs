<?php

namespace App\Services;

use App\Models\Theme;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ThemeService
{

    public function applyGlobalSearch(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('color', 'like', '%' . $keyword . '%');
        });
    }
    public static function getThemeForDropdown()
    {
        return Theme::pluck('name','id');
    }
}