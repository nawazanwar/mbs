<?php

namespace  App\Services;

use App\Models\Subject;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

;
class SubjectService
{
    public static function getSubjectForDropdown()
    {
        return Subject::where('school_id', SchoolService::getSchoolId())
            ->pluck('name', 'id');
    }

    public function applyGlobalSearch(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('name', 'like', '%' . $keyword . '%')
                ->orWhere('email', 'like', '%' . $keyword . '%');
        });
    }

    public static function getSubjectTypeForDropdown(): array
    {
        return array(
            'mandatory' => __('school.mandatory'),
            'optional' => __('school.optional'),
        );
    }
}
