<?php

namespace App\Services;
use App\Models\ExpenditureHead;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ExpenditureService
{
    public static function getHeadDropdown()
    {
        return ExpenditureHead::pluck('title','id');
    }

    public function applyGlobalSearchForExpenditureHead(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }

    public function applyGlobalSearchForExpenditure($request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
}