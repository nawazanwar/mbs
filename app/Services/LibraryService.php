<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class LibraryService
{
    public static function applyGlobalSearchForBook(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }

    public static function applyGlobalSearchForEBook(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
    public static function applyGlobalSearchForBookIssue(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
}