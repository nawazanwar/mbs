<?php

namespace App\Services;

class PaymentService
{
    public static function getPaymentMethodForDropdown(): array
    {
        return array(
            'cash' => __('school.cash'),
            'cheque' => __('school.cheque'),
            'receipt' => __('school.receipt')
        );
    }

    public static function getPaidStatusDropdown(): array
    {
        return array(
            'paid' => __('school.paid'),
            'unpaid' => __('school.unpaid')
        );
    }

    public static function getApplicableDiscountDropdown(): array
    {
        return array(
            '1' => __('school.yes'),
            '0' => __('school.no')
        );
    }
}