<?php

namespace  App\Services;

use App\Models\VisitorPurpose;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class VisitorService
{
    public static function getVisitorPurposesForDropdown(): array
    {
        return VisitorPurpose::all()->pluck('purpose','id')->toArray();
    }
    public function applyGlobalSearchForVisitorPurpose(Request $request, $data)
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }
    public function applyGlobalSearchForVisitor(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }
}