<?php

namespace App\Services;

use App\Models\Subject;
use App\Models\Teacher;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeacherService
{

    public static function getTeacherDropdown()
    {
        return Teacher::join('users', 'teachers.user_id', 'users.id')
            ->where('teachers.school_id', SchoolService::getSchoolId())
            ->pluck('users.name', 'teachers.id');
    }

    public function applyGlobalSearch(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }
}
