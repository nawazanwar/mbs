<?php

namespace App\Services;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;


class FeeService
{
    public static function getGeneralFeeTypesDropdown(): array
    {
        return array(
            'fee' => __('school.general_fee'),
            'hostel' => __('school.hostel'),
            'transport' => __('school.transport'),
        );
    }
    public static function getFeeTypeForDropdown(): array
    {
        return array(
            'flat' => __('school.flat'),
            'percentage' => __('school.percentage'),

        );
    }

    public function applyGlobalSearchForFeeType(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('amount', 'like', '%' . $keyword . '%')
                ->orWhere('reference', 'like', '%' . $keyword . '%');
        });
    }
}
