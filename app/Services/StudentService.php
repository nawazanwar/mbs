<?php

namespace App\Services;

use App\Models\Student;
use App\Models\StudentType;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class StudentService
{
    public static function getStudentsForDropdown()
    {
        return Student::join('users', 'students.user_id', 'users.id')
            ->where('students.school_id', SchoolService::getSchoolId())
            ->pluck('users.name', 'students.id');
    }

    public static function getStudentTypeForDropdown()
    {
        return StudentType::where('school_id', SchoolService::getSchoolId())
            ->pluck('name', 'id');
    }

    public function applyGlobalSearchForStudent(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }

    public function applyGlobalSearchForStudentType(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('type', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }

    public static function getStudentGroupsForDrodown(): array
    {
        return [
            'science' => __('school.science'),
            'arts' => __('school.arts'),
            'commerce' => __('school.commerce')
        ];
    }
}
