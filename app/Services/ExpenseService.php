<?php

namespace App\Services;

use App\Enum\DateEnum;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class ExpenseService
{
    public static function getYearlyExpenses(): string
    {
        $current_date = DateEnum::getTranslationKeyBy(DateEnum::CURRENT_DATE);
        $expense = "[";
        $expense_query = DB::select("SELECT m.month, IFNULL(SUM(t.amount),0) as expense_amount 
		FROM ( SELECT 1 AS MONTH UNION SELECT 2 AS MONTH UNION SELECT 3 AS MONTH 
		UNION SELECT 4 AS MONTH UNION SELECT 5 AS MONTH UNION SELECT 6 AS MONTH 
		UNION SELECT 7 AS MONTH UNION SELECT 8 AS MONTH UNION SELECT 9 AS MONTH 
		UNION SELECT 10 AS MONTH UNION SELECT 11 AS MONTH UNION SELECT 12 AS MONTH ) AS m 
		LEFT JOIN transactions t ON m.month = MONTH(t.trans_date) AND YEAR(t.trans_date)=YEAR('$current_date') 
		AND t.trans_type='expense' GROUP BY m.month ORDER BY m.month ASC");
        foreach ($expense_query as $row) {
            $expense .= $row->expense_amount . ",";
        }

        return $expense . "]";
    }

    public function getMonthlyExpenses()
    {
        return Transaction::selectRaw("IFNULL(SUM(amount),0) as total")
            ->where("dr_cr", "dr")
            ->whereMonth("trans_date", DateEnum::getTranslationKeyBy(DateEnum::CURRENT_MONT))
            ->whereYear("trans_date", DateEnum::getTranslationKeyBy(DateEnum::CURRENT_YEAR))
            ->first()
            ->total;
    }
}