<?php

namespace  App\Services;



use Illuminate\Http\Request;


class CallLogService
{

    public function applyGlobalSearchForCallLog(Request $request, $data)
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }

}