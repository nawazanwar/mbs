<?php

namespace App\Services;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class LectureService
{

    public static function getLectureTypeForDropdown(): array
    {
        return array(
            'youtube' => __('school.youtube'),
            'vimeo' => __('school.vimeo'),
            'powerpoint' => __('school.powerpoint'),
        );
    }

    public function applyGlobalSearch(Request $request, Builder $data): Builder
    {
        $keyword = $request->query('s');
        return $data->where(function ($q) use ($keyword) {
            $q->where('title', 'like', '%' . $keyword . '%')
                ->orWhere('note', 'like', '%' . $keyword . '%');
        });
    }
}
