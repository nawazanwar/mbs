<?php

namespace App\Models;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CertificateType extends Model implements Permissions
{
    use HasFactory;
    
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_certificate_type');
                    break;
                case 'create':
                case 'store':
                    return array('create_certificate_type');
                    break;
                case 'edit':
                case 'update':
                    return array('update_certificate_type');
                    break;
                case 'delete':
                    return array('delete_certificate_type');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_certificate_type',
            'create_certificate_type',
            'update_certificate_type',
            'delete_certificate_type',
        );
    }
}
