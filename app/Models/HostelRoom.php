<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class HostelRoom extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::HOSTEL_ROOMS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'hostel_id',
        'room_no',
        'room_type',
        'total_seat',
        'cost',
        'note',
        'status',
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function hostel(): BelongsTo
    {
        return $this->belongsTo(Hostel::class, 'hostel_id');
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_hostel_room');
                    break;
                case 'create':
                case 'store':
                    return array('create_hostel_room');
                    break;
                case 'edit':
                case 'update':
                    return array('update_hostel_room');
                    break;
                case 'delete':
                    return array('delete_hostel_room');
                    break;

                default:
                    return array();
            }

        }
        return array(
            'view_hostel_room',
            'create_hostel_room',
            'update_hostel_room',
            'delete_hostel_room',

        );
    }
}
