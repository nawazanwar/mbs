<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingPayment extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_setting_payment');
                    break;
                case 'create':
                case 'store':
                    return array('create_setting_payment');
                    break;
                case 'edit':
                case 'update':
                    return array('update_setting_payment');
                    break;
                case 'delete':
                    return array('delete_setting_payment');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_setting_payment',
            'create_setting_payment',
            'update_setting_payment',
            'delete_setting_payment',
        );
    }
}
