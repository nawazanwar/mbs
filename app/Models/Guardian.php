<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Guardian extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::GUARDIANS;

    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'user_id',
        'national_id',
        'profession',
        'present_address',
        'permanent_address',
        'religion',
        'other_info',
        'active'
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_guardian');
                    break;
                case 'create':
                case 'store':
                    return array('create_guardian');
                    break;
                case 'edit':
                case 'update':
                    return array('update_guardian');
                    break;
                case 'delete':
                    return array('delete_guardian');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_guardian',
            'create_guardian',
            'update_guardian',
            'delete_guardian',

        );
    }
}
