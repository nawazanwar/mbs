<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AdmitCardSetting extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::ADMIT_CARD_SETTINGS;

    protected $fillable = [
        'created_by', 'updated_by', 'school_id', 'border_color', 'top_bg', 'bottom_bg', 'school_logo', 'school_name', 'school_name_font_size', 'school_name_color', 'school_address', 'school_address_color', 'admit_font_size', 'admit_color', 'admit_bg', 'title_font_size', 'title_color', 'value_font_size', 'value_color', 'bottom_text', 'bottom_text_color', 'bottom_text_align', 'status'
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_admit_card_setting');
                    break;
                case 'create':
                case 'store':
                    return array('create_admit_card_setting');
                    break;
                case 'edit':
                case 'update':
                    return array('update_admit_card_setting');
                    break;
                case 'delete':
                    return array('delete_admit_card_setting');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_admit_card_setting',
            'create_admit_card_setting',
            'update_admit_card_setting',
            'delete_admit_card_setting',
        );
    }
}
