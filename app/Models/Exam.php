<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::EXAMS;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'academic_year_id',
        'title','start_date',
        'note', 
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_exam');
                    break;
                case 'create':
                case 'store':
                    return array('create_exam');
                    break;
                case 'edit':
                case 'update':
                    return array('update_exam');
                    break;
                case 'delete':
                    return array('delete_exam');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_exam',
            'create_exam',
            'update_exam',
            'delete_exam',
        );
    }
}
