<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hrm extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_hrm');
                    break;
                case 'create':
                case 'store':
                    return array('create_hrm');
                    break;
                case 'edit':
                case 'update':
                    return array('update_hrm');
                    break;
                case 'delete':
                    return array('delete_hrm');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_hrm',
            'create_hrm',
            'update_hrm',
            'delete_hrm',
        );
    }
}
