<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageSms extends Model implements Permissions
{
    use HasFactory;
    protected $table=TableEnum::MESSAGES;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_message_sms');
                    break;
                case 'create':
                case 'store':
                    return array('create_message_sms');
                    break;
                case 'edit':
                case 'update':
                    return array('update_message_sms');
                    break;
                case 'delete':
                    return array('delete_message_sms');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_message_sms',
            'create_message_sms',
            'update_message_sms',
            'delete_message_sms',
        );
    }
}
