<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbsentEmail extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_absent_email');
                    break;
                case 'create':
                case 'store':
                    return array('create_absent_email');
                    break;
                case 'edit':
                case 'update':
                    return array('update_absent_email');
                    break;
                case 'delete':
                    return array('delete_absent_email');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_absent_email',
            'create_absent_email',
            'update_absent_email',
            'delete_absent_email',
        );
    }
}
