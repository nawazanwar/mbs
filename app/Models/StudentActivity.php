<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentActivity extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::STUDENT_ACTIVITIES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'student_id ',
        'class_id',
        'section_id',
        'academic_year_id',
        'activity',
        'activity_date',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_student_activity');
                    break;
                case 'create':
                case 'store':
                    return array('create_student_activity');
                    break;
                case 'edit':
                case 'update':
                    return array('update_student_activity');
                    break;
                case 'delete':
                    return array('delete_student_activity');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_student_activity',
            'create_student_activity',
            'update_student_activity',
            'delete_student_activity',


        );
    }
}
