<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ExpenditureHead extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::EXPENDITURE_HEADS;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'title',
        'note',
        'status'
    ];
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_expenditure_head');
                    break;
                case 'create':
                case 'store':
                    return array('create_expenditure_head');
                    break;
                case 'edit':
                case 'update':
                    return array('update_expenditure_head');
                    break;
                case 'delete':
                    return array('delete_expenditure_head');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_expenditure_head',
            'create_expenditure_head',
            'update_expenditure_head',
            'delete_expenditure_head',

        );
    }
}
