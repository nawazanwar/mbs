<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PayrollSalaryGrade extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::SALARY_GRADES;
    protected $fillable=[
        'grade_name',
        'basic_salary',
        'house_rent',
        'transport',
        'medical',
        'over_time_hourly_rate',
        'provident_fund',
        'hourly_rate',
        'total_allowance',
        'total_deduction',
        'gross_salary',
        'net_salary',
        'note',
        'status',
    ];

    public function head(): BelongsTo
    {
        return $this->belongsTo(IncomeHead::class, 'salary_grade_id');
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_payroll_salary_grade');
                    break;
                case 'create':
                case 'store':
                    return array('create_payroll_salary_grade');
                    break;
                case 'edit':
                case 'update':
                    return array('update_payroll_salary_grade');
                    break;
                case 'delete':
                    return array('delete_payroll_salary_grade');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_payroll_salary_grade',
            'create_payroll_salary_grade',
            'update_payroll_salary_grade',
            'delete_payroll_salary_grade',

        );
    }
}
