<?php

namespace App\Models;


use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Lecture extends Model implements Permissions
{
    use HasFactory;

    public $fillable =[
            'created_by',
            'updated_by',
            'school_id',
            'class_id',
            'section_id',
            'student_id',
            'title',
            'lecture_type',
            'note',
            'status',
    ];
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class,'updated_by');
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_lecture');
                    break;
                case 'create':
                case 'store':
                    return array('create_lecture');
                    break;
                case 'edit':
                case 'update':
                    return array('update_lecture');
                    break;
                case 'delete':
                    return array('delete_lecture');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_lecture',
            'create_lecture',
            'update_lecture',
            'delete_lecture',

        );
    }
}
