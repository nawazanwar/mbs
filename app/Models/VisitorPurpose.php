<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class VisitorPurpose extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::VISITOR_PURPOSES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'purpose',
        'status'
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_visitor_purpose');
                    break;
                case 'create':
                case 'store':
                    return array('create_visitor_purpose');
                    break;
                case 'edit':
                case 'update':
                    return array('update_visitor_purpose');
                    break;
                case 'delete':
                    return array('delete_visitor_purpose');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_visitor_purpose',
            'create_visitor_purpose',
            'update_visitor_purpose',
            'delete_visitor_purpose',
        );
    }
}
