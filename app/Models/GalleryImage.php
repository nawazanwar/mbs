<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::GALLERY_IMAGES;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'gallery_id',
        'image',
        'caption',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_gallery_image');
                    break;
                case 'create':
                case 'store':
                    return array('create_gallery_image');
                    break;
                case 'edit':
                case 'update':
                    return array('update_gallery_image');
                    break;
                case 'delete':
                    return array('delete_gallery_image');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_gallery_image',
            'create_gallery_image',
            'update_gallery_image',
            'delete_gallery_image',

        );
    }
}
