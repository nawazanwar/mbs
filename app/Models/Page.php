<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::PAGES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'page_location',
        'page_slug',
        'page_title',
        'page_description',
        'page_image',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_page');
                    break;
                case 'create':
                case 'store':
                    return array('create_page');
                    break;
                case 'edit':
                case 'update':
                    return array('update_page');
                    break;
                case 'delete':
                    return array('delete_page');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_page',
            'create_page',
            'update_page',
            'delete_page',


        );
    }
}
