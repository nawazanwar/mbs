<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamTerm extends Model implements Permissions
{
    use HasFactory;
    public $fillable = [
            'created_by',
            'updated_by',
            'school_id',
            'academic_year_id',
            'title',
            'start_date',
            'note',
            'status'
    ];

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_exam_term');
                    break;
                case 'create':
                case 'store':
                    return array('create_exam_term');
                    break;
                case 'edit':
                case 'update':
                    return array('update_exam_term');
                    break;
                case 'delete':
                    return array('delete_exam_term');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_exam_term',
            'create_exam_term',
            'update_exam_term',
            'delete_exam_term',


        );
    }
}
