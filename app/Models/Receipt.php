<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receipt extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_receipt');
                    break;
                case 'create':
                case 'store':
                    return array('create_receipt');
                    break;
                case 'edit':
                case 'update':
                    return array('update_receipt');
                    break;
                case 'delete':
                    return array('delete_receipt');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_receipt',
            'create_receipt',
            'update_receipt',
            'delete_receipt',

        );
    }
}
