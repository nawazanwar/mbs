<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComplainType extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::COMPLAIN_TYPES;
    protected $fillable=[
        'created_by',
         'updated_by',
          'school_id',
          'type', 
          'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_complain_type');
                    break;
                case 'create':
                case 'store':
                    return array('create_complain_type');
                    break;
                case 'edit':
                case 'update':
                    return array('update_complain_type');
                    break;
                case 'delete':
                    return array('delete_complain_type');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_complain_type',
            'create_complain_type',
            'update_complain_type',
            'delete_complain_type',

        );
    }
}
