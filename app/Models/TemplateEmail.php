<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateEmail extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::EMAIL_TEMPLATES;
    protected $fillable = [
        'created_by','updated_by','school_id','role_id','title','template',	'status',
    ];

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_email_template');
                    break;
                case 'create':
                case 'store':
                    return array('create_email_template');
                    break;
                case 'edit':
                case 'update':
                    return array('update_email_template');
                    break;
                case 'delete':
                    return array('delete_email_template');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_email_template',
            'create_email_template',
            'update_email_template',
            'delete_email_template',

        );
    }
}
