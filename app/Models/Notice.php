<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notice extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::NOTICES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'role_id',
        'title',
        'date',
        'notice',
        'is_view_on_web',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_notice');
                    break;
                case 'create':
                case 'store':
                    return array('create_notice');
                    break;
                case 'edit':
                case 'update':
                    return array('update_notice');
                    break;
                case 'delete':
                    return array('delete_notice');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_notice',
            'create_notice',
            'update_notice',
            'delete_notice',
        );
    }
}
