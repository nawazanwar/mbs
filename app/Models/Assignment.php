<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Assignment extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::ASSIGNMENTS;
    protected $fillable=[
        'created_by',
        'updated_by',
        'school_id',
        'academic_year_id',
        'class_id', 'section_id',
        'subject_id',
        'title',
        'deadline',
        'note',
        'assignment',
        'status',
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public function academicYear(): BelongsTo
    {
        return $this->belongsTo(AcademicYear::class);
    }
    public function classModel(): BelongsTo
    {
        return $this->belongsTo(ClassModel::class);
    }

    public function section(): BelongsTo
    {
        return $this->belongsTo(Section::class);
    }
    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_assignment');
                    break;
                case 'create':
                case 'store':
                    return array('create_assignment');
                    break;
                case 'edit':
                case 'update':
                    return array('update_assignment');
                    break;
                case 'delete':
                    return array('delete_assignment');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_assignment',
            'create_assignment',
            'update_assignment',
            'delete_assignment',

        );
    }
}
