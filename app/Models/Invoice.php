<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::INVOICES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'custom_invoice_id',
        'is_applicable_discount',
        'academic_year_id',
        'invoice_type',
        'class_id',
        'student_id',
        'month',
        'gross_amount',
        'net_amount',
        'discount',
        'paid_status',
        'temp_amount',
        'date',
        'note',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_invoice');
                    break;
                case 'create':
                case 'store':
                    return array('create_invoice');
                    break;
                case 'edit':
                case 'update':
                    return array('update_invoice');
                    break;
                case 'delete':
                    return array('delete_invoice');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_invoice',
            'create_invoice',
            'update_invoice',
            'delete_invoice',


        );
    }
}
