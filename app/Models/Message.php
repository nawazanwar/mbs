<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model implements Permissions
{
    use HasFactory;
    protected $table=TableEnum::MESSAGES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'academic_year_id',
        'subject',
        'body',
        'attachment',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_message');
                    break;
                case 'create':
                case 'store':
                    return array('create_message');
                    break;
                case 'edit':
                case 'update':
                    return array('update_message');
                    break;
                case 'delete':
                    return array('delete_message');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_message',
            'create_message',
            'update_message',
            'delete_message',
        );
    }
}
