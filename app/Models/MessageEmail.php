<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MessageEmail extends Model implements Permissions
{
    use HasFactory;
    protected $table=TableEnum::MESSAGES;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_message_email');
                    break;
                case 'create':
                case 'store':
                    return array('create_message_email');
                    break;
                case 'edit':
                case 'update':
                    return array('update_message_email');
                    break;
                case 'delete':
                    return array('delete_message_email');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_message_email',
            'create_message_email',
            'update_message_email',
            'delete_message_email',
        );
    }
}
