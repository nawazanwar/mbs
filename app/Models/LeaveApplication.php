<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::LEAVE_APPLICATIONS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'academic_year_id',
        'role_id',
        'user_id',
        'class_id',
        'type_id',
        'leave_from',
        'leave_to',
        'leave_day',
        'leave_reason',
        'leave_note',
        'leave_date',
        'leave_status',
        'attachment',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_leave_application');
                    break;
                case 'create':
                case 'store':
                    return array('create_leave_application');
                    break;
                case 'edit':
                case 'update':
                    return array('update_leave_application');
                    break;
                case 'delete':
                    return array('delete_leave_application');
                    break;

                default:
                    return array();
            }

        }

        return array(
            'view_leave_application',
            'create_leave_application',
            'update_leave_application',
            'delete_leave_application',


        );
    }
}
