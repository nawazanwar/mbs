<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class IdCardSetting extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::ID_CARD_SETTINGS;

    protected $fillable = [
        'created_by', 'updated_by', 'school_id', 'border_color', 'top_bg', 'bottom_bg', 'school_logo', 'school_name', 'school_name_font_size', 'school_name_color', 'school_address', 'school_address_color', 'id_no_font_size', 'id_no_color', 'id_no_bg', 'title_font_size', 'title_color', 'value_font_size', 'value_color', 'bottom_text', 'bottom_text_color', 'bottom_text_align', 'status'
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_id_card_setting');
                    break;
                case 'create':
                case 'store':
                    return array('create_id_card_setting');
                    break;
                case 'edit':
                case 'update':
                    return array('update_id_card_setting');
                    break;
                case 'delete':
                    return array('delete_id_card_setting');
                    break;

                default:
                    return array();
            }

        }

        return array(
            'view_id_card_setting',
            'create_id_card_setting',
            'update_id_card_setting',
            'delete_id_card_setting',

        );
    }
}
