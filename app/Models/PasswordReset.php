<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_password_reset');
                    break;
                case 'create':
                case 'store':
                    return array('create_password_reset');
                    break;
                case 'edit':
                case 'update':
                    return array('update_password_reset');
                    break;
                case 'delete':
                    return array('delete_password_reset');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_password_reset',
            'create_password_reset',
            'update_password_reset',
            'delete_password_reset',

        );
    }
}
