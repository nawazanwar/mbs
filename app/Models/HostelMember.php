<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HostelMember extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::hostel_member;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'user_id',
        'hostel_id',
        'room_id',
        'custom_member_id',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_hostel_member');
                    break;
                case 'create':
                case 'store':
                    return array('create_hostel_member');
                    break;
                case 'edit':
                case 'update':
                    return array('update_hostel_member');
                    break;
                case 'delete':
                    return array('delete_hostel_member');
                    break;
                default:
                    return array();
            }

        }
        return array(
            'view_hostel_member',
            'create_hostel_member',
            'update_hostel_member',
            'delete_hostel_member',

        );
    }
}
