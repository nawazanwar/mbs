<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attendance extends Model implements Permissions
{

    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_attendance');
                    break;
                case 'create':
                case 'store':
                    return array('create_attendance');
                    break;
                case 'edit':
                case 'update':
                    return array('update_attendance');
                    break;
                case 'delete':
                    return array('delete_attendance');
                    break;

                default:
                    return array();
            }

        }
        return array(
            'view_attendance',
            'create_attendance',
            'update_attendance',
            'delete_attendance',
        );
    }
}
