<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CallLog extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::PHONE_CALL_LOGS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'call_type',
        'name',
        'phone',
        'call_duration',
        'call_date',
        'next_follow_up',
        'note',
        'status',
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_call_log');
                    break;
                case 'create':
                case 'store':
                    return array('create_call_log');
                    break;
                case 'edit':
                case 'update':
                    return array('update_call_log');
                    break;
                case 'delete':
                    return array('delete_call_log');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_call_log',
            'create_call_log',
            'update_call_log',
            'delete_call_log',

        );
    }
}
