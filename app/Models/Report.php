<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Report extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_report');
                    break;
                case 'create':
                case 'store':
                    return array('create_report');
                    break;
                case 'edit':
                case 'update':
                    return array('update_report');
                    break;
                case 'delete':
                    return array('delete_report');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_report',
            'create_report',
            'update_report',
            'delete_report',

        );
    }
}
