<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Theme extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::THEMES;
    protected $fillable = [
        'name',
        'color',
        'status'
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_theme');
                    break;
                case 'create':
                case 'store':
                    return array('create_theme');
                    break;
                case 'edit':
                case 'update':
                    return array('update_theme');
                    break;
                case 'delete':
                    return array('delete_theme');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_theme',
            'create_theme',
            'update_theme',
            'delete_theme',
        );
    }
}
