<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LibraryEbook extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::LIBRARY_EBOOKS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'class_id',
        'subject_id',
        'name',
        'author',
        'edition',
        'language',
        'cover_image',
        'file_name',
        'status',
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_library_ebook');
                    break;
                case 'create':
                case 'store':
                    return array('create_library_ebook');
                    break;
                case 'edit':
                case 'update':
                    return array('update_library_ebook');
                    break;
                case 'delete':
                    return array('delete_library_ebook');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_library_ebook',
            'create_library_ebook',
            'update_library_ebook',
            'delete_library_ebook',


        );
    }
}
