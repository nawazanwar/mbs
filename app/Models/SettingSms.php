<?php

namespace App\Models;
use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingSms extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::SETTING_SMS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'clickatell_username',
        'clickatell_password ',
        'clickatell_api_key',
        'clickatell_from_number',
        'clickatell_status',
        'twilio_account_sid',
        'twilio_auth_token',
        'twilio_from_number',
        'clickatell_mo_no',
        'twilio_status',
        'bulk_username',
        'bulk_password',
        'bulk_status',
        'msg91_auth_key',
        'msg91_sender_id',
        'msg91_status',
        'plivo_auth_id',
        'plivo_auth_token',
        'plivo_from_number',
        'plivo_status',
        'textlocal_username',
        'textlocal_hash_key',
        'textlocal_sender_id',
        'textlocal_status',
        'smscountry_username',
        'smscountry_password',
        'smscountry_sender_id',
        'smscountry_status',
        'betasms_username',
        'betasms_password',
        'betasms_sender_id',
        'betasms_status',
        'bulk_pk_username',
        'bulk_pk_password',
        'bulk_pk_sender_id',
        'bulk_pk_status',
        'cluster_auth_key',
        'cluster_sender_id',
        'cluster_router',
        'cluster_status',
        'alpha_username',
        'alpha_hash',
        'alpha_type',
        'alpha_status',
        'bdbulk_hash',
        'bdbulk_type',
        'bdbulk_status',
        'mim_api_key',
        'mim_type',
        'mim_sender_id',
        'mim_status',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_setting_sms_sms');
                    break;
                case 'create':
                case 'store':
                    return array('create_setting_sms');
                    break;
                case 'edit':
                case 'update':
                    return array('update_setting_sms');
                    break;
                case 'delete':
                    return array('delete_setting_sms');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_setting_sms',
            'create_setting_sms',
            'update_setting_sms',
            'delete_setting_sms',
        );
    }
}
