<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Income extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::INCOMES;

    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'income_head_id',
        'amount',
        'income_via',
        'reference',
        'payment_method',
        'note',
        'status'
    ];

    public function head(): BelongsTo
    {
        return $this->belongsTo(IncomeHead::class, 'income_head_id');
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_income');
                    break;
                case 'create':
                case 'store':
                    return array('create_income');
                    break;
                case 'edit':
                case 'update':
                    return array('update_income');
                    break;
                case 'delete':
                    return array('delete_income');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_income',
            'create_income',
            'update_income',
            'delete_income',

        );
    }
}
