<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayrollSalaryPayment extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::SALARY_PAYMENTS;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_payroll_salary_payment');
                    break;
                case 'create':
                case 'store':
                    return array('create_payroll_salary_payment');
                    break;
                case 'edit':
                case 'update':
                    return array('update_payroll_salary_payment');
                    break;
                case 'delete':
                    return array('delete_payroll_salary_payment');
                    break;

                default:
                    return array();
            }

        }

        return array(
            'view_payroll_salary_payment',
            'create_payroll_salary_payment',
            'update_payroll_salary_payment',
            'delete_payroll_salary_payment',
        );
    }
}
