<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeIdCard extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_employee_id_card');
                    break;
                case 'create':
                case 'store':
                    return array('create_employee_id_card');
                    break;
                case 'edit':
                case 'update':
                    return array('update_employee_id_card');
                    break;
                case 'delete':
                    return array('delete_employee_id_card');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_employee_id_card',
            'create_employee_id_card',
            'update_employee_id_card',
            'delete_employee_id_card',

        );
    }
}
