<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAdmitCard extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_student_admit_card');
                    break;
                case 'create':
                case 'store':
                    return array('create_student_admit_card');
                    break;
                case 'edit':
                case 'update':
                    return array('update_student_admit_card');
                    break;
                case 'delete':
                    return array('delete_student_admit_card');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_student_admit_card',
            'create_student_admit_card',
            'update_student_admit_card',
            'delete_student_admit_card',

        );
    }
}
