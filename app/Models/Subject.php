<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Subject extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::SUBJECTS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'class_id',
        'teacher_id',
        'type',
        'name',
        'code',
        'author',
        'note',
        'status'
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function class(): BelongsTo
    {
        return $this->belongsTo(ClassModel::class);
    }
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_subject');
                    break;
                case 'create':
                case 'store':
                    return array('create_subject');
                    break;
                case 'edit':
                case 'update':
                    return array('update_subject');
                    break;
                case 'delete':
                    return array('delete_subject');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_subject',
            'create_subject',
            'update_subject',
            'delete_subject',

        );
    }
}
