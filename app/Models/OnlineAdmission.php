<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OnlineAdmission extends Model implements Permissions
{
    use HasFactory;

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_online_admission');
                    break;
                case 'create':
                case 'store':
                    return array('create_online_admission');
                    break;
                case 'edit':
                case 'update':
                    return array('update_online_admission');
                    break;
                case 'delete':
                    return array('delete_online_admission');
                    break;
                default:
                    return array();
            }

        }
        return array(
            'view_online_admission',
            'create_online_admission',
            'update_online_admission',
            'delete_online_admission',
        );
    }
}
