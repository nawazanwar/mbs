<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_profile');
                    break;
                case 'create':
                case 'store':
                    return array('create_profile');
                    break;
                case 'edit':
                case 'update':
                    return array('update_profile');
                    break;
                case 'delete':
                    return array('delete_profile');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_profile',
            'create_profile',
            'update_profile',
            'delete_profile',
        );
    }
}
