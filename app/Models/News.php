<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::NEWS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'title',
        'date',
        'image',
        'news',
        'is_view_on_web',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_news');
                    break;
                case 'create':
                case 'store':
                    return array('create_news');
                    break;
                case 'edit':
                case 'update':
                    return array('update_news');
                    break;
                case 'delete':
                    return array('delete_news');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_news',
            'create_news',
            'update_news',
            'delete_news',
        );
    }
}
