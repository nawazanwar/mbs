<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Language extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::LANGUAGES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'name',
        'label',
        'status'
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_language');
                    break;
                case 'create':
                case 'store':
                    return array('create_language');
                    break;
                case 'edit':
                case 'update':
                    return array('update_language');
                    break;
                case 'delete':
                    return array('delete_language');
                    break;

                default:
                    return array();
            }

        }

        return array(
            'view_language',
            'create_language',
            'update_language',
            'delete_language',

        );
    }
}
