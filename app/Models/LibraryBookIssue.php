<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LibraryBookIssue extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::LIBRARY_BOOK_ISSUE;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'academic_year_id',
        'library_member_id',
        'book_id',
        'issue_date',
        'due_date',
        'return_date',
        'is_returned',
        'status',
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_library_book_issue');
                    break;
                case 'create':
                case 'store':
                    return array('create_library_book_issue');
                    break;
                case 'edit':
                case 'update':
                    return array('update_library_book_issue');
                    break;
                case 'delete':
                    return array('delete_library_book_issue');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_library_book_issue',
            'create_library_book_issue',
            'update_library_book_issue',
            'delete_library_book_issue',


        );
    }
}
