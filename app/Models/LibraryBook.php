<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LibraryBook extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::LIBRARY_BOOKS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'custom_id',
        'title',
        'isbn_no',
        'edition',
        'author',
        'language',
        'price',
        'qty',
        'cover',
        'rack_no',
        'status',
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_library_book');
                    break;
                case 'create':
                case 'store':
                    return array('create_library_book');
                    break;
                case 'edit':
                case 'update':
                    return array('update_library_book');
                    break;
                case 'delete':
                    return array('delete_library_book');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_library_book',
            'create_library_book',
            'update_library_book',
            'delete_library_book',


        );
    }
}
