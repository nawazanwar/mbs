<?php

namespace App\Models\Definition;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostFix extends Model
{
    use HasFactory;
}
