<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::HOLIDAYS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'title',
        'date_from',
        'date_to',
        'note',
        'is_view_on_web',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_holiday');
                    break;
                case 'create':
                case 'store':
                    return array('create_holiday');
                    break;
                case 'edit':
                case 'update':
                    return array('update_holiday');
                    break;
                case 'delete':
                    return array('delete_holiday');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_holiday',
            'create_holiday',
            'update_holiday',
            'delete_holiday',


        );
    }
}
