<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Complain extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::COMPLAINS;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'academic_year_id', 
        'role_id', 
        'user_id', 
        'class_id', 
        'type_id', 
        'description', 
        'action_note', 
        'complain_date',
        'action_date', 
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_complain');
                    break;
                case 'create':
                case 'store':
                    return array('create_complain');
                    break;
                case 'edit':
                case 'update':
                    return array('update_complain');
                    break;
                case 'delete':
                    return array('delete_complain');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_complain',
            'create_complain',
            'update_complain',
            'delete_complain',

        );
    }
}
