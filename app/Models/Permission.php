<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Permission extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::PERMISSIONS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'name',
        'label',

    ];

    public static function getPermissions($model)
    {
        return $model::modulePermissions();
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, TableEnum::ROLE_PERMISSION);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_permission');
                    break;
                case 'create':
                case 'store':
                    return array('create_permission');
                    break;
                case 'edit':
                case 'update':
                    return array('update_permission');
                    break;
                case 'delete':
                    return array('delete_permission');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_permission',
            'create_permission',
            'update_permission',
            'delete_permission',
        );
    }

    public static function syncPermissions(): bool
    {
        $models = self::getModels(app_path() . "/Models", '\App\Models\\');
        $permissions = array();
        foreach ($models as $model) {
            if (method_exists($model, 'modulePermissions')) {
                $permissions = array_merge($permissions, $model::modulePermissions());
            }
        }
        $existingPermissions = self::all()->pluck('name')->toArray();
        $permissionsToBeDeleted = array_diff($existingPermissions, $permissions);
        $permissionsToBeAdded = array_diff($permissions, $existingPermissions);
        if (!empty($permissionsToBeDeleted)) {
            Permission::whereIn('name', $permissionsToBeDeleted)->delete();
        }
        foreach ($permissionsToBeAdded as $permission) {
            Permission::create([
                'name' => $permission,
                'label' => ucwords(str_replace('_', ' ', $permission)),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'created_by' => 1,
                'updated_by' => 1
            ]);
        }
        return true;
    }

    public static function getModels($path, $namespace): array
    {
        $models = [];
        $results = scandir($path);
        foreach ($results as $result) {
            if ($result === '.' or $result === '..')
                continue;
            $filename = $path . '/' . $result;
            if (is_dir($filename)) {
                $models = array_merge($models, self::getModels($filename));
            } else {
                $models[] = substr($namespace . $result, 0, -4);
            }
        }
        return $models;
    }
}
