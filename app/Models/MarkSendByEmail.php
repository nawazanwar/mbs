<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarkSendByEmail extends Model
{
    use HasFactory;
    public $fillable = [
            'created_by',
            'updated_by',
            'school_id',
            'exam_id',
            'class_id',
            'role_id',
            'sender_role_id',
            'academic_year_id',
            'receiver',
            'subject_id',
            'note',
            'status',
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_mark_send_by_email');
                    break;
                case 'create':
                case 'store':
                    return array('create_mark_send_by_email');
                    break;
                case 'edit':
                case 'update':
                    return array('update_mark_send_by_email');
                    break;
                case 'delete':
                    return array('delete_mark_send_by_email');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_mark_send_by_email',
            'create_mark_send_by_email',
            'update_mark_send_by_email',
            'delete_mark_send_by_email',

        );
    }
}
