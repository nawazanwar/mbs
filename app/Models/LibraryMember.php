<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibraryMember extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::BOOKS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'user_id',
        'custom_member_id',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_library_member');
                    break;
                case 'create':
                case 'store':
                    return array('create_library_member');
                    break;
                case 'edit':
                case 'update':
                    return array('update_library_member');
                    break;
                case 'delete':
                    return array('delete_library_member');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_library_member',
            'create_library_member',
            'update_library_member',
            'delete_library_member',
        );
    }
}
