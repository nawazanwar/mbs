<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Teacher extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::TEACHERS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'national_id',
        'salary_grade_id ',
        'salary_type',
        'responsibility',
        'present_address',
        'permanent_address',
        'gender',
        'blood_group',
        'religion',
        'dob',
        'joining_date',
        'resign_date',
        'resume',
        'facebook_url',
        'linkedin_url',
        'twitter_url',
        'google_plus_url',
        'instagram_url',
        'pinterest_url',
        'youtube_url',
        'is_view_on_web',
        'other_info',
        'display_order',
        'active',
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class,'created_by');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class,'updated_by');
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_teacher');
                    break;
                case 'create':
                case 'store':
                    return array('create_teacher');
                    break;
                case 'edit':
                case 'update':
                    return array('update_teacher');
                    break;
                case 'delete':
                    return array('delete_teacher');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_teacher',
            'create_teacher',
            'update_teacher',
            'delete_teacher',
        );
    }
}
