<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamMark extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_exam_mark');
                    break;
                case 'create':
                case 'store':
                    return array('create_exam_mark');
                    break;
                case 'edit':
                case 'update':
                    return array('update_exam_mark');
                    break;
                case 'delete':
                    return array('delete_exam_mark');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_exam_mark',
            'create_exam_mark',
            'update_exam_mark',
            'delete_exam_mark',

        );
    }
}
