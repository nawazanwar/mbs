<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostalReceive extends Model implements Permissions
{
    use HasFactory;
    protected $table=TableEnum::POSTAL_RECEIVES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'from_title',
        'reference',
        'address',
        'receive_date',
        'to_title',
        'attachment',
        'note',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_postal_receive');
                    break;
                case 'create':
                case 'store':
                    return array('create_postal_receive');
                    break;
                case 'edit':
                case 'update':
                    return array('update_postal_receive');
                    break;
                case 'delete':
                    return array('delete_postal_receive');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_postal_receive',
            'create_postal_receive',
            'update_postal_receive',
            'delete_postal_receive',


        );
    }
}
