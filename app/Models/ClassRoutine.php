<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassRoutine extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::ROUTINES;
    protected $fillable=[
        'created_by', 'updated_by', 'school_id', 'academic_year_id', 'class_id', 'section_id', 'subject_id', 'day', 'start_time', 'end_time', 'room_no', 'created_at', 'updated_at', 'deleted_at'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_class_routine');
                    break;
                case 'create':
                case 'store':
                    return array('create_class_routine');
                    break;
                case 'edit':
                case 'update':
                    return array('update_class_routine');
                    break;
                case 'delete':
                    return array('delete_class_routine');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_class_routine',
            'create_class_routine',
            'update_class_routine',
            'delete_class_routine',

        );
    }
}
