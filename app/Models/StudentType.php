<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class StudentType extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::STUDENT_TYPES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'name',
        'note',
        'status'
    ];
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class,'created_by');
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class,'updated_by');
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_student_type');
                    break;
                case 'create':
                case 'store':
                    return array('create_student_type');
                    break;
                case 'edit':
                case 'update':
                    return array('update_student_type');
                    break;
                case 'delete':
                    return array('delete_student_type');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_student_type',
            'create_student_type',
            'update_student_type',

        );
    }
}
