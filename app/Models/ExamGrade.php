<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamGrade extends Model implements Permissions
{
    use HasFactory;

    public $fillable = [
            'created_by',
            'updated_by',
            'school_id',
            'academic_year_id',
            'grade_name',
            'grade_point',
            'mark_from',
            'mark_to',
            'note',
            'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_exam_grade');
                    break;
                case 'create':
                case 'store':
                    return array('create_exam_grade');
                    break;
                case 'edit':
                case 'update':
                    return array('update_exam_grade');
                    break;
                case 'delete':
                    return array('delete_exam_grade');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_exam_grade',
            'create_exam_grade',
            'update_exam_grade',
            'delete_exam_grade',

        );
    }
}
