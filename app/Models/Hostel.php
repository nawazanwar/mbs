<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Hostel extends Model implements Permissions
{
    use HasFactory;
    protected $table=TableEnum::HOSTELS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'name',
        'type',
        'address',
        'note',
        'status'
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_hostel');
                    break;
                case 'create':
                case 'store':
                    return array('create_hostel');
                    break;
                case 'edit':
                case 'update':
                    return array('update_hostel');
                    break;
                case 'delete':
                    return array('delete_hostel');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_hostel',
            'create_hostel',
            'update_hostel',
            'delete_hostel',


        );
    }
}
