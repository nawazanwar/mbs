<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DueFeeSms extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_due_fee_sms');
                    break;
                case 'create':
                case 'store':
                    return array('create_due_fee_sms');
                    break;
                case 'edit':
                case 'update':
                    return array('update_due_fee_sms');
                    break;
                case 'delete':
                    return array('delete_due_fee_sms');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_due_fee_sms',
            'create_due_fee_sms',
            'update_due_fee_sms',
            'delete_due_fee_sms',

        );
    }
}
