<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Discount extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::DISCOUNTS;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'title',
        'discount_type',
        'amount',
        'note',
        'status'
    ];

    public function head(): BelongsTo
    {
        return $this->belongsTo(Discount::class, 'discount_id');
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_discount');
                    break;
                case 'create':
                case 'store':
                    return array('create_discount');
                    break;
                case 'edit':
                case 'update':
                    return array('update_discount');
                    break;
                case 'delete':
                    return array('delete_discount');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_discount',
            'create_discount',
            'update_discount',
            'delete_discount',
        );
    }
}
