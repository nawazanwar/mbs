<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificate extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::CERTIFICATES;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'name', 
        'top_title', 
        'sub_title_middle', 
        'main_text', 
        'footer_left', 
        'footer_middle', 
        'footer_right', 
        'background', 
        'status'
    ];

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_certificate');
                    break;
                case 'create':
                case 'store':
                    return array('create_certificate');
                    break;
                case 'edit':
                case 'update':
                    return array('update_certificate');
                    break;
                case 'delete':
                    return array('delete_certificate');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_certificate',
            'create_certificate',
            'update_certificate',
            'delete_certificate',

        );
    }
}
