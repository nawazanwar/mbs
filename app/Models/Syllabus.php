<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Syllabus extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::SYLLABUSES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'class_id',
        'subject_id',
        'academic_year_id',
        'title',
        'syllabus',
        'note',
        'status'
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function class(): BelongsTo
    {
        return $this->belongsTo(ClassModel::class);
    }

    public function subject(): BelongsTo
    {
        return $this->belongsTo(Subject::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_syllabus');
                    break;
                case 'create':
                case 'store':
                    return array('create_syllabus');
                    break;
                case 'edit':
                case 'update':
                    return array('update_syllabus');
                    break;
                case 'delete':
                    return array('delete_syllabus');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_syllabus',
            'create_syllabus',
            'update_syllabus',
            'delete_syllabus',
        );
    }
}
