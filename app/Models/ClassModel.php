<?php

namespace App\Models;
use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ClassModel extends Model implements Permissions
{
    use HasFactory;
    protected $table=TableEnum::CLASSES;
    protected $fillable=[
        'created_by',
        'updated_by',
        'school_id',
        'teacher_id',
        'name',
        'numeric_name',
        'note',
        'status',
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_class');
                    break;
                case 'create':
                case 'store':
                    return array('create_class');
                    break;
                case 'edit':
                case 'update':
                    return array('update_class');
                    break;
                case 'delete':
                    return array('delete_class');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_class',
            'create_class',
            'update_class',
            'delete_class',

        );
    }
}
