<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarkSendBySms extends Model
{
    use HasFactory;
    public $fillable = [
            'created_by',
            'updated_by',
            'school_id',
            'exam_id',
            'class_id',
            'role_id',
            'sender_role_id',
            'academic_year_id',
            'receivers',
            'sms_gateway',
            'body',
            'status',
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_mark_send_by_sms');
                    break;
                case 'create':
                case 'store':
                    return array('create_mark_send_by_sms');
                    break;
                case 'edit':
                case 'update':
                    return array('update_mark_send_by_sms');
                    break;
                case 'delete':
                    return array('delete_mark_send_by_sms');
                    break;
                default:
                    return array();
            }
        }

        return array(
            'view_mark_send_by_sms',
            'create_mark_send_by_sms',
            'update_mark_send_by_sms',
            'delete_mark_send_by_sms',
        );
    }
}
