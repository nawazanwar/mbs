<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Slider extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::SLIDERS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'title ',
        'image',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_slider');
                    break;
                case 'create':
                case 'store':
                    return array('create_slider');
                    break;
                case 'edit':
                case 'update':
                    return array('update_slider');
                    break;
                case 'delete':
                    return array('delete_slider');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_slider',
            'create_slider',
            'update_slider',
            'delete_slider',

        );
    }
}
