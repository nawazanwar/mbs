<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Section extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::SECTIONS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'class_id',
        'teacher_id',
        'name',
        'note',
        'status'
    ];
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
    public function class(): BelongsTo
    {
        return $this->belongsTo(ClassModel::class);
    }
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_section');
                    break;
                case 'create':
                case 'store':
                    return array('create_section');
                    break;
                case 'edit':
                case 'update':
                    return array('update_section');
                    break;
                case 'delete':
                    return array('delete_section');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_section',
            'create_section',
            'update_section',
            'delete_section',

        );
    }
}
