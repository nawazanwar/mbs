<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostalDispatch extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::POSTAL_DISPATCHES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'to_title',
        'reference',
        'address',
        'from_title',
        'dispatch_date',
        'attachment',
        'note',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_postal_dispatch');
                    break;
                case 'create':
                case 'store':
                    return array('create_postal_dispatch');
                    break;
                case 'edit':
                case 'update':
                    return array('update_postal_dispatch');
                    break;
                case 'delete':
                    return array('delete_postal_dispatch');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_postal_dispatch',
            'create_postal_dispatch',
            'update_postal_dispatch',
            'delete_postal_dispatch',

        );
    }
}
