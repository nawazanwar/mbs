<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamSuggestion extends Model implements Permissions
{
    use HasFactory;
    public $fillable = [
            'created_by',
            'updated_by',
            'school_id',
            'academic_year_id',
            'exam_id',
            'class_id',
            'subject_id',
            'title',
            'note',
            'attachment',
            'status',
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_exam_suggestion');
                    break;
                case 'create':
                case 'store':
                    return array('create_exam_suggestion');
                    break;
                case 'edit':
                case 'update':
                    return array('update_exam_suggestion');
                    break;
                case 'delete':
                    return array('delete_exam_suggestion');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_exam_suggestion',
            'create_exam_suggestion',
            'update_exam_suggestion',
            'delete_exam_suggestion',

        );
    }
}
