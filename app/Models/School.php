<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class School extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::SCHOOLS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'language_id',
        'theme_id',
        'logo',
        'name',
        'code',
        'reg_date',
        'address',
        'phone',
        'email',
        'fax',
        'latitude',
        'longitude',
        'about',
        'facebook',
        'twitter',
        'linkedin',
        'google_plus',
        'youtube',
        'instagram',
        'pinterest',
        'is_main_branch',
        'parent_school_id'
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_school');
                    break;
                case 'create':
                case 'store':
                    return array('create_school');
                    break;
                case 'edit':
                case 'update':
                    return array('update_school');
                    break;
                case 'delete':
                    return array('delete_school');
                    break;
                default:
                    return array();
            }

        }
        return array(
            'view_school',
            'create_school',
            'update_school',
            'delete_school',
        );
    }
}
