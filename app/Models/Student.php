<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Student extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::STUDENTS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'student_type_id ',
        'class_id',
        'section_id',
        'admission_no',
        'admission_date',
        'dob',
        'guardian_id',
        'relation_with',
        'registration_no',
        'roll_no',
        'group',
        'present_address',
        'permanent_address',
        'gender',
        'blood_group',
        'religion',
        'caste',
        'age',
        'other_info',
        'is_library_member',
        'is_hostel_member',
        'is_transport_member',
        'discount_id',
        'previous_school',
        'previous_class',
        'transfer_certificate',
        'health_condition',
        'national_id',
        'second_language',
        'father_name',
        'father_phone',
        'father_education',
        'father_profession',
        'father_designation',
        'father_photo',
        'mother_name',
        'mother_phone',
        'mother_education',
        'mother_profession',
        'mother_designation',
        'mother_photo',
        'active',
        'guardian_type'
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function type(): BelongsTo
    {
        return $this->belongsTo(StudentType::class, 'student_type_id');
    }

    public function class(): BelongsTo
    {
        return $this->belongsTo(ClassModel::class, 'class_id');
    }

    public function section(): BelongsTo
    {
        return $this->belongsTo(Section::class, 'section_id');
    }
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_student');
                    break;
                case 'create':
                case 'store':
                    return array('create_student');
                    break;
                case 'edit':
                case 'update':
                    return array('update_student');
                    break;
                case 'delete':
                    return array('delete_student');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_student',
            'create_student',
            'update_student',
            'delete_student',


        );
    }
}
