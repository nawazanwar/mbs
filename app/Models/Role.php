<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Role extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::ROLES;

    protected $fillable = [
        'created_by',
        'updated_by',
        'name',
        'label'
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, TableEnum::ROLE_USER);
    }

    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(Permission::class, TableEnum::ROLE_PERMISSION);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_role');
                    break;
                case 'create':
                case 'store':
                    return array('create_role');
                    break;
                case 'edit':
                case 'update':
                    return array('update_role');
                    break;
                case 'delete':
                    return array('delete_role');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_role',
            'create_role',
            'update_role',
            'delete_role',

        );
    }

}
