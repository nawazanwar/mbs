<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayrollSalaryHistory extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_payroll_salary_history');
                    break;
                case 'create':
                case 'store':
                    return array('create_payroll_salary_history');
                    break;
                case 'edit':
                case 'update':
                    return array('update_payroll_salary_history');
                    break;
                case 'delete':
                    return array('delete_payroll_salary_history');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_payroll_salary_history',
            'create_payroll_salary_history',
            'update_payroll_salary_history',
            'delete_payroll_salary_history',

        );
    }
}
