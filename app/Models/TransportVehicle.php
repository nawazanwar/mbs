<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TransportVehicle extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::TRANSPORT_VEHICLES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'number ',
        'model',
        'driver',
        'license',
        'contact',
        'note',
        'is_allocated',
        'status'
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_transport_vehicle');
                    break;
                case 'create':
                case 'store':
                    return array('create_transport_vehicle');
                    break;
                case 'edit':
                case 'update':
                    return array('update_transport_vehicle');
                    break;
                case 'delete':
                    return array('delete_transport_vehicle');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_transport_vehicle',
            'create_transport_vehicle',
            'update_transport_vehicle',
            'delete_transport_vehicle',
        );
    }
}
