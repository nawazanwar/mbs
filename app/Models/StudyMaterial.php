<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudyMaterial extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::STUDY_MATERIALS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'class_id ',
        'subject_id',
        'title',
        'description',
        'material',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_study_material');
                    break;
                case 'create':
                case 'store':
                    return array('create_study_material');
                    break;
                case 'edit':
                case 'update':
                    return array('update_study_material');
                    break;
                case 'delete':
                    return array('delete_study_material');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_study_material',
            'create_study_material',
            'update_study_material',
            'delete_study_material',

        );
    }
}
