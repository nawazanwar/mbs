<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransportMember extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::TRANSPORT_MEMBERS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'user_id ',
        'route_id',
        'route_stop_id',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_transport_member');
                    break;
                case 'create':
                case 'store':
                    return array('create_transport_member');
                    break;
                case 'edit':
                case 'update':
                    return array('update_transport_member');
                    break;
                case 'delete':
                    return array('delete_transport_member');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_transport_member',
            'create_transport_member',
            'update_transport_member',
            'delete_transport_member',

        );
    }
}
