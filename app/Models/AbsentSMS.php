<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AbsentSMS extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_absent_sms');
                    break;
                case 'create':
                case 'store':
                    return array('create_absent_sms');
                    break;
                case 'edit':
                case 'update':
                    return array('update_absent_sms');
                    break;
                case 'delete':
                    return array('delete_absent_sms');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_absent_sms',
            'create_absent_sms',
            'update_absent_sms',
            'delete_absent_sms',
        );
    }
}
