<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Visitor extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::VISITORS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'academic_year_id ',
        'role_id',
        'user_id',
        'class_id',
        'purpose_id',
        'name',
        'phone',
        'check_in',
        'check_out',
        'note',
        'status'
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_visitor');
                    break;
                case 'create':
                case 'store':
                    return array('create_visitor');
                    break;
                case 'edit':
                case 'update':
                    return array('update_visitor');
                    break;
                case 'delete':
                    return array('delete_visitor');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_visitor',
            'create_visitor',
            'update_visitor',
            'delete_visitor',
        );
    }
}
