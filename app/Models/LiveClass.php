<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LiveClass extends Model implements Permissions
{
    use HasFactory;
    protected $table=TableEnum::LIVE_CLASSES;
    protected $fillable=[
        'created_by','updated_by','school_id','academic_year_id','class_id','section_id','subject_id','teacher_id','meeting_id','meeting_password','class_date','start_time','end_time','live_class','send_notification','class_type','note','status',
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_live_class');
                    break;
                case 'create':
                case 'store':
                    return array('create_live_class');
                    break;
                case 'edit':
                case 'update':
                    return array('update_live_class');
                    break;
                case 'delete':
                    return array('delete_live_class');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_live_class',
            'create_live_class',
            'update_live_class',
            'delete_live_class',

        );
    }
}
