<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamSchedule extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::EXAM_SCHEDULES;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'exam_id',
        'class_id',
        'subject_id',
        'academic_year_id', 
        'exam_date',
        'start_time',
        'end_time',
        'room_no',
        'note',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_exam_schedule');
                    break;
                case 'create':
                case 'store':
                    return array('create_exam_schedule');
                    break;
                case 'edit':
                case 'update':
                    return array('update_exam_schedule');
                    break;
                case 'delete':
                    return array('delete_exam_schedule');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_exam_schedule',
            'create_exam_schedule',
            'update_exam_schedule',
            'delete_exam_schedule',
        );
    }
}
