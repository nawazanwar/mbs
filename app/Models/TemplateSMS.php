<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TemplateSMS extends Model implements Permissions
{
    use HasFactory;
    protected $table = TableEnum::SMS_TEMPLATES;
    protected $fillable = [
        'created_by','updated_by','school_id','role_id','title','template',	'status',
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_sms_template');
                    break;
                case 'create':
                case 'store':
                    return array('create_sms_template');
                    break;
                case 'edit':
                case 'update':
                    return array('update_sms_template');
                    break;
                case 'delete':
                    return array('delete_sms_template');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_sms_template',
            'create_sms_template',
            'update_sms_template',
            'delete_sms_template',

        );
    }
}
