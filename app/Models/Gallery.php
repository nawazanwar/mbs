<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::GALLERIES;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'title',
        'note',
        'is_view_on_web',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_gallery');
                    break;
                case 'create':
                case 'store':
                    return array('create_gallery');
                    break;
                case 'edit':
                case 'update':
                    return array('update_gallery');
                    break;
                case 'delete':
                    return array('delete_gallery');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_gallery',
            'create_gallery',
            'update_gallery',
            'delete_gallery',


        );
    }
}
