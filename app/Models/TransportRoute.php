<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TransportRoute extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::TRANSPORT_ROUTES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'vehicle_ids',
        'title',
        'route_start',
        'route_end',
        'note',
        'status',
    ];

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_transport_route');
                    break;
                case 'create':
                case 'store':
                    return array('create_transport_route');
                    break;
                case 'edit':
                case 'update':
                    return array('update_transport_route');
                    break;
                case 'delete':
                    return array('delete_transport_route');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_transport_route',
            'create_transport_route',
            'update_transport_route',
            'delete_transport_route',

        );
    }
}
