<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeeCollection extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_fee_collection');
                    break;
                case 'create':
                case 'store':
                    return array('create_fee_collection');
                    break;
                case 'edit':
                case 'update':
                    return array('update_fee_collection');
                    break;
                case 'delete':
                    return array('delete_fee_collection');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_fee_collection',
            'create_fee_collection',
            'update_fee_collection',
            'delete_fee_collection',


        );
    }
}
