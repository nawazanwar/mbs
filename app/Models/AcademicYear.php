<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AcademicYear extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::ACADEMIC_YEAR;
    protected $fillable = [
        'created_by', 
        'updated_by', 
        'school_id', 
        'start_session',
        'end_session',
        'note', 
        'is_running', 
        'status'
    ];
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_academic_year');
                    break;
                case 'create':
                case 'store':
                    return array('create_academic_year');
                    break;
                case 'edit':
                case 'update':
                    return array('update_academic_year');
                    break;
                case 'delete':
                    return array('delete_academic_year');
                    break;
                default:
                    return array();
            }

        }
        return array(
            'view_academic_year',
            'create_academic_year',
            'update_academic_year',
            'delete_academic_year',
        );
    }
}
