<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class IncomeHead extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::INCOME_HEADS;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'head_type',
        'title',
        'note',
        'status'
    ];
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_income_head');
                    break;
                case 'create':
                case 'store':
                    return array('create_income_head');
                    break;
                case 'edit':
                case 'update':
                    return array('update_income_head');
                    break;
                case 'delete':
                    return array('delete_income_head');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_income_head',
            'create_income_head',
            'update_income_head',
            'delete_income_head',

        );
    }
}
