<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail, Permissions
{
    use Notifiable, HasApiTokens, HasFactory, SoftDeletes;
    use HasFactory;

    protected $table = TableEnum::USERS;

    protected $fillable = [
        'school_id',
        'photo',
        'name',
        'phone',
        'email',
        'temp_password',
        'password',
        'active'
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, TableEnum::ROLE_USER);
    }

    public function hasRole($role): bool
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }
        return !!$role->intersect($this->roles);
    }

    public function getRoleName()
    {
        return $this->roles[0]->name;
    }

    public function getRoleLabel()
    {
        return $this->roles[0]->label;
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_user');
                    break;
                case 'create':
                case 'store':
                    return array('create_user');
                    break;
                case 'edit':
                case 'update':
                    return array('update_user');
                    break;
                case 'delete':
                    return array('delete_user');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_user',
            'create_user',
            'update_user',
            'delete_user',

        );
    }

    public function ability($permission = null): bool
    {
        return !is_null($permission) && $this->checkPermission($permission);
    }

    protected function checkPermission($permission)
    {
        $permissions = $this->getAllPermissionForAllRoles();
        if ($permissions === true) {
            return true;
        }
        $permissionArray = is_array($permission) ? $permission : [$permission];
        return count(array_intersect($permissions, $permissionArray));
    }

    protected function getAllPermissionForAllRoles()
    {
        $permissions = array();
        $roles = $this->roles->load('permissions');

        if (!$roles) {
            return true;
        }

        foreach ($roles as $role) {
            foreach ($role->permissions as $permission) {
                $permissions[] = $permission->toArray();
            }
        }

        return array_map('strtolower', array_unique($this->array_flatten(array_map(function ($permission) {
            return $permission['name'];
        }, $permissions))));

    }

    function array_flatten($array)
    {
        if (!is_array($array)) {
            return FALSE;
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $result = array_merge($result, $this->array_flatten($value));
            } else {
                $result[$key] = $value;
            }
        }
        return $result;
    }

    public function isSuperAdmin()
    {

    }
}
