<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExamAttendance extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::EXAM_ATTENDANCES;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'exam_id',
        'class_id',
        'section_id',
        'subject_id', 
        'academic_year_id',
        'student_id',
        'is_attend',
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_exam_attendance');
                    break;
                case 'create':
                case 'store':
                    return array('create_exam_attendance');
                    break;
                case 'edit':
                case 'update':
                    return array('update_exam_attendance');
                    break;
                case 'delete':
                    return array('delete_exam_attendance');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_exam_attendance',
            'create_exam_attendance',
            'update_exam_attendance',
            'delete_exam_attendance',
        );
    }
}
