<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::EMPLOYEES;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'user_id',
        'role_id',
        'national_id',
        'designation_id',
        'salary_grade_id',
        'salary_type',
        'name',
        'email',
        'phone',
        'present_address',
        'permanent_address',
        'gender',
        'blood_group',
        'religion',
        'dob',
        'joining_date',
        'resign_date',
        'photo',
        'resume',
        'facebook_url',
        'linkedin_url',
        'twitter_url',
        'google_plus_url',
        'instagram_url',
        'pinterest_url',
        'youtube_url',
        'other_info',
        'display_order',
        'is_view_on_web', 
        'status'
];
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public function designation(): BelongsTo
    {
        return $this->belongsTo(Designation::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_employee');
                    break;
                case 'create':
                case 'store':
                    return array('create_employee');
                    break;
                case 'edit':
                case 'update':
                    return array('update_employee');
                    break;
                case 'delete':
                    return array('delete_employee');
                    break;

                default:
                    return array();
            }

        }

        return array(
            'view_employee',
            'create_employee',
            'update_employee',
            'delete_employee',


        );
    }
}
