<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::EVENTS;
    protected $fillable=[
        'created_by', 
        'updated_by', 
        'school_id', 
        'role_id',
        'title',
        'event_place',
        'event_from',
        'event_to',
        'image',
        'note',
        'is_view_on_web', 
        'status'
    ];
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_event');
                    break;
                case 'create':
                case 'store':
                    return array('create_event');
                    break;
                case 'edit':
                case 'update':
                    return array('update_event');
                    break;
                case 'delete':
                    return array('delete_event');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_event',
            'create_event',
            'update_event',
            'delete_event',

        );
    }
}
