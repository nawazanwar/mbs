<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Definition extends Model implements Permissions
{
    use HasFactory;

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_Definition');
                    break;
                case 'create':
                case 'store':
                    return array('create_Definition');
                    break;
                case 'edit':
                case 'update':
                    return array('update_Definition');
                    break;
                case 'delete':
                    return array('delete_Definition');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_Definition',
            'create_Definition',
            'update_Definition',
            'delete_Definition',


        );
    }
}
