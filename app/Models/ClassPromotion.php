<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClassPromotion extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_class_promotion');
                    break;
                case 'create':
                case 'store':
                    return array('create_class_promotion');
                    break;
                case 'edit':
                case 'update':
                    return array('update_class_promotion');
                    break;
                case 'delete':
                    return array('delete_class_promotion');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_class_promotion',
            'create_class_promotion',
            'update_class_promotion',
            'delete_class_promotion',

        );
    }
}
