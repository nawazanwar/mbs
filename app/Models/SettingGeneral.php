<?php

namespace App\Models;

use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingGeneral extends Model implements Permissions
{
    use HasFactory;
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_setting_general');
                    break;
                case 'create':
                case 'store':
                    return array('create_setting_general');
                    break;
                case 'edit':
                case 'update':
                    return array('update_setting_general');
                    break;
                case 'delete':
                    return array('delete_setting_general');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_setting_general',
            'create_setting_general',
            'update_setting_general',
            'delete_setting_general',
        );
    }
}
