<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LeaveType extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::LEAVE_TYPES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'role_id',
        'type',
        'total_leave',
        'status'
    ];
    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_leave_type');
                    break;
                case 'create':
                case 'store':
                    return array('create_leave_type');
                    break;
                case 'edit':
                case 'update':
                    return array('update_leave_type');
                    break;
                case 'delete':
                    return array('delete_leave_type');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_leave_type',
            'create_leave_type',
            'update_leave_type',
            'delete_leave_type',


        );
    }
}
