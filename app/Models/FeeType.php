<?php

namespace App\Models;

use App\Enum\TableEnum;
use App\Interfaces\Permissions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FeeType extends Model implements Permissions
{
    use HasFactory;

    protected $table = TableEnum::FEE_TYPES;
    protected $fillable = [
        'created_by',
        'updated_by',
        'school_id',
        'title',
        'type',
        'note',
        'status'
    ];

    public function school(): BelongsTo
    {
        return $this->belongsTo(School::class);
    }

    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public static function modulePermissions($middleware = false, $route = null): array
    {
        if ($middleware) {

            switch ($route) {
                case 'view':
                    return array('view_fee_type');
                    break;
                case 'create':
                case 'store':
                    return array('create_fee_type');
                    break;
                case 'edit':
                case 'update':
                    return array('update_fee_type');
                    break;
                case 'delete':
                    return array('delete_fee_type');
                    break;
                default:
                    return array();
            }

        }

        return array(
            'view_fee_type',
            'create_fee_type',
            'update_fee_type',
            'delete_fee_type',


        );
    }
}
