<?php

namespace App\Providers;

use App\Models\AbsentEmail;
use App\Models\AbsentSMS;
use App\Models\AcademicYear;
use App\Models\AdmitCardSetting;
use App\Models\TitleModel;
use App\Models\AssignmentSubmission;
use App\Models\Attendance;
use App\Models\EmployeeIdCard;
use App\Models\IdCardSetting;
use App\Models\LibraryBook;
use App\Models\LibraryBookIssue;
use App\Models\LibraryEbook;
use App\Models\CallLog;
use App\Models\Certificate;
use App\Models\CertificateType;
use App\Models\HostelMember;
use App\Models\HostelRoom;
use App\Models\LibraryMember;
use App\Models\LiveClass;
use App\Models\ClassPromotion;
use App\Models\ClassRoutine;
use App\Models\Complain;
use App\Models\ComplainType;
use App\Models\Designation;
use App\Models\Discount;
use App\Models\DueFeeEmail;
use App\Models\DueFeeSms;
use App\Models\Employee;
use App\Models\Event;
use App\Models\Exam;
use App\Models\ExamAttendance;
use App\Models\ExamGrade;
use App\Models\ExamMark;
use App\Models\Expenditure;
use App\Models\ExpenditureHead;
use App\Models\FeeCollection;
use App\Models\FeeType;
use App\Models\Gallery;
use App\Models\GalleryImage;
use App\Models\Guardian;
use App\Models\Holiday;
use App\Models\Hostel;
use App\Models\Income;
use App\Models\IncomeHead;
use App\Models\Invoice;
use App\Models\Language;
use App\Models\LeaveApplication;
use App\Models\LeaveType;
use App\Models\Lecture;
use App\Models\Message;
use App\Models\MessageEmail;
use App\Models\MessageSms;
use App\Models\News;
use App\Models\Notice;
use App\Models\OnlineAdmission;
use App\Models\Page;
use App\Models\PasswordReset;
use App\Models\Permission;
use App\Models\PostalDispatch;
use App\Models\PostalReceive;
use App\Models\Profile;
use App\Models\Receipt;
use App\Models\Role;
use App\Models\PayrollSalaryGrade;
use App\Models\PayrollSalaryHistory;
use App\Models\PayrollSalaryPayment;
use App\Models\School;
use App\Models\Section;
use App\Models\SettingEmail;
use App\Models\SettingGeneral;
use App\Models\SettingPayment;
use App\Models\SettingSms;
use App\Models\Slider;
use App\Models\Student;
use App\Models\StudentActivity;
use App\Models\StudentAdmitCard;
use App\Models\StudentIdCard;
use App\Models\StudentType;
use App\Models\StudyMaterial;
use App\Models\Subject;
use App\Models\Syllabus;
use App\Models\Teacher;
use App\Models\TeacherIdCard;
use App\Models\TemplateEmail;
use App\Models\TemplateSMS;
use App\Models\Theme;
use App\Models\TransportMember;
use App\Models\TransportVehicle;
use App\Models\User;
use App\Models\TransportRoute;
use App\Models\Visitor;
use App\Models\VisitorPurpose;
use App\Policies\AbsentEmailPolicy;
use App\Policies\AbsentSMSPolicy;
use App\Policies\AcademicYearPolicy;
use App\Policies\AssignmentPolicy;
use App\Policies\AssignmentSubmissionPolicy;
use App\Policies\AttendancePolicy;
use App\Policies\CallLogPolicy;
use App\Policies\CertificatePolicy;
use App\Policies\CertificateTypePolicy;
use App\Policies\ClassPromotionPolicy;
use App\Policies\ClassRoutinePolicy;
use App\Policies\ComplainPolicy;
use App\Policies\ComplainTypePolicy;
use App\Policies\DesignationPolicy;

use App\Policies\DiscountPolicy;
use App\Policies\DueFeeEmailPolicy;
use App\Policies\DueFeeSmsPolicy;
use App\Policies\EmployeeIdCardPolicy;
use App\Policies\EmployeePolicy;
use App\Policies\EventPolicy;
use App\Policies\ExamAttendancePolicy;
use App\Policies\ExamGradePolicy;
use App\Policies\ExamMarkPolicy;
use App\Policies\ExamPolicy;
use App\Policies\ExpenditureHeadPolicy;
use App\Policies\ExpenditurePolicy;
use App\Policies\FeeCollectionPolicy;
use App\Policies\FeeTypePolicy;
use App\Policies\GalleryImagePolicy;
use App\Policies\GalleryPolicy;
use App\Policies\GuardianPolicy;
use App\Policies\HolidayPolicy;
use App\Policies\HostelMemberPolicy;
use App\Policies\HostelPolicy;
use App\Policies\HostelRoomPolicy;
use App\Policies\IdCardSettingPolicy;
use App\Policies\IncomeHeadPolicy;
use App\Policies\IncomePolicy;
use App\Policies\InvoicePolicy;
use App\Policies\LanguagePolicy;
use App\Policies\LeaveApplicationPolicy;
use App\Policies\LeaveTypePolicy;
use App\Policies\LecturePolicy;
use App\Policies\LibraryBookIssuePolicy;
use App\Policies\LibraryBookPolicy;
use App\Policies\LibraryEbookPolicy;
use App\Policies\LibraryMemberPolicy;
use App\Policies\LiveClassPolicy;
use App\Policies\MessageEmailPolicy;

use App\Policies\MessagePolicy;
use App\Policies\MessageSmsPolicy;
use App\Policies\NewsPolicy;
use App\Policies\NoticePolicy;
use App\Policies\OnlineAdmissionPolicy;
use App\Policies\PagePolicy;
use App\Policies\PasswordResetPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\PermissionResetPolicy;

use App\Policies\PostalDispatchPolicy;
use App\Policies\PostalReceivePolicy;
use App\Policies\ProfilePolicy;
use App\Policies\ReceiptPolicy;
use App\Policies\RolePolicy;
use App\Policies\PayrollSalaryGradePolicy;
use App\Policies\PayrollSalaryHistoryPolicy;
use App\Policies\PayrollSalaryPaymentPolicy;
use App\Policies\SchoolPolicy;
use App\Policies\SectionPolicy;
use App\Policies\SettingEmailPolicy;
use App\Policies\SettingGeneralPolicy;
use App\Policies\SettingPaymentPolicy;
use App\Policies\SettingSmsPolicy;
use App\Policies\AdmitCardSettingPolicy;
use App\Policies\StudentActivityPolicy;
use App\Policies\StudentAdmitCardPolicy;
use App\Policies\StudentIdCardPolicy;
use App\Policies\StudentPolicy;
use App\Policies\StudentTypePolicy;
use App\Policies\StudyMaterialPolicy;
use App\Policies\SubjectPolicy;
use App\Policies\SyllabusPolicy;
use App\Policies\TeacherIdCardPolicy;
use App\Policies\TeacherPolicy;
use App\Policies\TemplateEmailPolicy;
use App\Policies\TemplateSMSPolicy;
use App\Policies\ThemePolicy;
use App\Policies\TransportMemberPolicy;
use App\Policies\TransportVehiclePolicy;
use App\Policies\UserPolicy;
use App\Policies\TransportRoutePolicy;
use App\Policies\VisitorPolicy;
use App\Policies\VisitorPurposePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

        TitleModel::class => AssignmentPolicy::class,
        AssignmentSubmission::class => AssignmentSubmissionPolicy::class,
        OnlineAdmission::class => OnlineAdmissionPolicy::class,


        Guardian::class => GuardianPolicy::class,

        AbsentEmail::class => AbsentEmailPolicy::class,
        AbsentSMS::class => AbsentSMSPolicy::class,

        Attendance::class => AttendancePolicy::class,
        Designation::class => DesignationPolicy::class,

        CallLog::class => CallLogPolicy::class,

        Certificate::class => CertificatePolicy::class,
        CertificateType::class => CertificateTypePolicy::class,

        SettingPayment::class => SettingPaymentPolicy::class,
        SettingGeneral::class => SettingGeneralPolicy::class,
        SettingEmail::class => SettingEmailPolicy::class,
        SettingSms::class => SettingSmsPolicy::class,
        AcademicYear::class => AcademicYearPolicy::class,
        Role::class => RolePolicy::class,
        Permission::class => PermissionPolicy::class,
        User::class => UserPolicy::class,

        LiveClass::class => LiveClassPolicy::class,
        ClassPromotion::class => ClassPromotionPolicy::class,
        ClassRoutine::class => ClassRoutinePolicy::class,

        Complain::class => ComplainPolicy::class,
        ComplainType::class => ComplainTypePolicy::class,

        Discount::class => DiscountPolicy::class,
        DueFeeEmail::class => DueFeeEmailPolicy::class,
        DueFeeSms::class => DueFeeSmsPolicy::class,
        Employee::class => EmployeePolicy::class,

        Event::class => EventPolicy::class,
        Exam::class => ExamPolicy::class,
        ExamAttendance::class => ExamAttendancePolicy::class,
        ExamGrade::class => ExamGradePolicy::class,
        ExamMark::class => ExamMarkPolicy::class,

        Expenditure::class => ExpenditurePolicy::class,
        ExpenditureHead::class => ExpenditureHeadPolicy::class,

        FeeCollection::class => FeeCollectionPolicy::class,
        FeeType::class => FeeTypePolicy::class,
        Gallery::class => GalleryPolicy::class,
        GalleryImage::class => GalleryImagePolicy::class,

        Holiday::class => HolidayPolicy::class,

        Hostel::class => HostelPolicy::class,
        HostelRoom::class => HostelRoomPolicy::class,
        HostelMember::class => HostelMemberPolicy::class,

        TransportVehicle::class => TransportVehiclePolicy::class,
        TransportRoute::class => TransportRoutePolicy::class,
        TransportMember::class => TransportMemberPolicy::class,


        Income::class => IncomePolicy::class,
        IncomeHead::class => IncomeHeadPolicy::class,
        Invoice::class => InvoicePolicy::class,
        Language::class => LanguagePolicy::class,

        LeaveApplication::class => LeaveApplicationPolicy::class,
        LeaveType::class => LeaveTypePolicy::class,

        Lecture::class => LecturePolicy::class,

        MessageEmail::class => MessageEmailPolicy::class,
        MessageSms::class => MessageSmsPolicy::class,
        Message::class => MessagePolicy::class,

        News::class => NewsPolicy::class,
        Notice::class => NoticePolicy::class,
        Page::class => PagePolicy::class,

        PasswordReset::class => PasswordResetPolicy::class,


        PostalDispatch::class => PostalDispatchPolicy::class,
        PostalReceive::class => PostalReceivePolicy::class,

        Profile::class => ProfilePolicy::class,
        Receipt::class => ReceiptPolicy::class,


        PayrollSalaryGrade::class => PayrollSalaryGradePolicy::class,
        PayrollSalaryHistory::class => PayrollSalaryHistoryPolicy::class,
        PayrollSalaryPayment::class => PayrollSalaryPaymentPolicy::class,

        School::class => SchoolPolicy::class,
        Section::class => SectionPolicy::class,
        Slider::class => AdmitCardSettingPolicy::class,
        Student::class => StudentPolicy::class,

        StudentActivity::class => StudentActivityPolicy::class,
        StudentType::class => StudentTypePolicy::class,
        StudyMaterial::class => StudyMaterialPolicy::class,

        Subject::class => SubjectPolicy::class,
        Syllabus::class => SyllabusPolicy::class,

        Teacher::class => TeacherPolicy::class,

        Theme::class => ThemePolicy::class,


        Visitor::class => VisitorPolicy::class,
        VisitorPurpose::class => VisitorPurposePolicy::class,

        LibraryBook::class => LibraryBookPolicy::class,
        LibraryMember::class => LibraryMemberPolicy::class,
        LibraryBookIssue::class => LibraryBookIssuePolicy::class,
        LibraryEbook::class => LibraryEbookPolicy::class,

        TemplateSMS::class => TemplateSMSPolicy::class,
        TemplateEmail::class => TemplateEmailPolicy::class,

        AdmitCardSetting::class => AdmitCardSettingPolicy::class,
        EmployeeIdCard::class => EmployeeIdCardPolicy::class,
        IdCardSetting::class => IdCardSettingPolicy::class,
        StudentAdmitCard::class => StudentAdmitCardPolicy::class,
        StudentIdCard::class => StudentIdCardPolicy::class,
        TeacherIdCard::class => TeacherIdCardPolicy::class

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
