<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssignmentSubmissionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_assignment_submission');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_assignment_submission');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_assignment_submission');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_assignment_submission');
    }

}
