<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamSchedulePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_exam_schedule');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_exam_schedule');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_exam_schedule');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_exam_schedule');
    }

}
