<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DueFeeSmsPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_due_fee_sms');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_due_fee_sms');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_due_fee_sms');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_due_fee_sms');
    }

}
