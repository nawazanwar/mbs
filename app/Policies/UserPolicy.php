<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_user');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_user');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_user');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_user');
    }

}
