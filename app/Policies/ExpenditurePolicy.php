<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpenditurePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_expenditure');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_expenditure');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_expenditure');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_expenditure');
    }

}
