<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class IdCardSettingPolicy
{
    use HandlesAuthorization;


    public function view(User $user):bool
    {
        return  $user->ability('view_id_card_setting');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_id_card_setting');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_id_card_setting');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_id_card_setting');
    }

}
