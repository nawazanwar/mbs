<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GalleryPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_gallery');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_gallery');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_gallery');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_gallery');
    }

}
