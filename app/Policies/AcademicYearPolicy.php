<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AcademicYearPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user):bool
    {
        return  $user->ability('view_academic_year');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_academic_year');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_academic_year');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_academic_year');
    }
}
