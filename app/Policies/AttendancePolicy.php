<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AttendancePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_attendance');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_attendance');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_attendance');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_attendance');
    }

}
