<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassRoutinePolicy
{
    use HandlesAuthorization;
    
    public function view(User $user):bool
    {
        return  $user->ability('view_class_routine');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_class_routine');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_class_routine');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_class_routine');
    }

}
