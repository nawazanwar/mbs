<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GalleryImagePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_gallery_image');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_gallery_image');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_gallery_image');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_gallery_image');
    }

}
