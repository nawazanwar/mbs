<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubjectPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_subject');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_subject');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_subject');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_subject');
    }

}
