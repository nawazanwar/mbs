<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_message');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_message');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_message');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_message');
    }

}
