<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SectionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_section');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_section');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_section');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_section');
    }

}
