<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TeacherIdCardPolicy
{
    use HandlesAuthorization;

    public function view(User $user):bool
    {
        return  $user->ability('view_teacher_id_card');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_teacher_id_card');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_teacher_id_card');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_teacher_id_card');
    }

}
