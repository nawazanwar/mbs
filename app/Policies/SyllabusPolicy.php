<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SyllabusPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_syllabus');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_syllabus');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_syllabus');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_syllabus');
    }

}
