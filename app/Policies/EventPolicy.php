<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_event');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_event');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_event');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_event');
    }

}
