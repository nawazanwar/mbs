<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReportPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_report');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_report');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_report');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_report');
    }

}
