<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NoticePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_notice');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_notice');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_notice');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_notice');
    }

}
