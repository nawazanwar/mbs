<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LecturePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_lecture');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_lecture');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_lecture');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_lecture');
    }

}
