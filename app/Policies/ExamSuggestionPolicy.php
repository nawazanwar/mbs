<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamSuggestionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_exam_suggestion');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_exam_suggestion');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_exam_suggestion');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_exam_suggestion');
    }

}
