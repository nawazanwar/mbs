<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PagePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_page');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_page');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_page');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_page');
    }

}
