<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CertificatePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_certificate');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_certificate');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_certificate');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_certificate');
    }

}
