<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LanguagePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_language');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_language');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_language');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_language');
    }

}
