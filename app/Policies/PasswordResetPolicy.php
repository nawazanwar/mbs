<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PasswordResetPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_password_reset');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_password_reset');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_password_reset');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_password_reset');
    }

}
