<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThemePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_theme');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_theme');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_theme');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_theme');
    }

}
