<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DesignationPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_designation');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_designation');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_designation');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_designation');
    }

}
