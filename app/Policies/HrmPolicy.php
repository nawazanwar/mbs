<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HrmPolicy
{

    use HandlesAuthorization;


    public function view(User $user):bool
    {
        return  $user->ability('view_hrm');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_hrm');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_hrm');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_hrm');
    }

}
