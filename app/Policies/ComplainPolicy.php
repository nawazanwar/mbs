<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ComplainPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_complain');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_complain');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_complain');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_complain');
    }

}
