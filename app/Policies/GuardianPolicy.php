<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class GuardianPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user):bool
    {
       return  $user->ability('view_guardian');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_guardian');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_guardian');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_guardian');
    }

}
