<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoicePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_invoice');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_invoice');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_invoice');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_invoice');
    }

}
