<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransportMemberPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_transport_member');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_transport_member');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_transport_member');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_transport_member');
    }

}
