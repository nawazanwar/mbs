<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentTypePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_student_type');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_student_type');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_student_type');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_student_type');
    }

}
