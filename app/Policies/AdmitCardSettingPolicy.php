<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdmitCardSettingPolicy
{
    use HandlesAuthorization;


    public function view(User $user):bool
    {
        return  $user->ability('view_admit_card_setting');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_admit_card_setting');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_admit_card_setting');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_admit_card_setting');
    }

}
