<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OnlineAdmissionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_online_admission');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_online_admission');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_online_admission');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_online_admission');
    }

}
