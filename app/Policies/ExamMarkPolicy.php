<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamMarkPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_exam_mark');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_exam_mark');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_exam_mark');
    }
    public function delete(User $user)
    {
        return  $user->ability('delete_exam_mark');
    }

}
