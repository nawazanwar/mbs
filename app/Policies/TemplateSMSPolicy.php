<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TemplateSMSPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_sms_template');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_sms_template');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_sms_template');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_sms_template');
    }

}
