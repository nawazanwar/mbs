<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReceiptPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_receipt');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_receipt');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_receipt');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_receipt');
    }

}
