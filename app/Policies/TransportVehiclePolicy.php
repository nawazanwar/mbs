<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransportVehiclePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_transport_vehicle');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_transport_vehicle');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_transport_vehicle');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_transport_vehicle');
    }

}
