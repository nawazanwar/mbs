<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AssignmentPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_assignment');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_assignment');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_assignment');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_assignment');
    }
}
