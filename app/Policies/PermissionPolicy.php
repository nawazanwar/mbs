<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_permission');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_permission');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_permission');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_permission');
    }

}
