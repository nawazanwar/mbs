<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_student');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_student');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_student');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_student');
    }

}
