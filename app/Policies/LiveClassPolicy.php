<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LiveClassPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_live_class');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_live_class');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_live_class');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_live_class');
    }

}
