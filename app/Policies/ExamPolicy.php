<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_exam');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_exam');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_exam');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_exam');
    }

}
