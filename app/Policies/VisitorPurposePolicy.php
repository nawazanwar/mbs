<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class VisitorPurposePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_visitor_purpose');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_visitor_purpose');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_visitor_purpose');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_visitor_purpose');
    }

}
