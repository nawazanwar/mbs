<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HostelPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_hostel');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_hostel');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_hostel');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_hostel');
    }

}
