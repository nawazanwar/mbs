<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CertificateTypePolicy
{
    use HandlesAuthorization;


    public function viewAny(User $user)
    {
        return $user->isSuperAdmin();
    }

    public function view(User $user):bool
    {
        return  $user->ability('view_certificate_type');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_certificate_type');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_certificate_type');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_certificate_type');
    }

}
