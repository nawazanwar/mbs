<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostalReceivePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_postal_receive');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_postal_receive');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_postal_receive');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_postal_receive');
    }

}
