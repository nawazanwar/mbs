<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_news');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_news');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_news');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_news');
    }

}
