<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FeeTypePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_fee_type');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_fee_type');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_fee_type');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_fee_type');
    }

}
