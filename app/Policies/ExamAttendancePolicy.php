<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamAttendancePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_exam_attendance');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_exam_attendance');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_exam_attendance');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_exam_attendance');
    }

}
