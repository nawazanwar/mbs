<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CallLogPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_call_log');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_call_log');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_call_log');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_call_log');
    }

}
