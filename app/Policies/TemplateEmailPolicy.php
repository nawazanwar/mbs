<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TemplateEmailPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_email_template');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_email_template');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_email_template');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_email_template');
    }

}
