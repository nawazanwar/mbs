<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LibraryBookIssuePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_library_book_issue');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_library_book_issue');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_library_book_issue');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_library_book_issue');
    }

}
