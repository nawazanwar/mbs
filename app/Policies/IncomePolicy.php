<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class IncomePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_income');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_income');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_income');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_income');
    }

}
