<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DefinitionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view__Definition');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create__Definition');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update__Definition');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete__Definition');
    }

}
