<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_employee');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_employee');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_employee');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_employee');
    }

}
