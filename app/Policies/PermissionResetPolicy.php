<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionResetPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_permission_reset');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_permission_reset');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_permission_reset');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_permission_reset');
    }

}
