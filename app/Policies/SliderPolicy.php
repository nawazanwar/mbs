<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SliderPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_slider');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_slider');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_slider');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_slider');
    }

}
