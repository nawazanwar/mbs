<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DiscountPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_discount');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_discount');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_discount');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_discount');
    }

}
