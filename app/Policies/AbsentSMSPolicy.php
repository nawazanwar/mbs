<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AbsentSMSPolicy
{
    use HandlesAuthorization;


    public function view(User $user):bool
    {
        return  $user->ability('view_absent_sms');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_absent_sms');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_absent_sms');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_absent_sms');
    }

}
