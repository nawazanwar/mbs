<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PayrollSalaryPaymentPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_payroll_salary_payment');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_payroll_salary_payment');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_payroll_salary_payment');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_payroll_salary_payment');
    }

}
