<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamGradePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_exam_grade');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_exam_grade');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_exam_grade');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_exam_grade');
    }

}
