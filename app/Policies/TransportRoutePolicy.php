<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransportRoutePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_transport_route');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_transport_route');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_transport_route');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_transport_route');
    }

}
