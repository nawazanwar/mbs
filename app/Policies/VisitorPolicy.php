<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class VisitorPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_visitor');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_visitor');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_visitor');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_visitor');
    }

}
