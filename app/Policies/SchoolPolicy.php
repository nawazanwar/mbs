<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchoolPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_school');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_school');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_school');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_school');
    }

}
