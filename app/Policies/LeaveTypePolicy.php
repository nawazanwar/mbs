<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeaveTypePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_leave_type');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_leave_type');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_leave_type');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_leave_type');
    }

}
