<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class FeeCollectionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_fee_collection');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_fee_collection');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_fee_collection');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_fee_collection');
    }

}
