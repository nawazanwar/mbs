<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LeaveApplicationPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_leave_application');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_leave_application');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_leave_application');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_leave_application');
    }

}
