<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClassPromotionPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_class_promotion');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_class_promotion');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_class_promotion');
    }
    public function delete(User $user):bool
    {
        return $user->ability('delete_class_promotion');
    }
}
