<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeIdCardPolicy
{
    use HandlesAuthorization;


    public function view(User $user):bool
    {
        return  $user->ability('view_employee_id_card');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_employee_id_card');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_employee_id_card');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_employee_id_card');
    }

}
