<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessageEmailPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_message_email');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_message_email');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_message_email');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_message_email');
    }

}
