<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HostelRoomPolicy
{
    use HandlesAuthorization;
    
    public function view(User $user):bool
    {
        return  $user->ability('view_hostel_room');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_hostel_room');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_hostel_room');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_hostel_room');
    }

}
