<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExpenditureHeadPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_expenditure_head');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_expenditure_head');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_expenditure_head');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_expenditure_head');
    }

}
