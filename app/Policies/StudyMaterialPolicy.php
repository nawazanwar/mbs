<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudyMaterialPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_study_material');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_study_material');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_study_material');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_study_material');
    }

}
