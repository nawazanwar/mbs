<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HolidayPolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_holiday');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_holiday');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_holiday');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_holiday');
    }

}
