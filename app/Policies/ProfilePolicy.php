<?php

namespace App\Policies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfilePolicy
{
    use HandlesAuthorization;

    
    public function view(User $user):bool
    {
        return  $user->ability('view_profile');
    }
    public function create(User $user):bool
    {
        return  $user->ability('create_profile');
    }
    public function update(User $user):bool
    {
        return  $user->ability('update_profile');
    }
    public function delete(User $user):bool
    {
        return  $user->ability('delete_profile');
    }

}
