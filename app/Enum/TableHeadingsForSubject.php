<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSubject extends AbstractEnum
{

    public const NAME = 'name';
    public const SUBJECT_CODE = 'subject_code';
    public const GRADE = 'class';
    public const TEACHER = 'teacher';
    public const TYPE = 'type';
    public const AUTHOR = 'author';
    public const NOTE = 'note';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [


            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::SUBJECT_CODE => __(sprintf('%s.%s', 'school', self::SUBJECT_CODE)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::TEACHER => __(sprintf('%s.%s', 'school', self::TEACHER)),
            self::TYPE => __(sprintf('%s.%s', 'school', self::TYPE)),
            self::AUTHOR => __(sprintf('%s.%s', 'school', self::AUTHOR)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),

        ];
    }
}
