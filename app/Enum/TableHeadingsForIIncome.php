<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForIIncome extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const INCOME_HEAD = 'income_head';
    public const AMOUNT = 'amount';
    public const INCOME_VIA = 'income_via';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::INCOME_HEAD => __(sprintf('%s.%s', 'school', self::INCOME_HEAD)),
            self::AMOUNT => __(sprintf('%s.%s', 'school', self::AMOUNT)),
            self::INCOME_VIA => __(sprintf('%s.%s', 'school', self::INCOME_VIA)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),

        ];
    }
}
