<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAccountingDueFeeEmail extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const RECEIVER_TYPE = 'receiver_type';
    public const SUBJECT = 'subject';
    public const TIME = 'time';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::RECEIVER_TYPE => __(sprintf('%s.%s', 'school', self::RECEIVER_TYPE)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::TIME => __(sprintf('%s.%s', 'school', self::TIME)),
        ];
    }
}
