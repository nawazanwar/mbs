<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForEmployee extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const DESIGNATION = 'designation';
    public const PHONE = 'phone';
    public const EMAIL = 'email';
    public const JOINING_DATE = 'joining_date';
    public const IS_VIEW_ON_WEB = 'is_view_on_web';
    public const DISPLAY_ORDER = 'display_order';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::DESIGNATION => __(sprintf('%s.%s', 'school', self::DESIGNATION)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::JOINING_DATE => __(sprintf('%s.%s', 'school', self::JOINING_DATE)),
            self::IS_VIEW_ON_WEB => __(sprintf('%s.%s', 'school', self::IS_VIEW_ON_WEB)),
            self::DISPLAY_ORDER => __(sprintf('%s.%s', 'school', self::DISPLAY_ORDER)),

        ];
    }
}
