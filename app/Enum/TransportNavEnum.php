<?php

declare(strict_types=1);

namespace App\Enum;

class TransportNavEnum extends AbstractEnum
{
    public const VEHICLE = 'vehicle';
    public const TRANSPORT_ROUTE = 'transport_route';
    public const TRANSPORT_MEMBER = 'transport_member';

    public static function getValues(): array
    {
        return array();
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::VEHICLE => route('dashboard.transport.vehicles.index'),
            self::TRANSPORT_ROUTE => route('dashboard.transport.routes.index'),
            self::TRANSPORT_MEMBER => route('dashboard.transport.members.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::VEHICLE => '<i class="bx bx-bed fs-1"></i>',
            self::TRANSPORT_ROUTE =>'<i class="bx bx-bed fs-1"></i>',
            self::TRANSPORT_MEMBER =>'<i class="bx bx-user fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::VEHICLE => __(sprintf('%s.%s', 'school', self::VEHICLE)),
            self::TRANSPORT_ROUTE => __(sprintf('%s.%s', 'school', self::TRANSPORT_ROUTE)),
            self::TRANSPORT_MEMBER => __(sprintf('%s.%s', 'school', self::TRANSPORT_MEMBER))
        ];
    }
}
