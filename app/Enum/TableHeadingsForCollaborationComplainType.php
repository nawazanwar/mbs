<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForCollaborationComplainType extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const COMPLAIN_TYPE = 'complain_type';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::COMPLAIN_TYPE => __(sprintf('%s.%s', 'school', self::COMPLAIN_TYPE)),
        ];
    }
}
