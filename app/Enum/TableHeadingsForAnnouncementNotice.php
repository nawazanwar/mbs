<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAnnouncementNotice extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const DATE = 'date';
    public const NOTICE_FOR = 'notice_for';
    public const IS_VIEW_ON_WEB = 'is_view_on_web';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::DATE => __(sprintf('%s.%s', 'school', self::DATE)),
            self::NOTICE_FOR => __(sprintf('%s.%s', 'school', self::NOTICE_FOR)),
            self::IS_VIEW_ON_WEB => __(sprintf('%s.%s', 'school', self::IS_VIEW_ON_WEB)),
        ];
    }
}
