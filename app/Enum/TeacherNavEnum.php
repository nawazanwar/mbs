<?php

declare(strict_types=1);

namespace App\Enum;

class TeacherNavEnum extends AbstractEnum
{
    public const TEACHER = 'teacher';
    public const LECTURE = 'lecture';

    public static function getValues(): array
    {
        return [
            self::TEACHER,
            self::LECTURE
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::TEACHER => route('dashboard.teachers.index'),
            self::LECTURE => route('dashboard.lectures.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::TEACHER => '<i class="bx bx-user fs-1"></i>',
            self::LECTURE => '<i class="bx bx-circle fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::TEACHER => __(sprintf('%s.%s', 'school', self::TEACHER)),
            self::LECTURE => __(sprintf('%s.%s', 'school', self::LECTURE))
        ];
    }
}
