<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForExamMark extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const EXAM_TITLE = 'exam_title';
    public const EXAM_DATE = 'exam_date';
    public const ACADEMIC_YEAR = 'academic_year';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::EXAM_TITLE => __(sprintf('%s.%s', 'school', self::EXAM_TITLE)),
            self::EXAM_DATE => __(sprintf('%s.%s', 'school', self::EXAM_DATE)),
            self::ACADEMIC_YEAR => __(sprintf('%s.%s', 'school', self::ACADEMIC_YEAR)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),



        ];
    }
}
