<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForGallery extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const NOTE = 'note';
    public const IS_VIEW_ON_WEB = 'is_view_on_web';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
            self::IS_VIEW_ON_WEB => __(sprintf('%s.%s', 'school', self::IS_VIEW_ON_WEB)),

        ];
    }
}
