<?php

declare(strict_types=1);

namespace App\Enum;

class GalleryNavEnum extends AbstractEnum
{
    public const GALLERY = 'gallery';
    public const GALLERY_IMAGES = 'gallery_image';

    public static function getValues(): array
    {
        return array();
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::GALLERY => route('dashboard.gallery.index'),
            self::GALLERY_IMAGES => route('dashboard.gallery.images.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::GALLERY => '<i class="bx bx-image fs-1"></i>',
            self::GALLERY_IMAGES =>'<i class="bx bx-image fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::GALLERY => __(sprintf('%s.%s', 'school', self::GALLERY)),
            self::GALLERY_IMAGES => __(sprintf('%s.%s', 'school', self::GALLERY_IMAGES))
        ];
    }
}
