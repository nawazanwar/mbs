<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForEmailTemplate extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const RECEIVER_TYPE = 'receiver_type';
    public const TITLE = 'title';
    public const TEMPLATE = 'template';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::RECEIVER_TYPE => __(sprintf('%s.%s', 'school', self::RECEIVER_TYPE)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::TEMPLATE => __(sprintf('%s.%s', 'school', self::TEMPLATE)),
       
        ];
    }
}
