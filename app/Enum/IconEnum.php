<?php

declare(strict_types=1);

namespace App\Enum;

class IconEnum extends AbstractEnum
{
    public const dashboard = '<i class="fa fa-dashboard fa-3x fs-1"></i>';
    public const CONFIGURATIONS = '<i class="fa fa-gears fa-3x fs-1"></i>';
    public const ADMINISTRATIONS = '<i class="fa fa-user-plus fa-3x fs-1"></i>';
    public const EXAM = '<i class="fa fa-graduation-cap fa-3x fs-1"></i>';
    public const COLLABORATION = 'collaborations';
    public const HRM = 'hrm';
    public const FINANCE = 'finance';
    public const OPERATIONS = 'operations';
    public const ACADEMICS = 'academics';
    public const FRONT_OFFICE = 'front_office';
    public const TEACHER = 'teacher';
    public const GUARDIAN = 'guardian';
    public const STUDENT = 'student';
    public const ASSIGNMENT = 'assignment';
    public const GENERATE_CARD = 'generate_card';
    public const CERTIFICATE = 'certificate';
    public const REPORT = 'report';

    public static function getValues(): array
    {
        return  array();
    }

    public static function getTranslationKeys(): array
    {
        return  array();
    }
}
