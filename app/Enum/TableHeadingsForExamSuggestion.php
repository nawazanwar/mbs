<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForExamSuggestion extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const EXAM_TERM = 'exam_term';
    public const GRADE = 'class';
    public const SUBJECT = 'subject';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::EXAM_TERM => __(sprintf('%s.%s', 'school', self::EXAM_TERM)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),


        ];
    }
}
