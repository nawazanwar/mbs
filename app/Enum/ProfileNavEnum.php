<?php

declare(strict_types=1);

namespace App\Enum;

class ProfileNavEnum extends AbstractEnum
{

    public const MY_PROFILE = 'my_profile';
    public const RESET_PASSWORD = 'reset_password';

    public const LOGOUT = 'logout';

    public static function getValues(): array
    {
        return [
            self::MY_PROFILE,
            self::RESET_PASSWORD,
            self::LOGOUT

        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::MY_PROFILE => route('dashboard.profile.index'),
            self::RESET_PASSWORD => route('dashboard.password.reset.index'),
            self::LOGOUT => route('logout'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::MY_PROFILE => '<i class="bx bx-building fs-1"></i>',
            self::RESET_PASSWORD =>'<i class="bx bx-dollar fs-1"></i>',
            self::LOGOUT =>'<i class="bx bx-dollar fs-1"></i>'

        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::MY_PROFILE => __(sprintf('%s.%s', 'school', self::MY_PROFILE)),
            self::RESET_PASSWORD => __(sprintf('%s.%s', 'school', self::RESET_PASSWORD)),
            self::LOGOUT => __(sprintf('%s.%s', 'school', self::LOGOUT))
        ];
    }
}
