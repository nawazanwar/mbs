<?php

declare(strict_types=1);

namespace App\Enum;

class DesignationNavEnum extends AbstractEnum
{
    public const MANAGE_DESIGNATION = 'manage_designation';
    public const MANAGE_EMPLOYEE = 'manage_employee';

    public static function getValues(): array
    {
        return [
            self::MANAGE_DESIGNATION,
            self::MANAGE_EMPLOYEE,
        ];
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::MANAGE_DESIGNATION => '<i class="bx bx-user fs-1"></i>',
            self::MANAGE_EMPLOYEE => '<i class="bx bx-user fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::MANAGE_DESIGNATION => route('dashboard.designations.index'),
            self::MANAGE_EMPLOYEE => route('dashboard.employees.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::MANAGE_DESIGNATION => __(sprintf('%s.%s', 'school', self::MANAGE_DESIGNATION)),
            self::MANAGE_EMPLOYEE => __(sprintf('%s.%s', 'school', self::MANAGE_EMPLOYEE)),
        ];
    }
}
