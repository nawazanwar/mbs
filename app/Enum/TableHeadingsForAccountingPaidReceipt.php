<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAccountingPaidReceipt extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const INVOICE_NUMBER = 'invoice_number';
    public const STUDENT = 'student';
    public const GRADE = 'class';
    public const STATUS = 'status';
    public const AMOUNT = 'amount';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::INVOICE_NUMBER => __(sprintf('%s.%s', 'school', self::INVOICE_NUMBER)),
            self::STUDENT => __(sprintf('%s.%s', 'school', self::STUDENT)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::STATUS => __(sprintf('%s.%s', 'school', self::STATUS)),
            self::AMOUNT => __(sprintf('%s.%s', 'school', self::AMOUNT)),

        ];
    }
}
