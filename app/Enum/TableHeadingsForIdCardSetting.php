<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForIdCardSetting extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const BORDER_COLOR = 'border_color';
    public const TOP_BACKGROUND = 'top_background';
    public const BOTTOM_SIGNATURE = 'bottom_background';
    public const SIGNATURE_BACKGROUND = 'signature_background';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::BORDER_COLOR => __(sprintf('%s.%s', 'school', self::BORDER_COLOR)),
            self::TOP_BACKGROUND => __(sprintf('%s.%s', 'school', self::TOP_BACKGROUND)),
            self::BOTTOM_SIGNATURE => __(sprintf('%s.%s', 'school', self::BOTTOM_SIGNATURE)),
            self::SIGNATURE_BACKGROUND => __(sprintf('%s.%s', 'school', self::SIGNATURE_BACKGROUND)),





        ];
    }
}
