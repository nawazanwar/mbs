<?php

declare(strict_types=1);

namespace App\Enum;

class MarkNavEnum extends AbstractEnum
{
    public const MANAGE_MARK = 'manage_mark';
    public const EXAM_TERM_RESULT = 'exam_term_result';
    public const EXAM_FINAL_RESULT = 'exam_final_result';
    public const MERIT_LIST = 'merit_list';
    public const MARK_SHEET = 'mark_sheet';
    public const RESULT_CARD = 'result_card';
    public const MARK_SEND_BY_EMAIL = 'mark_send_by_email';
    public const MARK_SEND_BY_SMS = 'mark_send_by_sms';
    public const RESULT_SEND_BY_EMAIL = 'result_send_by_email';
    public const RESULT_SEND_BY_SMS = 'result_send_by_sms';

    public static function getValues(): array
    {
        return [
            self::MANAGE_MARK,
            self::EXAM_TERM_RESULT,
            self::EXAM_FINAL_RESULT,
            self::MERIT_LIST,
            self::MARK_SHEET,
            self::RESULT_CARD,
            self::MARK_SEND_BY_EMAIL,
            self::MARK_SEND_BY_SMS,
            self::RESULT_SEND_BY_EMAIL,
            self::RESULT_SEND_BY_SMS,
        ];
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::MANAGE_MARK => '<i class="bx bxs-graduation fs-1"></i>',
            self::EXAM_TERM_RESULT => '<i class="bx bx-happy-beaming fs-1"></i>',
            self::EXAM_FINAL_RESULT => '<i class="bx bx-anchor fs-1"></i>',
            self::MERIT_LIST => '<i class="bx bx-adjust fs-1"></i>',
            self::MARK_SHEET => '<i class="bx bx-align-justify fs-1"></i>',
            self::RESULT_CARD => '<i class="bx bxs-file-txt fs-1"></i>',
            self::MARK_SEND_BY_EMAIL => '<i class="bx bxs-file-txt fs-1"></i>',
            self::MARK_SEND_BY_SMS => '<i class="bx bxs-file-txt fs-1"></i>',
            self::RESULT_SEND_BY_EMAIL => '<i class="bx bxs-file-txt fs-1"></i>',
            self::RESULT_SEND_BY_SMS => '<i class="bx bxs-file-txt fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::MANAGE_MARK => route('dashboard.exams.index', ['type' => 'manage-mark']),
            self::EXAM_TERM_RESULT => route('dashboard.exams.index', ['type' => 'term-result']),
            self::EXAM_FINAL_RESULT => route('dashboard.exams.index', ['type' => 'final-result']),
            self::MERIT_LIST => route('dashboard.exams.index', ['type' => 'merit-result']),
            self::MARK_SHEET => route('dashboard.exams.index', ['type' => 'mark-sheet']),
            self::RESULT_CARD => route('dashboard.exams.index', ['type' => 'result-card']),
            self::MARK_SEND_BY_EMAIL => route('dashboard.mark.send.by.email.index'),
            self::MARK_SEND_BY_SMS =>  route('dashboard.mark.send.by.sms.index'),
            self::RESULT_SEND_BY_EMAIL => route('dashboard.result.send.by.email.index'),
            self::RESULT_SEND_BY_SMS => route('dashboard.result.send.by.sms.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::MANAGE_MARK => __(sprintf('%s.%s', 'school', self::MANAGE_MARK)),
            self::EXAM_TERM_RESULT => __(sprintf('%s.%s', 'school', self::EXAM_TERM_RESULT)),
            self::EXAM_FINAL_RESULT => __(sprintf('%s.%s', 'school', self::EXAM_FINAL_RESULT)),
            self::MERIT_LIST => __(sprintf('%s.%s', 'school', self::MERIT_LIST)),
            self::MARK_SHEET => __(sprintf('%s.%s', 'school', self::MARK_SHEET)),
            self::RESULT_CARD => __(sprintf('%s.%s', 'school', self::RESULT_CARD)),
            self::MARK_SEND_BY_EMAIL => __(sprintf('%s.%s', 'school', self::MARK_SEND_BY_EMAIL)),
            self::MARK_SEND_BY_SMS => __(sprintf('%s.%s', 'school', self::MARK_SEND_BY_SMS)),
            self::RESULT_SEND_BY_EMAIL => __(sprintf('%s.%s', 'school', self::RESULT_SEND_BY_EMAIL)),
            self::RESULT_SEND_BY_SMS => __(sprintf('%s.%s', 'school', self::RESULT_SEND_BY_SMS))
        ];
    }
}
