<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSalaryGrade extends AbstractEnum
{
    public const GRADE_NAME = 'grade_name';
    public const BASIC_SALARY = 'basic_salary';
    public const HOUSE_RENT = 'house_rent';
    public const TRANSPORT = 'transport';
    public const MEDICAL_ALLOWANCE = 'medical_allowance';
    public const OVER_TIME_HOURLY_RATE = 'over_time_hourly_rate';
    public const PROVIDENT_FUND = 'provident_fund';
    public const HOURLY_RATE = 'hourly_rate';
    public const TOTAL_ALLOWANCE = 'total_allowance';
    public const TOTAL_DEDUCTION = 'total_deduction';
    public const GROSS_SALARY = 'gross_salary';
    public const NET_SALARY = 'net_salary';
    public const NOTE = 'note';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::GRADE_NAME => __(sprintf('%s.%s', 'school', self::GRADE_NAME)),
            self::BASIC_SALARY => __(sprintf('%s.%s', 'school', self::BASIC_SALARY)),
            self::HOUSE_RENT => __(sprintf('%s.%s', 'school', self::HOUSE_RENT)),
            self::TRANSPORT => __(sprintf('%s.%s', 'school', self::TRANSPORT)),
            self::MEDICAL_ALLOWANCE => __(sprintf('%s.%s', 'school', self::MEDICAL_ALLOWANCE)),
            self::OVER_TIME_HOURLY_RATE => __(sprintf('%s.%s', 'school', self::OVER_TIME_HOURLY_RATE)),
            self::PROVIDENT_FUND => __(sprintf('%s.%s', 'school', self::PROVIDENT_FUND)),
            self::TOTAL_ALLOWANCE => __(sprintf('%s.%s', 'school', self::TOTAL_ALLOWANCE)),
            self::TOTAL_DEDUCTION => __(sprintf('%s.%s', 'school', self::TOTAL_DEDUCTION)),
            self::GROSS_SALARY => __(sprintf('%s.%s', 'school', self::GROSS_SALARY)),
            self::NET_SALARY => __(sprintf('%s.%s', 'school', self::NET_SALARY)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE))


        ];
    }
}
