<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForPromotion extends AbstractEnum
{
    public const NAME = 'school';
    public const ROLL_NO = 'roll_no';
    public const PHOTO = 'photo';
    public const TOTAL_MARK = 'total_mark';
    public const GPA = 'gpa';
    public const RESULT = 'result';
    public const POSITION = 'position';
    public const CLASS_OPTION = 'class_option';
    public const NEXT_ROLL_NO = 'next_roll_no';





    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::ROLL_NO => __(sprintf('%s.%s', 'school', self::ROLL_NO)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::TOTAL_MARK => __(sprintf('%s.%s', 'school', self::TOTAL_MARK)),
            self::GPA => __(sprintf('%s.%s', 'school', self::GPA)),
            self::RESULT => __(sprintf('%s.%s', 'school', self::RESULT)),
            self::POSITION => __(sprintf('%s.%s', 'school', self::POSITION)),
            self::CLASS_OPTION => __(sprintf('%s.%s', 'school', self::CLASS_OPTION)),
            self::NEXT_ROLL_NO => __(sprintf('%s.%s', 'school', self::NEXT_ROLL_NO)),




        ];
    }
}
