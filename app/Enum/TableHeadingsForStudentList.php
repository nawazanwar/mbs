<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForStudentList extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const GROUP = 'group';
    public const GRADE = 'class';
    public const SECTION = 'section';
    public const ROLL_NO = 'roll_no';
    public const EMAIL = 'email';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::GROUP => __(sprintf('%s.%s', 'school', self::GROUP)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::ROLL_NO => __(sprintf('%s.%s', 'school', self::ROLL_NO)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
        ];
    }
}
