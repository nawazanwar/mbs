<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForExamAttendance extends AbstractEnum
{
    public const NAME = 'name';
    public const PHONE = 'phone';
    public const ROLL_NO = 'roll_no';
    public const PHOTO = 'photo';
    public const ATTEND_ALL = 'attend_all';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::NAME=> __(sprintf('%s.%s', 'school', self::NAME)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::ROLL_NO => __(sprintf('%s.%s', 'school', self::ROLL_NO)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::ATTEND_ALL => __(sprintf('%s.%s', 'school', self::ATTEND_ALL)),


        ];
    }
}
