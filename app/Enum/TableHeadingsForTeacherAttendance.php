<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForTeacherAttendance extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const RESPONSIBILITY = 'responsibility';
    public const PHONE = 'phone';
    public const ROLL_NO = 'roll_no';
    public const PRESENT_ALL = 'present_all';
    public const LATE_ALL = 'late_all';
    public const ABSENT_ALL = 'absent_all';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::RESPONSIBILITY => __(sprintf('%s.%s', 'school', self::RESPONSIBILITY)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::PRESENT_ALL => __(sprintf('%s.%s', 'school', self::PRESENT_ALL)),
            self::LATE_ALL => __(sprintf('%s.%s', 'school', self::LATE_ALL)),
            self::ABSENT_ALL => __(sprintf('%s.%s', 'school', self::ABSENT_ALL)),
        ];
    }
}
