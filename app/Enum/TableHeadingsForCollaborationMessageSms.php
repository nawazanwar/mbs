<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForCollaborationMessageSms extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const SESSION_YEAR = 'session_year';
    public const RECEIVER_TYPE = 'receiver_type';
    public const SMS = 'sms';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::SESSION_YEAR => __(sprintf('%s.%s', 'school', self::SESSION_YEAR)),
            self::RECEIVER_TYPE => __(sprintf('%s.%s', 'school', self::RECEIVER_TYPE)),
            self::SMS => __(sprintf('%s.%s', 'school', self::SMS)),
        ];
    }
}
