<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForFrontendAboutSchool extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const ABOUT_SCHOOL = 'school_about';
    public const IMAGE = 'image';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::ABOUT_SCHOOL => __(sprintf('%s.%s', 'school', self::ABOUT_SCHOOL)),
            self::IMAGE => __(sprintf('%s.%s', 'school', self::IMAGE)),

        ];
    }
}
