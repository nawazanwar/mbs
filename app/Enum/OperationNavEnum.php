<?php

declare(strict_types=1);

namespace App\Enum;

class OperationNavEnum extends AbstractEnum
{
    public const LIBRARY = 'library';
    public const TRANSPORT = 'transport';
    public const HOSTEL = 'hostel';

    public static function getValues(): array
    {
        return [
            self::LIBRARY,
            self::TRANSPORT,
            self::HOSTEL
        ];
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::LIBRARY => '<i class="bx bxs-book fs-1"></i>',
            self::TRANSPORT => '<i class="bx bx-bus fs-1"></i>',
            self::HOSTEL => '<i class="bx bx-hotel fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::LIBRARY => route('dashboard.library.books.index'),
            self::TRANSPORT => route('dashboard.transport.vehicles.index'),
            self::HOSTEL => route('dashboard.hostels.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::LIBRARY => __(sprintf('%s.%s', 'school', self::LIBRARY)),
            self::TRANSPORT => __(sprintf('%s.%s', 'school', self::TRANSPORT)),
            self::HOSTEL => __(sprintf('%s.%s', 'school', self::HOSTEL))
        ];
    }
}