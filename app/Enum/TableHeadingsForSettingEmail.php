<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSettingEmail extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const EMAIL_PROTOCOL = 'email_protocol';
    public const EMAIL_TYPE = 'email_type';
    public const CHAR_SET = 'char_set';
    public const FROM_NAME= 'from_name';
    public const FROM_EMAIL= 'from_email';




    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::EMAIL_PROTOCOL => __(sprintf('%s.%s', 'school', self::EMAIL_PROTOCOL)),
            self::EMAIL_TYPE => __(sprintf('%s.%s', 'school', self::EMAIL_TYPE)),
            self::CHAR_SET => __(sprintf('%s.%s', 'school', self::CHAR_SET)),
            self::FROM_NAME => __(sprintf('%s.%s', 'school', self::FROM_NAME)),
            self::FROM_EMAIL => __(sprintf('%s.%s', 'school', self::FROM_EMAIL)),


        ];
    }
}
