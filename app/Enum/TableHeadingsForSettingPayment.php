<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSettingPayment extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PAYPAL = 'paypal';
    public const PAY_U_MONEY = 'pay_u_money';
    public const PAY_TM = 'pay_tm';
    public const PAY_STACK= 'pay_stack';





    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PAYPAL => __(sprintf('%s.%s', 'school', self::PAYPAL)),
            self::PAY_U_MONEY => __(sprintf('%s.%s', 'school', self::PAY_U_MONEY)),
            self::PAY_TM => __(sprintf('%s.%s', 'school', self::PAY_TM)),
            self::PAY_STACK => __(sprintf('%s.%s', 'school', self::PAY_STACK)),



        ];
    }
}
