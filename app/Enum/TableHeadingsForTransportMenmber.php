<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForTransportMenmber extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const GRADE = 'class';
    public const SECTION = 'section';
    public const ROLL_NO = 'roll_no';
    public const TRANSPORT_ROUTE = 'transport_route';
    public const STOP_NAME = 'stop_name';
    public const STOP_KM = 'stop_km';
    public const STOP_FARE = 'stop_fare';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::ROLL_NO => __(sprintf('%s.%s', 'school', self::ROLL_NO)),
            self::TRANSPORT_ROUTE => __(sprintf('%s.%s', 'school', self::TRANSPORT_ROUTE)),
            self::STOP_NAME => __(sprintf('%s.%s', 'school', self::STOP_NAME)),
            self::STOP_KM => __(sprintf('%s.%s', 'school', self::STOP_KM)),
            self::STOP_FARE => __(sprintf('%s.%s', 'school', self::STOP_FARE)),


        ];
    }
}
