<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForExamGrade extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const GRADE_NAME = 'grade_name';
    public const GRADE_POINT = 'grade_point';
    public const MARK_FROM = 'mark_from';
    public const MARK_TO = 'mark_to';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::GRADE_NAME => __(sprintf('%s.%s', 'school', self::GRADE_NAME)),
            self::GRADE_POINT => __(sprintf('%s.%s', 'school', self::GRADE_POINT)),
            self::MARK_FROM => __(sprintf('%s.%s', 'school', self::MARK_FROM)),
            self::MARK_TO => __(sprintf('%s.%s', 'school', self::MARK_TO)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),


        ];
    }
}
