<?php

declare(strict_types=1);

namespace App\Enum;

class AttendanceNavEnum extends AbstractEnum
{
    public const STUDENT_ATTENDANCE = 'student_attendance';
    public const TEACHER_ATTENDANCE= 'teacher_attendance';
    public const EMPLOYEE_ATTENDANCE= 'employee_attendance';
    public const ABSENT_EMAIL= 'absent_email';
    public const ABSENT_SMS= 'absent_sms';

    public static function getValues(): array
    {
        return [
            self::STUDENT_ATTENDANCE,
            self::TEACHER_ATTENDANCE,
            self::EMPLOYEE_ATTENDANCE,
            self::ABSENT_EMAIL,
            self::ABSENT_SMS
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::STUDENT_ATTENDANCE => route('dashboard.attendances.index',['type'=>'student']),
            self::TEACHER_ATTENDANCE => route('dashboard.attendances.index',['type'=>'teacher']),
            self::EMPLOYEE_ATTENDANCE => route('dashboard.attendances.index',['type'=>'employee']),
            self::ABSENT_EMAIL => route('dashboard.absent.emails.index'),
            self::ABSENT_SMS => route('dashboard.absent.sms.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::STUDENT_ATTENDANCE => '<i class="bx bx-user-plus fs-1"></i>',
            self::TEACHER_ATTENDANCE =>'<i class="bx bx-certification fs-1"></i>',
            self::EMPLOYEE_ATTENDANCE =>'<i class="bx bx-certification fs-1"></i>',
            self::ABSENT_EMAIL =>'<i class="bx bx-certification fs-1"></i>',
            self::ABSENT_SMS =>'<i class="bx bx-certification fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::STUDENT_ATTENDANCE =>__(sprintf('%s.%s','school',self::STUDENT_ATTENDANCE)),
            self::TEACHER_ATTENDANCE =>__(sprintf('%s.%s','school',self::TEACHER_ATTENDANCE)),
            self::EMPLOYEE_ATTENDANCE =>__(sprintf('%s.%s','school',self::EMPLOYEE_ATTENDANCE)),
            self::ABSENT_EMAIL =>__(sprintf('%s.%s','school',self::ABSENT_EMAIL)),
            self::ABSENT_SMS =>__(sprintf('%s.%s','school',self::ABSENT_SMS)),
        ];
    }
}
