<?php

declare(strict_types=1);

namespace App\Enum;

class LanguageEnum extends AbstractEnum
{
    public const ENGLISH = 'en';
    public const CHINES = 'ch';
    public const MALAY = 'mal';
    public const TAMIL = 'tam';

    public static function getValues(): array
    {
        return [
            self::ENGLISH,
            self::CHINES,
            self::MALAY,
            self::TAMIL
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::ENGLISH => __(sprintf('%s.%s', 'school', self::ENGLISH)),
            self::CHINES => __(sprintf('%s.%s', 'school', self::CHINES)),
            self::MALAY => __(sprintf('%s.%s', 'school', self::MALAY)),
            self::TAMIL => __(sprintf('%s.%s', 'school', self::TAMIL))
        ];
    }
}
