<?php

declare(strict_types=1);

namespace App\Enum;

class DateFormatEnum extends AbstractEnum
{
    public const ONE = 'Y-m-d';
    public const TWO = 'd-m-Y';
    public const THREE = 'd/m/Y';
    public const FOUR = 'm/d/Y';
    public const FIVE = 'm.d.Y';
    public const SIX = 'j, n, Y';
    public const SEVEN = 'F j, Y';
    public const EIGHT = 'M j, Y';
    public const NINE = 'j M, Y';

    public static function getValues(): array
    {
        return [
            self::ONE,
            self::TWO,
            self::THREE,
            self::FOUR,
            self::FIVE,
            self::SIX,
            self::SEVEN,
            self::EIGHT,
            self::NINE
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::ONE => self::ONE,
            self::TWO => self::TWO,
            self::THREE => self::THREE,
            self::FOUR => self::FOUR,
            self::FIVE => self::FIVE,
            self::SIX => self::SIX,
            self::SEVEN => self::SEVEN,
            self::EIGHT => self::EIGHT,
            self::NINE => self::NINE
        ];
    }
}
