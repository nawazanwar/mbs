<?php

declare(strict_types=1);

namespace App\Enum;

class StudentNavEnum extends AbstractEnum
{
    public const STUDENT_TYPE = 'student_type';
    public const STUDENT_LIST = 'student_list';
    public const ADMIT_STUDENT = 'admit_student';
    public const BULK_ADMISSION = 'bulk_admission';
    public const ONLINE_ADMISSION = 'online_admission';
    public const STUDENT_ACTIVITY = 'student_activity';

    public static function getValues(): array
    {
        return [
            self::STUDENT_TYPE,
            self::STUDENT_LIST,
            self::ADMIT_STUDENT,
            self::BULK_ADMISSION,
            self::ONLINE_ADMISSION,
            self::STUDENT_ACTIVITY,
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::STUDENT_TYPE => route('dashboard.student.types.index'),
            self::STUDENT_LIST => route('dashboard.students.index'),
            self::ADMIT_STUDENT => route('dashboard.students.create'),
            self::BULK_ADMISSION => route('dashboard.students.create',['type'=>'bulk']),
            self::ONLINE_ADMISSION => route('dashboard.online.admissions.index'),
            self::STUDENT_ACTIVITY => route('dashboard.student.activities.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::STUDENT_TYPE => '<i class="bx bx-building fs-1"></i>',
            self::STUDENT_LIST => '<i class="bx bx-list-ul fs-1"></i>',
            self::ADMIT_STUDENT => '<i class="bx bx-chip fs-1"></i>',
            self::BULK_ADMISSION => '<i class="bx bx-chip fs-1"></i>',
            self::ONLINE_ADMISSION => '<i class="bx bx-certification fs-1"></i>',
            self::STUDENT_ACTIVITY => '<i class="bx bx-compass fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }


    public static function getTranslationKeys(): array
    {
        return [
            self::STUDENT_TYPE => __(sprintf('%s.%s', 'school', self::STUDENT_TYPE)),
            self::STUDENT_LIST => __(sprintf('%s.%s', 'school', self::STUDENT_LIST)),
            self::ADMIT_STUDENT => __(sprintf('%s.%s', 'school', self::ADMIT_STUDENT)),
            self::BULK_ADMISSION => __(sprintf('%s.%s', 'school', self::BULK_ADMISSION)),
            self::ONLINE_ADMISSION => __(sprintf('%s.%s', 'school', self::ONLINE_ADMISSION)),
            self::STUDENT_ACTIVITY => __(sprintf('%s.%s', 'school', self::STUDENT_ACTIVITY))
        ];
    }
}
