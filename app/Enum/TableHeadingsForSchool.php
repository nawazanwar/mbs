<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSchool extends AbstractEnum
{
    public const LANGUAGE = 'language';
    public const THEME = 'theme';
    public const LOGO = 'logo';
    public const NAME = 'name';
    public const SCHOOL_CODE= 'code';
    public const REG_DATE= 'reg_date';
    public const ADDRESS = 'address';
    public const PHONE = 'phone';
    public const EMAIL = 'email';
    public const FAX = 'fax';
    public const LATITUDE = 'latitude';
    public const LONGITUDE = 'longitude';
    public const ABOUT = 'about';
    public const FACEBOOK = 'facebook';
    public const TWITTER = 'twitter';
    public const LINKEDIN = 'linkedin';
    public const GOOGLE_PLUS = 'google_plus';
    public const YOUTUBE = 'youtube';
    public const INSTAGRAM = 'instagram';
    public const PINTEREST = 'pinterest';
    public const STATUS = 'status';
    public const IS_MAIN_BRANCH = 'is_main_branch';
    public const PARENT_SCHOOL = 'parent_school';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::LANGUAGE => __(sprintf('%s.%s', 'school', self::LANGUAGE)),
            self::THEME => __(sprintf('%s.%s', 'school', self::THEME)),
            self::LOGO => __(sprintf('%s.%s', 'school', self::LOGO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::SCHOOL_CODE => __(sprintf('%s.%s', 'school', self::SCHOOL_CODE)),
            self::REG_DATE => __(sprintf('%s.%s', 'school', self::REG_DATE)),
            self::ADDRESS => __(sprintf('%s.%s', 'school', self::ADDRESS)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::FAX => __(sprintf('%s.%s', 'school', self::FAX)),
            self::LATITUDE => __(sprintf('%s.%s', 'school', self::LATITUDE)),
            self::LONGITUDE => __(sprintf('%s.%s', 'school', self::LONGITUDE)),
            self::ABOUT => __(sprintf('%s.%s', 'school', self::ABOUT)),
            self::FACEBOOK => __(sprintf('%s.%s', 'school', self::FACEBOOK)),
            self::TWITTER => __(sprintf('%s.%s', 'school', self::TWITTER)),
            self::LINKEDIN => __(sprintf('%s.%s', 'school', self::LINKEDIN)),
            self::GOOGLE_PLUS => __(sprintf('%s.%s', 'school', self::GOOGLE_PLUS)),
            self::YOUTUBE => __(sprintf('%s.%s', 'school', self::YOUTUBE)),
            self::INSTAGRAM => __(sprintf('%s.%s', 'school', self::INSTAGRAM)),
            self::STATUS => __(sprintf('%s.%s', 'school', self::STATUS)),
            self::IS_MAIN_BRANCH => __(sprintf('%s.%s', 'school', self::IS_MAIN_BRANCH)),
            self::PARENT_SCHOOL => __(sprintf('%s.%s', 'school', self::PARENT_SCHOOL)),

        ];
    }
}
