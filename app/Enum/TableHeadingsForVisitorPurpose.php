<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForVisitorPurpose extends AbstractEnum
{
    public const VISITOR_PURPOSE = 'visitor_purpose';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::VISITOR_PURPOSE => __(sprintf('%s.%s', 'school', self::VISITOR_PURPOSE)),


        ];
    }
}
