<?php

declare(strict_types=1);

namespace App\Enum;

class PermissionEnum extends AbstractEnum
{
    public const SETTING = 'settings';
    public const THEME = 'theme';
    public const LANGUAGE = 'language';
    public const ADMINISTRATOR = 'administrator';
    public const FEE_COLLECTION = 'fee_collection';
    public const HUMAN_RESOURCE = 'human_resource';
    public const TEACHER = 'teacher';
    public const ACADEMIC_ACTIVITY = 'academic_activity';
    public const GUARDIAN = 'guardian';
    public const STUDENT = 'student';
    public const ATTENDANCE = 'attendance';
    public const ASSIGNMENT = 'assignment';
    public const EXAM = 'exam';
    public const EXAM_MARK = 'exam_mark';
    public const LIBRARY = 'library';
    public const TRANSPORT = 'transport';
    public const HOSTEL = 'hostel';
    public const MESSAGE_EMAIL_SMS = 'message_email_sms';
    public const ANNOUNCEMENT = 'announcement';
    public const EVENT = 'event';
    public const FRONT_OFFICE = 'front_office';
    public const ACCOUNTING = 'accounting';
    public const REPORT = 'report';
    public const CERTIFICATE = 'certificate';
    public const MEDIA_GALLERY = 'media_gallery';
    public const FRONT_END = 'front_office';
    public const PAYROLL = 'payroll';
    public const COMPLAIN = 'complain';
    public const USER_COMPLAIN = 'user_complain';
    public const USER_LEAVE = 'user_leave';
    public const LEAVE = 'leave';
    public const LEAVE_MANAGEMENT = 'leave_management';
    public const ID_CARD_ADMIT_CARD = 'id_card_admit_card';

    public static function getValues(): array
    {
        return [
            self::SETTING => [
                'general_setting',
                'payment_setting',
                'sms_setting',
                'email_setting',
                'global_search',
                'global_session_change'
            ],
            self::THEME => [
                'theme'
            ],
            self::LANGUAGE => [
                'language'
            ],
            self::ADMINISTRATOR => [
                'academic_year',
                'user_role',
                'role_permission',
                'reset_user_password',
                'backup_data_base',
                'manage_user',
                'school',
                'payment',
                'sms',
                'sms_template',
                'email_template',
                'activity_log',
                'super_admin',
                'guardian_feedback',
                'general_setting',
                'email_setting',
                'user_credentials',
                'username',
            ],
            self::HUMAN_RESOURCE => [
                'designation',
                'employee'
            ],
            self::TEACHER => [
                'teacher',
                'teacher_lecture'
            ],
            self::GUARDIAN => [
                'guardian',
                'feedback',
            ],
            self::STUDENT => [
                'student',
                'bulk_import',
                'student_activity',
                'online_admission',
                'student_type'
            ],
            self::ATTENDANCE => [
                'employee_attendance',
                'teacher_attendance',
                'student_attendance',
                'absent_email',
                'absent_sms'
            ],
            self::ASSIGNMENT => [
                'assignment',
                'assignment_submission'
            ],
            self::EXAM => [
                'exam_term',
                'exam_grade',
                'exam_schedule',
                'exam_suggestion',
                'exam_attendance'
            ],
            self::EXAM_MARK => [
                'exam_mark',
                'mark_sheet',
                'result',
                'mark_send_by_sms',
                'mark_send_by_email',
                'exam_result',
                'final_result',
                'merit_list',
                'result_email',
                'result_sms',
                'result_card'
            ],
            self::LIBRARY => [
                'library_book',
                'library_member',
                'issue_and_return',
                'e_book'
            ],
            self::TRANSPORT => [
                'vehicle',
                'transport_route',
                'transport_member'
            ],
            self::HOSTEL => [
                'hostel',
                'hostel_room',
                'hostel_member'
            ],
            self::MESSAGE_EMAIL_SMS => [
                'email',
                'tex_sms',
                'text'
            ],
            self::ANNOUNCEMENT => [
                'notice',
                'news',
                'holiday'
            ],
            self::EVENT => [
                'event'
            ],
            self::FRONT_OFFICE => [
                'visitor',
                'visitor_purpose',
                'call_logs',
                'postal_dispatch',
                'postal_receive',
                'front_office'
            ],
            self::ACCOUNTING => [
                'expenditure_head',
                'expenditure',
                'income_head',
                'income',
                'invoice',
                'payment',
                'discount',
                'fee_type',
                'due_fee_email',
                'due_fee_sms',
                'invoice_receipt'
            ],
            self::REPORT => [
                'report'
            ],
            self::CERTIFICATE => [
                'certificate_type',
                'certificate'
            ],
            self::MEDIA_GALLERY => [
                'gallery',
                'image'
            ],
            self::FRONT_END => [
                'frontend',
                'home_slider',
                'about'
            ],
            self::PAYROLL => [
                'salary_grade',
                'payment',
                'history'
            ],
            self::COMPLAIN => [
                'complain',
                'complain_type'
            ],
            self::USER_COMPLAIN => [
                'user_complain'
            ],
            self::USER_LEAVE => [
                'user_leave'
            ],
            self::LEAVE_MANAGEMENT => [
                'leave_management',
                'leave_type',
                'leave_application',
                'waiting_leave',
                'approve_leave',
                'decline_leave'
            ],
            self::ID_CARD_ADMIT_CARD => [
                'id_admit_card',
                'teacher_id_card',
                'employee_id_card',
                'student_id_card',
                'id_card_setting',
                'admit_card_setting',
                'admit_card',
                'school_id_setting',
                'school_admit_setting'
            ],
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
        ];
    }
}
