<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAcademicYear extends AbstractEnum
{
    public const SESSION_START = 'session_start';
    public const SESSION_END = 'session_end';
    public const NOTE = 'note';





    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SESSION_START => __(sprintf('%s.%s', 'school', self::SESSION_START)),
            self::SESSION_END => __(sprintf('%s.%s', 'school', self::SESSION_END)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),


        ];
    }
}
