<?php

declare(strict_types=1);

namespace App\Enum;

class AuthorizationNavEnum extends AbstractEnum
{
    public const ROLES = 'roles';
    public const PERMISSIONS = 'permissions';
    public const USERS = 'users';

    public static function getValues(): array
    {
        return [
            self::ROLES,
            self::PERMISSIONS,
            self::USERS
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::ROLES => route('dashboard.roles.index'),
            self::PERMISSIONS => route('dashboard.permissions.index'),
            self::USERS => route('dashboard.users.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::ROLES => '<i class="bx bx-user fs-1"></i>',
            self::PERMISSIONS => '<i class="bx bx-user fs-1"></i>',
            self::USERS => '<i class="bx bx-user fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }


    public static function getTranslationKeys(): array
    {
        return [
            self::ROLES => __(sprintf('%s.%s', 'school', self::ROLES)),
            self::PERMISSIONS => __(sprintf('%s.%s', 'school', self::PERMISSIONS)),
            self::USERS => __(sprintf('%s.%s', 'school', self::USERS))
        ];
    }
}
