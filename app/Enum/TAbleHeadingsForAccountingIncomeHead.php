<?php

declare(strict_types=1);

namespace App\Enum;

class TAbleHeadingsForAccountingIncomeHead extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const INCOME_HEAD = 'income_head';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::INCOME_HEAD => __(sprintf('%s.%s', 'school', self::INCOME_HEAD)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
        ];
    }
}

