<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForVisitorInfo extends AbstractEnum
{
    public const NAME = 'name';
    public const PHONE = 'phone';
    public const EMAIL = 'email';
    public const TO_MEET = 'to_meet';
    public const CHECK_IN = 'check_in';
    public const CHECK_OUT = 'check_out';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::TO_MEET => __(sprintf('%s.%s', 'school', self::TO_MEET)),
            self::CHECK_IN => __(sprintf('%s.%s', 'school', self::CHECK_IN)),
            self::CHECK_OUT => __(sprintf('%s.%s', 'school', self::CHECK_OUT)),

        ];
    }
}
