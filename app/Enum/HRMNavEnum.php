<?php

declare(strict_types=1);

namespace App\Enum;

class HRMNavEnum extends AbstractEnum
{
    public const PERSON_TYPES = 'person_types';
    public const DESIGNATIONS = 'designations';
    public const DEPARTMENTS = 'departments';
    public const EMPLOYERS = 'employers';
    public const EMPLOYEE_TYPES = 'employee_types';
    public const EMPLOYEE_SUB_TYPES = 'employee_sub_types';
    public const CASTS = 'casts';
    public const TAX_STATUSES = 'tax_statuses';
    public const TAX_TYPES = 'tax_types';
    public const JOB_STATUSES = 'job_statuses';
    public const BUSINESS_TYPE = 'business_type';
    public const BUSINESS_SUB_TYPE = 'business_sub_type';


    public static function getValues(): array
    {
        return [
            self::PERSON_TYPES,
            self::DESIGNATIONS,
            self::DEPARTMENTS,
            self::EMPLOYERS,
            self::EMPLOYEE_TYPES,
            self::EMPLOYEE_SUB_TYPES,
            self::CASTS,
            self::TAX_STATUSES,
            self::TAX_TYPES,
            self::JOB_STATUSES,
            self::BUSINESS_TYPE,
            self::BUSINESS_SUB_TYPE,
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::PERSON_TYPES => route('dashboard.hrm-person-types.index'),
            self::DESIGNATIONS => route('dashboard.hrm-designations.index'),
            self::DEPARTMENTS => route('dashboard.hrm-departments.index'),
            self::EMPLOYERS => route('dashboard.hrm-employers.index'),
            self::EMPLOYEE_TYPES => route('dashboard.hrm-emp-types.index'),
            self::EMPLOYEE_SUB_TYPES => route('dashboard.hrm-emp-sub-types.index'),
            self::CASTS => route('dashboard.hrm-casts.index'),
            self::TAX_STATUSES => route('dashboard.hrm-tax-statuses.index'),
            self::TAX_TYPES => route('dashboard.hrm-tax-types.index'),
            self::JOB_STATUSES => route('dashboard.hrm-job-statuses.index'),
            self::BUSINESS_TYPE => route('dashboard.hrm-business-types.index'),
            self::BUSINESS_SUB_TYPE => route('dashboard.hrm-business-sub-types.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }


    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::PERSON_TYPES => '<i class="bx bx-user-minus fs-1"></i>',
            self::DESIGNATIONS => '<i class="bx bx-check-circle fs-1"></i>',
            self::DEPARTMENTS => '<i class="bx bx-check-circle fs-1"></i>',
            self::EMPLOYERS => '<i class="bx bx-check-circle fs-1"></i>',
            self::EMPLOYEE_TYPES => '<i class="bx bx-check-circle fs-1"></i>',
            self::EMPLOYEE_SUB_TYPES => '<i class="bx bx-check-circle fs-1"></i>',
            self::CASTS => '<i class="bx bx-check-circle fs-1"></i>',
            self::TAX_STATUSES => '<i class="bx bx-check-circle fs-1"></i>',
            self::TAX_TYPES => '<i class="bx bx-check-circle fs-1"></i>',
            self::JOB_STATUSES => '<i class="bx bx-check-circle fs-1"></i>',
            self::BUSINESS_TYPE => '<i class="bx bx-check-circle fs-1"></i>',
            self::BUSINESS_SUB_TYPE => '<i class="bx bx-check-circle fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::PERSON_TYPES => __(sprintf('%s.%s', 'school', self::PERSON_TYPES)),
            self::DESIGNATIONS => __(sprintf('%s.%s', 'school', self::DESIGNATIONS)),
            self::DEPARTMENTS => __(sprintf('%s.%s', 'school', self::DEPARTMENTS)),
            self::EMPLOYERS => __(sprintf('%s.%s', 'school', self::EMPLOYERS)),
            self::EMPLOYEE_TYPES => __(sprintf('%s.%s', 'school', self::EMPLOYEE_TYPES)),
            self::EMPLOYEE_SUB_TYPES => __(sprintf('%s.%s', 'school', self::EMPLOYEE_SUB_TYPES)),
            self::CASTS => __(sprintf('%s.%s', 'school', self::CASTS)),
            self::TAX_STATUSES => __(sprintf('%s.%s', 'school', self::TAX_STATUSES)),
            self::TAX_TYPES => __(sprintf('%s.%s', 'school', self::TAX_TYPES)),
            self::JOB_STATUSES => __(sprintf('%s.%s', 'school', self::JOB_STATUSES)),
            self::BUSINESS_TYPE => __(sprintf('%s.%s', 'school', self::BUSINESS_TYPE)),
            self::BUSINESS_SUB_TYPE => __(sprintf('%s.%s', 'school', self::BUSINESS_SUB_TYPE)),
        ];
    }
}
