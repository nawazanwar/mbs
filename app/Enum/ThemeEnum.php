<?php

declare(strict_types=1);

namespace App\Enum;

class ThemeEnum extends AbstractEnum
{
    public const JAZZBERRY_JAM = 'jazzberry_jam';
    public const BLACK = 'black';
    public const UMBER = 'umber';
    public const MEDIUM_PURPLE = 'medium_purple';
    public const LIME_GREEN = 'lime_green';
    public const REBECCA_PURPLE = 'rebecca_purple';
    public const RADICAL_RED = 'radical_red';
    public const DODGER_BLUE = 'dodger_blue';
    public const MAROON = 'maroon';
    public const DARK_ORANGE = 'dark_orange';
    public const DEEP_PINK = 'deep_pink';
    public const TRINIDAD = 'trinidad';
    public const SLATE_GRAY = 'slate_gray';
    public const LIGHT_SEA_GREEN = 'light_sea_green';
    public const NAVY_BLUE = 'navy_blue';
    public const RED = 'red';

    public static function getValues(): array
    {
        return [
            self::JAZZBERRY_JAM,
            self::BLACK,
            self::UMBER,
            self::MEDIUM_PURPLE,
            self::LIME_GREEN,
            self::REBECCA_PURPLE,
            self::RADICAL_RED,
            self::DODGER_BLUE,
            self::MAROON,
            self::DARK_ORANGE,
            self::DEEP_PINK,
            self::TRINIDAD,
            self::SLATE_GRAY,
            self::LIGHT_SEA_GREEN,
            self::NAVY_BLUE,
            self::RED
        ];
    }

    public static function getColorByKey($key): ?string
    {
       $data= [
            self::JAZZBERRY_JAM=>'9F134E',
            self::BLACK=>'23282d',
            self::UMBER=>'745D0B',
            self::MEDIUM_PURPLE=>'9370DB',
            self::LIME_GREEN=>'32CD32',
            self::REBECCA_PURPLE=>'663399',
            self::RADICAL_RED=>'FB2E50',
            self::DODGER_BLUE=>'1E90FF',
            self::MAROON=>'800000',
            self::DARK_ORANGE=>'FF8C00',
            self::DEEP_PINK=>'FF1493',
            self::TRINIDAD=>'CC4F26',
            self::SLATE_GRAY=>'2A3F54',
            self::LIGHT_SEA_GREEN=>'20B2AA',
            self::NAVY_BLUE=>'001f67',
            self::RED=>'e80000'
        ];
        if (!is_null($key) && array_key_exists($key, $data)) {
            return $data[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::JAZZBERRY_JAM => __(sprintf('%s.%s', 'school', self::JAZZBERRY_JAM)),
            self::BLACK => __(sprintf('%s.%s', 'school', self::BLACK)),
            self::UMBER => __(sprintf('%s.%s', 'school', self::UMBER)),
            self::MEDIUM_PURPLE => __(sprintf('%s.%s', 'school', self::MEDIUM_PURPLE)),

            self::LIME_GREEN => __(sprintf('%s.%s', 'school', self::LIME_GREEN)),
            self::REBECCA_PURPLE => __(sprintf('%s.%s', 'school', self::REBECCA_PURPLE)),
            self::RADICAL_RED => __(sprintf('%s.%s', 'school', self::RADICAL_RED)),
            self::DODGER_BLUE => __(sprintf('%s.%s', 'school', self::DODGER_BLUE)),
            self::MAROON => __(sprintf('%s.%s', 'school', self::MAROON)),
            self::DARK_ORANGE => __(sprintf('%s.%s', 'school', self::DARK_ORANGE)),

            self::DEEP_PINK => __(sprintf('%s.%s', 'school', self::DEEP_PINK)),
            self::TRINIDAD => __(sprintf('%s.%s', 'school', self::TRINIDAD)),
            self::SLATE_GRAY => __(sprintf('%s.%s', 'school', self::SLATE_GRAY)),
            self::LIGHT_SEA_GREEN => __(sprintf('%s.%s', 'school', self::LIGHT_SEA_GREEN)),
            self::NAVY_BLUE => __(sprintf('%s.%s', 'school', self::NAVY_BLUE)),
            self::RED => __(sprintf('%s.%s', 'school', self::RED))
        ];
    }
}
