<?php

declare(strict_types=1);

namespace App\Enum;

class ReportNavEnum extends AbstractEnum
{
    public const INCOME = 'income';
    public const EXPENSE = 'expense';
    public const INVOICE = 'invoice';
    public const DUE_FEE = 'due_fee';
    public const FEE_COLLECTION = 'fee_collection';
    public const ACCOUNTING_BALANCE = 'accounting_balance';
    public const LIBRARY = 'library';
    public const STUDENT_ATTENDANCE = 'student_attendance';
    public const STUDENT_YEARLY_ATTENDANCE = 'student_yearly_attendance';
    public const TEACHER_ATTENDANCE = 'teacher_attendance';
    public const TEACHER_YEARLY_ATTENDANCE = 'teacher_yearly_attendance';
    public const EMPLOYEE_ATTENDANCE = 'employee_attendance';
    public const EMPLOYEE_YEARLY_ATTENDANCE = 'employee_yearly_attendance';
    public const STUDENT = 'student';
    public const STUDENT_INVOICE = 'student_invoice';
    public const STUDENT_ACTIVITY = 'student_activity';
    public const PAYROLL = 'payroll';
    public const DAILY_TRANSACTION = 'daily_transaction';
    public const DAILY_STATEMENT = 'daily_statement';
    public const EXAM_RESULT = 'exam_result';

    public static function getValues(): array
    {
        return [
            self::INCOME,
            self::EXPENSE,
            self::INVOICE,
            self::DUE_FEE,
            self::FEE_COLLECTION,
            self::ACCOUNTING_BALANCE,
            self::LIBRARY,
            self::STUDENT_ATTENDANCE,
            self::STUDENT_YEARLY_ATTENDANCE,
            self::TEACHER_ATTENDANCE,
            self::TEACHER_YEARLY_ATTENDANCE,
            self::EMPLOYEE_ATTENDANCE,
            self::EMPLOYEE_YEARLY_ATTENDANCE,
            self::STUDENT,
            self::STUDENT_INVOICE,
            self::STUDENT_ACTIVITY,
            self::PAYROLL,
            self::DAILY_TRANSACTION,
            self::DAILY_STATEMENT,
            self::EXAM_RESULT
        ];
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::INCOME=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::EXPENSE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::INVOICE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::DUE_FEE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::FEE_COLLECTION=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::ACCOUNTING_BALANCE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::LIBRARY=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::STUDENT_ATTENDANCE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::STUDENT_YEARLY_ATTENDANCE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::TEACHER_ATTENDANCE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::TEACHER_YEARLY_ATTENDANCE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::EMPLOYEE_ATTENDANCE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::EMPLOYEE_YEARLY_ATTENDANCE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::STUDENT=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::STUDENT_INVOICE=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::STUDENT_ACTIVITY=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::PAYROLL=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::DAILY_TRANSACTION=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::DAILY_STATEMENT=>'<i class="bx bx-bar-chart fs-1"></i>',
            self::EXAM_RESULT=>'<i class="bx bx-bar-chart fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::INCOME => route('dashboard.reports.index', ['type' => self::INCOME]),
            self::EXPENSE=> route('dashboard.reports.index', ['type' => self::EXPENSE]),
            self::INVOICE=> route('dashboard.reports.index', ['type' => self::INVOICE]),
            self::DUE_FEE=> route('dashboard.reports.index', ['type' => self::DUE_FEE]),
            self::FEE_COLLECTION=> route('dashboard.reports.index', ['type' => self::FEE_COLLECTION]),
            self::ACCOUNTING_BALANCE=> route('dashboard.reports.index', ['type' => self::ACCOUNTING_BALANCE]),
            self::LIBRARY=> route('dashboard.reports.index', ['type' => self::LIBRARY]),

            self::STUDENT=> route('dashboard.reports.index', ['type' => self::STUDENT]),
            self::STUDENT_INVOICE=> route('dashboard.reports.index', ['type' => self::STUDENT_INVOICE]),
            self::STUDENT_ACTIVITY=> route('dashboard.reports.index', ['type' => self::STUDENT_ACTIVITY]),
            self::STUDENT_ATTENDANCE=> route('dashboard.reports.index', ['type' => self::STUDENT_ATTENDANCE]),
            self::STUDENT_YEARLY_ATTENDANCE=> route('dashboard.reports.index', ['type' => self::STUDENT_YEARLY_ATTENDANCE]),

            self::TEACHER_ATTENDANCE=> route('dashboard.reports.index', ['type' => self::TEACHER_ATTENDANCE]),
            self::TEACHER_YEARLY_ATTENDANCE=> route('dashboard.reports.index', ['type' => self::TEACHER_YEARLY_ATTENDANCE]),

            self::EMPLOYEE_ATTENDANCE=> route('dashboard.reports.index', ['type' => self::EMPLOYEE_ATTENDANCE]),
            self::EMPLOYEE_YEARLY_ATTENDANCE=> route('dashboard.reports.index', ['type' => self::EMPLOYEE_YEARLY_ATTENDANCE]),

            self::PAYROLL=> route('dashboard.reports.index', ['type' => self::PAYROLL]),
            self::DAILY_TRANSACTION=> route('dashboard.reports.index', ['type' => self::DAILY_TRANSACTION]),
            self::DAILY_STATEMENT=> route('dashboard.reports.index', ['type' => self::DAILY_STATEMENT]),
            self::EXAM_RESULT=> route('dashboard.reports.index', ['type' => self::EXAM_RESULT])
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::INCOME => __(sprintf('%s.%s', 'school', self::INCOME)),
            self::EXPENSE => __(sprintf('%s.%s', 'school', self::EXPENSE)),
            self::INVOICE => __(sprintf('%s.%s', 'school', self::INVOICE)),
            self::DUE_FEE => __(sprintf('%s.%s', 'school', self::DUE_FEE)),

            self::FEE_COLLECTION => __(sprintf('%s.%s', 'school', self::FEE_COLLECTION)),
            self::ACCOUNTING_BALANCE => __(sprintf('%s.%s', 'school', self::ACCOUNTING_BALANCE)),
            self::LIBRARY => __(sprintf('%s.%s', 'school', self::LIBRARY)),

            self::STUDENT => __(sprintf('%s.%s', 'school', self::STUDENT)),
            self::STUDENT_INVOICE => __(sprintf('%s.%s', 'school', self::STUDENT_INVOICE)),
            self::STUDENT_ACTIVITY => __(sprintf('%s.%s', 'school', self::STUDENT_ACTIVITY)),
            self::STUDENT_ATTENDANCE => __(sprintf('%s.%s', 'school', self::STUDENT_ATTENDANCE)),
            self::STUDENT_YEARLY_ATTENDANCE => __(sprintf('%s.%s', 'school', self::STUDENT_YEARLY_ATTENDANCE)),

            self::TEACHER_ATTENDANCE => __(sprintf('%s.%s', 'school', self::TEACHER_ATTENDANCE)),
            self::TEACHER_YEARLY_ATTENDANCE => __(sprintf('%s.%s', 'school', self::TEACHER_YEARLY_ATTENDANCE)),

            self::EMPLOYEE_ATTENDANCE => __(sprintf('%s.%s', 'school', self::EMPLOYEE_ATTENDANCE)),
            self::EMPLOYEE_YEARLY_ATTENDANCE => __(sprintf('%s.%s', 'school', self::EMPLOYEE_YEARLY_ATTENDANCE)),

            self::PAYROLL => __(sprintf('%s.%s', 'school', self::PAYROLL)),
            self::DAILY_TRANSACTION => __(sprintf('%s.%s', 'school', self::DAILY_TRANSACTION)),
            self::DAILY_STATEMENT => __(sprintf('%s.%s', 'school', self::DAILY_STATEMENT)),
            self::EXAM_RESULT => __(sprintf('%s.%s', 'school', self::EXAM_RESULT)),
        ];
    }
}
