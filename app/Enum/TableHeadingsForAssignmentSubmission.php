<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAssignmentSubmission extends AbstractEnum
{

    public const ASSIGMENT = 'assignment';
    public const GRADE = 'class';
    public const SECTION = 'section';
    public const SUBJECT = 'subject';
    public const ACADEMIC_YEAR = 'academic_year';
    public const SUBMITTED_BY = 'submitted_by';
    public const SUBMITTED_AT = 'submitted_at';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::ASSIGMENT => __(sprintf('%s.%s', 'school', self::ASSIGMENT)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::ACADEMIC_YEAR => __(sprintf('%s.%s', 'school', self::ACADEMIC_YEAR)),
            self::SUBMITTED_BY => __(sprintf('%s.%s', 'school', self::SUBMITTED_BY)),
            self::SUBMITTED_AT => __(sprintf('%s.%s', 'school', self::SUBMITTED_AT)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
        ];
    }
}
