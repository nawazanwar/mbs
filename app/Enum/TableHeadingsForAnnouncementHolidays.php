<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAnnouncementHolidays extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const FROM_DATE = 'from_date';
    public const TO_DATE = 'to_date';
    public const IS_VIEW_ON_WEB = 'is_view_on_web';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::FROM_DATE => __(sprintf('%s.%s', 'school', self::FROM_DATE)),
            self::TO_DATE => __(sprintf('%s.%s', 'school', self::TO_DATE)),
            self::IS_VIEW_ON_WEB => __(sprintf('%s.%s', 'school', self::IS_VIEW_ON_WEB)),
        ];
    }
}
