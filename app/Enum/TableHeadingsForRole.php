<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForRole extends AbstractEnum
{


    public const NAME = 'name';
    public const LABEL= 'label';





    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::LABEL => __(sprintf('%s.%s', 'school', self::LABEL)),



        ];
    }
}
