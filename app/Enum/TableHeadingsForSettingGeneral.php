<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSettingGeneral extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const GRADE = 'class';
    public const SECTION= 'section';
    public const ROLL_NO= 'roll_no';
    public const HOSTEL= 'hostel';
    public const ROOM_NO= 'room_no';
    public const COST_PER_SEAT= 'cost_per_seat';




    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::ROLL_NO => __(sprintf('%s.%s', 'school', self::ROLL_NO)),
            self::HOSTEL => __(sprintf('%s.%s', 'school', self::HOSTEL)),
            self::ROOM_NO => __(sprintf('%s.%s', 'school', self::ROOM_NO)),
            self::COST_PER_SEAT => __(sprintf('%s.%s', 'school', self::COST_PER_SEAT)),


        ];
    }
}
