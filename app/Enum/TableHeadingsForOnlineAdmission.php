<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForOnlineAdmission extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const GRADE = 'class';
    public const SECTION = 'section';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            ];
    }
}
