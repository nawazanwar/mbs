<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForTheme extends AbstractEnum
{
    public const NAME = 'name';
    public const COLOR = 'color';
    public const STATUS = 'status';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::COLOR => __(sprintf('%s.%s', 'school', self::COLOR)),
            self::STATUS => __(sprintf('%s.%s', 'school', self::STATUS))
        ];
    }
}
