<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForStudentActivity extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const STUDENT = 'student';
    public const GRADE = 'class';
    public const SECTION = 'section';
    public const ACTIVITY = 'activity';
    public const DATE = 'date';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::STUDENT => __(sprintf('%s.%s', 'school', self::STUDENT)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::ACTIVITY => __(sprintf('%s.%s', 'school', self::ACTIVITY)),
            self::DATE => __(sprintf('%s.%s', 'school', self::DATE)),

        ];
    }
}
