<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAccountingExpenditureHead extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const EXPENDITURE_HEAD = 'expenditure_head';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::EXPENDITURE_HEAD => __(sprintf('%s.%s', 'school', self::EXPENDITURE_HEAD)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),

        ];
    }
}
