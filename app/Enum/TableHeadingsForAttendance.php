<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAttendance extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const STUDENT = 'student';
    public const DATE = 'date';
    public const ONE = '1';
    public const TWO = '2';
    public const THREE = '3';
    public const FOUR = '4';
    public const FIVE = '5';
    public const SIX = '6';
    public const SEVEN = '7';
    public const EIGHT = '8';
    public const NINE = '9';
    public const TEN = '10';
    public const ELEVEN = '11';
    public const TWELVE = '12';
    public const THIRTEEN = '13';
    public const FOURTEEN = '14';
    public const FIFTEEN = '15';
    public const SIXTEEN = '16';
    public const SEVENTEEN = '17';
    public const EIGHTEEN = '18';
    public const NINETEEN = '19';
    public const TWENTY = '20';
    public const TWENTY_ONE = '21';
    public const TWENTY_TWO = '22';
    public const TWENTY_THREE = '23';
    public const TWENTY_FOUR = '24';
    public const TWENTY_FIVE = '25';
    public const TWENTY_SIX = '26';
    public const TWENTY_SEVEN = '27';
    public const TWENTY_EIGHT = '28';
    public const TWENTY_NINE = '29';
    public const THIRTY = '30';
    public const THIRTY_ONE = '31';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::STUDENT => __(sprintf('%s.%s', 'school', self::STUDENT)),
            self::DATE => __(sprintf('%s.%s', 'school', self::DATE)),
            self::ONE => 1,
            self::TWO => 2,
            self::THREE => 3,
            self::FOUR => 4,
            self::FIVE => 5,
            self::SIX => 6,
            self::SEVEN => 7,
            self::EIGHT => 8,
            self::NINE => 9,
            self::TEN => 10,
            self::ELEVEN => 11,
            self::TWELVE => 12,
            self::THIRTEEN => 13,
            self::FOURTEEN => 14,
            self::FIFTEEN => 15,
            self::SIXTEEN => 16,
            self::SEVENTEEN => 17,
            self::EIGHTEEN => 18,
            self::NINETEEN => 19,
            self::TWENTY => 20,
            self::TWENTY_ONE => 21,
            self::TWENTY_TWO => 22,
            self::TWENTY_THREE => 23,
            self::TWENTY_FOUR => 24,
            self::TWENTY_FIVE => 25,
            self::TWENTY_SIX => 26,
            self::TWENTY_SEVEN => 27,
            self::TWENTY_EIGHT => 28,
            self::TWENTY_NINE => 29,
            self::THIRTY => 30,
            self::THIRTY_ONE =>31,


        ];
    }
}
