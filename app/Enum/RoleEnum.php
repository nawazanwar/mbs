<?php

declare(strict_types=1);

namespace App\Enum;

class RoleEnum extends AbstractEnum
{
    public const ROLE_SUPER_ADMIN = 'super-admin';
    public const ROLE_ADMIN = 'admin';
    public const ROLE_STUDENT = 'student';
    public const ROLE_TEACHER = 'teacher';
    public const ROLE_PARENT = 'parent';
    public const ROLE_LIBRARIAN = 'librarian';
    public const ROLE_ACCOUNTANT = 'accountant';
    public const ROLE_EMPLOYEE = 'employee';
    public const ROLE_GUARDIAN = 'guardian';

    public static function getValues(): array
    {
        return [
            self::ROLE_SUPER_ADMIN,
            self::ROLE_ADMIN,
            self::ROLE_TEACHER,
            self::ROLE_STUDENT,
            self::ROLE_PARENT,
            self::ROLE_LIBRARIAN,
            self::ROLE_ACCOUNTANT,
            self::ROLE_EMPLOYEE,
            self::ROLE_GUARDIAN
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::ROLE_SUPER_ADMIN => __(self::ROLE_SUPER_ADMIN),
            self::ROLE_ADMIN => __(self::ROLE_ADMIN),
            self::ROLE_TEACHER => __(self::ROLE_TEACHER),
            self::ROLE_STUDENT => __(self::ROLE_STUDENT),
            self::ROLE_PARENT => __(self::ROLE_PARENT),
            self::ROLE_LIBRARIAN => __(self::ROLE_LIBRARIAN),
            self::ROLE_ACCOUNTANT => __(self::ROLE_ACCOUNTANT),
            self::ROLE_EMPLOYEE => __(self::ROLE_EMPLOYEE),
            self::ROLE_GUARDIAN => __(self::ROLE_GUARDIAN)
        ];
    }
}
