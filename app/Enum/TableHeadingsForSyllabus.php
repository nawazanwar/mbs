<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSyllabus extends AbstractEnum
{

    public const TITLE = 'title';
    public const GRADE = 'class';
    public const SUBJECT = 'subject';
    public const SESSION_YEAR = 'session_year';
    public static function getValues(): array
    {
        return [

        ];
    }
    public static function getTranslationKeys(): array
    {
        return [
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::SESSION_YEAR => __(sprintf('%s.%s', 'school', self::SESSION_YEAR)),
        ];
    }
}
