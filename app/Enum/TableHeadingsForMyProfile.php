<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForMyProfile extends AbstractEnum
{
    public const EMAIL = 'email';
    public const PRESENT_ADDRESS = 'present_address';
    public const GENDER = 'gender';
    public const RELIGION = 'religion';
    public const OTHER_INFO = 'other_info';
    public const PHONE = 'phone';
    public const PERMANENT_ADDRESS = 'permanent_address';
    public const BLOOD_GROUP = 'blood_group';
    public const BIRTH_DATE = 'birth_date';
    public const RESUME = 'resume';






    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::PRESENT_ADDRESS => __(sprintf('%s.%s', 'school', self::PRESENT_ADDRESS)),
            self::GENDER => __(sprintf('%s.%s', 'school', self::GENDER)),
            self::RELIGION => __(sprintf('%s.%s', 'school', self::RELIGION)),
            self::OTHER_INFO => __(sprintf('%s.%s', 'school', self::OTHER_INFO)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::PERMANENT_ADDRESS => __(sprintf('%s.%s', 'school', self::PERMANENT_ADDRESS)),
            self::BLOOD_GROUP => __(sprintf('%s.%s', 'school', self::BLOOD_GROUP)),
            self::BIRTH_DATE => __(sprintf('%s.%s', 'school', self::BIRTH_DATE)),
            self::RESUME => __(sprintf('%s.%s', 'school', self::RESUME)),





        ];
    }
}
