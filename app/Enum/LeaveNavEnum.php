<?php

declare(strict_types=1);

namespace App\Enum;

class LeaveNavEnum extends AbstractEnum
{

    public const LEAVE_TYPE = 'leave_type';
    public const LEAVE_APPLICATION = 'leave_application';
    public const WAITING_APPLICATION = 'waiting_application';
    public const APPROVED_APPLICATION = 'approved_application';
    public const DECLINED_APPLICATION = 'declined_application';

    public static function getValues(): array
    {
        return [
            self::LEAVE_TYPE,
            self::LEAVE_APPLICATION,
            self::WAITING_APPLICATION,
            self::APPROVED_APPLICATION,
            self::DECLINED_APPLICATION

        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::LEAVE_TYPE => route('dashboard.leave.types.index'),
            self::LEAVE_APPLICATION => route('dashboard.leave.applications.index'),
            self::WAITING_APPLICATION => route('dashboard.leave.applications.index',['type'=>'waiting']),
            self::APPROVED_APPLICATION => route('dashboard.leave.types.index',['type'=>'approved']),
            self::DECLINED_APPLICATION => route('dashboard.leave.types.index',['type'=>'declined']),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::LEAVE_TYPE => '<i class="bx bx-building fs-1"></i>',
            self::LEAVE_APPLICATION =>'<i class="bx bx-dollar fs-1"></i>',
            self::WAITING_APPLICATION =>'<i class="bx bx-dollar fs-1"></i>',
            self::APPROVED_APPLICATION =>'<i class="bx bx-dollar fs-1"></i>',
            self::DECLINED_APPLICATION =>'<i class="bx bx-dollar fs-1"></i>'

        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::LEAVE_TYPE => __(sprintf('%s.%s', 'school', self::LEAVE_TYPE)),
            self::LEAVE_APPLICATION => __(sprintf('%s.%s', 'school', self::LEAVE_APPLICATION)),
            self::WAITING_APPLICATION => __(sprintf('%s.%s', 'school', self::WAITING_APPLICATION)),
            self::APPROVED_APPLICATION => __(sprintf('%s.%s', 'school', self::APPROVED_APPLICATION)),
            self::DECLINED_APPLICATION => __(sprintf('%s.%s', 'school', self::DECLINED_APPLICATION))
        ];
    }
}
