<?php

declare(strict_types=1);

namespace App\Enum;

class AssignmentNavEnum extends AbstractEnum
{
    public const ASSIGNMENT = 'assignment';
    public const ASSIGNMENT_SUBMISSION = 'assignment_submission';

    public static function getValues(): array
    {
        return array();
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::ASSIGNMENT => route('dashboard.assignments.index'),
            self::ASSIGNMENT_SUBMISSION => route('dashboard.assignment.submissions.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::ASSIGNMENT => '<i class="bx bx-calculator fs-1"></i>',
            self::ASSIGNMENT_SUBMISSION => '<i class="bx bx-ghost fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::ASSIGNMENT => __(sprintf('%s.%s', 'school', self::ASSIGNMENT)),
            self::ASSIGNMENT_SUBMISSION => __(sprintf('%s.%s', 'school', self::ASSIGNMENT_SUBMISSION)),
        ];
    }
}
