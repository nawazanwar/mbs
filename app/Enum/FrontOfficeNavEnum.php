<?php

declare(strict_types=1);

namespace App\Enum;

class FrontOfficeNavEnum extends AbstractEnum
{
    public const VISITOR_PURPOSE = 'visitor_purpose';
    public const VISITOR_INFO = 'visitor_info';
    public const CALL_LOG = 'call_log';
    public const POSTAL_DISPATCH = 'postal_dispatch';
    public const POSTAL_RECEIVE = 'postal_receive';

    public static function getValues(): array
    {
        return [
            self::VISITOR_PURPOSE,
            self::VISITOR_INFO,
            self::CALL_LOG,
            self::POSTAL_DISPATCH,
            self::POSTAL_RECEIVE
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::VISITOR_PURPOSE => route('dashboard.visitor.purposes.index'),
            self::VISITOR_INFO => route('dashboard.visitors.index'),
            self::CALL_LOG => route('dashboard.call.logs.index'),
            self::POSTAL_DISPATCH => route('dashboard.postal.dispatches.index'),
            self::POSTAL_RECEIVE => route('dashboard.postal.receives.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::VISITOR_PURPOSE => '<i class="bx bx-certification fs-1"></i>',
            self::VISITOR_INFO => '<i class="bx bx-certification fs-1"></i>',
            self::CALL_LOG => '<i class="bx bx-certification fs-1"></i>',
            self::POSTAL_DISPATCH => '<i class="bx bx-certification fs-1"></i>',
            self::POSTAL_RECEIVE => '<i class="bx bx-certification fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::VISITOR_PURPOSE => __(sprintf('%s.%s', 'school', self::VISITOR_PURPOSE)),
            self::VISITOR_INFO => __(sprintf('%s.%s', 'school', self::VISITOR_INFO)),
            self::CALL_LOG => __(sprintf('%s.%s', 'school', self::CALL_LOG)),
            self::POSTAL_DISPATCH => __(sprintf('%s.%s', 'school', self::POSTAL_DISPATCH)),
            self::POSTAL_RECEIVE => __(sprintf('%s.%s', 'school', self::POSTAL_RECEIVE))
        ];
    }
}
