<?php

declare(strict_types=1);

namespace App\Enum;

class HostelNavEnum extends AbstractEnum
{
    public const MANAGE_HOSTEL = 'manage_hostel';
    public const MANAGE_ROOM = 'manage_room';
    public const MANAGE_MEMBER = 'manage_member';

    public static function getValues(): array
    {
        return array();
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::MANAGE_HOSTEL => route('dashboard.hostels.index'),
            self::MANAGE_ROOM => route('dashboard.hostel.rooms.index'),
            self::MANAGE_MEMBER => route('dashboard.hostel.members.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::MANAGE_HOSTEL => '<i class="bx bx-bed fs-1"></i>',
            self::MANAGE_ROOM =>'<i class="bx bx-bed fs-1"></i>',
            self::MANAGE_MEMBER =>'<i class="bx bx-user fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::MANAGE_HOSTEL => __(sprintf('%s.%s', 'school', self::MANAGE_HOSTEL)),
            self::MANAGE_ROOM => __(sprintf('%s.%s', 'school', self::MANAGE_ROOM)),
            self::MANAGE_MEMBER => __(sprintf('%s.%s', 'school', self::MANAGE_MEMBER))
        ];
    }
}
