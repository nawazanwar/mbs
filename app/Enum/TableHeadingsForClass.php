<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForClass extends AbstractEnum
{

    public const NAME = 'name';
    public const NUMERIC_NAME = 'numeric_name';
    public const CLASS_TEACHER = 'class_teacher';
    public const NOTE = 'note';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::NUMERIC_NAME => __(sprintf('%s.%s', 'school', self::NUMERIC_NAME)),
            self::CLASS_TEACHER => __(sprintf('%s.%s', 'school', self::CLASS_TEACHER)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),



        ];
    }
}
