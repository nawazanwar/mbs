<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForHrm extends AbstractEnum
{
    public const HR_ID = 'hr_id';
    public const NAME = 'name';
    public const CNIC = 'cnic';
    public const CONTACT_1 = 'contact_1';
    public const CONTACT_2 = 'contact_2';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::HR_ID => __(sprintf('%s.%s', 'school', self::HR_ID)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::CNIC => __(sprintf('%s.%s', 'school', self::CNIC)),
            self::CONTACT_1 => __(sprintf('%s.%s', 'school', self::CONTACT_1)),
            self::CONTACT_2 => __(sprintf('%s.%s', 'school', self::CONTACT_2)),


        ];
    }
}
