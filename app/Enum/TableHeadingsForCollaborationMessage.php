<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForCollaborationMessage extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const STATUS = 'status';
    public const SENDER = 'sender';
    public const SUBJECT = 'subject';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::STATUS => __(sprintf('%s.%s', 'school', self::STATUS)),
            self::SENDER => __(sprintf('%s.%s', 'school', self::SENDER)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
        ];
    }
}
