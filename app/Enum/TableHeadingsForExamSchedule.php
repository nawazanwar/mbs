<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForExamSchedule extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const EXAM_TERM = 'exam_term';
    public const GRADE = 'class';
    public const SUBJECT = 'subject';
    public const DATE = 'date';
    public const TIME = 'time';
    public const ROOM_NO = 'room_no';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::EXAM_TERM => __(sprintf('%s.%s', 'school', self::EXAM_TERM)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::DATE => __(sprintf('%s.%s', 'school', self::DATE)),
            self::TIME => __(sprintf('%s.%s', 'school', self::TIME)),
            self::ROOM_NO => __(sprintf('%s.%s', 'school', self::ROOM_NO)),


        ];
    }
}
