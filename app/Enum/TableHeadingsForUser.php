<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForUser extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const PHONE= 'phone';
    public const EMAIL= 'email';
    public const PASSWORD= 'password';
    public const ACTIVE= 'active';






    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::PASSWORD => __(sprintf('%s.%s', 'school', self::PASSWORD)),
            self::ACTIVE => __(sprintf('%s.%s', 'school', self::ACTIVE)),



        ];
    }
}
