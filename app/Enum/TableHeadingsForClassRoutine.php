<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForClassRoutine extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'numeric_name';
    public const GRADE = 'class';
    public const SUBJECT = 'class_teacher';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),




        ];
    }
}
