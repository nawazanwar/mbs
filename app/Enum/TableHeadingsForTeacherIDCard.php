<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForTeacherIDCard extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const ACADEMIC_YEAR = 'academic_year';
    public const APPLICANT_TYPE = 'applicant_type';
    public const LEAVE_TYPE = 'leave_type';
    public const APPLICANT = 'applicant';
    public const STATUS = 'status';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::ACADEMIC_YEAR => __(sprintf('%s.%s', 'school', self::ACADEMIC_YEAR)),
            self::APPLICANT_TYPE => __(sprintf('%s.%s', 'school', self::APPLICANT_TYPE)),
            self::LEAVE_TYPE => __(sprintf('%s.%s', 'school', self::LEAVE_TYPE)),
            self::APPLICANT => __(sprintf('%s.%s', 'school', self::APPLICANT)),
            self::STATUS => __(sprintf('%s.%s', 'school', self::STATUS)),




        ];
    }
}
