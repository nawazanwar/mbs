<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForGuardian extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const PHONE = 'phone';
    public const PROFESSION = 'profession';
    public const EMAIL = 'email';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::PROFESSION => __(sprintf('%s.%s', 'school', self::PROFESSION)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
        ];
    }
}
