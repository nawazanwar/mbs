<?php

declare(strict_types=1);

namespace App\Enum;

class ExamNavEnum extends AbstractEnum
{
    public const EXAM_GRADE = 'exam_grade';
    public const EXAM_TERM = 'exam_term';
    public const EXAM_SCHEDULE = 'exam_schedule';
    public const EXAM_SUGGESTION = 'exam_suggestion';
    public const EXAM_ATTENDANCE = 'exam_attendance';
    public const EXAM_MARK = 'exam_mark';

    public static function getValues(): array
    {
        return [
            self::EXAM_GRADE,
            self::EXAM_TERM,
            self::EXAM_SUGGESTION,
            self::EXAM_SCHEDULE,
            self::EXAM_ATTENDANCE,
            self::EXAM_MARK,
        ];
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::EXAM_GRADE => '<i class="bx bxs-graduation fs-1"></i>',
            self::EXAM_TERM => '<i class="bx bx-happy-beaming fs-1"></i>',
            self::EXAM_SUGGESTION => '<i class="bx bx-anchor fs-1"></i>',
            self::EXAM_SCHEDULE => '<i class="bx bx-adjust fs-1"></i>',
            self::EXAM_ATTENDANCE => '<i class="bx bx-align-justify fs-1"></i>',
            self::EXAM_MARK => '<i class="bx bxs-file-txt fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::EXAM_GRADE => route('dashboard.exam.grades.index'),
            self::EXAM_TERM => route('dashboard.exam.terms.index'),
            self::EXAM_SUGGESTION => route('dashboard.exam.suggestions.index'),
            self::EXAM_SCHEDULE => route('dashboard.exam.schedules.index'),
            self::EXAM_ATTENDANCE => route('dashboard.exam.attendances.index'),
            self::EXAM_MARK => route('dashboard.exam.marks.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::EXAM_GRADE => __(sprintf('%s.%s', 'school', self::EXAM_GRADE)),
            self::EXAM_TERM => __(sprintf('%s.%s', 'school', self::EXAM_TERM)),
            self::EXAM_SUGGESTION => __(sprintf('%s.%s', 'school', self::EXAM_SUGGESTION)),
            self::EXAM_SCHEDULE => __(sprintf('%s.%s', 'school', self::EXAM_SCHEDULE)),
            self::EXAM_ATTENDANCE => __(sprintf('%s.%s', 'school', self::EXAM_ATTENDANCE)),
            self::EXAM_MARK => __(sprintf('%s.%s', 'school', self::EXAM_MARK))
        ];
    }
}
