<?php

declare(strict_types=1);

namespace App\Enum;

class LibraryNavEnum extends AbstractEnum
{
    public const LIBRARY_BOOK = 'library_book';
    public const LIBRARY_MEMBER = 'library_member';
    public const LIBRARY_BOOK_ISSUE = 'library_book_issue';
    public const LIBRARY_EBOOK = 'library_ebook';


    public static function getValues(): array
    {
        return [
            self::LIBRARY_BOOK,
            self::LIBRARY_MEMBER,
            self::LIBRARY_BOOK_ISSUE,
            self::LIBRARY_EBOOK,
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::LIBRARY_BOOK => route('dashboard.library.books.index'),
            self::LIBRARY_MEMBER => route('dashboard.library.members.index'),
            self::LIBRARY_BOOK_ISSUE => route('dashboard.library.book_issues.index'),
            self::LIBRARY_EBOOK => route('dashboard.library.ebooks.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::LIBRARY_BOOK => '<i class="bx bx-certification fs-1"></i>',
            self::LIBRARY_MEMBER => '<i class="bx bx-certification fs-1"></i>',
            self::LIBRARY_BOOK_ISSUE => '<i class="bx bx-certification fs-1"></i>',
            self::LIBRARY_EBOOK => '<i class="bx bx-certification fs-1"></i>'

        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::LIBRARY_BOOK => __(sprintf('%s.%s', 'school', self::LIBRARY_BOOK)),
            self::LIBRARY_MEMBER => __(sprintf('%s.%s', 'school', self::LIBRARY_MEMBER)),
            self::LIBRARY_BOOK_ISSUE => __(sprintf('%s.%s', 'school', self::LIBRARY_BOOK_ISSUE)),
            self::LIBRARY_EBOOK => __(sprintf('%s.%s', 'school', self::LIBRARY_EBOOK))

        ];
    }
}
