<?php

declare(strict_types=1);

namespace App\Enum;

class FrontendNavEnum extends AbstractEnum
{
    public const FRONT_PAGE = 'front_page';
    public const MANAGE_SLIDER = 'manage_slider';
    public const ABOUT_SCHOOL = 'about_school';

    public static function getValues(): array
    {
        return [
            self::FRONT_PAGE,
            self::MANAGE_SLIDER,
            self::ABOUT_SCHOOL
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::FRONT_PAGE => route('dashboard.pages.index'),
            self::MANAGE_SLIDER => route('dashboard.sliders.index'),
            self::ABOUT_SCHOOL => route('dashboard.schools.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::FRONT_PAGE => '<i class="bx bx-screenshot fs-1"></i>',
            self::MANAGE_SLIDER => '<i class="bx bx-slider fs-1"></i>',
            self::ABOUT_SCHOOL => '<i class="bx bx-building fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::FRONT_PAGE => __(sprintf('%s.%s', 'school', self::FRONT_PAGE)),
            self::MANAGE_SLIDER => __(sprintf('%s.%s', 'school', self::MANAGE_SLIDER)),
            self::ABOUT_SCHOOL => __(sprintf('%s.%s', 'school', self::ABOUT_SCHOOL))
        ];
    }
}
