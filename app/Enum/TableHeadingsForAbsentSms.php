<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAbsentSms extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const SESSION_YEAR = 'session_year';
    public const RECEIVER_TYPE = 'receiver_type';
    public const SUBJECT = 'subject';
    public const SEND_DATE = 'send_date';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::SESSION_YEAR => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::RECEIVER_TYPE => __(sprintf('%s.%s', 'school', self::SESSION_YEAR)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::SEND_DATE => __(sprintf('%s.%s', 'school', self::SEND_DATE)),

        ];
    }
}
