<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForTransportRoute extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const ROUTE_NAME = 'route_name';
    public const ROUTE_START = 'route_start';
    public const ROUTE_END = 'route_end';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::ROUTE_NAME => __(sprintf('%s.%s', 'school', self::ROUTE_NAME)),
            self::ROUTE_START => __(sprintf('%s.%s', 'school', self::ROUTE_START)),
            self::ROUTE_END => __(sprintf('%s.%s', 'school', self::ROUTE_END)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),


        ];
    }
}
