<?php

declare(strict_types=1);

namespace App\Enum;

class DashBoardNavEnum extends AbstractEnum
{
    public const STUDENTS = 'students';
    public const PARENTS = 'parents';
    public const TEACHERS = 'teachers';
    public const EMPLOYEES = 'users';

    public static function getValues(): array
    {
        return [
            self::STUDENTS,
            self::PARENTS,
            self::TEACHERS,
            self::EMPLOYEES
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::STUDENTS => '<i class="fa fa-group fa-3x fs-1"></i><br>' . __(self::STUDENTS),
            self::PARENTS => '<i class="fa fa-group fa-3x fs-1"></i><br>' . __(self::PARENTS),
            self::TEACHERS => '<i class="fa fa-group fa-3x fs-1"></i><br>' . __(self::TEACHERS),
            self::EMPLOYEES => '<i class="fa fa-group fa-3x fs-1"></i><br>' . __(self::EMPLOYEES)
        ];
    }
}
