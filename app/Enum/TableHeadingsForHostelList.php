<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForHostelList extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const HOSTEL = 'hostel';
    public const HOSTEL_TYPE = 'hostel_type';
    public const ADDRESS = 'address';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::HOSTEL => __(sprintf('%s.%s', 'school', self::HOSTEL)),
            self::HOSTEL_TYPE => __(sprintf('%s.%s', 'school', self::HOSTEL_TYPE)),
            self::ADDRESS => __(sprintf('%s.%s', 'school', self::ADDRESS)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
        ];
    }
}
