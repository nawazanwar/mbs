<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForCallLog extends AbstractEnum
{


    public const NAME = 'name';
    public const PHONE = 'phone';
    public const CALL_DURATION = 'call_duration';
    public const CALL_DATE = 'call_date';
    public const CALL_TYPE = 'call_type';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [



            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::CALL_DURATION => __(sprintf('%s.%s', 'school', self::CALL_DURATION)),
            self::CALL_DATE => __(sprintf('%s.%s', 'school', self::CALL_DATE)),
            self::CALL_TYPE => __(sprintf('%s.%s', 'school', self::CALL_TYPE)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),


        ];
    }
}
