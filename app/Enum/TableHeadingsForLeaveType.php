<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForLeaveType extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const APPLICANT_TYPE = 'applicant_type';
    public const LEAVE_TYPE = 'leave_type';
    public const TOTAL_LEAVE = 'total_leave';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::APPLICANT_TYPE => __(sprintf('%s.%s', 'school', self::APPLICANT_TYPE)),
            self::LEAVE_TYPE => __(sprintf('%s.%s', 'school', self::LEAVE_TYPE)),
            self::TOTAL_LEAVE => __(sprintf('%s.%s', 'school', self::TOTAL_LEAVE)),




        ];
    }
}
