<?php

declare(strict_types=1);

namespace App\Enum;

class PayrollNavEnum extends AbstractEnum
{
    public const SALARY_GRADE = 'salary_grade';
    public const SALARY_PAYMENT = 'salary_payment';
    public const SALARY_HISTORY = 'salary_history';

    public static function getValues(): array
    {
        return [
            self::SALARY_GRADE,
            self::SALARY_PAYMENT,
            self::SALARY_HISTORY
        ];
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::SALARY_GRADE => '<i class="bx bx-dollar fs-1"></i>',
            self::SALARY_PAYMENT => '<i class="bx bx-money fs-1"></i>',
            self::SALARY_HISTORY => '<i class="bx bx-package fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::SALARY_GRADE => route('dashboard.salary.grades.index'),
            self::SALARY_PAYMENT => route('dashboard.salary.payments.index'),
            self::SALARY_HISTORY => route('dashboard.salary.histories.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::SALARY_GRADE => __(sprintf('%s.%s', 'school', self::SALARY_GRADE)),
            self::SALARY_PAYMENT => __(sprintf('%s.%s', 'school', self::SALARY_PAYMENT)),
            self::SALARY_HISTORY => __(sprintf('%s.%s', 'school', self::SALARY_HISTORY))
        ];
    }
}