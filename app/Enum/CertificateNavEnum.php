<?php

declare(strict_types=1);

namespace App\Enum;

class CertificateNavEnum extends AbstractEnum
{

    public const GENERATE_CERTIFICATE = 'generate_certificate';
    public const CERTIFICATE_TYPE = 'certificate_type';

    public static function getValues(): array
    {
        return [
            self::GENERATE_CERTIFICATE,
            self::CERTIFICATE_TYPE
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::GENERATE_CERTIFICATE => route('dashboard.certificates.index'),
            self::CERTIFICATE_TYPE => route('dashboard.certificate.types.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::GENERATE_CERTIFICATE => '<i class="bx bx-certification fs-1"></i>',
            self::CERTIFICATE_TYPE => '<i class="bx bx-certification fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::GENERATE_CERTIFICATE => __(sprintf('%s.%s', 'school', self::GENERATE_CERTIFICATE)),
            self::CERTIFICATE_TYPE => __(sprintf('%s.%s', 'school', self::CERTIFICATE_TYPE))
        ];
    }
}
