<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForManageRoom extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const ROOM_NO = 'room_no';
    public const ROOM_TYPE = 'room_type';
    public const TOTAL_SEAT = 'total_seat';
    public const HOSTEL = 'hostel';
    public const COST = 'cost_per_seat';
    public const NOTE = 'note';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::ROOM_NO => __(sprintf('%s.%s', 'school', self::ROOM_NO)),
            self::ROOM_TYPE => __(sprintf('%s.%s', 'school', self::ROOM_TYPE)),
            self::TOTAL_SEAT => __(sprintf('%s.%s', 'school', self::TOTAL_SEAT)),
            self::HOSTEL => __(sprintf('%s.%s', 'school', self::HOSTEL)),
            self::COST => __(sprintf('%s.%s', 'school', self::COST)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),


        ];
    }
}
