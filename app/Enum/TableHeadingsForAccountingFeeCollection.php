<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAccountingFeeCollection extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const INVOICE_NUMBER = 'invoice_number';
    public const STUDENT = 'student';
    public const GRADE = 'class';
    public const GROSS_AMOUNT = 'gross_amount';
    public const DISCOUNT = 'discount';
    public const NET_AMOUNT = 'net_amount';
    public const STATUS = 'status';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::INVOICE_NUMBER => __(sprintf('%s.%s', 'school', self::INVOICE_NUMBER)),
            self::STUDENT => __(sprintf('%s.%s', 'school', self::STUDENT)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::GROSS_AMOUNT => __(sprintf('%s.%s', 'school', self::GROSS_AMOUNT)),
            self::DISCOUNT => __(sprintf('%s.%s', 'school', self::DISCOUNT)),
            self::NET_AMOUNT => __(sprintf('%s.%s', 'school', self::NET_AMOUNT)),
            self::STATUS => __(sprintf('%s.%s', 'school', self::STATUS)),
        ];
    }
}
