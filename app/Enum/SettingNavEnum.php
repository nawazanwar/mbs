<?php

declare(strict_types=1);

namespace App\Enum;

class SettingNavEnum extends AbstractEnum
{

    public const SCHOOL_SETTINGS = 'school_settings';
    public const PAYMENT_SETTINGS = 'payment_settings';
    public const SMS_SETTINGS = 'sms_settings';
    public const EMAIL_SETTINGS = 'email_settings';

    public static function getValues(): array
    {
        return [
            self::SCHOOL_SETTINGS,
            self::PAYMENT_SETTINGS,
            self::SMS_SETTINGS,
            self::EMAIL_SETTINGS
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::SCHOOL_SETTINGS => route('dashboard.settings.index', ['type' => 'school']),
            self::PAYMENT_SETTINGS => route('dashboard.settings.index', ['type' => 'payment']),
            self::SMS_SETTINGS => route('dashboard.settings.index', ['type' => 'sms']),
            self::EMAIL_SETTINGS => route('dashboard.settings.index', ['type' => 'email']),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::SCHOOL_SETTINGS => '<i class="bx bx-building fs-1"></i>',
            self::PAYMENT_SETTINGS =>'<i class="bx bx-dollar fs-1"></i>',
            self::SMS_SETTINGS => '<i class="bx bx-envelope fs-1"></i>',
            self::EMAIL_SETTINGS => '<i class="bx bx-envelope-open fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::SCHOOL_SETTINGS => __(sprintf('%s.%s', 'school', self::SCHOOL_SETTINGS)),
            self::PAYMENT_SETTINGS => __(sprintf('%s.%s', 'school', self::PAYMENT_SETTINGS)),
            self::SMS_SETTINGS => __(sprintf('%s.%s', 'school', self::SMS_SETTINGS)),
            self::EMAIL_SETTINGS => __(sprintf('%s.%s', 'school', self::EMAIL_SETTINGS))
        ];
    }
}
