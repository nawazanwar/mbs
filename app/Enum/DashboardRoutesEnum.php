<?php

declare(strict_types=1);

namespace App\Enum;

class DashboardRoutesEnum extends AbstractEnum
{

    public const PAYMENTS = 'dashboard.payments';
    public const GENERAL_SETTINGS = 'dashboard.general_settings';
    public const SCHOOLS = 'dashboard.schools';

    public const ACADEMIC_YEARS = 'dashboard.academics';
    public const ACADEMIC_PROMOTIONS = 'dashboard.academic_promotions';

    public const ROLES = 'dashboard.roles';
    public const PERMISSIONS = 'dashboard.permissions';
    public const USERS = 'dashboard.users';
    public const PROFILE = 'dashboard.profiles';

    public const ACTIVITIES = 'dashboard.activities';
    public const FEEDBACKS = 'dashboard.feedbacks';
    public const BACKUPS = 'dashboard.backups';
    public const LANGUAGES = 'dashboard.languages';
    public const THEMES = 'dashboard.themes';
    public const REPORTS = 'dashboard.reports';

    public const CERTIFICATE_TYPES = 'dashboard.certificate_types';
    public const CERTIFICATE_GENERATIONS = 'dashboard.certificate_generations';

    public const CARD_SETTINGS = 'dashboard.cards.settings';
    public const CARDS = 'dashboard.cards';

    public const ASSIGNMENTS = 'dashboard.assignments';
    public const ASSIGNMENT_SUBMISSIONS = 'dashboard.assignment_submissions';

    public const STUDENT_TYPES = 'dashboard.assignments';
    public const STUDENTS = 'dashboard.students';
    public const STUDENT_ACTIVITIES = 'dashboard.student_activities';

    public const GUARDIANS = 'dashboard.guardians';

    public const TEACHERS = 'dashboard.teachers';
    public const TEACHER_LECTURES = 'dashboard.teacher_lectures';

    public const VISITOR_PURPOSE = 'dashboard.visitor_purposes';
    public const VISITORS = 'dashboard.visitors';
    public const CALL_LOGS = 'dashboard.call_logs';
    public const POSTAL = 'dashboard.postal';

    public const HOSTELS = 'dashboard.hostels';
    public const HOSTEL_ROOMS = 'dashboard.hostel_rooms';
    public const HOSTEL_MEMBERS = 'dashboard.hostel_members';

    public const TRANSPORT_VEHICLES = 'dashboard.transport_vehicles';
    public const TRANSPORT_ROUTES = 'dashboard.transport_routes';
    public const TRANSPORT_MEMBERS = 'dashboard.transport_members';

    public const LIBRARY_BOOKS = 'dashboard.library_books';
    public const LIBRARY_MEMBERS = 'dashboard.library_members';
    public const LIBRARY_ISSUE_BOOKS = 'dashboard.library_issue_books';
    public const LIBRARY_E_BOOKS = 'dashboard.library_e_books';

    public const PAYROLL_PAYMENTS = 'dashboard.payroll_payments';
    public const PAYROLL_HISTORY = 'dashboard.payroll_history';
    public const PAYROLL_SALARY_GRADES = 'dashboard.payroll_salary_grades';
    public const PAYROLL_SALARY_PAYMENTS = 'dashboard.payroll_salary_payments';

    public const HRM_DESIGNATIONS = 'dashboard.hrm_designations';
    public const HRM_EMPLOYEES = 'dashboard.hrm_employees';

    public const MESSAGES = 'dashboard.messages';
    public const EMAILS = 'dashboard.emails';

    public const COMPLAIN_TYPES = 'dashboard.complain_types';
    public const COMPLAINS = 'dashboard.complains';

    public const EVENTS = 'dashboard.events';
    public const NEWS = 'dashboard.news';
    public const NOTICES = 'dashboard.notices';
    public const HOLIDAYS = 'dashboard.holidays';

    public const GALLERIES = 'dashboard.gallery';
    public const GALLERY_IMAGES = 'dashboard.gallery_images';

    public const PAGES = 'dashboard.pages';
    public const SLIDERS = 'dashboard.sliders';

    public const EXAM_TERMS = 'dashboard.exam_terms';
    public const EXAM_GRADES = 'dashboard.exam_grades';
    public const EXAM_SCHEDULES = 'dashboard.exam_schedules';
    public const EXAM_SUGGESTIONS = 'dashboard.exam_suggestions';
    public const EXAM_ATTENDANCES = 'dashboard.exam_attendances';
    public const EXAM_MARKS = 'dashboard.exam_marks';
    public const EXAM_RESULTS = 'dashboard.exam_results';
    public const EXAM_MERIT_LISTS = 'dashboard.exam_marit_lists';
    public const EXAM_MARK_SHEET = 'dashboard.exam_mark_sheet';
    public const EXAM_RESULT_CARD = 'dashboard.exam_result_card';
    public const EXAM_MARK_SEND = 'dashboard.exam_mark_send';
    public const EXAM_RESULT_SEND = 'dashboard.exam_result_send';

    public const SMS_TEMPLATES = 'dashboard.sms_templates';
    public const EMAIL_TEMPLATES = 'dashboard.email_templates';

    public const LEAVE_TYPES = 'dashboard.leave_types';
    public const LEAVE_APPLICATIONS = 'dashboard.leave_applications';
    public const LEAVES = 'dashboard.leaves';

    public const CLASSES = 'dashboard.classes';
    public const LIVE_CLASSES = 'dashboard.live_classes';
    public const SECTIONS = 'dashboard.sections';
    public const SUBJECTS = 'dashboard.subjects';
    public const SYLLABUS = 'dashboard.syllabus';
    public const STUDY_MATERIALS = 'dashboard.study_materials';
    public const CLASS_ROUTINE = 'dashboard.class_routines';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
        ];
    }
}
