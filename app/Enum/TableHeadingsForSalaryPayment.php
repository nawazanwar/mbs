<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSalaryPayment extends AbstractEnum
{
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const MONTH = 'month';
    public const GRADE_NAME = 'grade_name';
    public const SALARY_TYPE = 'salary_type';
    public const TOTAL_ALLOWANCE = 'total_allowance';
    public const TOTAL_DEDUCTION = 'total_deduction';
    public const GROSS_SALARY = 'gross_salary';
    public const NET_SALARY = 'net_salary';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::MONTH => __(sprintf('%s.%s', 'school', self::MONTH)),
            self::GRADE_NAME => __(sprintf('%s.%s', 'school', self::GRADE_NAME)),
            self::SALARY_TYPE => __(sprintf('%s.%s', 'school', self::SALARY_TYPE)),
            self::TOTAL_ALLOWANCE => __(sprintf('%s.%s', 'school', self::TOTAL_ALLOWANCE)),
            self::TOTAL_DEDUCTION => __(sprintf('%s.%s', 'school', self::TOTAL_DEDUCTION)),
            self::GROSS_SALARY => __(sprintf('%s.%s', 'school', self::GROSS_SALARY)),
            self::NET_SALARY => __(sprintf('%s.%s', 'school', self::NET_SALARY)),


        ];
    }
}
