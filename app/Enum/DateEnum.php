<?php

declare(strict_types=1);

namespace App\Enum;

class DateEnum extends AbstractEnum
{
    public const CURRENT_MONT = 'current_month';
    public const CURRENT_YEAR = 'current_year';
    public const CURRENT_DATE = 'current_date';

    public static function getValues(): array
    {
        return [
            self::CURRENT_MONT,
            self::CURRENT_YEAR,
            self::CURRENT_DATE
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::CURRENT_YEAR => date('m'),
            self::CURRENT_MONT => date('Y'),
            self::CURRENT_DATE => date("Y-m-d"),
        ];
    }
}
