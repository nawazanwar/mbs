<?php

declare(strict_types=1);

namespace App\Enum;

class FinanceNavEnum extends AbstractEnum
{
    public const PAYROLL = 'payroll';
    public const ACCOUNTING = 'accounting';

    public static function getValues(): array
    {
        return [
            self::PAYROLL,
            self::ACCOUNTING
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::PAYROLL => route('dashboard.salary.payments.index'),
            self::ACCOUNTING => route('dashboard.discounts.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::PAYROLL => '<i class="bx bx-dollar fs-1"></i>',
            self::ACCOUNTING => '<i class="bx bx-calculator fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::PAYROLL => __(sprintf('%s.%s', 'school', self::PAYROLL)),
            self::ACCOUNTING => __(sprintf('%s.%s', 'school', self::ACCOUNTING))
        ];
    }
}
