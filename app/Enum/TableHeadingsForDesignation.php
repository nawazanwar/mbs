<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForDesignation extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const DESIGNATION = 'designation';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::DESIGNATION => __(sprintf('%s.%s', 'school', self::DESIGNATION)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),

        ];
    }
}
