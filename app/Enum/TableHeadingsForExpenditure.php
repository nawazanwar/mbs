<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForExpenditure extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const EXPENDITURE_HEAD = 'expenditure_head';
    public const AMOUNT = 'amount';
    public const EXPENDITURE_VIA = 'expenditure_via';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::EXPENDITURE_HEAD => __(sprintf('%s.%s', 'school', self::EXPENDITURE_HEAD)),
            self::AMOUNT => __(sprintf('%s.%s', 'school', self::AMOUNT)),
            self::EXPENDITURE_VIA => __(sprintf('%s.%s', 'school', self::EXPENDITURE_VIA)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),

        ];
    }
}
