<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForFrontendManageSlide extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const IMAGE = 'image';
    public const TITLE = 'title';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::IMAGE => __(sprintf('%s.%s', 'school', self::IMAGE)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),

        ];
    }
}
