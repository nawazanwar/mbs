<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForFrontEndPage extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const LOCATION = 'location';
    public const TITLE = 'title';
    public const IMAGE = 'image';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::LOCATION => __(sprintf('%s.%s', 'school', self::LOCATION)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::IMAGE => __(sprintf('%s.%s', 'school', self::IMAGE)),
        ];
    }
}
