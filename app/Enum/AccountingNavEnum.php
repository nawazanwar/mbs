<?php

declare(strict_types=1);

namespace App\Enum;

class
AccountingNavEnum extends AbstractEnum
{
    public const DISCOUNT = 'discount';
    public const FEE_TYPE = 'fee_type';
    public const FEE_COLLECTION = 'fee_collection';
    public const MANAGE_INVOICE = 'manage_invoice';
    public const DUE_INVOICE = 'due_invoice';
    public const DUE_RECEIPT = 'due_receipt';
    public const PAID_RECEIPT = 'paid_receipt';
    public const DUE_FEE_EMAIL = 'due_fee_email';
    public const DUE_FEE_SMS = 'due_fee_sms';
    public const INCOME = 'income';
    public const INCOME_HEAD = 'income_head';
    public const EXPENDITURE = 'expenditure';
    public const EXPENDITURE_HEAD = 'expenditure_head';

    public static function getValues(): array
    {
        return array();
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::DISCOUNT => route('dashboard.discounts.index'),
            self::FEE_TYPE => route('dashboard.fee.types.index'),
            self::FEE_COLLECTION => route('dashboard.fee.collections.index'),
            self::MANAGE_INVOICE => route('dashboard.invoices.index'),
            self::DUE_INVOICE => route('dashboard.invoices.index', ['type' => 'dues']),
            self::DUE_RECEIPT => route('dashboard.receipts.index', ['type' => 'dues']),
            self::PAID_RECEIPT => route('dashboard.receipts.index', ['type' => 'paid']),
            self::DUE_FEE_EMAIL => route('dashboard.dues.fee.emails.index'),
            self::DUE_FEE_SMS => route('dashboard.dues.fee.sms.index'),
            self::INCOME_HEAD => route('dashboard.income.heads.index'),
            self::EXPENDITURE_HEAD => route('dashboard.expenditure.heads.index'),
            self::INCOME => route('dashboard.incomes.index'),
            self::EXPENDITURE => route('dashboard.expenditures.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::DISCOUNT => '<i class="bx bx-calculator fs-1"></i>',
            self::FEE_TYPE =>'<i class="bx bx-ghost fs-1"></i>',
            self::FEE_COLLECTION => '<i class="bx bxl-gitlab fs-1"></i>',
            self::MANAGE_INVOICE => '<i class="bx bx-calculator fs-1"></i>',
            self::DUE_INVOICE => '<i class="bx bx-calculator fs-1"></i>',
            self::DUE_RECEIPT => '<i class="bx bx-calculator fs-1"></i>',
            self::PAID_RECEIPT => '<i class="bx bx-calculator fs-1"></i>',
            self::DUE_FEE_EMAIL => '<i class="bx bx-calculator fs-1"></i>',
            self::DUE_FEE_SMS => '<i class="bx bx-calculator fs-1"></i>',
            self::INCOME_HEAD => '<i class="bx bx-calculator fs-1"></i>',
            self::EXPENDITURE_HEAD => '<i class="bx bx-calculator fs-1"></i>',
            self::INCOME => '<i class="bx bx-calculator fs-1"></i>',
            self::EXPENDITURE => '<i class="bx bx-calculator fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::DISCOUNT => __(sprintf('%s.%s', 'school', self::DISCOUNT)),
            self::FEE_TYPE => __(sprintf('%s.%s', 'school', self::FEE_TYPE)),
            self::FEE_COLLECTION => __(sprintf('%s.%s', 'school', self::FEE_COLLECTION)),
            self::MANAGE_INVOICE => __(sprintf('%s.%s', 'school', self::MANAGE_INVOICE)),
            self::DUE_INVOICE => __(sprintf('%s.%s', 'school', self::DUE_INVOICE)),
            self::DUE_RECEIPT => __(sprintf('%s.%s', 'school', self::DUE_RECEIPT)),
            self::PAID_RECEIPT => __(sprintf('%s.%s', 'school', self::PAID_RECEIPT)),
            self::DUE_FEE_EMAIL => __(sprintf('%s.%s', 'school', self::DUE_FEE_EMAIL)),
            self::DUE_FEE_SMS => __(sprintf('%s.%s', 'school', self::DUE_FEE_SMS)),
            self::INCOME_HEAD => __(sprintf('%s.%s', 'school', self::INCOME_HEAD)),
            self::EXPENDITURE_HEAD => __(sprintf('%s.%s', 'school', self::EXPENDITURE_HEAD)),
            self::INCOME => __(sprintf('%s.%s', 'school', self::INCOME)),
            self::EXPENDITURE => __(sprintf('%s.%s', 'school', self::EXPENDITURE))
        ];
    }
}
