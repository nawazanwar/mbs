<?php

declare(strict_types=1);

namespace App\Enum;

class AnnouncementNavEnum extends AbstractEnum
{
    public const NOTICE = 'notice';
    public const NEWS = 'news';
    public const HOLIDAYS = 'holidays';

    public static function getValues(): array
    {
        return array();
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::NOTICE => route('dashboard.notices.index'),
            self::NEWS => route('dashboard.news.index'),
            self::HOLIDAYS => route('dashboard.holidays.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::NOTICE => '<i class="bx bx-bell fs-1"></i>',
            self::NEWS =>'<i class="bx bx-news fs-1"></i>',
            self::HOLIDAYS => '<i class="bx bx-network-chart fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::NOTICE => __(sprintf('%s.%s', 'school', self::NOTICE)),
            self::NEWS => __(sprintf('%s.%s', 'school', self::NEWS)),
            self::HOLIDAYS => __(sprintf('%s.%s', 'school', self::HOLIDAYS))
        ];
    }
}
