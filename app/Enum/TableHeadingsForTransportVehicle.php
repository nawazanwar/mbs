<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForTransportVehicle extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const VEHICLE_NUMBER = 'vehicle_number';
    public const VEHICLE_MODEL = 'vehicle_model';
    public const DRIVER = 'driver';
    public const VEHICLE_LICENCE = 'vehicle_licence';
    public const VEHICLE_CONTACT = 'vehicle_contact';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::VEHICLE_NUMBER => __(sprintf('%s.%s', 'school', self::VEHICLE_NUMBER)),
            self::VEHICLE_MODEL => __(sprintf('%s.%s', 'school', self::VEHICLE_MODEL)),
            self::DRIVER => __(sprintf('%s.%s', 'school', self::DRIVER)),
            self::VEHICLE_LICENCE => __(sprintf('%s.%s', 'school', self::VEHICLE_LICENCE)),
            self::VEHICLE_CONTACT => __(sprintf('%s.%s', 'school', self::VEHICLE_CONTACT)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),

        ];
    }
}
