<?php

declare(strict_types=1);

namespace App\Enum;

class TemplateNavEnum extends AbstractEnum
{

    public const SMS_TEMPLATES = 'sms_template';
    public const EMAIL_TEMPLATES = 'email_template';

    public static function getValues(): array
    {
        return [
            self::SMS_TEMPLATES,
            self::EMAIL_TEMPLATES
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::SMS_TEMPLATES => route('dashboard.template.sms.index'),
            self::EMAIL_TEMPLATES => route('dashboard.template.emails.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::SMS_TEMPLATES => '<i class="bx bx-building fs-1"></i>',
            self::EMAIL_TEMPLATES =>'<i class="bx bx-dollar fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::SMS_TEMPLATES => __(sprintf('%s.%s', 'school', self::SMS_TEMPLATES)),
            self::EMAIL_TEMPLATES => __(sprintf('%s.%s', 'school', self::EMAIL_TEMPLATES)),
        ];
    }
}
