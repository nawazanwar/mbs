<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForCollaborationManageComplain extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const ACADEMIC_YEAR = 'academic_year';
    public const COMPLAIN_BY = 'complain_by';
    public const COMPLAIN_TYPE = 'complain_type';
    public const COMPLAIN_DATE = 'complain_date';
    public const ACTION_DATE = 'complain_date';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::ACADEMIC_YEAR => __(sprintf('%s.%s', 'school', self::ACADEMIC_YEAR)),
            self::COMPLAIN_BY => __(sprintf('%s.%s', 'school', self::COMPLAIN_BY)),
            self::COMPLAIN_TYPE => __(sprintf('%s.%s', 'school', self::COMPLAIN_TYPE)),
            self::COMPLAIN_DATE => __(sprintf('%s.%s', 'school', self::COMPLAIN_DATE)),
            self::ACTION_DATE => __(sprintf('%s.%s', 'school', self::ACTION_DATE)),
        ];
    }
}
