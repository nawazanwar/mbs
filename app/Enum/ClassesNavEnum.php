<?php

declare(strict_types=1);

namespace App\Enum;

class ClassesNavEnum extends AbstractEnum
{

    public const LIVE_CLASS = 'live_class';
    public const CLASSES = 'class';
    public const SECTION = 'section';
    public const SUBJECT = 'subject';
    public const SYLLABUS = 'syllabus';
    public const STUDY_MATERIAL = 'study_material';
    public const CLASS_ROUTINE = 'class_routine';
    public const PROMOTION = 'promotion';

    public static function getValues(): array
    {
        return [
            self::LIVE_CLASS,
            self::CLASSES,
            self::SECTION,
            self::SUBJECT,
            self::SYLLABUS,
            self::STUDY_MATERIAL,
            self::CLASS_ROUTINE,
            self::PROMOTION,


        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::LIVE_CLASS => route('dashboard.classes.live.index'),
            self::CLASSES => route('dashboard.classes.index'),
            self::SECTION => route('dashboard.sections.index'),
            self::SUBJECT => route('dashboard.subjects.index'),
            self::SYLLABUS => route('dashboard.syllabus.index'),
            self::STUDY_MATERIAL => route('dashboard.study.materials.index'),
            self::CLASS_ROUTINE => route('dashboard.class.routines.index'),
            self::PROMOTION => route('dashboard.class.promotions.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::LIVE_CLASS => '<i class="bx bx-building fs-1"></i>',
            self::CLASSES => '<i class="bx bx-dollar fs-1"></i>',
            self::SECTION => '<i class="bx bx-dollar fs-1"></i>',
            self::SUBJECT => '<i class="bx bx-dollar fs-1"></i>',
            self::SYLLABUS => '<i class="bx bx-dollar fs-1"></i>',
            self::STUDY_MATERIAL => '<i class="bx bx-dollar fs-1"></i>',
            self::CLASS_ROUTINE => '<i class="bx bx-dollar fs-1"></i>',
            self::PROMOTION => '<i class="bx bx-dollar fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::LIVE_CLASS => __(sprintf('%s.%s', 'school', self::LIVE_CLASS)),
            self::CLASSES => __(sprintf('%s.%s', 'school', self::CLASSES)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::SYLLABUS => __(sprintf('%s.%s', 'school', self::SYLLABUS)),
            self::STUDY_MATERIAL => __(sprintf('%s.%s', 'school', self::STUDY_MATERIAL)),
            self::CLASS_ROUTINE => __(sprintf('%s.%s', 'school', self::CLASS_ROUTINE)),
            self::PROMOTION => __(sprintf('%s.%s', 'school', self::PROMOTION)),
        ];
    }
}
