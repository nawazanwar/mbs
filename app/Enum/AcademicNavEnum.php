<?php

declare(strict_types=1);

namespace App\Enum;

class AcademicNavEnum extends AbstractEnum
{
    public const ACADEMIC_YEAR = 'academic_year';

    public const GRADE = 'class';
    public const SECTION = 'section';
    public const SUBJECT = 'subject';
    public const SYLLABUS = 'syllabus';
    public const PROFILE = 'profile';

    public static function getValues(): array
    {
        return [
            self::ACADEMIC_YEAR,

            self::GRADE,
            self::SECTION,
            self::SUBJECT,
            self::SYLLABUS,
            self::PROFILE

        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::ACADEMIC_YEAR => route('dashboard.academic.years.index'),

            self::GRADE => route('dashboard.classes.index'),
            self::SECTION => route('dashboard.sections.index'),
            self::SUBJECT => route('dashboard.subjects.index'),
            self::SYLLABUS => route('dashboard.syllabus.index'),
            self::PROFILE => route('dashboard.profile.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::ACADEMIC_YEAR => '<i class="bx bx-cube fs-1"></i>',

            self::GRADE => '<i class="bx bx-cog fs-1"></i>',
            self::SECTION => '<i class="bx bx-caret-right fs-1"></i>',
            self::SUBJECT => '<i class="bx bx-caret-left fs-1"></i>',
            self::SYLLABUS => '<i class="bx bx-caret-down fs-1"></i>',
            self::PROFILE => '<i class="bx bx-lock fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }


    public static function getTranslationKeys(): array
    {
        return [
            self::ACADEMIC_YEAR => __(sprintf('%s.%s', 'school', self::ACADEMIC_YEAR)),

            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::SYLLABUS => __(sprintf('%s.%s', 'school', self::SYLLABUS)),
            self::PROFILE => __(sprintf('%s.%s', 'school', self::PROFILE))
        ];
    }
}
