<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForStudentType extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const STUDENT_TYPE = 'student_type';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::STUDENT_TYPE => __(sprintf('%s.%s', 'school', self::STUDENT_TYPE)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
        ];
    }
}
