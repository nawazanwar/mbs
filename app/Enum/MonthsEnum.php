<?php

declare(strict_types=1);

namespace App\Enum;

class MonthsEnum extends AbstractEnum
{
    public const JANUARY = 'january';
    public const FEBRUARY = 'february';
    public const MARCH = 'march';
    public const APRIL = 'april';
    public const MAY = 'may';
    public const JUNE = 'june';
    public const JULY = 'july';
    public const AUGUST = 'august';
    public const SEPTEMBER = 'september';
    public const OCTOBER = 'october';
    public const NOVEMBER = 'november';
    public const DECEMBER = 'december';

    public static function getValues(): array
    {
        return [
            self::JANUARY,
            self::FEBRUARY,
            self::MARCH,
            self::APRIL,
            self::MAY,
            self::JUNE,
            self::JULY,
            self::AUGUST,
            self::SEPTEMBER,
            self::OCTOBER,
            self::NOVEMBER,
            self::DECEMBER
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::JANUARY => __(self::JANUARY),
            self::FEBRUARY => __(self::FEBRUARY),
            self::MARCH => __(self::MARCH),
            self::APRIL => __(self::APRIL),
            self::MAY => __(self::MAY),
            self::JUNE => __(self::JUNE),
            self::JULY => __(self::JULY),
            self::AUGUST => __(self::AUGUST),
            self::SEPTEMBER => __(self::SEPTEMBER),
            self::OCTOBER => __(self::OCTOBER),
            self::NOVEMBER => __(self::NOVEMBER),
            self::DECEMBER => __(self::DECEMBER),
        ];
    }
}
