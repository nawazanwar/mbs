<?php

declare(strict_types=1);

namespace App\Enum;

class ComplainNavEnum extends AbstractEnum
{
    public const COMPLAIN_TYPE = 'complain_type';
    public const MANAGE_COMPLAIN = 'manage_complain';

    public static function getValues(): array
    {
        return [
            self::COMPLAIN_TYPE,
            self::MANAGE_COMPLAIN
        ];
    }

    public static function getRouteKeys(): array
    {
        return [
            self::COMPLAIN_TYPE => null,
            self::MANAGE_COMPLAIN => null
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::COMPLAIN_TYPE => route('dashboard.complain.types.index'),
            self::MANAGE_COMPLAIN => route('dashboard.complains.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::COMPLAIN_TYPE => '<i class="bx bx-comment fs-1"></i>',
            self::MANAGE_COMPLAIN => '<i class="bx bx-envelope fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::COMPLAIN_TYPE => __(sprintf('%s.%s', 'school', self::COMPLAIN_TYPE)),
            self::MANAGE_COMPLAIN => __(sprintf('%s.%s', 'school', self::MANAGE_COMPLAIN))
        ];
    }
}
