<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForGalleryImage extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const IMAGE = 'image';
    public const CAPTION = 'caption';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::IMAGE => __(sprintf('%s.%s', 'school', self::IMAGE)),
            self::CAPTION => __(sprintf('%s.%s', 'school', self::CAPTION)),

        ];
    }
}
