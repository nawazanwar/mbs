<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAccountingFeeType extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
        ];
    }
}
