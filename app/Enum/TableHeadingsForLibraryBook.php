<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForLibraryBook extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const BOOK_ID = 'book_id';
    public const ISBN_NO = 'isbn_no';
    public const EDITION = 'edition';
    public const AUTHOR = 'author';
    public const LANGUAGE = 'language';
    public const PRICE = 'price';
    public const QUANTITY = 'quantity';
    public const COVER = 'cover';
    public const RACK_NO = 'rack_no';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::BOOK_ID => __(sprintf('%s.%s', 'school', self::BOOK_ID)),
            self::ISBN_NO => __(sprintf('%s.%s', 'school', self::ISBN_NO)),
            self::EDITION => __(sprintf('%s.%s', 'school', self::EDITION)),
            self::AUTHOR => __(sprintf('%s.%s', 'school', self::AUTHOR)),
            self::LANGUAGE => __(sprintf('%s.%s', 'school', self::LANGUAGE)),
            self::PRICE => __(sprintf('%s.%s', 'school', self::PRICE)),
            self::QUANTITY => __(sprintf('%s.%s', 'school', self::QUANTITY)),
            self::COVER => __(sprintf('%s.%s', 'school', self::COVER)),
            self::RACK_NO => __(sprintf('%s.%s', 'school', self::RACK_NO)),

        ];
    }
}
