<?php

declare(strict_types=1);

namespace App\Enum;

class DaysEnum extends AbstractEnum
{
    public const MONDAY = 'monday';
    public const TUESDAY = 'tuesday';
    public const WEDNESDAY = 'wednesday';
    public const THURSDAY = 'thursday';
    public const FRIDAY = 'friday';
    public const SATURDAY = 'saturday';
    public const SUNDAY = 'sunday';

    public static function getValues(): array
    {
        return [
            self::MONDAY,
            self::TUESDAY,
            self::WEDNESDAY,
            self::THURSDAY,
            self::FRIDAY,
            self::SATURDAY,
            self::SUNDAY
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::MONDAY => __(self::MONDAY),
            self::TUESDAY => __(self::TUESDAY),
            self::WEDNESDAY => __(self::WEDNESDAY),
            self::THURSDAY => __(self::THURSDAY),
            self::FRIDAY => __(self::FRIDAY),
            self::SATURDAY => __(self::SATURDAY),
            self::SUNDAY => __(self::SUNDAY)
        ];
    }
}
