<?php

declare(strict_types=1);

namespace App\Enum;

class MainNavEnum extends AbstractEnum
{
    public const DASHBOARD = 'dashboard';
    public const AUTHORIZATION = 'authorization';
    public const CONFIGURATIONS = 'configurations';
    public const COLLABORATION = 'collaborations';
    public const HRM = 'hrm';
    public const FINANCE = 'finance';
    public const OPERATIONS = 'operations';
    public const FRONT_OFFICE = 'front_office';
    public const REPORT = 'report';
    public const DEFINITIONS = 'definitions';
    public static function getValues(): array
    {
        return [
            self::DASHBOARD,
            self::AUTHORIZATION,
            self::CONFIGURATIONS,
            self::COLLABORATION,
            self::HRM,
            self::FINANCE,
            self::OPERATIONS,
            self::FRONT_OFFICE,
            self::REPORT,
            self::DEFINITIONS,
        ];
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::DASHBOARD => '<i class="bx bxs-dashboard fs-1"></i>',
            self::AUTHORIZATION => '<i class="bx bx-user fs-1"></i>',
            self::CONFIGURATIONS => '<i class="bx bx-cog fs-1"></i>',
            self::REPORT => '<i class="bx bx-bar-chart fs-1"></i>',
            self::COLLABORATION => '<i class="bx bx-recycle fs-1"></i>',
            self::FRONT_OFFICE => '<i class="bx bxl-telegram fs-1"></i>',
            self::HRM => '<i class="bx bxs-user-rectangle fs-1"></i>',
            self::OPERATIONS => '<i class="bx bx-cog fs-1"></i>',
            self::FINANCE => '<i class="bx bx-dollar fs-1"></i>',
            self::DEFINITIONS => '<i class="bx bx-cog fs-1"></i>',

        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::DASHBOARD => route('dashboard.index'),
            self::AUTHORIZATION => route('dashboard.roles.index'),
            self::CONFIGURATIONS => route('dashboard.index', ['type' => self::CONFIGURATIONS]),
            self::REPORT => route('dashboard.reports.index'),
            self::COLLABORATION => route('dashboard.index', ['type' => self::COLLABORATION]),
            self::FRONT_OFFICE => route('dashboard.visitor.purposes.index'),
            self::HRM =>  route('dashboard.hrm.index'),
            self::OPERATIONS => route('dashboard.index', ['type' => self::OPERATIONS]),
            self::FINANCE => route('dashboard.index', ['type' => self::FINANCE]),
            self::DEFINITIONS => route('dashboard.index', ['type' => self::DEFINITIONS]),

        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::DASHBOARD => __(sprintf('%s.%s', 'school', self::DASHBOARD)),
            self::AUTHORIZATION => __(sprintf('%s.%s', 'school', self::AUTHORIZATION)),
            self::CONFIGURATIONS => __(sprintf('%s.%s', 'school', self::CONFIGURATIONS)),
            self::COLLABORATION => __(sprintf('%s.%s', 'school', self::COLLABORATION)),
            self::HRM => __(sprintf('%s.%s', 'school', self::HRM)),
            self::FINANCE => __(sprintf('%s.%s', 'school', self::FINANCE)),
            self::OPERATIONS => __(sprintf('%s.%s', 'school', self::OPERATIONS)),
            self::FRONT_OFFICE => __(sprintf('%s.%s', 'school', self::FRONT_OFFICE)),
            self::REPORT => __(sprintf('%s.%s', 'school', self::REPORT)),
            self::DEFINITIONS => __(sprintf('%s.%s', 'school', self::DEFINITIONS)),
        ];
    }
}
