<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSection extends AbstractEnum
{
    public const SECTION = 'section';
    public const GRADE = 'class';
    public const GRADE_TEACHER = 'class_teacher';
    public const NOTE = 'note';



    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::GRADE_TEACHER => __(sprintf('%s.%s', 'school', self::GRADE_TEACHER)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),




        ];
    }
}
