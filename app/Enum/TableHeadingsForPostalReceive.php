<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForPostalReceive extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TO_TITLE = 'to_title';
    public const REFERENCE = 'reference';
    public const FROM_TITLE = 'from_title';
    public const RECEIVE_DATE = 'receive_date';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TO_TITLE => __(sprintf('%s.%s', 'school', self::TO_TITLE)),
            self::REFERENCE => __(sprintf('%s.%s', 'school', self::REFERENCE)),
            self::FROM_TITLE => __(sprintf('%s.%s', 'school', self::FROM_TITLE)),
            self::RECEIVE_DATE => __(sprintf('%s.%s', 'school', self::RECEIVE_DATE)),



        ];
    }
}
