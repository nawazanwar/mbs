<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForCollaborationEvent extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const EVENT_FOR = 'event_for';
    public const EVENT_PLACE = 'event_place';
    public const FROM_DATE = 'from_date';
    public const TO_DATE = 'to_date';
    public const IMAGE = 'image';
    public const IS_VIEW_ON_WEB = 'is_view_on_web';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::EVENT_FOR => __(sprintf('%s.%s', 'school', self::EVENT_FOR)),
            self::EVENT_PLACE => __(sprintf('%s.%s', 'school', self::EVENT_PLACE)),
            self::FROM_DATE => __(sprintf('%s.%s', 'school', self::FROM_DATE)),
            self::TO_DATE => __(sprintf('%s.%s', 'school', self::TO_DATE)),
            self::IMAGE => __(sprintf('%s.%s', 'school', self::IMAGE)),
            self::IS_VIEW_ON_WEB => __(sprintf('%s.%s', 'school', self::IS_VIEW_ON_WEB)),


        ];
    }
}
