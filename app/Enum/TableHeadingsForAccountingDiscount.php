<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAccountingDiscount extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const DISCOUNT_TYPE = 'discount_type';
    public const AMOUNT = 'amount';
    public const NOTE = 'note';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::DISCOUNT_TYPE => __(sprintf('%s.%s', 'school', self::DISCOUNT_TYPE)),
            self::AMOUNT => __(sprintf('%s.%s', 'school', self::AMOUNT)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
        ];
    }
}
