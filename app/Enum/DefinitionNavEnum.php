<?php

declare(strict_types=1);

namespace App\Enum;

class
DefinitionNavEnum extends AbstractEnum
{
    public const TITLES = 'titles';
    public const BLOOD_GROUP = 'blood_group';
    public const GENDERS = 'genders';
    public const RELIGIONS = 'religions';
    public const PROFESSIONS = 'professions';
    public const SPECIALIZATIONS = 'specializations';
    public const QUALIFICATIONS = 'qualifications';
    public const RELATIONS = 'relations';
    public const PREFIXES = 'prefixes';
    public const POSTFIXES = 'postfixes';
    public const TERRITORIES = 'territories';
    public const NATIONALITIES = 'nationalities';
    public const COUNTRIES = 'countries';
    public const MINISTRIES = 'ministries';
    public const ORGANIZATIONS = 'organizations';
    public const RANKS = 'ranks';
    public const GRADES = 'grades';
    public const WINGS = 'wings';
    public const STATES = 'states';
    public const PROVINCES = 'provinces';
    public const DISTRICTS = 'districts';
    public const TEHSILS = 'tehsils';
    public const COLONIES = 'colonies';


    public static function getValues(): array
    {
        return array();
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::TITLES => route('dashboard.discounts.index'),
            self::BLOOD_GROUP => route('dashboard.fee.types.index'),
            self::GENDERS => route('dashboard.fee.collections.index'),
            self::RELIGIONS => route('dashboard.invoices.index'),
            self::PROFESSIONS => route('dashboard.invoices.index', ['type' => 'dues']),
            self::SPECIALIZATIONS => route('dashboard.receipts.index', ['type' => 'dues']),
            self::QUALIFICATIONS => route('dashboard.receipts.index', ['type' => 'paid']),
            self::RELATIONS => route('dashboard.dues.fee.emails.index'),
            self::PREFIXES => route('dashboard.dues.fee.sms.index'),
            self::POSTFIXES => route('dashboard.dues.fee.sms.index'),
            self::TERRITORIES => route('dashboard.income.heads.index'),
            self::NATIONALITIES => route('dashboard.expenditure.heads.index'),
            self::COUNTRIES => route('dashboard.expenditure.heads.index'),
            self::MINISTRIES => route('dashboard.expenditure.heads.index'),
            self::ORGANIZATIONS => route('dashboard.expenditure.heads.index'),
            self::RANKS => route('dashboard.expenditure.heads.index'),
            self::GRADES => route('dashboard.expenditure.heads.index'),
            self::WINGS => route('dashboard.expenditure.heads.index'),
            self::STATES => route('dashboard.expenditure.heads.index'),
            self::PROVINCES => route('dashboard.incomes.index'),
            self::DISTRICTS => route('dashboard.expenditures.index'),
            self::TEHSILS => route('dashboard.expenditures.index'),
            self::COLONIES => route('dashboard.expenditures.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::TITLES => '<i class="bx bx-calculator fs-1"></i>',
            self::BLOOD_GROUP => '<i class="bx bx-ghost fs-1"></i>',
            self::GENDERS => '<i class="bx bxl-gitlab fs-1"></i>',
            self::RELIGIONS => '<i class="bx bx-calculator fs-1"></i>',
            self::PROFESSIONS => '<i class="bx bx-calculator fs-1"></i>',
            self::SPECIALIZATIONS => '<i class="bx bx-calculator fs-1"></i>',
            self::QUALIFICATIONS => '<i class="bx bx-calculator fs-1"></i>',
            self::RELATIONS => '<i class="bx bx-calculator fs-1"></i>',
            self::PREFIXES => '<i class="bx bx-calculator fs-1"></i>',
            self::POSTFIXES => '<i class="bx bx-calculator fs-1"></i>',
            self::TERRITORIES => '<i class="bx bx-calculator fs-1"></i>',
            self::NATIONALITIES => '<i class="bx bx-calculator fs-1"></i>',
            self::COUNTRIES => '<i class="bx bx-calculator fs-1"></i>',
            self::ORGANIZATIONS => '<i class="bx bx-calculator fs-1"></i>',
            self::MINISTRIES => '<i class="bx bx-calculator fs-1"></i>',
            self::RANKS => '<i class="bx bx-calculator fs-1"></i>',
            self::GRADES => '<i class="bx bx-calculator fs-1"></i>',
            self::WINGS => '<i class="bx bx-calculator fs-1"></i>',
            self::STATES => '<i class="bx bx-calculator fs-1"></i>',
            self::PROVINCES => '<i class="bx bx-calculator fs-1"></i>',
            self::DISTRICTS => '<i class="bx bx-calculator fs-1"></i>',
            self::TEHSILS => '<i class="bx bx-calculator fs-1"></i>',
            self::COLONIES => '<i class="bx bx-calculator fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::TITLES => __(sprintf('%s.%s', 'school', self::TITLES)),
            self::BLOOD_GROUP => __(sprintf('%s.%s', 'school', self::BLOOD_GROUP)),
            self::GENDERS => __(sprintf('%s.%s', 'school', self::GENDERS)),
            self::RELIGIONS => __(sprintf('%s.%s', 'school', self::RELIGIONS)),
            self::PROFESSIONS => __(sprintf('%s.%s', 'school', self::PROFESSIONS)),
            self::SPECIALIZATIONS => __(sprintf('%s.%s', 'school', self::SPECIALIZATIONS)),
            self::QUALIFICATIONS => __(sprintf('%s.%s', 'school', self::QUALIFICATIONS)),
            self::RELATIONS => __(sprintf('%s.%s', 'school', self::RELATIONS)),
            self::PREFIXES => __(sprintf('%s.%s', 'school', self::PREFIXES)),
            self::POSTFIXES => __(sprintf('%s.%s', 'school', self::POSTFIXES)),
            self::TERRITORIES => __(sprintf('%s.%s', 'school', self::TERRITORIES)),
            self::NATIONALITIES => __(sprintf('%s.%s', 'school', self::NATIONALITIES)),
            self::COUNTRIES => __(sprintf('%s.%s', 'school', self::COUNTRIES)),
            self::MINISTRIES => __(sprintf('%s.%s', 'school', self::MINISTRIES)),
            self::ORGANIZATIONS => __(sprintf('%s.%s', 'school', self::ORGANIZATIONS)),
            self::RANKS => __(sprintf('%s.%s', 'school', self::RANKS)),
            self::GRADES => __(sprintf('%s.%s', 'school', self::GRADES)),
            self::WINGS => __(sprintf('%s.%s', 'school', self::WINGS)),
            self::STATES => __(sprintf('%s.%s', 'school', self::STATES)),
            self::PROVINCES => __(sprintf('%s.%s', 'school', self::PROVINCES)),
            self::DISTRICTS => __(sprintf('%s.%s', 'school', self::DISTRICTS)),
            self::TEHSILS => __(sprintf('%s.%s', 'school', self::TEHSILS)),
            self::COLONIES => __(sprintf('%s.%s', 'school', self::COLONIES))
        ];
    }
}
