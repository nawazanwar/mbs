<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAssignment extends AbstractEnum
{
    public const TITTLE = 'tittle';
    public const GRADE = 'class';
    public const SECTION = 'section';
    public const SUBJECT = 'subject';
    public const DEADLINE = 'deadline';
    public const ASSIGNMENT = 'assignment';
    public const ACADEMIC_YEAR = 'academic_year';
    public const NOTE = 'note';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [


            self::TITTLE => __(sprintf('%s.%s', 'school', self::TITTLE)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::DEADLINE => __(sprintf('%s.%s', 'school', self::DEADLINE)),
            self::ASSIGNMENT => __(sprintf('%s.%s', 'school', self::ASSIGNMENT)),
            self::ACADEMIC_YEAR => __(sprintf('%s.%s', 'school', self::ACADEMIC_YEAR)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),
        ];
    }
}
