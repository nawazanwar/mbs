<?php

declare(strict_types=1);

namespace App\Enum;

class ConfigurationNavEnum extends AbstractEnum
{
    public const THEME = 'theme';
    public const LANGUAGE = 'language';
    public const TEMPLATES = 'templates';
    public const MANAGE_LEAVE = 'manage_leave';

    public static function getValues(): array
    {
        return [
            self::THEME,
            self::LANGUAGE,
            self::TEMPLATES,
            self::MANAGE_LEAVE,
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::THEME => route('dashboard.themes.index'),
            self::LANGUAGE => route('dashboard.languages.index'),
            self::TEMPLATES => route('dashboard.template.sms.index'),
            self::MANAGE_LEAVE => route('dashboard.leave.types.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::THEME => '<i class="bx bx-cube fs-1"></i>',
            self::LANGUAGE =>'<i class="bx bx-globe fs-1"></i>',
            self::TEMPLATES => '<i class="bx bx-cube fs-1"></i>',
            self::MANAGE_LEAVE => '<i class="bx bx-bell fs-1"></i>',
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::THEME => __(sprintf('%s.%s', 'school', self::THEME)),
            self::LANGUAGE => __(sprintf('%s.%s', 'school', self::LANGUAGE)),
            self::TEMPLATES => __(sprintf('%s.%s', 'school', self::TEMPLATES)),
            self::MANAGE_LEAVE => __(sprintf('%s.%s', 'school', self::MANAGE_LEAVE)),
        ];
    }
}
