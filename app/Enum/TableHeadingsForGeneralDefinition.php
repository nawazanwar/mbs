<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForGeneralDefinition extends AbstractEnum
{
    public const NAME = 'name';
    public const SLUG = 'slug';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::SLUG => __(sprintf('%s.%s', 'school', self::SLUG)),


        ];
    }
}
