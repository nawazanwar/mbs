<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForAccountingDueFeeSms extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const SESSION_YEAR = 'session_year';
    public const RECEIVE_TYPE = 'receive_type';
    public const TIME = 'time';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::SESSION_YEAR => __(sprintf('%s.%s', 'school', self::SESSION_YEAR)),
            self::RECEIVE_TYPE => __(sprintf('%s.%s', 'school', self::RECEIVE_TYPE)),
            self::TIME => __(sprintf('%s.%s', 'school', self::TIME)),
        ];
    }
}
