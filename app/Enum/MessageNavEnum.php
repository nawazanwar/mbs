<?php

declare(strict_types=1);

namespace App\Enum;

class MessageNavEnum extends AbstractEnum
{
    public const MESSAGE = 'message';
    public const EMAIL = 'email';
    public const SMS = 'sms';

    public static function getValues(): array
    {
        return [
            self::MESSAGE,
            self::EMAIL,
            self::SMS,
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::MESSAGE => route('dashboard.messages.index'),
            self::EMAIL => route('dashboard.message.emails.index'),
            self::SMS => route('dashboard.message.sms.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::MESSAGE => '<i class="bx bx-comment fs-1"></i>',
            self::EMAIL => '<i class="bx bx-envelope fs-1"></i>',
            self::SMS => '<i class="bx bx-envelope fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::MESSAGE => __(sprintf('%s.%s', 'school', self::MESSAGE)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::SMS => __(sprintf('%s.%s', 'school', self::SMS))
        ];
    }
}
