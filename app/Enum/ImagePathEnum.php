<?php

declare(strict_types=1);

namespace App\Enum;

class ImagePathEnum extends AbstractEnum
{
    public const ASSIGNMENT = 'uploads/assignments/';

    public static function getValues(): array
    {
        return array();
    }

    public static function getTranslationKeys(): array
    {
        return array();
    }
}
