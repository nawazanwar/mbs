<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForLiveClass extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const GRADE = 'class';
    public const SECTION = 'section';
    public const SUBJECT = 'subject';
    public const TEACHER = 'teacher';
    public const CLASS_DATE = 'class_date';
    public const START_TIME = 'start_time';
    public const END_TIME = 'end_time';
    public const STATUS = 'status';





    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::STATUS => __(sprintf('%s.%s', 'school', self::STATUS)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::TEACHER => __(sprintf('%s.%s', 'school', self::TEACHER)),
            self::CLASS_DATE => __(sprintf('%s.%s', 'school', self::CLASS_DATE)),
            self::START_TIME => __(sprintf('%s.%s', 'school', self::START_TIME)),
            self::END_TIME => __(sprintf('%s.%s', 'school', self::END_TIME)),




        ];
    }
}
