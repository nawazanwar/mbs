<?php

declare(strict_types=1);

namespace App\Enum;

class TimezoneEnum extends AbstractEnum
{
    public const PACIFIC_MIDWAY = '(GMT-11:00) Midway Island';
    public const US_SAMOA = '(GMT-11:00) Samoa';
    public const US_HAWAII = '(GMT-10:00) Hawaii';
    public const US_ALASKA = '(GMT-09:00) Alaska';
    public const US_PACIFIC = '(GMT-08:00) Pacific Time (US & Canada)';
    public const AMERICA_TIJUANA = '(GMT-08:00) Tijuana';
    public const US_ARIZONA = '(GMT-07:00) Arizona ';
    public const US_MOUNTAIN = '(GMT-07:00) Mountain Time (US & Canada)';

    public static function getValues(): array
    {
        return [
            self::PACIFIC_MIDWAY,
            self::US_SAMOA,
            self::US_ALASKA,
            self::US_HAWAII,
            self::US_PACIFIC,
            self::AMERICA_TIJUANA,
            self::US_ARIZONA,
            self::US_MOUNTAIN,
        ];
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::ENGLISH => __(self::ENGLISH),
            self::CHINES => __(self::CHINES),
            self::MALAY => __(self::MALAY),
            self::TAMIL => __(self::TAMIL),
        ];
    }
}
