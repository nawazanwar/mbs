<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForTeacherList extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const NATIONAL_ID = 'national_id';
    public const RESPONSIBILITY = 'responsibility';
    public const PHONE = 'phone';
    public const GENDER = 'gender';
    public const BLOOD_GROUP = 'blood_group';
    public const RELIGION = 'religion';
    public const DOB = 'dob';
    public const PRESENT_ADDRESS = 'present_address';
    public const PERMANENT_address = 'permanent_address';
    public const EMAIL = 'email';
    public const USER_NAME = 'user_name';
    public const PASSWORD = 'password';
    public const SALARY_GRADE = 'salary_grade';
    public const SALARY_TYPE = 'salary_type';
    public const JOINING_DATE = 'joining_date';
    public const RESUME = 'resume';
    public const IS_VieW_ON_WEB = 'is_view_on_web';
    public const FACEBOOK_URL = 'facebook_url';
    public const LINKEDIN_URL = 'linkedin_url';
    public const TWITTER_URL = 'twitter_url';
    public const GOOGLE_PLUS_URL = 'google_plus_url';
    public const INSTAGRAM_URL = 'instagram_url';
    public const YOUTUBE_URL = 'youtube_url';
    public const PINTEREST_URL = 'pinterest_url';
    public const OTHER_INFO = 'other_info';
    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::NATIONAL_ID => __(sprintf('%s.%s', 'school', self::NATIONAL_ID)),
            self::RESPONSIBILITY => __(sprintf('%s.%s', 'school', self::RESPONSIBILITY)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::GENDER => __(sprintf('%s.%s', 'school', self::GENDER)),
            self::BLOOD_GROUP => __(sprintf('%s.%s', 'school', self::BLOOD_GROUP)),
            self::RELIGION => __(sprintf('%s.%s', 'school', self::RELIGION)),
            self::DOB => __(sprintf('%s.%s', 'school', self::DOB)),
            self::PRESENT_ADDRESS => __(sprintf('%s.%s', 'school', self::PRESENT_ADDRESS)),
            self::PERMANENT_address => __(sprintf('%s.%s', 'school', self::PERMANENT_address)),
            self::SALARY_TYPE => __(sprintf('%s.%s', 'school', self::SALARY_TYPE)),
            self::USER_NAME => __(sprintf('%s.%s', 'school', self::USER_NAME)),
            self::PASSWORD => __(sprintf('%s.%s', 'school', self::PASSWORD)),
            self::SALARY_GRADE => __(sprintf('%s.%s', 'school', self::SALARY_GRADE)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::JOINING_DATE => __(sprintf('%s.%s', 'school', self::JOINING_DATE)),
            self::RESUME => __(sprintf('%s.%s', 'school', self::RESUME)),
            self::IS_VieW_ON_WEB => __(sprintf('%s.%s', 'school', self::IS_VieW_ON_WEB)),
            self::FACEBOOK_URL => __(sprintf('%s.%s', 'school', self::FACEBOOK_URL)),
            self::LINKEDIN_URL => __(sprintf('%s.%s', 'school', self::LINKEDIN_URL)),
            self::TWITTER_URL => __(sprintf('%s.%s', 'school', self::TWITTER_URL)),
            self::GOOGLE_PLUS_URL => __(sprintf('%s.%s', 'school', self::GOOGLE_PLUS_URL)),
            self::INSTAGRAM_URL => __(sprintf('%s.%s', 'school', self::INSTAGRAM_URL)),
            self::YOUTUBE_URL => __(sprintf('%s.%s', 'school', self::YOUTUBE_URL)),
            self::PINTEREST_URL => __(sprintf('%s.%s', 'school', self::PINTEREST_URL)),
            self::OTHER_INFO => __(sprintf('%s.%s', 'school', self::OTHER_INFO)),

        ];
    }
}
