<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForSettingSms extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const CLICK_TELL = 'click_tell';
    public const TWILIO = 'twilio';
    public const BULK = 'bulk';
    public const MSG_91= 'msg_91';
    public const PLIVO = 'plivo';
    public const TEXT_LOCAL= 'text_local';
    public const SMS_COUNTRY= 'sms_country';
    public const BETA_SMS= 'beta_sms';
    public const BULK_PK= 'bulk_pk';
    public const SMS_CLUSTER= 'sms_cluster';
    public const ALPHA_NET= 'alpha_net';
    public const BD_BULK= 'bd_bulk';
    public const MIM_SMS= 'min_sms';
    public const MIN_SMS= 'min_sms';




    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::CLICK_TELL => __(sprintf('%s.%s', 'school', self::CLICK_TELL)),
            self::TWILIO => __(sprintf('%s.%s', 'school', self::TWILIO)),
            self::BULK => __(sprintf('%s.%s', 'school', self::BULK)),
            self::MSG_91 => __(sprintf('%s.%s', 'school', self::MSG_91)),
            self::PLIVO => __(sprintf('%s.%s', 'school', self::PLIVO)),
            self::TEXT_LOCAL => __(sprintf('%s.%s', 'school', self::TEXT_LOCAL)),
            self::SMS_COUNTRY => __(sprintf('%s.%s', 'school', self::SMS_COUNTRY)),
            self::BETA_SMS => __(sprintf('%s.%s', 'school', self::BETA_SMS)),
            self::BULK_PK => __(sprintf('%s.%s', 'school', self::BULK_PK)),
            self::SMS_CLUSTER => __(sprintf('%s.%s', 'school', self::SMS_CLUSTER)),
            self::ALPHA_NET => __(sprintf('%s.%s', 'school', self::ALPHA_NET)),
            self::BD_BULK => __(sprintf('%s.%s', 'school', self::BD_BULK)),
            self::MIN_SMS => __(sprintf('%s.%s', 'school', self::MIN_SMS)),


        ];
    }
}
