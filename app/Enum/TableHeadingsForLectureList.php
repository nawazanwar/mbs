<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForLectureList extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const TITLE = 'title';
    public const GRADE = 'class';
    public const SECTION = 'section';
    public const SUBJECT = 'subject';
    public const TEACHER = 'teacher';
    public const CLASS_LECTURE = 'class_lecture';
    public const ACADEMIC_YEAR = 'academic_year';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::SECTION => __(sprintf('%s.%s', 'school', self::SECTION)),
            self::SUBJECT => __(sprintf('%s.%s', 'school', self::SUBJECT)),
            self::TEACHER => __(sprintf('%s.%s', 'school', self::TEACHER)),
            self::CLASS_LECTURE => __(sprintf('%s.%s', 'school', self::CLASS_LECTURE)),
            self::ACADEMIC_YEAR => __(sprintf('%s.%s', 'school', self::ACADEMIC_YEAR)),


        ];
    }
}
