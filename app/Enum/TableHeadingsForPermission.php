<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForPermission extends AbstractEnum
{
    public const FUNCTION_NAME ='function_name';
    public const VIEW ='view';
    public const CREATE ='create';
    public const UPDATE ='update';
    public const delete ='delete';





    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [


            self::FUNCTION_NAME => __(sprintf('%s.%s', 'school', self::FUNCTION_NAME)),
            self::VIEW => __(sprintf('%s.%s', 'school', self::VIEW)),
            self::CREATE => __(sprintf('%s.%s', 'school', self::CREATE)),
            self::UPDATE => __(sprintf('%s.%s', 'school', self::UPDATE)),
            self::delete => __(sprintf('%s.%s', 'school', self::delete)),


        ];
    }
}
