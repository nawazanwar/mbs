<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForLibraryEbook extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const NAME = 'name';
    public const GRADE = 'class';
    public const AUTHOR = 'author';
    public const LANGUAGE = 'language';
    public const EDITION = 'edition';
    public const COVER_IMAGE = 'cover_image';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::GRADE => __(sprintf('%s.%s', 'school', self::GRADE)),
            self::AUTHOR => __(sprintf('%s.%s', 'school', self::AUTHOR)),
            self::LANGUAGE => __(sprintf('%s.%s', 'school', self::LANGUAGE)),
            self::EDITION => __(sprintf('%s.%s', 'school', self::EDITION)),
            self::COVER_IMAGE => __(sprintf('%s.%s', 'school', self::COVER_IMAGE)),


        ];
    }
}
