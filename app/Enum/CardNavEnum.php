<?php

declare(strict_types=1);

namespace App\Enum;

class CardNavEnum extends AbstractEnum
{
    public const ID_CARD_SETTING = 'id_card_setting';
    public const ADMIT_CARD_SETTING= 'admit_card_setting';
    public const TEACHER_ID_CARD= 'teacher_id_card';
    public const EMPLOYEE_ID_CARD= 'employee_id_card';
    public const STUDENT_ID_CARD= 'student_id_card';
    public const STUDENT_ADMIT_CARD= 'student_admit_card';

    public static function getValues(): array
    {
        return [
            self::ID_CARD_SETTING,
            self::ADMIT_CARD_SETTING,
            self::TEACHER_ID_CARD,
            self::EMPLOYEE_ID_CARD,
            self::STUDENT_ID_CARD,
            self::STUDENT_ADMIT_CARD
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::ID_CARD_SETTING => route('dashboard.id-card-settings.index'),
            self::ADMIT_CARD_SETTING => route('dashboard.admit-card-settings.index'),
            self::TEACHER_ID_CARD => route('dashboard.teacher-id-cards.index'),
            self::EMPLOYEE_ID_CARD => route('dashboard.employee-id-cards.index'),
            self::STUDENT_ID_CARD => route('dashboard.student-id-cards.index'),
            self::STUDENT_ADMIT_CARD => route('dashboard.student-admit-cards.index')
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::ID_CARD_SETTING => '<i class="bx bx-user-plus fs-1"></i>',
            self::ADMIT_CARD_SETTING =>'<i class="bx bx-certification fs-1"></i>',
            self::TEACHER_ID_CARD =>'<i class="bx bx-certification fs-1"></i>',
            self::EMPLOYEE_ID_CARD =>'<i class="bx bx-certification fs-1"></i>',
            self::STUDENT_ID_CARD =>'<i class="bx bx-certification fs-1"></i>',
            self::STUDENT_ADMIT_CARD =>'<i class="bx bx-certification fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getTranslationKeys(): array
    {
        return [
            self::ID_CARD_SETTING =>__(sprintf('%s.%s','school',self::ID_CARD_SETTING)),
            self::ADMIT_CARD_SETTING =>__(sprintf('%s.%s','school',self::ADMIT_CARD_SETTING)),
            self::TEACHER_ID_CARD =>__(sprintf('%s.%s','school',self::TEACHER_ID_CARD)),
            self::STUDENT_ID_CARD =>__(sprintf('%s.%s','school',self::STUDENT_ID_CARD)),
            self::STUDENT_ADMIT_CARD =>__(sprintf('%s.%s','school',self::STUDENT_ADMIT_CARD)),
        ];
    }
}
