<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForLibraryBookIssue extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const STUDENT = 'student';
    public const TITLE = 'title';
    public const BOOK_ID = 'book_id';
    public const ISSUE_DATE = 'issue_date';
    public const DUE_DATE = 'due_date';
    public const RETURN_DATE = 'return_date';
    public const BOOK_COVER = 'book_cover';

    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::STUDENT => __(sprintf('%s.%s', 'school', self::STUDENT)),
            self::TITLE => __(sprintf('%s.%s', 'school', self::TITLE)),
            self::BOOK_ID => __(sprintf('%s.%s', 'school', self::BOOK_ID)),
            self::ISSUE_DATE => __(sprintf('%s.%s', 'school', self::ISSUE_DATE)),
            self::DUE_DATE => __(sprintf('%s.%s', 'school', self::DUE_DATE)),
            self::RETURN_DATE => __(sprintf('%s.%s', 'school', self::RETURN_DATE)),
            self::BOOK_COVER => __(sprintf('%s.%s', 'school', self::BOOK_COVER)),

        ];
    }
}
