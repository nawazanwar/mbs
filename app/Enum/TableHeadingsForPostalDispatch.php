<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForPostalDispatch extends AbstractEnum
{

    public const TO_TITLE = 'to_title';
    public const REFERENCE = 'reference';
    public const ADDRESS = 'address';
    public const FROM_TITLE = 'from_title';
    public const DISPATCH_DATE = 'dispatch_date';
    public const ATTACHMENT = 'attachment';
    public const NOTE = 'note';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [


            self::TO_TITLE => __(sprintf('%s.%s', 'school', self::TO_TITLE)),
            self::REFERENCE => __(sprintf('%s.%s', 'school', self::REFERENCE)),
            self::ADDRESS => __(sprintf('%s.%s', 'school', self::ADDRESS)),
            self::FROM_TITLE => __(sprintf('%s.%s', 'school', self::FROM_TITLE)),
            self::DISPATCH_DATE => __(sprintf('%s.%s', 'school', self::DISPATCH_DATE)),
            self::ATTACHMENT => __(sprintf('%s.%s', 'school', self::ATTACHMENT)),
            self::NOTE => __(sprintf('%s.%s', 'school', self::NOTE)),



        ];
    }
}
