<?php

declare(strict_types=1);

namespace App\Enum;

class CollaborationNavEnum extends AbstractEnum
{
    public const MESSAGE = 'text';
    public const COMPLAIN = 'complain';
    public const ANNOUNCEMENT = 'announcement';
    public const EVENT = 'event';
    public const MEDIA_GALLERY = 'media_gallery';
    public const MANAGE_FRONT_END = 'manage_front_end';

    public static function getValues(): array
    {
        return [
            self::MESSAGE,
            self::COMPLAIN,
            self::ANNOUNCEMENT,
            self::EVENT,
            self::MEDIA_GALLERY,
            self::MANAGE_FRONT_END
        ];
    }

    public static function getRoute($key = null)
    {
        $routes = array(
            self::MESSAGE => route('dashboard.messages.index'),
            self::COMPLAIN => route('dashboard.complains.index'),
            self::ANNOUNCEMENT => route('dashboard.notices.index'),
            self::EVENT => route('dashboard.events.index'),
            self::MEDIA_GALLERY => route('dashboard.gallery.index'),
            self::MANAGE_FRONT_END => route('dashboard.pages.index'),
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }

    public static function getIcon($key = null): ?string
    {
        $routes = array(
            self::MESSAGE => '<i class="bx bx-comment fs-1"></i>',
            self::COMPLAIN => '<i class="bx bx-comment-detail fs-1"></i>',
            self::ANNOUNCEMENT => '<i class="bx lni-bullhorn fs-1"></i>',
            self::EVENT => '<i class="bx bx-calendar fs-1"></i>',
            self::MEDIA_GALLERY => '<i class="bx bx-image fs-1"></i>',
            self::MANAGE_FRONT_END => '<i class="bx bx-desktop fs-1"></i>'
        );
        if (!is_null($key) && array_key_exists($key, $routes)) {
            return $routes[$key];
        } else {
            return null;
        }
    }


    public static function getTranslationKeys(): array
    {
        return [
            self::MESSAGE => __(sprintf('%s.%s', 'school', self::MESSAGE)),
            self::COMPLAIN => __(sprintf('%s.%s', 'school', self::COMPLAIN)),
            self::ANNOUNCEMENT => __(sprintf('%s.%s', 'school', self::ANNOUNCEMENT)),
            self::EVENT => __(sprintf('%s.%s', 'school', self::EVENT)),
            self::MEDIA_GALLERY => __(sprintf('%s.%s', 'school', self::MEDIA_GALLERY)),
            self::MANAGE_FRONT_END => __(sprintf('%s.%s', 'school', self::MANAGE_FRONT_END)),
        ];
    }
}
