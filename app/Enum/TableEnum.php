<?php

declare(strict_types=1);

namespace App\Enum;

class TableEnum extends AbstractEnum
{
    public const ACADEMIC_YEAR = 'academic_years';
    public const ASSIGNMENTS = 'assignments';
    public const ASSIGNMENT_SUBMISSIONS = 'assignment_submissions';

    public const ADMIT_CARD_SETTINGS = 'admit_card_settings';

    public const LIBRARY_BOOKS = 'library_books';
    public const LIBRARY_BOOK_ISSUE = 'library_book_issues';

    public const CERTIFICATES = 'certificates';
    public const CLASSES = 'classes';
    public const COMPLAINS = 'complains';
    public const COMPLAIN_TYPES = 'complain_types';
    public const CONTINENTS = 'continents';
    public const COUNTRIES = 'countries';
    public const CITIES = 'cities';

    public const DESIGNATIONS = 'designations';
    public const DISCOUNTS = 'discounts';
    public const DIVISIONS = 'divisions';

    public const LIBRARY_EBOOKS = 'library_ebooks';
    public const EMAIL_SETTINGS = 'email_settings';
    public const EMAIL_TEMPLATES = 'email_templates';
    public const EMPLOYEES = 'employees';
    public const EMPLOYEE_ATTENDANCES = 'employee_attendances';
    public const EVENTS = 'events';
    public const EXAMS = 'exams';
    public const EXAM_ATTENDANCES = 'exam_attendances';
    public const EXAM_SCHEDULES = 'exam_schedules';

    public const EXPENDITURES = 'expenditures';
    public const EXPENDITURE_HEADS = 'expenditure_heads';


    public const FEE_AMOUNTS = 'fee_amounts';
    public const FEE_TYPES = 'fee_types';
    public const FINAL_RESULTS = 'final_results';

    public const GALLERIES = 'gallery';
    public const GALLERY_IMAGES = 'gallery_images';
    public const GLOBAL_SETTINGS = 'global_settings';
    public const GMSMS_SESSIONS = 'gmsms_sessions';
    public const GRADES = 'grades';
    public const GUARDIANS = 'guardians';
    public const GUARDIAN_FEEDBACKS = 'guardian_feedbacks';

    public const HOLIDAYS = 'holidays';

    public const HOSTELS = 'hostels';
    public const HOSTEL_ROOMS = 'hostel_rooms';

    public const ID_CARD_SETTINGS = 'id_card_settings';

    public const INCOMES = 'incomes';
    public const INCOME_HEADS = 'income_heads';

    public const INVOICES = 'invoices';
    public const INVOICE_DETAILS = 'invoice_details';

    public const LANGUAGES = 'languages';
    public const LANGUAGE_SCHOOL = 'language_school';

    public const LEAVE_APPLICATIONS = 'leave_applications';
    public const LEAVE_TYPES = 'leave_types';
    public const LIBRARY_MEMBERS = 'library_members';
    public const LIVE_CLASSES = 'live_classes';

    public const MARKS = 'marks';
    public const MARK_EMAILS = 'mark_emails';
    public const MARK_SMS = 'mark_sms';
    public const MESSAGES = 'messages';
    public const MESSAGE_RELATIONSHIPS = 'message_relationships';
    public const MODULES = 'modules';

    public const NEWS = 'news';
    public const NOTICES = 'notices';

    public const OPERATIONS = 'operations';

    public const PAGES = 'pages';
    public const PAYMENT_SETTINGS = 'payment_settings';

    public const PERMISSIONS = 'permissions';
    public const PERMISSION_ROLE = 'permission_role';

    public const PERSONAL_ACCESS_TOKENS = 'personal_access_tokens';
    public const PHONE_CALL_LOGS = 'phone_call_logs';
    public const POSTAL_DISPATCHES = 'postal_dispatches';
    public const POSTAL_RECEIVES = 'postal_receives';
    public const PRIVILEGES = 'privileges';
    public const PURCHASE = 'purchase';

    public const REPLIES = 'replies';
    public const RESULTS = 'results';

    public const ROLES = 'roles';

    public const ROLE_PERMISSION = 'role_permission';
    public const ROLE_USER = 'role_user';
    public const ROOM = 'rooms';
    public const ROUTE_STOPS = 'route_stops';
    public const ROUTINES = 'routines';

    public const SALARY_GRADES = 'salary_grades';
    public const SALARY_PAYMENTS = 'salary_payments';
    public const SALARY_SCHOOLS = 'salary_schools';
    public const SCHOOLS = 'schools';
    public const SECTIONS = 'sections';
    public const SLIDERS = 'sliders';
    public const SMS_SETTINGS = 'sms_settings';
    public const SMS_TEMPLATES = 'sms_templates';
    public const STUDENTS = 'students';
    public const STUDENT_ACTIVITIES = 'student_activities';
    public const STUDENT_ATTENDANCES = 'student_attendances';
    public const STUDENT_TYPES = 'student_types';
    public const SUBJECTS = 'subjects';
    public const SUGGESTIONS = 'suggestions';
    public const SYLLABUSES = 'syllabuses';
    public const SYSTEM_ADMIN = 'system_admin';
    public const STUDY_MATERIALS = 'study_materials';

    public const TEACHERS = 'teachers';
    public const TEACHER_ATTENDANCES = 'teacher_attendances';
    public const TEXT_MESSAGES = 'text_messages';

    public const THEMES = 'themes';
    public const THEME_SCHOOL = 'theme_school';

    public const TRANSACTIONS = 'transactions';
    public const TRANSPORT_ROUTES = 'transport_routes';
    public const TRANSPORT_VEHICLES = 'transport_vehicles';


    public const USERS = 'users';
    public const VEHICLES = 'vehicles';
    public const VIDEO_LECTURES = 'video_lectures';
    public const VISITORS = 'visitors';
    public const VISITOR_PURPOSES = 'visitor_purposes';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

        ];
    }
}
