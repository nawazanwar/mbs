<?php

declare(strict_types=1);

namespace App\Enum;

class TableHeadingsForEmployeeAttendance extends AbstractEnum
{
    public const SCHOOL = 'school';
    public const PHOTO = 'photo';
    public const NAME = 'name';
    public const DESIGNATION = 'designation';
    public const PHONE = 'phone';
    public const EMAIL = 'email';
    public const PRESENT_ALL = 'present_all';
    public const LATE_ALL = 'late_all';
    public const ABSENT_ALL = 'absent_all';


    public static function getValues(): array
    {
        return [

        ];
    }

    public static function getTranslationKeys(): array
    {
        return [

            self::SCHOOL => __(sprintf('%s.%s', 'school', self::SCHOOL)),
            self::PHOTO => __(sprintf('%s.%s', 'school', self::PHOTO)),
            self::NAME => __(sprintf('%s.%s', 'school', self::NAME)),
            self::DESIGNATION => __(sprintf('%s.%s', 'school', self::DESIGNATION)),
            self::PHONE => __(sprintf('%s.%s', 'school', self::PHONE)),
            self::EMAIL => __(sprintf('%s.%s', 'school', self::EMAIL)),
            self::PRESENT_ALL => __(sprintf('%s.%s', 'school', self::PRESENT_ALL)),
            self::LATE_ALL => __(sprintf('%s.%s', 'school', self::LATE_ALL)),
            self::ABSENT_ALL => __(sprintf('%s.%s', 'school', self::ABSENT_ALL)),
        ];
    }
}
