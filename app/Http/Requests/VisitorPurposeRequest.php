<?php

namespace App\Http\Requests;

use App\Models\VisitorPurpose;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;

class VisitorPurposeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    public function createData()
    {
        $model = VisitorPurpose::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            return $model;
        }
    }
}
