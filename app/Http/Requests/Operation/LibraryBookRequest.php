<?php

namespace App\Http\Requests\Operation;

use App\Models\Discount;
use App\Models\LibraryBook;
use App\Models\PayrollSalaryGrade;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;

class LibraryBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function createData()
    {
        $model = LibraryBook::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            return $model;
        }

    }
}
