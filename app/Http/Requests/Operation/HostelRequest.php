<?php

namespace App\Http\Requests\Operation;

use App\Enum\TableEnum;
use App\Models\Hostel;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class HostelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id) {
            return [
                'name' => ['required', 'name', Rule::unique(TableEnum::HOSTELS)->ignore($this->id)]
            ];
        } else {
            return [
                'name' => ['required', Rule::unique(TableEnum::HOSTELS)],
            ];
        }
    }

    public function createData()
    {
        $model = Hostel::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            return $model;
        }
    }
}
