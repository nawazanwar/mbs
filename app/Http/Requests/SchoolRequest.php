<?php

namespace App\Http\Requests;

use App\Enum\RoleEnum;
use App\Enum\TableEnum;
use App\Models\Role;
use App\Models\School;
use App\Models\User;
use App\Traits\General;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class SchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [Rule::unique(TableEnum::SCHOOLS)],
            'code' => [Rule::unique(TableEnum::SCHOOLS)],
            'email' => [Rule::unique(TableEnum::USERS)],
        ];
    }

    public function createData()
    {
        $model = School::create($this->all());
        if ($model) {
            $this->savePhoto($model);
            $this->makeItUser($model);
            return $model;
        }
    }

    private function makeItUser($model)
    {
        $user = User::create([
            'email' => $model->email,
            'temp_password' => 'admin123',
            'password' => Hash::make('admin123'),
            'name' => 'School Admin',
            'school_id' => $model->id
        ]);

        $user->roles()->sync(Role::whereLabel(RoleEnum::ROLE_ADMIN)->first());
    }

    private function savePhoto($model)
    {
        if ($this->file('logo')) {
            $photoFile = $this->file('logo');
            $photo = Image::make($photoFile);
            $photo->resize(
                config('image.avatar.width'),
                config('image.avatar.height'), function ($constraint) {
                $constraint->aspectRatio();
            });
            $path = 'uploads/school/logo/' . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->logo = $path;
            $model->save();
        }
    }
}
