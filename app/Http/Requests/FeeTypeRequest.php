<?php

namespace App\Http\Requests;

use App\Enum\TableEnum;
use App\Models\Discount;
use App\Models\FeeType;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class FeeTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id) {
            return [
                'title' => ['title', Rule::unique(TableEnum::FEE_TYPES)->ignore($this->id)]
            ];
        } else {
            return [
                'title' => [Rule::unique(TableEnum::FEE_TYPES)],
            ];
        }
    }

    public function createData()
    {
        $model = FeeType::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            return $model;
        }
    }
}
