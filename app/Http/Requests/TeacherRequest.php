<?php

namespace App\Http\Requests;

use App\Enum\RoleEnum;
use Illuminate\Foundation\Http\FormRequest;
use App\Traits\General;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;
use App\Models\Role;
use App\Models\Teacher;
use App\Models\User;
use App\Services\SchoolService;
use Illuminate\Support\Facades\Hash;

class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id) {
            return [
                'email' => ['required', 'email', Rule::unique('users')->ignore($this->id)]
            ];
        } else {
            return [
                'email' => ['required', Rule::unique('users')],
            ];
        }

    }

    public function createData()
    {
        $model = Teacher::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            $this->saveResume($model);
            $this->makeItUser($model);
            return $model;
        }
    }

    private function saveResume($model)
    {
        if ($this->file('resume')) {
            $resumeFile = $this->file('resume');
            $resume = Image::make($resumeFile);
            $resume->resize(
                config('image.avatar.width'),
                config('image.avatar.height'), function ($constraint) {
                $constraint->aspectRatio();
            });
            $path = 'uploads/teachers/teachers/' . General::generateFileName($resumeFile);
            $resume->save($path, 100);
            $model->resume = $path;
            $model->save();
        }
    }

    private function makeItUser($model)
    {
        $password = $this->has('password') ? $this->password : 'user1234';
        $teacher = User::create([
            'email' => $this->email,
            'temp_password' => $password,
            'password' => Hash::make($password),
            'name' => $this->name,
            'school_id' => $model->school_id,
            'phone' => $this->phone,
        ]);
        $model->user_id = $teacher->id;
        if ($model->save()) {
            $teacher->roles()->sync(Role::whereName(RoleEnum::ROLE_TEACHER)->first());
            $this->savePhoto($teacher);
        }

    }

    private function savePhoto($model)
    {
        if ($this->file('photo')) {
            $photoFile = $this->file('photo');
            $photo = Image::make($photoFile);
            $photo->resize(
                config('image.avatar.width'),
                config('image.avatar.height'), function ($constraint) {
                $constraint->aspectRatio();
            });
            $path = 'uploads/teachers/' . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->photo = $path;
            $model->save();
        }
    }
}
