<?php

namespace App\Http\Requests;

use App\Models\IncomeHead;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class IncomeHeadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id) {
            return [
                'title' => ['title', Rule::unique('income_heads')->ignore($this->id)]
            ];
        } else {
            return [
                'title' => [Rule::unique('income_heads')],
            ];
        }

    }

    public function createData()
    {
        $model = IncomeHead::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            return $model;
        }
    }
}
