<?php

namespace App\Http\Requests;

use App\Models\Discount;
use App\Models\PayrollSalaryGrade;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;

class HrmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function createData()
    {

    }
}
