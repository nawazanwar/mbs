<?php

namespace App\Http\Requests\Configuration;

use App\Enum\TableEnum;
use App\Models\AcademicYear;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AcademicYearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_session' => [Rule::unique(TableEnum::ACADEMIC_YEAR)->where('school_id',$this->school_id)],
            'end_session' => [Rule::unique(TableEnum::ACADEMIC_YEAR)->where('school_id',$this->school_id)],
        ];
    }

    public function createData()
    {
        $model = AcademicYear::create($this->all());
        if ($model) {
            $model->save();
            return $model;
        }
    }
}
