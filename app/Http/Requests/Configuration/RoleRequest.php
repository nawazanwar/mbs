<?php

namespace App\Http\Requests\Configuration;

use App\Enum\TableEnum;
use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [Rule::unique(TableEnum::ROLES)],
            'label' => [Rule::unique(TableEnum::ROLES)],
        ];
    }
    public function createData()
    {
        $model = Role::create($this->all());
        if ($model) {
            $model->save();
            return $model;
        }
    }

}
