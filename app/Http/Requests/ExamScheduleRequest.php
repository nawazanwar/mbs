<?php

namespace App\Http\Requests;

use App\Models\ExamSchedule;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;

class ExamScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    public function createData()
    {
        $model = ExamSchedule::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            return $model;
        }
    }
}
