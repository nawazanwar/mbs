<?php

namespace App\Http\Requests;

use App\Models\Income;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;

class IncomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function createData()
    {
        $model = Income::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            return $model;
        }

    }
}
