<?php

namespace App\Http\Requests;

use App\Enum\ImagePathEnum;
use App\Models\AssignmentSubmission;
use App\Services\SchoolService;
use App\Traits\General;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\Facades\Image;

class AssignmentSubmissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function createData()
    {
        $model = AssignmentSubmission::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            $this->saveFile($model);
            return $model;
        }
    }

    private function saveFile($model)
    {
        if ($this->file('submission')) {
            $photoFile = $this->file('submission');
            $photo = Image::make($photoFile);
            $path = ImagePathEnum::ASSIGNMENT . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->submission = $path;
            $model->save();
        }
    }
}
