<?php

namespace App\Http\Requests;

use App\Enum\RoleEnum;
use App\Models\Employee;
use App\Models\Role;
use App\Models\User;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use App\Traits\General;
use Intervention\Image\Facades\Image;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id) {
            return [
                'email' => ['required', 'email', Rule::unique('users')->ignore($this->id)]
            ];
        } else {
            return [
                'email' => ['required', Rule::unique('users')],
            ];
        }

    }

    public function createData()
    {
        $model = Employee::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            $this->savePhoto($model);
            $this->makeItUser($model);
            return $model;
        }
    }

    public function updateData()
    {
    }

    private function savePhoto($model)
    {
        if ($this->file('photo')) {
            $photoFile = $this->file('photo');
            $photo = Image::make($photoFile);
            $photo->resize(
                config('image.avatar.width'),
                config('image.avatar.height'), function ($constraint) {
                $constraint->aspectRatio();
            });
            $path = 'uploads/employee' . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->photo = $path;
            $model->save();
        }

        if ($this->file('resume')) {
            $resumeFile = $this->file('resume');
            $resume = Image::make($resumeFile);
            $resume->resize(
                config('image.avatar.width'),
                config('image.avatar.height'), function ($constraint) {
                $constraint->aspectRatio();
            });
            $path = 'uploads/employee/resume' . General::generateFileName($resumeFile);
            $resume->save($path, 100);
            $model->resume = $path;
            $model->save();
        }
    }

    private function makeItUser($model)
    {
        $user = User::create([
            'email' => $model->email,
            'temp_password' => $model->password,
            'password' => Hash::make($model->password),
            'name' => $model->name,
            'school_id' => $model->school_id,
            'avatar' => $model->photo
        ]);

        $user->roles()->sync(Role::whereLabel(RoleEnum::ROLE_EMPLOYEE)->first());
    }
}
