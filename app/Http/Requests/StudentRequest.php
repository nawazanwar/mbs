<?php

namespace App\Http\Requests;

use App\Enum\RoleEnum;
use App\Models\Guardian;
use App\Models\Student;
use Illuminate\Foundation\Http\FormRequest;
use App\Traits\General;
use Intervention\Image\Facades\Image;
use Illuminate\Validation\Rule;
use App\Models\Role;
use App\Models\Teacher;
use App\Models\User;
use App\Services\SchoolService;
use Illuminate\Support\Facades\Hash;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->id) {
            return [
                'email' => ['email', Rule::unique('users')->ignore($this->id)]
            ];
        } else {
            return [
                'email' => [Rule::unique('users')],
            ];
        }

    }

    public function createData()
    {
        $model = Student::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            $this->saveTransferCertificate($model);
            $this->saveFatherImage($model);
            $this->saveMotherImage($model);
            $this->makeItUser($model);
            $this->createGuardian($model);
            return $model;
        }
    }

    private function saveTransferCertificate($model)
    {
        if ($this->file('transfer_certificate')) {
            $resumeFile = $this->file('transfer_certificate');
            $resume = Image::make($resumeFile);
            $path = 'uploads/students/transfer_certificates/' . General::generateFileName($resumeFile);
            $resume->save($path, 100);
            $model->transfer_certificate = $path;
            $model->save();
        }
    }

    private function saveFatherImage($model)
    {
        if ($this->file('father_photo')) {
            $file = $this->file('father_photo');
            $resume = Image::make($file);
            $path = 'uploads/students/fathers/' . General::generateFileName($file);
            $resume->save($path, 100);
            $model->father_photo = $path;
            $model->save();
        }
    }

    private function saveMotherImage($model)
    {
        if ($this->file('mother_photo')) {
            $file = $this->file('mother_photo');
            $resume = Image::make($file);
            $path = 'uploads/students/mothers/' . General::generateFileName($file);
            $resume->save($path, 100);
            $model->mother_photo = $path;
            $model->save();
        }
    }

    private function makeItUser($model)
    {
        $password = $this->has('password') ? $this->password : 'user1234';
        $student = User::create([
            'email' => $this->email,
            'temp_password' => $password,
            'password' => Hash::make($password),
            'name' => $this->name,
            'school_id' => $model->school_id,
            'phone' => $this->phone,
        ]);
        $model->user_id = $student->id;
        if ($model->save()) {
            $student->roles()->sync(Role::whereName(RoleEnum::ROLE_STUDENT)->first());
            $this->savePhoto($student);
        }

    }

    private function savePhoto($model)
    {
        if ($this->file('photo')) {
            $photoFile = $this->file('photo');
            $photo = Image::make($photoFile);
            $photo->resize(
                config('image.avatar.width'),
                config('image.avatar.height'), function ($constraint) {
                $constraint->aspectRatio();
            });
            $path = 'uploads/students/' . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->photo = $path;
            $model->save();
        }
    }

    private function createGuardian($model)
    {
        $data = $this->input('guardian', array());
        $guardian = Guardian::create($data);
        $guardian->school_id = SchoolService::getSchoolId();
        if ($guardian->save()) {
            $model->guardian_id = $guardian->id;
            if ($model->save()) {
                $this->makeGuardianUser($guardian, $data);
            }
        }
    }

    private function makeGuardianUser($model, $data)
    {
        $password = $data['password'];
        $user = User::create([
            'email' => $data['email'],
            'temp_password' => $password,
            'password' => Hash::make($password),
            'name' => $data['name'],
            'school_id' => $model->school_id,
            'phone' => $data['phone'],
        ]);
        $user->school_id = SchoolService::getSchoolId();
        if($user->save()){
            $model->user_id = $user->id;
            if($model->save()){
                $user->roles()->sync(Role::whereName(RoleEnum::ROLE_GUARDIAN)->first());
                $this->saveGuardianPhoto($user);
            }
        }
    }

    private function saveGuardianPhoto($model)
    {
        if ($this->file('guardian_photo')) {
            $photoFile = $this->file('guardian_photo');
            $photo = Image::make($photoFile);
            $photo->resize(
                config('image.avatar.width'),
                config('image.avatar.height'), function ($constraint) {
                $constraint->aspectRatio();
            });
            $path = 'uploads/guardians/' . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->photo = $path;
            $model->save();
        }
    }
}
