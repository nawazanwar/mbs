<?php

namespace App\Http\Requests\Academic;

use App\Models\Syllabus;
use App\Services\PermissionService;
use App\Services\ClassService;
use App\Services\SchoolService;
use App\Services\SubjectService;
use App\Services\TeacherService;
use App\Traits\General;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\Facades\Image;

class SyllabusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    public function createData()
    {
        $model = Syllabus::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
//            $this->savePhoto($model);
            $model->save();
            return $model;
        }
    }
    private function savePhoto($model)
    {
        if ($this->file('assignment')) {
            $photoFile = $this->file('assignment');
            $photo = Image::make($photoFile);
            $path = 'uploads/syllabus/' . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->assignment = $path;
            $model->save();
        }
    }
}
