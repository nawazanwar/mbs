<?php

namespace App\Http\Requests;

use App\Models\PostalReceive;
use App\Services\SchoolService;
use Illuminate\Foundation\Http\FormRequest;
use App\Traits\General;
use Intervention\Image\Facades\Image;

class PostalRecieveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
    public function createData()
    {
        $model = PostalReceive::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $this->savePhoto($model);
            $model->save();
            return $model;
        }
    }
    private function savePhoto($model)
    {
        if ($this->file('attachment')) {
            $photoFile = $this->file('attachment');
            $photo = Image::make($photoFile);
            $path = 'uploads/postalrecieve/' . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->attachment = $path;
            $model->save();
        }
    }
}
