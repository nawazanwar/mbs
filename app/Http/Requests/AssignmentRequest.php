<?php

namespace App\Http\Requests;

use App\Enum\ImagePathEnum;
use App\Models\Assignment;
use App\Services\SchoolService;
use App\Traits\General;
use Illuminate\Foundation\Http\FormRequest;
use Intervention\Image\Facades\Image;

class AssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function createData()
    {
        $model = Assignment::create($this->all());
        if ($model) {
            $model->school_id = SchoolService::getSchoolId();
            $model->save();
            $this->saveFile($model);
            return $model;
        }
    }


    private function saveFile($model)
    {
        if ($this->file('assignment')) {
            $photoFile = $this->file('assignment');
            $photo = Image::make($photoFile);
            $path = ImagePathEnum::ASSIGNMENT . General::generateFileName($photoFile);
            $photo->save($path, 100);
            $model->assignment = $path;
            $model->save();
        }
    }

}
