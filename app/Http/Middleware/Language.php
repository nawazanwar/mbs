<?php

namespace App\Http\Middleware;

use App\Enum\LanguageEnum;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (
            Session()->has('app_locale') AND
            array_key_exists(Session()->get('app_locale'),LanguageEnum::getTranslationKeys())
        ) {
            App::setLocale(Session()->get('app_locale'));
        }
        else {
            App::setLocale(config('app.fallback_locale'));
        }
        return $next($request);
    }
}
