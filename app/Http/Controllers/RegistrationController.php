<?php

namespace App\Http\Controllers;

use App\Http\Requests\SchoolRequest;
use App\Models\School;
use App\Services\SchoolService;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;

class RegistrationController extends Controller
{
    private SchoolService $schoolService;
    use General;

    public function __construct(SchoolService $schoolService)
    {
        $this->makeMultipleDirectories('school', ['logo']);
        $this->schoolService = $schoolService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', School::class);
        return view('registration.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(SchoolRequest $request)
    {
        $this->authorize('create', School::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('home')->with('success', __('school.record_created_successfully'));
        }
    }
}

