<?php

namespace App\Http\Controllers;

use App\Enum\LanguageEnum;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function switchLang($lang): RedirectResponse
    {
        $languages = LanguageEnum::getTranslationKeys();
        if (array_key_exists($lang,$languages)) {
            Session::put('app_locale', $lang);
        }
        return Redirect::back();
    }
}
