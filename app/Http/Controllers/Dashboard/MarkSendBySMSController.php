<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Message;
use App\Models\MessageEmail;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MarkSendBySMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return \view('dashboard.exams.send-mark.by-sms.index');
    }
    public function create()
    {
        return \view('dashboard.exams.send-mark.by-sms.create');

    }

}
