<?php

namespace App\Http\Controllers\Dashboard;

use  App\Http\Controllers\Controller;
use App\Models\OnlineAdmission;
use App\Models\Student;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OnlineAdmissionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {

        $this->authorize('view',OnlineAdmission::class);
        return view('dashboard.admissions.online.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',OnlineAdmission::class);
        return view('dashboard.admissions.online.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create',OnlineAdmission::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',OnlineAdmission::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',OnlineAdmission::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',OnlineAdmission::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',OnlineAdmission::class);
    }
}
