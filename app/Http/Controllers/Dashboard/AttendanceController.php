<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Attendance;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Attendance::class);
        $type = $request->query('type');
        if ($type) {
            return view(sprintf("%s.%s.%s.index", 'dashboard.hrm', 'attendances', $type));
        } else {
            return \view('dashboard.hrm.attendances.index');
        }

    }


    /**
     * @throws AuthorizationException
     */
    public function create(): Response
    {
        $this->authorize('create', Attendance::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', Attendance::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Attendance::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Attendance::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Attendance::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Attendance::class);
    }
}
