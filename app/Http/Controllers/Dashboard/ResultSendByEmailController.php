<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResultSendByEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return \view('dashboard.exams.send-result.by-email.index');
    }
    public function create()
    {
        return \view('dashboard.exams.send-result.by-email.create');

    }
}
