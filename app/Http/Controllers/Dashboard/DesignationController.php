<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\DesignationRequest;
use App\Models\Designation;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DesignationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Designation::class);
        $data = Designation::with('school', 'createdBy', 'updatedBy');
        // if ($request->query('s')) {
        //     $data = $this->studentService->applyGlobalSearchForStudentType($request, $data);
        // }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.hrm.designations.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Designation::class);
        return view('dashboard.hrm.designations.create');

    }


    /**
     * @throws AuthorizationException
     */
    public function store(DesignationRequest $request)
    {
        $this->authorize('create', Designation::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.designations.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Designation::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Designation::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Designation::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Designation::class);
    }
}
