<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    use General;
    public function __construct()
    {
        $this->makeDirectory('employee');
        $this->makeDirectory('employee/resume');
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Employee::class);
        $data = Employee::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->studentService->applyGlobalSearchForStudent($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.hrm.employees.index',compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Employee::class);
        return view('dashboard.hrm.employees.create');

    }

    /**
     * @throws AuthorizationException
     */
    public function store(EmployeeRequest $request)
    {
        $this->authorize('create', Employee::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.employees.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view', Employee::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update', Employee::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Employee::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Employee::class);

    }
}
