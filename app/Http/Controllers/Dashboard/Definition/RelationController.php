<?php

namespace App\Http\Controllers\Dashboard\Definition;

use App\Http\Controllers\Controller;
use App\Models\Definition;
use App\Services\DefinitionService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class RelationController extends Controller
{

    private DefinitionService $definitionService;

    public function __construct(
        DefinitionService $definitionService
    )
    {
        $this->definitionService=$definitionService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Definition::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Definition::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create',Definition::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize('edit',Definition::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $this->authorize('edit',Definition::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy($id)
    {
        $this->authorize('delete',Definition::class);
    }
}
