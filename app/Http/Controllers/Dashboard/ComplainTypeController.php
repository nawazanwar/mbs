<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\ComplainType;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ComplainTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', ComplainType::class);
        return  view('dashboard.complains.types.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', ComplainType::class);
        return  view('dashboard.complains.types.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', ComplainType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', ComplainType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', ComplainType::class);
    }

    
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', ComplainType::class);
    }

    
    public function destroy(int $id): Response
    {
        $this->authorize('delete', ComplainType::class);
    }
}
