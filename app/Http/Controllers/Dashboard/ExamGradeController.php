<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamGradeRequest;
use App\Models\ExamGrade;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExamGradeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', ExamGrade::class);
        return view('dashboard.exams.grades.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', ExamGrade::class);
        return view('dashboard.exams.grades.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(ExamGradeRequest $request)
    {
        $this->authorize('create', ExamGrade::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.exam.grades.create')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', ExamGrade::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update', ExamGrade::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', ExamGrade::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', ExamGrade::class);

    }
}
