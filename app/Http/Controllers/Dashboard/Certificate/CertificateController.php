<?php

namespace App\Http\Controllers\Dashboard\Certificate;

use App\Http\Controllers\Controller;
use App\Models\Certificate;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CertificateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()

    {
        $this->authorize('view', Certificate::class);
        return view('dashboard.certificates.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Certificate::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', Certificate::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Certificate::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Certificate::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Certificate::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Certificate::class);
    }
}
