<?php

namespace App\Http\Controllers\Dashboard\Certificate;

use App\Http\Controllers\Controller;
use App\Models\CertificateType;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CertificateTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', CertificateType::class);
        return view('dashboard.certificates.types.index');

    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', CertificateType::class);
        return view('dashboard.certificates.types.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', CertificateType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view', CertificateType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', CertificateType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', CertificateType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Certificate::class);
    }
}
