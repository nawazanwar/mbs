<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Receipt;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ReceiptController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $this->authorize('view',Receipt::class);

        return view('dashboard.receipts.index');
    }
    public function create(): Response
    {
        $this->authorize('create',Receipt::class);

    }
    public function store(Request $request): Response
    {
        $this->authorize('create',Receipt::class);

    }
    public function show(int $id): Response
    {
        $this->authorize('view',Receipt::class);

    }
    public function edit(int $id): Response
    {
        $this->authorize('update',Receipt::class);
    }
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',Receipt::class);

    }
    public function destroy(int $id): Response
    {
        $this->authorize('delete',Receipt::class);

    }
}
