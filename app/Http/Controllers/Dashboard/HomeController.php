<?php

namespace App\Http\Controllers\Dashboard;

use App\Enum\RoleEnum;
use App\Http\Controllers\Controller;
use App\Services\LectureService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    private LectureService $userService;

    public function __construct(
        LectureService $userService
    )
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $sId = $request->query('sId');
        if ($sId) {
            session()->forget('sId');
            session()->put('sId', $sId);
           return redirect()->route('dashboard.index');
        }
        return view('dashboard.index');
    }
}
