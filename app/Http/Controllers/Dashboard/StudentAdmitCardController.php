<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\AdmitCardSetting;
use App\Models\EmployeeIdCard;
use App\Models\ExamAttendance;

use App\Models\IdCardSetting;
use App\Models\StudentAdmitCard;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentAdmitCardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', StudentAdmitCard::class);
        return view('dashboard.cards.student-admit-cards.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', StudentAdmitCard::class);
        return view('dashboard.cards.student-admit-cards.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', StudentAdmitCard::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', StudentAdmitCard::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', StudentAdmitCard::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', StudentAdmitCard::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', StudentAdmitCard::class);
    }
}
