<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ResultSendBySMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return \view('dashboard.exams.send-result.by-sms.index');
    }
    public function create()
    {
        return \view('dashboard.exams.send-result.by-sms.create');
    }
}
