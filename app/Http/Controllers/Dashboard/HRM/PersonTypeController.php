<?php

namespace App\Http\Controllers\Dashboard\HRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlockRequest;
use App\Http\Requests\UpdateBlockRequest;
use App\Models\Block;
use App\Models\Cast;
use App\Models\GeneralRelation;
use App\Models\Hrm;
use App\Models\HrmCardType;
use App\Models\HrmDepartment;
use App\Models\HRMPersonType;
use App\Models\Phase;
use App\Models\PropertyType;
use App\Repositories\CurdRepository;
use App\Services\HrmService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

class PersonTypeController extends Controller
{

    private HrmService $hrmService;

    public function __construct(
        HrmService $hrmService
    )
    {
        $this->hrmService=$hrmService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Hrm::class);
        return view('dashboard.hrm.person-types.index');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $pageTitle = 'Create New Person Type';
        $viewParams = [
            'pageTitle' => $pageTitle
        ];
        return view('dashboard.hrm.person-types.create', $viewParams);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|unique:h_r_m_person_types,name'
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model = $this->model->create($data);
        if ($this->model) {
            $this->model->slug = Str::slug($this->model->name, '-');
            $this->model->save();
            return redirect()
                ->route('hrm-person-types.index')
                ->with('successMessage', "Person Type is Created successfully");
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $this->model = $this->model->find($id);
        $pageTitle = __('hrm.edit') . " " . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model
        ];
        return view('hrm.person-types.edit', $viewParams);

    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'name' => "required|unique:h_r_m_person_types,id," . $id
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model->update($data, $id);
        $this->model = $this->model->find($id);
        $this->model->save();
        if ($this->model) {
            $this->model->slug = Str::slug($this->model->title, '-');
            $this->model->save();
            return redirect()->route('hrm-person-types.index')->with('successMessage', $this->model->name . " " . __('hrm.updated_success_message'));
        }
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('hrm-person-types.index')->with('errorMessage', $this->model->name . " deleted Successfully");
        }
    }
}
