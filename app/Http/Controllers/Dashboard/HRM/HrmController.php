<?php

namespace App\Http\Controllers\Dashboard\HRM;

use App\Http\Controllers\Controller;
use App\Models\Hrm;
use App\Services\HrmService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class HrmController extends Controller
{
    private HrmService $hrmService;

    public function __construct(
        HrmService $hrmService
    )
    {
        $this->hrmService=$hrmService;
        $this->middleware('auth');
    }


    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',Hrm::class);
        return view('dashboard.hrm.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Hrm::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create',Hrm::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show($id)
    {
        $this->authorize('view', Hrm::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize('update', Hrm::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', Hrm::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy($id)
    {
        $this->authorize('delete', Hrm::class);
    }
}
