<?php

namespace App\Http\Controllers\Dashboard\HRM;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlockRequest;
use App\Models\Hrm;
use App\Models\HRMBusinessSubType;
use App\Models\HRMBusinessType;
use App\Repositories\CurdRepository;
use App\Services\HrmService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Throwable;

class BusinessSubTypeController extends Controller
{

    private HrmService $hrmService;

    public function __construct(
        HrmService $hrmService
    )
    {
        $this->hrmService=$hrmService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Hrm::class);
        return view('dashboard.hrm.business-sub-types.index');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $viewParams = [
            'pageTitle' => 'Create New Business Sub Type'
        ];
        return view('dashboard.hrm.business-sub-types.create', $viewParams);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|unique:h_r_m_business_sub_types,name'
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model = $this->model->create($data);
        if ($this->model) {
            return redirect()
                ->route('hrm-business-sub-types.index')
                ->with('successMessage', "Business Sub Type is Created successfully");
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $this->model = $this->model->find($id);
        $pageTitle = __('hrm.edit') . " " . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model
        ];
        return view('hrm.business-sub-types.edit', $viewParams);

    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'name' => "required|unique:h_r_m_business_sub_types,id," . $id
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model->update($data, $id);
        $this->model = $this->model->find($id);
        $this->model->save();
        if ($this->model) {
            return redirect()->route('hrm-business-sub-types.index')->with('successMessage', $this->model->name . " " . __('hrm.updated_success_message'));
        }
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('hrm-business-sub-types.index')->with('errorMessage', $this->model->name . " deleted Successfully");
        }
    }
}
