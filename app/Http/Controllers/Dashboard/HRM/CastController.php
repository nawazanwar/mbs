<?php

namespace App\Http\Controllers\Dashboard\HRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlockRequest;
use App\Http\Requests\UpdateBlockRequest;
use App\Models\Block;
use App\Models\Cast;
use App\Models\GeneralRelation;
use App\Models\Hrm;
use App\Models\Phase;
use App\Models\PropertyType;
use App\Repositories\CurdRepository;
use App\Services\HrmService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

class CastController extends Controller
{

    private HrmService $hrmService;

    public function __construct(
        HrmService $hrmService
    )
    {
        $this->hrmService=$hrmService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Hrm::class);
        return view('dashboard.hrm.casts.index');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $pageTitle = 'Create New Cast';
        $viewParams = [
            'pageTitle' => $pageTitle
        ];
        return view('dashboard.hrm.casts.create', $viewParams);
    }

    /**
     * @param StoreBlockRequest $request
     * @return RedirectResponse
     */
    public function store(StoreBlockRequest $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|unique:casts,name'
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model = $this->model->create($data);
        if ($this->model) {
            return redirect()
                ->route('hrm-casts.index')
                ->with('successMessage', "Cast is Created successfully");
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $this->model = $this->model->find($id);
        $pageTitle = __('hrm.edit') . " " . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model
        ];
        return view('hrm.casts.edit', $viewParams);

    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'name' => "required|unique:casts,id," . $id
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model->update($data, $id);
        $this->model = $this->model->find($id);
        $this->model->save();
        if ($this->model) {
            return redirect()->route('hrm-casts.index')->with('successMessage', $this->model->name . " " . __('hrm.updated_success_message'));
        }
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('hrm-casts.index')->with('errorMessage', $this->model->name . " deleted Successfully");
        }
    }
}
