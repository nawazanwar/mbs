<?php

namespace App\Http\Controllers\Dashboard\HRM;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBlockRequest;
use App\Http\Requests\UpdateBlockRequest;
use App\Models\Block;
use App\Models\Cast;
use App\Models\GeneralRelation;
use App\Models\Hrm;
use App\Models\HrmDepartment;
use App\Models\HrmEmployee;
use App\Models\HrmEmployer;
use App\Models\Phase;
use App\Models\PropertyType;
use App\Repositories\CurdRepository;
use App\Services\HrmService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Throwable;

class EmployeeSubTypeController extends Controller
{

    private HrmService $hrmService;

    public function __construct(
        HrmService $hrmService
    )
    {
        $this->hrmService=$hrmService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Hrm::class);
        return view('dashboard.hrm.employee-sub-types.index');
    }

    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $viewParams = [
            'pageTitle' => 'Create New Employee Type'
        ];
        return view('dashboard.hrm.employee-sub-types.create', $viewParams);
    }

    /**
     * @param StoreBlockRequest $request
     * @return RedirectResponse
     */
    public function store(StoreBlockRequest $request): RedirectResponse
    {
        $request->validate([
            'name' => 'required|unique:hrm_employees,name'
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model = $this->model->create($data);
        if ($this->model) {
            return redirect()
                ->route('hrm-emp-sub-types.index')
                ->with('successMessage', "Employee Sub Type is Created successfully");
        }
    }

    /**
     * @param $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        $this->model = $this->model->find($id);
        $pageTitle = __('hrm.edit') . " " . $this->model->name;
        $viewParams = [
            'pageTitle' => $pageTitle,
            'model' => $this->model
        ];
        return view('hrm.employee-sub-types.edit', $viewParams);

    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'name' => "required|unique:hrm_employees,id," . $id
        ]);
        $data = $request->only($this->model->getModel()->fillable);
        $this->model->update($data, $id);
        $this->model = $this->model->find($id);
        $this->model->save();
        if ($this->model) {
            return redirect()->route('hrm-emp-sub-types.index')->with('successMessage', $this->model->name . " " . __('hrm.updated_success_message'));
        }
    }

    /**
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $this->model = $this->model->find($id);
        if ($this->model->delete($id)) {
            return redirect()->route('hrm-emp-sub-types.index')->with('errorMessage', $this->model->name . " deleted Successfully");
        }
    }
}
