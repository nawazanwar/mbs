<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\DueFeeEmail;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DueFeeEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',DueFeeEmail::class);
        return view('dashboard.finance.accounting.dues.fee.email.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', DueFeeEmail::class);
        return view('dashboard.finance.accounting.dues.fee.email.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', DueFeeEmail::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', DueFeeEmail::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update', DueFeeEmail::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', DueFeeEmail::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete', DueFeeEmail::class);
    }
}
