<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamScheduleRequest;
use App\Models\ExamSchedule;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExamScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
 /**
  * @throws AuthorizationException
  */
 public function index()
 {
  $this->authorize('view',ExamSchedule::class);

  return view('dashboard.exams.schedules.index');
 }

 /**
  * @throws AuthorizationException
  */
 public function create()
 {
  $this->authorize('create',ExamSchedule::class);
  return view('dashboard.exams.schedules.create');
 }

 /**
  * @throws AuthorizationException
  */
 public function store(ExamScheduleRequest $request)
 {
  $this->authorize('create',ExamSchedule::class);
  $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.exam.schedules.create')->with('success', __('school.record_created_successfully'));
        }

 }

 /**
  * @throws AuthorizationException
  */
 public function show(int $id): Response
 {
  $this->authorize('view',ExamSchedule::class);

 }

 /**
  * @throws AuthorizationException
  */
 public function edit(int $id)
 {
  $this->authorize('update',ExamSchedule::class);

 }

 /**
  * @throws AuthorizationException
  */
 public function update(Request $request, int $id): Response
 {
  $this->authorize('update',ExamSchedule::class);

 }

 /**
  * @throws AuthorizationException
  */
 public function destroy(int $id): Response
 {
  $this->authorize('delete',ExamSchedule::class);

 }
}
