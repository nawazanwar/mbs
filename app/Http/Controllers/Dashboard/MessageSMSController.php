<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\MessageEmail;

use App\Models\MessageSms;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MessageSMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', MessageSms::class);
        return \view('dashboard.messages.sms.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', MessageSms::class);
        return \view('dashboard.messages.sms.create');

    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', MessageSms::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', MessageSms::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', MessageSms::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', MessageSms::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', MessageSms::class);
    }
}
