<?php

namespace App\Http\Controllers\Dashboard\Finance    ;

use App\Http\Controllers\Controller;
use App\Http\Requests\IncomeRequest;
use App\Models\Income;

use App\Services\IncomeService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IncomeController extends Controller
{
    private IncomeService $incomeService;

    public function __construct(
        IncomeService $incomeService
    )
    {
        $this->incomeService=$incomeService;
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Income::class);

        $data = Income::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->incomeService->applyGlobalSearchForIncome($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.finance.accounting.incomes.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Income::class);
        return view('dashboard.finance.accounting.incomes.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(IncomeRequest $request)
    {
        $this->authorize('create', Income::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.incomes.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',Income::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',Income::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',Income::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',Income::class);
    }
}
