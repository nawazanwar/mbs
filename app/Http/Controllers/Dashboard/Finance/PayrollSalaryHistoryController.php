<?php

namespace App\Http\Controllers\Dashboard\Finance;

use App\Http\Controllers\Controller;
use App\Models\PayrollSalaryHistory;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PayrollSalaryHistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',PayrollSalaryHistory::class);
        return view('dashboard.finance.payroll.salary-history.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',PayrollSalaryHistory::class);
        return view('dashboard.finance.payroll.salary-history.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create',PayrollSalaryHistory::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',PayrollSalaryHistory::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',PayrollSalaryHistory::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',PayrollSalaryHistory::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',PayrollSalaryHistory::class);
    }
}
