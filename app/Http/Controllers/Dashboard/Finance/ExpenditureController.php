<?php

namespace App\Http\Controllers\Dashboard\Finance;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExpenditureRequest;
use App\Models\Expenditure;
use App\Services\ExpenditureService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExpenditureController extends Controller
{
    private ExpenditureService $expenditureService;

    public function __construct(
        ExpenditureService $expenditureService
    )
    {
        $this->expenditureService=$expenditureService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Expenditure::class);

        $data = Expenditure::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->expenditureService->applyGlobalSearchForExpenditure($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.finance.accounting.expenditures.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Expenditure::class);
        return view('dashboard.finance.accounting.expenditures.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(ExpenditureRequest $request)
    {
        $this->authorize('create', Expenditure::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.expenditures.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Expenditure::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update', Expenditure::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Expenditure::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete', Expenditure::class);
    }
}
