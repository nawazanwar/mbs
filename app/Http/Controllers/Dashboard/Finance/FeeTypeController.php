<?php

namespace App\Http\Controllers\Dashboard\Finance;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeeTypeRequest;
use App\Models\FeeType;

use App\Services\FeeService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FeeTypeController extends Controller
{
    private FeeService $feeTypeService;

    public function __construct(
        FeeService $feeTypeService
    )
    {
        $this->feeTypeService=$feeTypeService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',FeeType::class);
        $data = FeeType::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->feeTypeService->applyGlobalSearchForFeeType($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.finance.accounting.fee.types.index', compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',FeeType::class);
        return view('dashboard.finance.accounting.fee.types.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(FeeTypeRequest $request)
    {
        $this->authorize('create', FeeType::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.fee.types.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',FeeType::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',FeeType::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',FeeType::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',FeeType::class);

    }
}
