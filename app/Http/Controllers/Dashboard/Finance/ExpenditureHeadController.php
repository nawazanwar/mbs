<?php

namespace App\Http\Controllers\Dashboard\Finance;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExpenditureHeadRequest;
use App\Models\ExpenditureHead;

use App\Services\ExpenditureService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExpenditureHeadController extends Controller
{
    private ExpenditureService $expenditureService;

    public function __construct(
        ExpenditureService $expenditureService
    )
    {
        $this->expenditureService = $expenditureService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',ExpenditureHead::class);
        $data = ExpenditureHead::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->expenditureService->applyGlobalSearchForExpenditureHead($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.finance.accounting.expenditures.heads.index', compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',ExpenditureHead::class);
        return view('dashboard.finance.accounting.expenditures.heads.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(ExpenditureHeadRequest $request)
    {
        $this->authorize('create', ExpenditureHead::class);
        $model = $request->createData();
        if ($model) {
            return redirect()
                ->route('dashboard.expenditure.heads.index')
                ->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',ExpenditureHead::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',ExpenditureHead::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',ExpenditureHead::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',ExpenditureHead::class);

    }
}
