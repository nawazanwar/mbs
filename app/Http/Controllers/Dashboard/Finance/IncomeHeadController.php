<?php

namespace App\Http\Controllers\Dashboard\Finance;

use App\Http\Controllers\Controller;
use App\Http\Requests\IncomeHeadRequest;
use App\Models\IncomeHead;

use App\Services\IncomeService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IncomeHeadController extends Controller
{
    private IncomeService $incomeService;

    public function __construct(
        IncomeService $incomeService
    )
    {
        $this->incomeService = $incomeService;
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',IncomeHead::class);

        $data = IncomeHead::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->incomeService->applyGlobalSearchForIncomeHead($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.finance.accounting.incomes.heads.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',IncomeHead::class);
        return view('dashboard.finance.accounting.incomes.heads.create');

    }

    /**
     * @throws AuthorizationException
     */
    public function store(IncomeHeadRequest $request)
    {
        $this->authorize('create', IncomeHead::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.income.heads.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',IncomeHead::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',IncomeHead::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update',IncomeHead::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',IncomeHead::class);
    }
}
