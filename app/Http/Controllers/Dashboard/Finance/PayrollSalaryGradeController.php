<?php

namespace App\Http\Controllers\Dashboard\Finance;

use App\Http\Controllers\Controller;
use App\Http\Requests\LibraryBookRequest;
use App\Models\PayrollSalaryGrade;
use App\Services\SalaryService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PayrollSalaryGradeController extends Controller
{
    private SalaryService $salaryService;

    public function __construct(
        SalaryService $salaryService
    )
    {
        $this->salaryService=$salaryService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',PayrollSalaryGrade::class);

        $data = PayrollSalaryGrade::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->salaryService->applyGlobalSearchForSalaryGrade($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.finance.payroll.salary-grades.index', compact('data'));
    }

    /**
     */
    public function create()
    {
        // $this->authorize('create',PayrollSalaryGrade::class);
        return view('dashboard.finance.payroll.salary-grades.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(LibraryBookRequest $request)
    {
        $this->authorize('create', PayrollSalaryGrade::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.salary.grades.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',PayrollSalaryGrade::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',PayrollSalaryGrade::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',PayrollSalaryGrade::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',PayrollSalaryGrade::class);
    }
}
