<?php

namespace App\Http\Controllers\Dashboard\Finance;

use App\Http\Controllers\Controller;
use App\Http\Requests\FeeTypeRequest;
use App\Models\Discount;
use App\Services\DiscountService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DiscountController extends Controller
{
    private DiscountService $discountService;

    public function __construct(
        DiscountService $discountService
    )
    {
        $this->discountService=$discountService;
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Discount::class);

        $data = Discount::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->discountService->applyGlobalSearchForDiscount($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.finance.accounting.discounts.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Discount::class);
        return view('dashboard.finance.accounting.discounts.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(FeeTypeRequest $request)
    {
        $this->authorize('create', Discount::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.discounts.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',Discount::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update',Discount::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',Discount::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete',Discount::class);
    }
}
