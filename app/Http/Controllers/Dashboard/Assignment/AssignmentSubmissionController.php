<?php

namespace App\Http\Controllers\Dashboard\Assignment;

use App\Http\Controllers\Controller;
use App\Http\Requests\AssignmentSubmissionRequest;
use App\Models\AssignmentSubmission;
use App\Services\AssignmentService;
use App\Services\SchoolService;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AssignmentSubmissionController extends Controller
{
    private AssignmentService $assignmentService;
    use General;
    public function __construct(
        AssignmentService $assignmentService
    )
    {
        $this->assignmentService=$assignmentService;
        $this->middleware('auth');
        $this->makeDirectory('assignments');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', AssignmentSubmission::class);
        $data = AssignmentSubmission::with('school', 'createdBy', 'updatedBy')
            ->whereSchoolId(SchoolService::getSchoolId());
        if ($request->query('s')) {
            $data = $this->assignmentService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return \view('dashboard.assignment.submission.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', AssignmentSubmission::class);
        return \view('dashboard.assignment.submission.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(AssignmentSubmissionRequest $request)
    {
        $this->authorize('create', AssignmentSubmission::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.assignment.submissions.index')->with('success', __('school.record_created_successfully'));
        }

    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', AssignmentSubmission::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', AssignmentSubmission::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', AssignmentSubmission::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', AssignmentSubmission::class);
    }
}
