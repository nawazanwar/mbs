<?php

namespace App\Http\Controllers\Dashboard\Configuration;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeaveTypesRequest;
use App\Models\LeaveType;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LeaveTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', LeaveType::class);
        $data = LeaveType::with('school', 'createdBy', 'updatedBy');
        // if ($request->query('s')) {
        //     $data = $this->studentService->applyGlobalSearchForStudentType($request, $data);
        // }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.configuration.leaves.types.index',compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', LeaveType::class);
        return view('dashboard.configuration.leaves.types.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(LeaveTypesRequest $request)
    {
        $this->authorize('create', LeaveType::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.leave.types.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', LeaveType::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', LeaveType::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', LeaveType::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', LeaveType::class);

    }
}
