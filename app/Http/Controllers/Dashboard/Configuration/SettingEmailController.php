<?php

namespace App\Http\Controllers\Dashboard\Configuration;

use App\Http\Controllers\Controller;
use App\Models\ExamAttendance;

use App\Models\SettingEmail;
use App\Models\SettingPayment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SettingEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', SettingEmail::class);
        return view('dashboard.configuration.administrator.settings.emails.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', SettingEmail::class);
        return view('dashboard.configuration.administrator.settings.emails.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', SettingEmail::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', SettingEmail::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', SettingEmail::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', SettingEmail::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', SettingEmail::class);
    }
}
