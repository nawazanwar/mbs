<?php

namespace App\Http\Controllers\Dashboard\Configuration;

use App\Http\Controllers\Controller;
use App\Http\Requests\LeaveApplicationsRequest;
use App\Models\LeaveApplication;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LeaveApplicationController extends Controller
{
    use General;

    public function __construct()
    {
        $this->makeDirectory('leaveapplication');
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', LeaveApplication::class);
        return view('dashboard.configuration.leaves.applications.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', LeaveApplication::class);
        return view('dashboard.configuration.leaves.applications.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(LeaveApplicationsRequest $request): Response
    {
        $this->authorize('create', LeaveApplication::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.leave.applications.create')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', LeaveApplication::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', LeaveApplication::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', LeaveApplication::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', LeaveApplication::class);

    }
}
