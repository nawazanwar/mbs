<?php

namespace App\Http\Controllers\Dashboard\Configuration;

use App\Http\Controllers\Controller;
use App\Http\Requests\TemplateEmailRequest;
use App\Models\AbsentEmail;

use App\Models\TemplateEmail;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TemplateEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', TemplateEmail::class);
        return view('dashboard.configuration.templates.email.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',TemplateEmail::class);
        return view('dashboard.configuration.templates.email.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(TemplateEmailRequest $request)
    {
        $this->authorize('create',TemplateEmail::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.template.emails.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',TemplateEmail::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',TemplateEmail::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',TemplateEmail::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',TemplateEmail::class);
    }
}
