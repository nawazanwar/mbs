<?php

namespace App\Http\Controllers\Dashboard\Configuration;

use App\Http\Controllers\Controller;
use App\Http\Requests\Configuration\ThemeRequest;
use App\Models\Theme;
use App\Services\ThemeService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class ThemeController extends Controller
{
    private ThemeService $themeService;

    public function __construct(ThemeService $themeService)
    {
        $this->middleware('auth');
        $this->themeService = $themeService;
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Theme::class);
        $data = new Theme();
        if ($request->query('s')) {
            $data = $this->themeService->applyGlobalSearch($request,$data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.configuration.themes.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Theme::class);
        return view('dashboard.configuration.themes.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(ThemeRequest $request)
    {
        $this->authorize('create',Theme::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.themes.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view',Theme::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update',Theme::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update',Theme::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete',Theme::class);
    }
}
