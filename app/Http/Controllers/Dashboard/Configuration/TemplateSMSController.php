<?php

namespace App\Http\Controllers\Dashboard\Configuration;

use App\Http\Controllers\Controller;
use App\Http\Requests\TemplateSmsRequest;
use App\Models\TemplateSMS;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TemplateSMSController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', TemplateSMS::class);
        return view('dashboard.configuration.templates.sms.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',TemplateSMS::class);
        return view('dashboard.configuration.templates.sms.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(TemplateSmsRequest $request)
    {
        $this->authorize('create',TemplateSMS::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.template.sms.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',TemplateSMS::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',TemplateSMS::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',TemplateSMS::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',TemplateSMS::class);
    }
}
