<?php

namespace App\Http\Controllers\Dashboard\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentTypeRequest;
use App\Models\StudentType;

use App\Services\SchoolService;
use App\Services\StudentService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentTypeController extends Controller
{
    private StudentService $studentService;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', StudentType::class);
        $data = StudentType::with('school', 'createdBy', 'updatedBy')
            ->whereSchoolId(SchoolService::getSchoolId());
        if ($request->query('s')) {
            $data = $this->studentService->applyGlobalSearchForStudentType($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.students.types.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', StudentType::class);

        return view('dashboard.students.types.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(StudentTypeRequest $request)
    {
        $this->authorize('create', StudentType::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.student.types.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', StudentType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit($id): Response
    {
        $this->authorize('update', StudentType::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', StudentType::class);
    }


    public function destroy(int $id): Response
    {
        $this->authorize('delete', StudentType::class);
    }
}
