<?php

namespace App\Http\Controllers\Dashboard\Student;

use App\Http\Controllers\Controller;
use App\Http\Requests\StudentActivityRequest;
use App\Models\StudentActivity;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',StudentActivity::class);

        return view('dashboard.students.activities.index');
    }


    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',StudentActivity::class);

        return view('dashboard.students.activities.create');
    }


    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StudentActivityRequest $request)
    {
        $this->authorize('create',StudentActivity::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.student.activities.create')->with('success', __('school.record_created_successfully'));
        }

    }


    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',StudentActivity::class);

    }


    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',StudentActivity::class);
    }


    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',StudentActivity::class);

    }


    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',StudentActivity::class);
    }
}
