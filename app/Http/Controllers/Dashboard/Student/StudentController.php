<?php

namespace App\Http\Controllers\Dashboard\Student;

use  App\Http\Controllers\Controller;
use App\Http\Requests\StudentRequest;
use App\Models\Student;
use App\Services\SchoolService;
use App\Services\StudentService;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentController extends Controller
{
    use General;

    private StudentService $studentService;

    public function __construct(StudentService $studentService)
    {
        $this->studentService = $studentService;
        $this->middleware('auth');
        $this->makeDirectory('students');
        $this->makeDirectory('guardians');
        $this->makeMultipleDirectories('student', ['transfer_certificates', 'fathers', 'mothers']);
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {

        $this->authorize('view', Student::class);
        $data = Student::with('school', 'createdBy', 'updatedBy')
            ->whereSchoolId(SchoolService::getSchoolId());
        if ($request->query('s')) {
            $data = $this->studentService->applyGlobalSearchForStudent($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.students.index', compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', Student::class);
        if ($request->query('type') == 'bulk') {
            return view('dashboard.students.bulk.create');
        } else {
            return view('dashboard.students.create');
        }

    }


    /**
     * @throws AuthorizationException
     */
    public function store(StudentRequest $request)
    {
        $this->authorize('create', Student::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.students.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Student::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Student::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Student::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Student::class);
    }
}
