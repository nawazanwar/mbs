<?php

namespace App\Http\Controllers\Dashboard\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Requests\TeacherRequest;
use App\Models\Teacher;
use App\Services\SchoolService;
use App\Services\TeacherService;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TeacherController extends Controller
{
    use General;
    private TeacherService  $teacherService;

    public function __construct(TeacherService $teacherService)
    {

        $this->teacherService = $teacherService;
        $this->makeDirectory('teachers');
        $this->makeDirectory('teachers/resumes');
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Teacher::class);
        $data = Teacher::with('school', 'createdBy', 'updatedBy')
            ->whereSchoolId(SchoolService::getSchoolId());
        if ($request->query('s')) {
            $data = $this->teacherService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.teacher.index',compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Teacher::class);
        return view('dashboard.teacher.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(TeacherRequest $request)
    {
        $this->authorize('create',Teacher::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.teachers.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',Teacher::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',Teacher::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update',Teacher::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',Teacher::class);

    }
}
