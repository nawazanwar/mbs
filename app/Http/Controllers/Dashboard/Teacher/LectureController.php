<?php

namespace App\Http\Controllers\Dashboard\Teacher;

use App\Http\Controllers\Controller;
use App\Http\Requests\LectureRequest;
use App\Models\Lecture;
use App\Services\LectureService;
use App\Services\SchoolService;
use App\Services\TeacherService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LectureController extends Controller
{
    private LectureService $lectureService;

    public function __construct(LectureService $lectureService)
    {
        $this->lectureService = $lectureService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Lecture::class);
        $data = Lecture::with('school', 'createdBy', 'updatedBy')
            ->whereSchoolId(SchoolService::getSchoolId());
        if ($request->query('s')) {
            $data = $this->lectureService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.teacher.lectures.index',compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Lecture::class);
        return view('dashboard.teacher.lectures.create');

    }

    /**
     * @throws AuthorizationException
     */
    public function store(LectureRequest $request)
    {
        $this->authorize('create', Lecture::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.lectures.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Lecture::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Lecture::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Lecture::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Lecture::class);

    }
}
