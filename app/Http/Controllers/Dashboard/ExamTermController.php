<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamTermRequest;
use App\Models\ExamTerm;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExamTermController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', ExamTerm::class);
        return view('dashboard.exams.terms.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', ExamTerm::class);
        return view('dashboard.exams.terms.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(ExamTermRequest $request)
    {
        $this->authorize('create', ExamTerm::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.exam.terms.create')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', ExamTerm::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', ExamTerm::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', ExamTerm::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', ExamTerm::class);

    }
}
