<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;
use App\Models\LibraryEbook;
use App\Models\LibraryMember;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LibraryMemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
       $this->authorize('view', LibraryMember::class);
        return view('dashboard.operation.library.members.index');

    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', LibraryMember::class);
        return view('dashboard.operation.library.members.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', LibraryMember::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', LibraryMember::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', LibraryMember::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', LibraryMember::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', LibraryMember::class);
    }
}
