<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Operation\LibraryBookIssueRequest;
use App\Models\LibraryBook;
use App\Models\LibraryBookIssue;
use App\Services\LibraryService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LibraryBookIssueController extends Controller
{
    private LibraryService $libraryService;

    public function __construct(
        LibraryService $libraryService
    )
    {
        $this->libraryService=$libraryService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',LibraryBookIssue::class);

        $data = LibraryBookIssue::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->libraryService->applyGlobalSearchForBookIssue($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        $this->authorize('view', LibraryBookIssue::class);
        return view('dashboard.operation.library.book_issues.index',compact('data'));

    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', LibraryBookIssue::class);

        return view('dashboard.operation.library.book_issues.create');

    }


    /**
     * @throws AuthorizationException
     */
    public function store(LibraryBookIssueRequest $request)
    {
        $this->authorize('create', LibraryBookIssue::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.library.books.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', LibraryBookIssue::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', LibraryBookIssue::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', LibraryBookIssue::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', LibraryBookIssue::class);
    }
}
