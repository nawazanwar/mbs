<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;

use App\Http\Requests\Operation\VehicleRequest;
use App\Models\LibraryBookIssue;
use App\Models\TransportVehicle;
use App\Services\TransportService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TransportVehicleController extends Controller
{
    private TransportService $transportService;

    public function __construct(
        TransportService $transportService
    )
    {
        $this->transportService=$transportService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',TransportVehicle::class);
        $data = TransportVehicle::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->transportService->applyGlobalSearchForVehicles($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        $this->authorize('view', TransportVehicle::class);
        return view('dashboard.operation.transports.vehicles.index',compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', TransportVehicle::class);

        return view('dashboard.operation.transports.vehicles.create');

    }

    /**
     * @throws AuthorizationException
     */
    public function store(VehicleRequest $request)
    {
        $this->authorize('create', TransportVehicle::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.transport.vehicles.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', TransportVehicle::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', TransportVehicle::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', TransportVehicle::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', TransportVehicle::class);

    }
}
