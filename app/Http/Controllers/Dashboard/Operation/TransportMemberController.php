<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;
use App\Models\TransportMember;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TransportMemberController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',TransportMember::class);
        return view('dashboard.operation.transports.members.index');

    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',TransportMember::class);
        return view('dashboard.operation.transports.members.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create',TransportMember::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',TransportMember::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',TransportMember::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',TransportMember::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',TransportMember::class);

    }
}
