<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Operation\LibraryBookRequest;
use App\Models\HostelRoom;
use App\Models\LibraryBook;
use App\Models\LibraryEbook;
use App\Services\LibraryService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class LibraryBookController extends Controller
{
    private LibraryService $libraryService;

    public function __construct(
        LibraryService $libraryService
    )
    {
        $this->libraryService=$libraryService;
        $this->middleware('auth');
    }


    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',LibraryBook::class);

        $data = LibraryBook::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->libraryService->applyGlobalSearchForBook($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        $this->authorize('view', LibraryBook::class);
        return view('dashboard.operation.library.books.index',compact('data'));

    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', LibraryBook::class);
        return view('dashboard.operation.library.books.create');

    }


    /**
     * @throws AuthorizationException
     */
    public function store(LibraryBookRequest $request)
    {
        $this->authorize('create', LibraryBook::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.library.books.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', LibraryBook::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', LibraryBook::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', LibraryBook::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', LibraryBook::class);
    }
}
