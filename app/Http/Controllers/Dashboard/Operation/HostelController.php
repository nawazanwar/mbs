<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Operation\HostelRequest;
use App\Models\Hostel;
use App\Services\HostelService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HostelController extends Controller
{
    private HostelService $hostelService;

    public function __construct(
        HostelService $hostelService
    )
    {
        $this->hostelService=$hostelService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Hostel::class);
        $data = Hostel::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->hostelService->applyGlobalSearchForHostel($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.operation.hostels.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Hostel::class);
        return view('dashboard.operation.hostels.create');

    }


    /**
     * @throws AuthorizationException
     */
    public function store(HostelRequest $request)
    {
        $this->authorize('create', Hostel::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.hostels.index')->with('success', __('school.record_created_successfully'));
        }

    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Hostel::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Hostel::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Hostel::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Hostel::class);

    }
}
