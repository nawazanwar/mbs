<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Operation\TransportRouteRequest;
use App\Models\LibraryBookIssue;
use App\Models\TransportRoute;
use App\Services\TransportService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TransportRouteController extends Controller
{
    private TransportService $transportService;

    public function __construct(
        TransportService $transportService
    )
    {
        $this->transportService=$transportService;
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',TransportRoute::class);

        $data = TransportRoute::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->transportService->applyGlobalSearchForRoutes($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        $this->authorize('view', TransportRoute::class);
        return view('dashboard.operation.transports.routes.index',compact('data'));

    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',TransportRoute::class);

        return view('dashboard.operation.transports.routes.create');

    }

    /**
     * @throws AuthorizationException
     */
    public function store(TransportRouteRequest $request)
    {
        $this->authorize('create', TransportRoute::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.transport.routes.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',TransportRoute::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',TransportRoute::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',TransportRoute::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',TransportRoute::class);

    }
}
