<?php

namespace App\Http\Controllers\Dashboard\Operation;

use App\Http\Controllers\Controller;
use App\Http\Requests\Operation\HostelRoomRequest;
use App\Models\AbsentEmail;

use App\Models\Hostel;
use App\Models\HostelRoom;
use App\Services\HostelService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class HostelRoomController extends Controller
{
    private HostelService $hostelService;

    public function __construct(
        HostelService $hostelService
    )
    {
        $this->hostelService=$hostelService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',HostelRoom::class);

        $data = HostelRoom::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->hostelService->applyGlobalSearchForRoom($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        $this->authorize('view', HostelRoom::class);
        return view('dashboard.operation.hostels.rooms.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',HostelRoom::class);
        return view('dashboard.operation.hostels.rooms.create');

    }


    /**
     * @throws AuthorizationException
     */
    public function store(HostelRoomRequest $request)
    {
        $this->authorize('create', HostelRoom::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.hostel.rooms.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',HostelRoom::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',HostelRoom::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',HostelRoom::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',HostelRoom::class);
    }
}
