<?php

namespace App\Http\Controllers\Dashboard\Authorization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Configuration\PermissionRequest;
use App\Models\Permission;
use App\Services\PermissionService;
use App\Services\SchoolService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    private PermissionService $permissionService;

    public function __construct(
        PermissionService $permissionService
    )
    {
        $this->permissionService = $permissionService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Permission::class);

        $parent = Permission::getModels(app_path() . "/Models", '\App\Models\\');
        $models = array();
        foreach ($parent as $model) {
            if (method_exists($model, 'modulePermissions')) {
                array_push($models, $model);
            }
        }
        return view('dashboard.authorization.permissions.index',compact('models'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Permission::class);
        return view('dashboard.authorization.permissions.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(PermissionRequest $request)
    {
        $this->authorize('create', Permission::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.permissions.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view', Permission::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update', Permission::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update', Permission::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete', Permission::class);
    }
}
