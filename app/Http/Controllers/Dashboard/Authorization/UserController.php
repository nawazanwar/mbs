<?php

namespace App\Http\Controllers\Dashboard\Authorization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Configuration\UserRequest;
use App\Models\Discount;
use App\Models\User;
use App\Services\SchoolService;
use App\Services\UserService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->userService = $userService;
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', User::class);
        $data = User::with(['school', 'createdBy', 'updatedBy'])->whereSchoolId(SchoolService::getSchoolId());
        if ($request->query('s')) {
            $data = $this->userService->applyGlobalSearchForDiscount($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.authorization.users.index', compact('data'));
    }

}
