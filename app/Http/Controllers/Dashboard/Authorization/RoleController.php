<?php

namespace App\Http\Controllers\Dashboard\Authorization;

use App\Http\Controllers\Controller;
use App\Http\Requests\Configuration\RoleRequest;
use App\Models\Role;
use App\Services\RoleService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private RoleService $roleService;

    public function __construct(
        RoleService $roleService
    )
    {
        $this->roleService=$roleService;
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Role::class);
        $data = new Role();
        if ($request->query('s')) {
            $data = $this->roleService->applyGlobalSearch($request,$data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.authorization.roles.index',compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Role::class);
        return view('dashboard.authorization.roles.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(RoleRequest $request)
    {
        $this->authorize('create',Role::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.roles.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view',Role::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update',Role::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update',Role::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete',Role::class);
    }
}
