<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Gallery;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',    Gallery::class);

        return view('dashboard.gallery.index');
    }

    
    public function create()
    {
        $this->authorize('create',    Gallery::class);
        return view('dashboard.gallery.create');

    }

    
    public function store(Request $request): Response
    {
        $this->authorize('create',    Gallery::class);

    }

    
    public function show(int $id): Response
    {
        $this->authorize('view',    Gallery::class);

    }

    
    public function edit(int $id): Response
    {
        $this->authorize('update',    Gallery::class);

    }

    
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',    Gallery::class);

    }

    
    public function destroy(int $id): Response
    {
        $this->authorize('delete',    Gallery::class);

    }
}
