<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AcademicYear;
use App\Models\AdmitCardSetting;
use App\Models\EmployeeIdCard;
use App\Models\ExamAttendance;

use App\Models\IdCardSetting;
use App\Models\StudentAdmitCard;
use App\Models\StudentIdCard;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudentIdCardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', StudentIdCard::class);
        return view('dashboard.cards.student-id-cards.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', StudentIdCard::class);
        return view('dashboard.cards.student-id-cards.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', StudentIdCard::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', StudentIdCard::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', StudentIdCard::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', StudentIdCard::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', StudentIdCard::class);
    }
}
