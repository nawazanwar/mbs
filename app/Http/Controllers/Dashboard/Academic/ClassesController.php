<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Http\Requests\Academic\ClassesRequest;
use App\Models\ClassModel;
use App\Services\ClassService;
use App\Services\SchoolService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClassesController extends Controller
{


    private ClassService $classService;

    public function __construct(
        ClassService $classService
    )
    {
        $this->classService=$classService;
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $data = ClassModel::with('school', 'createdBy', 'updatedBy')
        ->whereSchoolId(SchoolService::getSchoolId());
        if ($request->query('s')) {
            $data = $this->classService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return  \view('dashboard.academic.classes.index',compact('data'));
    }
    public function create()
    {
        return  \view('dashboard.academic.classes.create');
    }
    public function store(ClassesRequest $request)
    {
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.classes.index')->with('success', __('school.record_created_successfully'));
        }
    }
    public function show(int $id): Response
    {
    }
    public function edit(int $id): Response
    {

    }
    public function update(Request $request, int $id): Response
    {

    }
    public function destroy(int $id): Response
    {
    }
}
