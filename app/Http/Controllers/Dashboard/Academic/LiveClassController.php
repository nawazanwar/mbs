<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Http\Requests\LiveClassRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\LiveClass;

class LiveClassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',LiveClass::class);

        return  \view('dashboard.academic.classes.live.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',LiveClass::class);
        return  \view('dashboard.academic.classes.live.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(LiveClassRequest $request)
    {
        $this->authorize('create',LiveClass::class);
        $model=$request->createData();
        if ($model) {
            return redirect()->route('dashboard.classes.live.create')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',LiveClass::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',LiveClass::class);

    }

    
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',LiveClass::class);
    }

    
    public function destroy(int $id): Response
    {
        $this->authorize('delete',LiveClass::class);

    }
}
