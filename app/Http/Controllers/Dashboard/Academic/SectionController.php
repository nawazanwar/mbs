<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Http\Requests\Academic\SectionRequest;
use App\Models\Section;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SectionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Section::class);
        $data = Section::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->sectionService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.academic.classes.sections.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Section::class);
        return \view('dashboard.academic.classes.sections.create');


    }


    /**
     * @throws AuthorizationException
     */
    public function store(SectionRequest $request)
    {
        $this->authorize('create', Section::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.sections.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Section::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Section::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Section::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Section::class);

    }
}
