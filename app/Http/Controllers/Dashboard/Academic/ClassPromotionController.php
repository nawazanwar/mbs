<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Models\ClassPromotion;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClassPromotionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', ClassPromotion::class);
        return \view('dashboard.academic.classes.promotions.index');

    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', ClassPromotion::class);
        return \view('dashboard.academic.classes.promotions.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', ClassPromotion::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', ClassPromotion::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', ClassPromotion::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update', ClassPromotion::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', ClassPromotion::class);
    }
}
