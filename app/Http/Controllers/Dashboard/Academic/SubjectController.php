<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Http\Requests\Academic\SubjectRequest;
use App\Models\Subject;
use App\Services\SubjectService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SubjectController extends Controller
{
    private SubjectService $subjectService;
    public function __construct(SubjectService $subjectService)
    {
        $this->middleware('auth');
        $this->subjectService = $subjectService;
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Subject::class);
        $data = Subject::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->subjectService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return \view('dashboard.academic.classes.subjects.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',Subject::class);
        return \view('dashboard.academic.classes.subjects.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(SubjectRequest $request)
    {
        $this->authorize('create',Subject::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.subjects.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view',Subject::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',Subject::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',Subject::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',Subject::class);

    }
}
