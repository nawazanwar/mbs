<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Http\Requests\Academic\StudyMaterialRequest;
use App\Models\StudyMaterial;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class StudyMaterialController extends Controller
{
    use General;
    public function __construct()
    {
        $this->makeDirectory('studymaterial');
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',StudyMaterial::class);
        return \view('dashboard.academic.classes.study-materials.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',StudyMaterial::class);
        return \view('dashboard.academic.classes.study-materials.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(StudyMaterialRequest $request)
    {
        $this->authorize('create',StudyMaterial::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.study.materials.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',StudyMaterial::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',StudyMaterial::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',StudyMaterial::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',StudyMaterial::class);
    }
}
