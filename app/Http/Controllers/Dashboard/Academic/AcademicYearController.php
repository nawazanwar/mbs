<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Http\Requests\Configuration\AcademicYearRequest;
use App\Models\AcademicYear;
use App\Services\AcademicYearService;
use App\Services\PermissionService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class AcademicYearController extends Controller
{
    private AcademicYearService $academicYearService;

    public function __construct(
        AcademicYearService $academicYearService
    )
    {
        $this->academicYearService=$academicYearService;
        $this->middleware('auth');
    }


    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',AcademicYear::class);

        $data = AcademicYear::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->academicYearService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.academic.academic-years.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',AcademicYear::class);
        return view('dashboard.academic.academic-years.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(AcademicYearRequest $request)
    {
        $this->authorize('create', AcademicYear::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.academic.years.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view',AcademicYear::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update',AcademicYear::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update',AcademicYear::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete',AcademicYear::class);
    }
}
