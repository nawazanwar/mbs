<?php

namespace App\Http\Controllers\Dashboard\Academic;

use App\Http\Controllers\Controller;
use App\Http\Requests\Academic\SyllabusRequest;
use App\Models\Syllabus;
use App\Services\SyllabusService;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SyllabusController extends Controller
{
    use General;
    private SyllabusService $syllabusService;

    public function __construct(SyllabusService $syllabusService)
    {
        $this->makeDirectory('syllabus');
        $this->middleware('auth');
        $this->syllabusService = $syllabusService;
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Syllabus::class);
        $data = Syllabus::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->syllabusService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return  \view('dashboard.academic.classes.syllabus.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('update',Syllabus::class);
        return  \view('dashboard.academic.classes.syllabus.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(SyllabusRequest $request)
    {
        $this->authorize('create',Syllabus::class);

        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.syllabus.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',Syllabus::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize('update',Syllabus::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',Syllabus::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',Syllabus::class);

    }
}
