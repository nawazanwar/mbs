<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\MessageEmail;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MessageEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', MessageEmail::class);
        return \view('dashboard.messages.email.index');
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', MessageEmail::class);
        return \view('dashboard.messages.email.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', MessageEmail::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', MessageEmail::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', MessageEmail::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', MessageEmail::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', MessageEmail::class);
    }
}
