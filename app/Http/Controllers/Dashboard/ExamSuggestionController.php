<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\ExamSuggestionRequest;
use App\Models\ExamSuggestion;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExamSuggestionController extends Controller
{
    use General;
    public function __construct()
    {
        $this->makeDirectory('exam_suggetions');
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', ExamSuggestion::class);
        return view('dashboard.exams.suggestions.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', ExamSuggestion::class);
        return view('dashboard.exams.suggestions.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(ExamSuggestionRequest $request)
    {
        $this->authorize('create', ExamSuggestion::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.exam.suggestions.create')->with('success', __('school.record_created_successfully'));
        }

    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', ExamSuggestion::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', ExamSuggestion::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update', ExamSuggestion::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', ExamSuggestion::class);
    }
}
