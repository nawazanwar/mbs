<?php

namespace App\Http\Controllers\Dashboard\FrontOffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostalDispatchRequest;
use App\Models\PostalDispatch;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostalDispatchController extends Controller
{
    use General;
    public function __construct()
    {
        $this->makeDirectory('postaldispatch');
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',PostalDispatch::class);

        return view('dashboard.front-office.postal.dispatches.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',PostalDispatch::class);

        return view('dashboard.front-office.postal.dispatches.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(PostalDispatchRequest $request)
    {
        $this->authorize('create',PostalDispatch::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.postal.dispatches.create')->with('success', __('school.record_created_successfully'));
        }

    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',PostalDispatch::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',PostalDispatch::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',PostalDispatch::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',PostalDispatch::class);

    }
}
