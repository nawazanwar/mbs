<?php

namespace App\Http\Controllers\Dashboard\FrontOffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\CallLogRequest;
use App\Models\CallLog;
use App\Services\CallLogService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;


class CallLogController extends Controller
{
    private CallLogService $callLogService;

    public function __construct(CallLogService $callLogService)
    {
        $this->callLogService = $callLogService;
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', CallLog::class);
        $data = CallLog::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->callLogService->applyGlobalSearchForCallLog($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.front-office.call-logs.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create(Request $request)
    {
        $this->authorize('create', CallLog::class);
        return view('dashboard.front-office.call-logs.create');

    }


    /**
     * @throws AuthorizationException
     */
    public function store(CallLogRequest $request)
    {
        $this->authorize('create', CallLog::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.call.logs.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id)
    {
        $this->authorize('view', CallLog::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id)
    {
        $this->authorize('update', CallLog::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id)
    {
        $this->authorize('update', CallLog::class);
    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id)
    {
        $this->authorize('delete', CallLog::class);
    }
}
