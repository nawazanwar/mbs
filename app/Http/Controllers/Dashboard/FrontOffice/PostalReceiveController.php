<?php

namespace App\Http\Controllers\Dashboard\FrontOffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostalRecieveRequest;
use App\Models\PostalReceive;
use App\Traits\General;
use Facade\Ignition\Solutions\MakeViewVariableOptionalSolution;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PostalReceiveController extends Controller
{
    use General;
    public function __construct()
    {
        $this->makeDirectory('postalrecieve');
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view',PostalReceive::class);

        return view('dashboard.front-office.postal.receives.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',PostalReceive::class);

        return view('dashboard.front-office.postal.receives.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(PostalRecieveRequest $request)
    {
        $this->authorize('create',PostalReceive::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.postal.receives.create')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',PostalReceive::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',PostalReceive::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',PostalReceive::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',PostalReceive::class);

    }
}
