<?php

namespace App\Http\Controllers\Dashboard\FrontOffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\VisitorPurposeRequest;
use App\Models\Visitor;
use App\Models\VisitorPurpose;
use App\Services\VisitorService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VisitorPurposeController extends Controller
{
    private VisitorService $visitorService;

    public function __construct(
        VisitorService $visitorService
    )
    {
        $this->visitorService=$visitorService;
        $this->middleware('auth');
    }
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view',Visitor::class);
        $data = VisitorPurpose::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->visitorService->applyGlobalSearchForVisitorPurpose($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);

        return view('dashboard.front-office.visitors.purposes.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create',VisitorPurpose::class);

        return view('dashboard.front-office.visitors.purposes.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(VisitorPurposeRequest $request)
    {
        $this->authorize('create',VisitorPurpose::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.visitor.purposes.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view',VisitorPurpose::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update',VisitorPurpose::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update',VisitorPurpose::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete',VisitorPurpose::class);

    }
}
