<?php

namespace App\Http\Controllers\Dashboard\FrontOffice;

use App\Http\Controllers\Controller;
use App\Http\Requests\VisitorRequest;
use App\Models\Visitor;
use App\Services\VisitorService;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VisitorController extends Controller
{
    private VisitorService $visitorService;

    public function __construct(VisitorService $visitorService)
    {
        $this->visitorService = $visitorService;
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Visitor::class);
        $data = Visitor::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->visitorService->applyGlobalSearchForVisitor($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.front-office.visitors.index',compact('data'));
    }


    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Visitor::class);
        return view('dashboard.front-office.visitors.create');
    }


    /**
     * @throws AuthorizationException
     */
    public function store(VisitorRequest $request)
    {
        $this->authorize('create', Visitor::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.visitors.index')->with('success', __('school.record_created_successfully'));
        }
    }


    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Visitor::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Visitor::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', Visitor::class);

    }


    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Visitor::class);

    }
}
