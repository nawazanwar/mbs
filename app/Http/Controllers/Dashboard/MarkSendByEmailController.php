<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\MarkSendByEmailRequest;
use App\Models\MarkSendByEmail;
use App\Models\Message;
use App\Models\MessageEmail;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MarkSendByEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        return \view('dashboard.exams.send-mark.by-email.index');
    }
    public function create()
    {
        return \view('dashboard.exams.send-mark.by-email.create');
    }
    public function store(MarkSendByEmailRequest $request)
    {
        $this->authorize('create', MarkSendByEmail::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.mark.send.by.email.create')->with('success', __('school.record_created_successfully'));
        }
    }
}
