<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\GuardianRequest;
use App\Models\Guardian;
use App\Services\GuardianService;
use App\Services\ThemeService;
use App\Traits\General;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GuardianController extends Controller
{

    use General;

    private GuardianService $guardianService;


    public function __construct(GuardianService  $guardianService)
    {
        $this->middleware('auth');
        $this->guardianService = $guardianService;
        $this->makeDirectory('guardians');
    }

    /**
     * @throws AuthorizationException
     */
    public function index(Request $request)
    {
        $this->authorize('view', Guardian::class);
        $data = Guardian::with('school', 'createdBy', 'updatedBy');
        if ($request->query('s')) {
            $data = $this->guardianService->applyGlobalSearch($request, $data);
        }
        $data = $data->orderBy('id', 'desc')->paginate(20);
        return view('dashboard.guardians.index', compact('data'));
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', Guardian::class);
        return view('dashboard.guardians.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(GuardianRequest $request)
    {
        $this->authorize('create', Guardian::class);
        $model = $request->createData();
        if ($model) {
            return redirect()->route('dashboard.guardians.index')->with('success', __('school.record_created_successfully'));
        }
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', Guardian::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', Guardian::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(GuardianRequest $request, int $id)
    {
        $this->authorize('update', Guardian::class);
        $request->updateData();
    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', Guardian::class);

    }
}
