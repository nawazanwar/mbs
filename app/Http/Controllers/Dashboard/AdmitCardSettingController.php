<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\AdmitCardSetting;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AdmitCardSettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('view', AdmitCardSetting::class);
        return view('dashboard.cards.admit-card-settings.index');
    }

    /**
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', AdmitCardSetting::class);
        return view('dashboard.cards.admit-card-settings.create');
    }

    /**
     * @throws AuthorizationException
     */
    public function store(Request $request): Response
    {
        $this->authorize('create', AdmitCardSetting::class);
    }

    /**
     * @throws AuthorizationException
     */
    public function show(int $id): Response
    {
        $this->authorize('view', AdmitCardSetting::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function edit(int $id): Response
    {
        $this->authorize('update', AdmitCardSetting::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function update(Request $request, int $id): Response
    {
        $this->authorize('update', AdmitCardSetting::class);

    }

    /**
     * @throws AuthorizationException
     */
    public function destroy(int $id): Response
    {
        $this->authorize('delete', AdmitCardSetting::class);
    }
}
