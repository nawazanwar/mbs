<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($sId = null, Request $request)
    {
        return view('website.index');
    }

    public function register_school()
    {
        return view('registration.index');
    }
}
