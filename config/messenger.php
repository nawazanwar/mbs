<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Models
    |--------------------------------------------------------------------------
    */

    'models' => [
        'text' => Gerardojbaez\Messenger\Models\Message::class,
        'thread' => Gerardojbaez\Messenger\Models\MessageThread::class,
        'participant' => Gerardojbaez\Messenger\Models\MessageThreadParticipant::class,
    ],

];
