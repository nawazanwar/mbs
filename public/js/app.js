$(function () {
    "use strict";
    $(".box-color").on('click', function () {
        let url = $(this).find('a').attr('href');
        if (url !== undefined) {
            location.assign(url);
        } else {
            window.history.back();
        }
    });
});

function getDate() {
    const currentDate = new Date();
    const day = currentDate.getDate();
    const month = currentDate.getMonth() + 1;
    const year = currentDate.getFullYear();
    return year + "-" + month + "-" + day;
}

document.addEventListener('DOMContentLoaded', function () {
    const calendarEl = document.getElementById('calendar');
    const calendar = new FullCalendar.Calendar(calendarEl, {
        headerToolbar: {
            left: '',
            center: 'title',
            right: ''
        },
        initialView: 'dayGridMonth',
        initialDate: getDate(),
        events: []
    });
    calendar.render();
});